<%@include file="../common/common.jsp"%>
<html>
<head>
    <title>
        <spring:message code="organ.info.tip"></spring:message>
    </title>
    <style type="text/css">
        .form-tow-column{
            display:grid;
            grid-template-columns:auto auto;

        }
        .form-group{
            margin: 0 10px;
        }
        fieldset>legend{
            font-size: 10px;
            border-bottom:none;
            margin-bottom: 0;
        }
        fieldset {
            display: block;
            margin-inline-start: 2px;
            margin-inline-end: 2px;
            padding-block-start: 0.35em;
            padding-inline-start: 0.75em;
            padding-inline-end: 0.75em;
            padding-block-end: 0.625em;
            min-inline-size: min-content;
            border: 1px solid #e5e5e5;;
            border-image: initial;
        }

        .gdmt-tb-th{
            height: 32px;
            background-color: #b5b5b5;
            background: #f5f5f5;
            border-top: #d4d4d4 1px solid;
            background-color: #fff;
        }
        .gdmt-tb-th > th{
            font-weight: normal;
            cursor: default;
            white-space: nowrap;
            overflow: hidden;
            text-align: center;
            vertical-align: middle !important;
            padding: 0px;
            height: 32px;
            background-color: #b5b5b5;
            box-sizing: border-box;
            color: #FFF;
            font-size: 12px;
        }
        .table-edit-data td{
            text-align: center;
            vertical-align: middle !important;
            padding: 0px;
            height: 32px;
            font-size: 12px;
            background-color: #f5f5f5;
        }
        .table-erow td{
            background-color: #ededed;
        }
        .table-edit-data .read{
            display: inline-block;
            margin-right: 10px;
        }
        .table-edit-data .edit{
            display: none;
        }
        .table-edit-tr .edit{
            display: inline-block;
        }
        .table-edit-tr .read{
            display: none;
        }
    </style>

    <script type="text/javascript">
        //页面自适应
        function resizePageSize(){
            _gridWidth = $(document).width()-215;/*  -189 是去掉左侧 菜单的宽度，   -12 是防止浏览器缩小页面 出现滚动条 恢复页面时  折行的问题 */
            _gridHeight = $(document).height()-98; /* -32 顶部主菜单高度，   -90 查询条件高度*/
        }

        $(function() {//
            resizePageSize();
            leftOrganLoad();
            $("#modify-organ-info-form-id").ajaxForm({
                dataType: "json",
                success : function(data) {
                    message(data.msg);
                    leftOrganLoad();
                },
                error : function() {
                    message(' <spring:message code="common.error"></spring:message>');
                },
                complete : function(response, status) {

                }
            });//加载

             //zengjiaorgan
            $("#add-organ-info-dialog-id").dialog({
                autoOpen : false,
                width : 400,
                modal : true,
                resizable : false,
                title: '<spring:message code='organ.add'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function() {
                            var flag = true;
                            var dom = $("#add-organ-info-form-id > div > .form-control");
                            dom.each(function(index,item) {
                                flag &= $(item).verification();
                                console.info(flag);
                            });
                            if(!flag){
                                return;
                            }
                            $("#add-organ-info-form-id").submit();
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });


            $("#add-organ-info-form-id").ajaxForm({
                dataType: "json",
                success : function(data) {
                    message(data.msg);
                    leftOrganLoad();
                    $("#add-organ-info-dialog-id").dialog('close');
                },
                error : function() {
                    message(' <spring:message code="common.error"></spring:message>');
                },
                complete : function(response, status) {

                }

            });

                      //bangding
            $("#bound-organ-dialog-id").dialog({
                autoOpen : false,
                width : 400,
                modal : true,
                resizable : false,
                title: '<spring:message code='common.bound'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function() {
                            var parentId = "";
                            var ztreeObj = $.fn.zTree.getZTreeObj("bound-organ-ztree-id");
                            var nodes = ztreeObj.getCheckedNodes(true);
                            $(nodes).each(function (index,item) {
                                parentId =item.id;
                            });
                            var organId = $("#mod-organ-id").val();
                            if(parentId === organId){
                                message('<spring:message code="organ.confirm"></spring:message>');
                                return;
                            }
                            $.ajax({
                                type: 'post',
                                async: false,
                                dataType : 'json',
                                url: '${pageContext.request.contextPath}/organ/boundOrgan',
                                data: [
                                    {name:"parentId",value:parentId},
                                    {name:"organId",value:organId}
                                ],
                                success: function (data) {
                                    message(data.msg);
                                    $("#bound-organ-dialog-id").dialog('close');
                                    leftOrganLoad();
                                },
                                error: function(msg){
                                    message(' <spring:message code="common.error"></spring:message>');
                                }
                            });
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });


            $("ul#tab-ul-organ-id > li").click(function(event) {
                $("ul#tab-ul-organ-id .active").removeClass("active");
                $(this).addClass("active");

                $("section.tab-content .in").removeClass("in");
                $("section.tab-content .active").removeClass("active");
                var tab = $(this).attr("tab");
                $("#" + tab).addClass("in");
                $("#" + tab).addClass("active")
            });

            //表头
            $("ul#tab-ul-organ-id > li:first").click();
            <%--var _columnWidth = _gridWidth/4;--%>
            <%--$("#flexigrid-id").flexigrid({--%>
            <%--    width : _gridWidth,--%>
            <%--    height : _gridHeight,--%>
            <%--    url : "${pageContext.request.contextPath}/organ/getDepartByOrganId",--%>
            <%--    dataType : 'json',--%>
            <%--    colModel : [--%>
            <%--        {display : 'depId',name : 'depId',width : 150,sortable : false,align : 'center',hide : 'true'},--%>
            <%--        {display : "<spring:message code='organ.id'/>",name : 'organId',width : _columnWidth, sortable : true,align : 'center'},--%>
            <%--        {display : "<spring:message code='branch.name'/>",name : 'branchNname',width : _columnWidth, sortable : true,align : 'center'},--%>
            <%--        {display : "<spring:message code='belong.center'/>",name : 'belongCenter ',width : _columnWidth, sortable : true,align : 'center'},--%>
            <%--    ],--%>
            <%--    resizable : false, //resizable table是否可伸缩--%>
            <%--    useRp : true,--%>
            <%--    usepager : true, //是否分页--%>
            <%--    autoload : false, //自动加载，即第一次发起ajax请求--%>
            <%--    hideOnSubmit : true, //是否在回调时显示遮盖--%>
            <%--    showcheckbox : true, //是否显示多选框--%>
            <%--    //rowhandler : rowDbclick, //是否启用行的扩展事情功能,在生成行时绑定事件，如双击，右键等--%>
            <%--    rowbinddata : true--%>
            <%--});--%>
            <%--$("#flexigrid-id").flexReload();--%>

            var _columnWidth = _gridWidth/3-19;
            $("#dep-flexigrid-id").flexigrid({
                width : _gridWidth,
                height : _gridHeight,
                url : "${pageContext.request.contextPath}/organ/queryDepListPage",
                dataType : 'json',
                colModel : [
                    {display : 'depId',name : 'depUuId',width : 150,sortable : false,align : 'center',hide : 'true'},
                    {display : "<spring:message code='organ.id'/>",name : 'organUuId',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='branch.name'/>",name : 'branchName',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='belong.center'/>",name : 'belongCenter',width : _columnWidth, sortable : true,align : 'center'},
                ],
                resizable : false, //resizable table是否可伸缩
                useRp : true,
                usepager : true, //是否分页
                autoload : false, //自动加载，即第一次发起ajax请求
                hideOnSubmit : true, //是否在回调时显示遮盖
                showcheckbox : true, //是否显示多选框
                //rowhandler : rowDbclick, //是否启用行的扩展事情功能,在生成行时绑定事件，如双击，右键等
                rowbinddata : true
            });
            $("#dep-flexigrid-id").flexReload();


        });//ready


        var leftOrganSetting = {
            check: {
                enable: false
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback : {
                onClick : onClickNode
            }
        };

        //左侧ztree加载
        function leftOrganLoad() {
            $.ajax({
                    type: 'post',
                    async: false,
                    dataType : 'json',
                    url: '${pageContext.request.contextPath}/organ/getOrganTree',
                    data: [],
                    success: function (data) {
                        $.fn.zTree.init($("#organ-left-tree-ul-id"), leftOrganSetting, data);
                    },
                    error: function(msg){
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );
        }

        //ztree回调事件 触发器
        function onClickNode(event, treeId, treeNode){
            //console.info(treeNode.id);
            $.ajax({
                    type: 'post',
                    async: false,
                    dataType : 'json',
                    url: '${pageContext.request.contextPath}/organ/getOrganInfoById',
                    data: [{name:"organId",value:treeNode.id}],
                    success: function (data) {
                        var dom = $("#modify-organ-info-form-id .form-control");
                        dom.each(function(index,item) {
                            var nameAttr = $(item).attr("name");
                            // console.info(data[nameAttr]);
                            $(item).val(data[nameAttr]);
                        });

                    },
                    error: function(msg){
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );

            $("#flexigrid-id").flexigrid({
                extParam:[
                    {name:"organId",value:treeNode.id}
                ]
            }).flexigrid();

        }

         //修改organfunction
        function modifyOrganInfo() {
            var flag = true;
            var dom = $("#modify-organ-info-form-id > div > .form-control");
            dom.each(function(index,item) {
                flag &= $(item).verification();
                console.info(flag);
            });
            if(!flag){
                return;
            }
            $("#modify-organ-info-form-id").submit();
        }

         //添加function
        function addOrganInfo() {
            $("#add-organ-info-dialog-id").dialog('open');
            $("#add-organ-info-form-id").resetForm();
        }

        // 删除function
        function deleteOrganInfo() {
            var modOrganId = $("#mod-organ-id").val();
            if(modOrganId == null || "" === modOrganId){
                return;
            }
            $.ajax({
                type: 'post',
                async: false,
                dataType : 'json',
                url: '${pageContext.request.contextPath}/organ/deleteOrganInfoById',
                data: [{name:"organId",value:modOrganId}],
                success: function (data) {
                    message(data.msg);
                    $("#modify-organ-info-form-id").resetForm();
                    leftOrganLoad();
                },
                error: function(msg){
                    message(' <spring:message code="common.error"></spring:message>');
                }
            });

        }

        var BoundOrganSetting = {
            check: {
                enable: true,
                chkStyle: "radio",
                radioType: "all"
            },
            data: {
                simpleData: {
                    enable: true
                }
            }
        };

        //绑定函数
        function boundOrganInfo () {
            var modOrganId = $("#mod-organ-id").val();
            if(modOrganId == null || '' === modOrganId){
                return;
            }
            $.ajax({
                    type: 'post',
                    async: false,
                    dataType : 'json',
                    url: '${pageContext.request.contextPath}/organ/getOrganTreeByOrganId',
                    data: [{name:"organId",value:modOrganId}],
                    success: function (data) {
                        $.fn.zTree.init($("#bound-organ-ztree-id"), BoundOrganSetting, data);
                        var ztreeObj = $.fn.zTree.getZTreeObj("bound-organ-ztree-id");
                        var nodes = ztreeObj.getNodesByParam("id",modOrganId,null);
                        ztreeObj.removeNode(nodes[0]);
                        $("#bound-organ-dialog-id").dialog('open');


                    },
                    error: function(msg){
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );
        }
        //解绑函数
        function unboundOrganInfo() {
            var modOrganId = $("#mod-organ-id").val();
            if(modOrganId == null || '' === modOrganId){
                return;
            }
            $.ajax({
                type: 'post',
                async: false,
                dataType : 'json',
                url: '${pageContext.request.contextPath}/organ/unBoundByOrganId',
                data: [{name:"organId",value:modOrganId}],
                success: function (data) {
                    leftOrganLoad();
                    message(data.msg);
                },
                error: function(msg){
                    message(' <spring:message code="common.error"></spring:message>');
                }
            });
        }

    </script>
</head>





<body style="display: flex;flex-direction: row">

<%--//显示ztree--%>
<nav>
    <spring:message code="organ.info.tip"></spring:message>
    <ul id="organ-left-tree-ul-id" class="ztree" ></ul>
</nav>

<article style="flex-grow: 1">

    <%-- 按钮   --%>
    <nav>
        <a href="#" class="btn" onclick="deleteOrganInfo();">
            <i class="fa fa-trash-o"></i>
            <span> <spring:message code="common.delete"></spring:message> </span>
        </a>
        <a href="#" class="btn" onclick="modifyOrganInfo();">
            <i class="glyphicon glyphicon-modal-window"></i>
            <span> <spring:message code="common.modify"></spring:message> </span>
        </a>
        <a href="#" class="btn" onclick="addOrganInfo();">
            <i class="fa fa-plus"></i>
            <span> <spring:message code="common.add"></spring:message> </span>
        </a>
        <a href="#" class="btn" onclick="boundOrganInfo();">
            <i class="fa fa-chain-broken"></i>
            <span> <spring:message code="common.bound"></spring:message> </span>
        </a>
        <a href="#" class="btn" onclick="unboundOrganInfo();">
            <i class="fa fa-unlock"></i>
            <span> <spring:message code="common.unbound"></spring:message> </span>
        </a>
    </nav>


     <%-- 表单   --%>
    <form id="modify-organ-info-form-id" action="${pageContext.request.contextPath}/organ/modifyOrganInfoById"   method="post" class="form-tow-column">
        <fieldset >
            <legend>
                <span><spring:message code="common.base" /></span>
            </legend>
            <div class="form-tow-column">
          <%--  organId  不能修改      --%>
        <div class="form-group">
            <span for="mod-organ-id"><spring:message code="organ.id"></spring:message> </span>
            <input name="organId" id="mod-organ-id" class="form-control" readonly>
        </div>
            <%--  organ名字       --%>
        <div class="form-group">
            <span for="mod-organ-name-id"><spring:message code="organ.name"></spring:message> </span>
            <input name="organName" id="mod-organ-name-id" class="form-control"  regex="^.{1,50}$" tip="input 1 to 50 char">
        </div>
            <%--     organ类型   --%>
        <div class="form-group">
            <span for="mod-parent-id"><spring:message code="parent.id"></spring:message> </span>
            <input name="parentId" id="mod-parent-id" class="form-control" regex="^\d$" tip="please choose">
        </div>
            <%--    描述            --%>
        <div class="form-group">
            <span for="mod-organ-description-id"><spring:message code="organ.description"></spring:message> </span>
            <input name="description" id="mod-organ-description-id" class="form-control">
        </div>
            </div>
        </fieldset>
    </form>


        <%--  deplist--%>
        <ul id="tab-ul-organ-id" class="nav nav-tabs">
            <li tab="tab-dep-id">
                <a href="#">
                    <i class="fa fa-university"></i>
                    <spring:message code="dep.list"></spring:message>
                </a>
            </li>
        </ul>

<%--        部门列表--%>
        <section class="tab-content">
            <article id="tab-dep-id" class="tab-pane fade in active">
                <table id="dep-flexigrid-id"></table>
            </article>
        </section>

</article>


<%--点击添加organ模块·--%>
<div id="add-organ-info-dialog-id" style="display: none">
    <form id="add-organ-info-form-id" action="${pageContext.request.contextPath}/organ/addOrganInfoById"
          method="post">



        <div class="form-group">
            <span for="add-organ-name"><spring:message code="organ.name"></spring:message> </span>
            <input name="organName" class="form-control" id="add-organ-name" >
        </div>



        <div class="form-group">
            <span for="add-organ-description-id"><spring:message code="organ.description"></spring:message> </span>
            <input name="description" class="form-control" id="add-organ-description-id" >
        </div>



        <div id="bound-organ-dialog-id" style="display: none">
            <ul id="bound-organ-ztree-id" class="ztree"></ul>
        </div>
    </form>
</div>



</body>
</html>