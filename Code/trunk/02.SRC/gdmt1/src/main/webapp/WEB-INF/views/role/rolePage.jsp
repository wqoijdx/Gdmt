<%@include file="../common/common.jsp"%>
<html>
<head>
    <title>
        <spring:message code="role.title.tip"></spring:message>
    </title>
    <script type="text/javascript">
        var _gridWidth;
        var _gridHeight;
        //页面自适应
        function resizePageSize(){
            _gridWidth = $(document).width()-2;/*  -189 是去掉左侧 菜单的宽度，   -12 是防止浏览器缩小页面 出现滚动条 恢复页面时  折行的问题 */
            _gridHeight = $(document).height()-32-85; /* -32 顶部主菜单高度，   -90 查询条件高度*/
        }

        $(function () {
            resizePageSize();
            var _columnWidth = _gridWidth/2-30;
            $("#flexigrid-id").flexigrid({
                width : _gridWidth,
                height : _gridHeight,
                url : "${pageContext.request.contextPath}/role/queryRoleListPage",
                dataType : 'json',
                colModel : [
                    {display : 'roleUuid',name : 'roleUuid',width : 150,sortable : false,align : 'center',hide : 'true'},
                    {display : "<spring:message code='role.name'/>",name : 'roleName',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='role.remarks'/>",name : 'remarks',width : _columnWidth, sortable : true,align : 'center'},
                ],
                resizable : false, //resizable table是否可伸缩
                useRp : true,
                usepager : true, //是否分页
                autoload : false, //自动加载，即第一次发起ajax请求
                hideOnSubmit : true, //是否在回调时显示遮盖
                showcheckbox : true, //是否显示多选框
                //rowhandler : rowDbclick, //是否启用行的扩展事情功能,在生成行时绑定事件，如双击，右键等
                rowbinddata : true
            });

            $("#flexigrid-id").flexReload();
            $("#role-menu-allocate-dialog-id").dialog({
                autoOpen : false,
                width : 400,
                modal : true,
                resizable : false,
                title: '<spring:message code='role.menu.allocate'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function() {
                            var roleId = $("#role-menu-allocate-ztree-id").attr("roleId");
                            var ztreeObj = $.fn.zTree.getZTreeObj("role-menu-allocate-ztree-id");
                            var checkNodes = ztreeObj.getCheckedNodes(true);
                            var menuArray = [];
                            $(checkNodes).each(function (index,item) {
                                menuArray.push(item.id);
                            });
                            $.ajax({
                                    type: 'post',
                                    async: false,
                                    dataType : 'json',
                                    url: '${pageContext.request.contextPath}/role/saveMenuIdAndRoleId',
                                    data: [{name:"roleUuid",value:roleId},
                                        {name:"menuArray",value:menuArray}],
                                    success: function (data) {
                                        $("#role-menu-allocate-dialog-id").dialog('close');
                                        message(data.msg);
                                    },
                                    error: function(msg){
                                        message(' <spring:message code="common.error"></spring:message>');
                                    }
                                }
                            );
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });

            $("#role-menu-add-dialog-id").dialog({
                autoOpen : false,
                width : 400,
                modal : true,
                resizable : false,
                title: '<spring:message code='role.menu.add'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function() {
                            var flag = true;
                            var dom = $("#add-role-info-form-id > div > .form-control");
                            dom.each(function(index,item) {
                                flag &= $(item).verification();
                                console.info(flag);
                            });
                            if(!flag){
                                return;
                            }
                            $("#add-role-info-form-id").submit();
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });
            /**
             * 覆盖form表单提交的逻辑
             */
            $("#add-role-info-form-id").ajaxForm({
                dataType: "json",
                success : function(data) {
                    console.log(data)
                    message(data.msg);
                    roleLoad();
                    $("#role-menu-add-dialog-id").dialog('close');
                },
                error : function() {
                    message(' <spring:message code="common.error"></spring:message>');
                },
                complete : function(response, status) {

                }
            });

            $("#role-menu-mod-dialog-id").dialog({
                autoOpen : false,
                width : 400,
                modal : true,
                resizable : false,
                title: '<spring:message code='role.menu.mod'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function() {
                            console.log('111111111111')
                            $("#mod-role-info-form-id").submit();
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });
            /**
             * 覆盖form表单提交的逻辑
             */
            $("#mod-role-info-form-id").ajaxForm({
                dataType: "json",
                success : function(data) {
                    console.log(data)
                    message(data.msg);
                    roleLoad();
                    $("#role-menu-mod-dialog-id").dialog('close');
                },
                error : function() {
                    message(' <spring:message code="common.error"></spring:message>');
                },
                complete : function(response, status) {

                }
            });

        });//ready

        /**
         * 刷新角色列表
         */
        function roleLoad() {
            resizePageSize();
            var _columnWidth = _gridWidth/2-30;
            $("#flexigrid-id").flexigrid({
                width : _gridWidth,
                height : _gridHeight,
                url : "${pageContext.request.contextPath}/role/queryRoleListPage",
                dataType : 'json',
                colModel : [
                    {display : 'roleUuid',name : 'roleUuid',width : 150,sortable : false,align : 'center',hide : 'true'},
                    {display : "<spring:message code='role.name'/>",name : 'roleName',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='role.remarks'/>",name : 'remarks',width : _columnWidth, sortable : true,align : 'center'},
                ],
                resizable : false, //resizable table是否可伸缩
                useRp : true,
                usepager : true, //是否分页
                autoload : false, //自动加载，即第一次发起ajax请求
                hideOnSubmit : true, //是否在回调时显示遮盖
                showcheckbox : true, //是否显示多选框
                //rowhandler : rowDbclick, //是否启用行的扩展事情功能,在生成行时绑定事件，如双击，右键等
                rowbinddata : true
            });

            $("#flexigrid-id").flexReload();
        }

        function queryBtn() {
            var searchRoleName = $("#searchRoleName").val();
            $("#flexigrid-id").flexOptions({
                extParam:[
                    {name:"searchRoleName",value:searchRoleName}
                ]
            }).flexReload();
            
        }
        var allocateSetting = {
            check: {
                enable: true
            },
            data: {
                simpleData: {
                    enable: true
                }
            }
        };
        function allocateMenuBtn() {
            var ids = $("#flexigrid-id").flexiData();
            if(ids.length !== 1){
                message('<spring:message code="common.select.one"></spring:message>');
                return;
            }
            $("#role-menu-allocate-ztree-id").attr("roleId",ids);

            $.ajax({
                    type: 'post',
                    async: false,
                    dataType : 'json',
                    url: '${pageContext.request.contextPath}/role/getMenuByRoleId',
                    data: [{name:"roleUuid",value:ids}],
                    success: function (data) {
                        $.fn.zTree.init($("#role-menu-allocate-ztree-id"), allocateSetting, data);
                        $("#role-menu-allocate-dialog-id").dialog('open');
                    },
                    error: function(msg){
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );

        }

        /**
         * 添加角色
         */
        function addRoleBtn() {
            $("#add-role-info-form-id").resetForm();
            $("#role-menu-add-dialog-id").dialog('open');
        }

        /**
         * 修改角色
         */
        function modRoleBtn() {
            $("#add-role-info-form-id").resetForm();
            var ids = $("#flexigrid-id").flexiData();
            if(ids.length !== 1){
                message('<spring:message code="common.select.one"></spring:message>');
                return;
            }
            $("#roleUuid").val(ids);
            $.ajax({
                url: '${pageContext.request.contextPath}/role?id=' + ids,
                dataType: 'json',
                async: false,
                success: function (resp) {
                    $('#roleName').val(resp.roleName)
                    $('#roleRemark').val(resp.remarks)
                }
            })

            $("#role-menu-mod-dialog-id").dialog('open');
        }

        /**
         * 删除角色
         */
        function delRoleBtn() {
            var ids = $("#flexigrid-id").flexiData();
            if(ids.length !== 1){
                message('<spring:message code="common.select.one"></spring:message>');
                return;
            }
            $.ajax({
                type: 'delete',
                dataType : 'json',
                url: '${pageContext.request.contextPath}/role/delete/' + ids,
                success: function (data) {
                    message(data.msg);
                    roleLoad();
                },
                error: function(msg){
                    message(' <spring:message code="common.error"></spring:message>');
                }
            });
        }
    </script>
</head>
<body>

    <spring:message code="role.title.tip"></spring:message>

    <div>
        <security:authorize access="hasRole('ROLE_cldk_data_role_query')">
            <label> <spring:message code='role.name'/>:</label> <input type="text" id="searchRoleName"/>
            <a class="btn" href="#" onclick="queryBtn();">
                <i class="glyphicon glyphicon-zoom-in"></i>
                <span><spring:message code='common.query'/></span>
            </a>
        </security:authorize>
        <security:authorize access="hasRole('ROLE_cldk_data_role_menu')">
            <a class="btn" href="#" onclick="allocateMenuBtn();">
                <i class="glyphicon glyphicon-wrench"></i>
                <span><spring:message code='role.menu.allocate'/></span>
            </a>
        </security:authorize>
        <security:authorize access="hasRole('ROLE_cldk_data_role_add')">
            <a class="btn" href="#" onclick="addRoleBtn();">
                <i class="fa fa-plus"></i>
                <span><spring:message code='role.menu.add'/></span>
            </a>
        </security:authorize>
        <security:authorize access="hasRole('ROLE_cldk_data_role_mod')">
            <a class="btn" href="#" onclick="modRoleBtn();">
                <i class="glyphicon glyphicon-modal-window"></i>
                <span><spring:message code='role.menu.mod'/></span>
            </a>
        </security:authorize>
        <security:authorize access="hasRole('ROLE_cldk_data_role_del')">
            <a class="btn" href="#" onclick="delRoleBtn();">
                <i class="fa fa-trash-o"></i>
                <span><spring:message code='role.menu.del'/></span>
            </a>
        </security:authorize>
    </div>

    <table id="flexigrid-id"></table>

    <!--分配权限弹窗 -->
    <div id="role-menu-allocate-dialog-id" style="display: none">
        <ul id="role-menu-allocate-ztree-id" class="ztree"></ul>
    </div>
    <!-- 添加角色弹窗 -->
    <div id="role-menu-add-dialog-id" style="display: none">
        <form id="add-role-info-form-id" action="${pageContext.request.contextPath}/role/create"
              method="post">
            <div class="form-group">
                <span for="add-menu-name-id"><spring:message code="role.name"></spring:message> </span>
                <input name="roleName" class="form-control" regex="^.{1,50}$" tip="input 1 to 50 char">
            </div>
            <div class="form-group">
                <span for="add-menu-icon-id"><spring:message code="role.remarks"></spring:message> </span>
                <input name="remarks" class="form-control">
            </div>
        </form>
    </div>

    <!-- 修改角色弹窗 -->
    <div id="role-menu-mod-dialog-id" style="display: none">
        <form id="mod-role-info-form-id" action="${pageContext.request.contextPath}/role/update"
              method="post">
            <div class="form-group">
                <input type="hidden" id="roleUuid" name="roleUuid" value="">
            </div>
            <div class="form-group">
                <span for="add-menu-name-id"><spring:message code="role.name"></spring:message> </span>
                <input name="roleName" id="roleName" class="form-control" regex="^.{1,50}$" tip="input 1 to 50 char">
            </div>
            <div class="form-group">
                <span for="add-menu-icon-id"><spring:message code="role.remarks"></spring:message> </span>
                <input name="remarks" id="roleRemark" class="form-control">
            </div>
        </form>
    </div>
</body>
</html>
