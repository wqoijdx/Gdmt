<%@include file="../common/common.jsp"%>
<html>
<head>
    <title>
        <spring:message code="dictionary.info.tip"></spring:message>
    </title>
    <style type="text/css">
        .form-tow-column{
            display: grid;
            grid-template-columns: auto auto;

        }
        .form-group{
            margin: 0 10px;

        }
        fieldset>legend{
            font-size: 10px;
            border-bottom:none;
            margin-bottom: 0;
        }
        fieldset {
            display: block;
            margin-inline-start: 2px;
            margin-inline-end: 2px;
            padding-block-start: 0.35em;
            padding-inline-start: 0.75em;
            padding-inline-end: 0.75em;
            padding-block-end: 0.625em;
            min-inline-size: min-content;
            border: 1px solid #e5e5e5;;
            border-image: initial;
        }
        .gdmt-tb-th{
            height: 32px;
            background-color: #b5b5b5;
            background: #f5f5f5;
            border-top: #d4d4d4 1px solid;
            background-color: #fff;
        }
        .gdmt-tb-th > th{
            font-weight: normal;
            cursor: default;
            white-space: nowrap;
            overflow: hidden;
            text-align: center;
            vertical-align: middle !important;
            padding: 0px;
            height: 32px;
            background-color: #b5b5b5;
            box-sizing: border-box;
            color: #FFF;
            font-size: 12px;
        }
        .table-edit-data td{
            text-align: center;
            vertical-align: middle !important;
            padding: 0px;
            height: 32px;
            font-size: 12px;
            background-color: #f5f5f5;
        }
        .table-erow td{
            background-color: #ededed;
        }
        .table-edit-data .read{
            display: inline-block;
            margin-right: 10px;
        }
        .table-edit-data .edit{
            display: none;
        }
        .table-edit-tr .edit{
            display: inline-block;
            margin-right: 10px;
        }
        .table-edit-tr .read{
            display: none;
        }
    </style>
    <script type="text/javascript">

        //页面自适应
        function resizePageSize(){
            _gridWidth = $(document).width()-12;/*  -189 是去掉左侧 菜单的宽度，   -12 是防止浏览器缩小页面 出现滚动条 恢复页面时  折行的问题 */
            _gridHeight = $(document).height()-32-85; /* -32 顶部主菜单高度，   -90 查询条件高度*/
        }

        $(function () {
            resizePageSize();
            leftDictionaryLoad();
            $("#modify-dictionary-info-form-id").ajaxForm({
                dataType: "json",
                success : function(data) {
                    message(data.msg);
                    leftDictionaryLoad();
                },
                error : function() {
                    message(' <spring:message code="common.error"></spring:message>');
                },
                complete : function(response, status) {

                }
            });

            $("#add-dictionary-info-dialog-id").dialog({
                autoOpen : false,
                width : 400,
                modal : true,
                resizable : false,
                title: '<spring:message code='add.dictionary'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function() {
                            var flag = true;
                            var dom = $("#add-dictionary-info-form-id > div > .form-control");
                            dom.each(function (index,item) {
                                flag &=  $(item).verification();
                            });
                            if (!flag){
                                return;
                            }
                            $("#add-dictionary-info-form-id").submit();
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });

            $("#add-dictionary-info-form-id").ajaxForm({
                dataType: "json",
                success : function(data) {
                    message(data.msg);
                    leftDictionaryLoad();
                    $("#add-dictionary-info-dialog-id").dialog("close");
                },
                error : function() {
                    message(' <spring:message code="common.error"></spring:message>');
                },
                complete : function(response, status) {

                }
            });

            $("ul#tab-ul-dictionary-id > li").click(function(event) {
                $("ul#tab-ul-dictionary-id .active").removeClass("active");
                $(this).addClass("active");

                $("section.tab-content .in").removeClass("in");
                $("section.tab-content .active").removeClass("active");
                var tab = $(this).attr("tab");
                $("#" + tab).addClass("in");
                $("#" + tab).addClass("active")
            });
            $("ul#tab-ul-menu-id > li:first").click();

            _gridWidth =  _gridWidth - 153;
            var _columnWidth = _gridWidth/3;
            $("#table-i18n-edit-id").tableEditData({
                width : _gridWidth,
                height:400,
                url:"${pageContext.request.contextPath}/dictionary/getI18nByDictionaryId",
                colModel : [
                    {display : 'dictId',name : 'dictId',width : 150,align : 'center',hide : 'true'},
                    {display : "<spring:message code='dictionary.lang'/>",name : 'languageId',width : _columnWidth},
                    {display : "<spring:message code='dictionary.name'/>",name : 'dictName',width : _columnWidth},
                    {display : "<spring:message code='common.operation'/>",name : 'dictName',width : _columnWidth,process: function(v,_trid,_row){
                        var html =  '<a href="#" class="read" onclick="editTable(\''+ _trid +'\');"><i class="fa fa-pencil"></i></a>';
                            html +=  '<a href="#" class="edit" onclick="saveTable(\''+ _trid +'\');"><i class="fa fa-floppy-o"></i></a>';
                            html +=  '<a href="#" class="delete" onclick="deleteTable(\''+ _trid +'\');"><i class="fa fa-trash-o"></i></a>';

                            return html;

                        }},

                ]
            });

        });
        var leftDictionarySetting = {
            check: {
                enable: false
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback : {
                onClick : onClickNode
            }
        };
        function leftDictionaryLoad() {
            $.ajax({
                    type: 'post',
                async: false,
                dataType : 'json',
                url: '${pageContext.request.contextPath}/dictionary/getDictionaryTree',
                data: [],
                success: function (data) {
                    $.fn.zTree.init($("#dictionary-left-tree-ul-id"), leftDictionarySetting, data);
                },
                error: function(msg){
                    message(' <spring:message code="common.error"></spring:message>');
                }
                }
            );
        }
        
        function onClickNode(event,treeId,treeNode) {
            console.info(treeNode.id);
            $.ajax({
                type: 'post',
                async: false,
                dataType : 'json',
                url: '${pageContext.request.contextPath}/dictionary/getDictionaryInfoById',
                data: [{name:"dictId",value:treeNode.id}],
                success: function (data) {
                    var dom = $("#modify-dictionary-info-form-id >fieldset  >div >div .form-control");
                    dom.each(function (index,item) {
                        var nameAttr = $(item).attr("name");
                        $(item).val(data[nameAttr]);
                    });
                },
                error: function(msg){
                    message(' <spring:message code="common.error"></spring:message>');
                }
            });
            $("#table-i18n-edit-id").tableEditOptions({
                extParam:[
                    {name:"dictId",value:treeNode.id}
                ]
            }).tableEditReload();

        }

        function modifyDictionaryInfo() {
            var flag = true;
            var dom = $("#modify-dictionary-info-form-id > div > .form-control");
            dom.each(function (index,item) {
                flag &=  $(item).verification();
            });
            if (!flag){
                return;
            }
            $("#modify-dictionary-info-form-id").submit();

        }
        
        function addDictionaryInfo() {
            $("#add-dictionary-info-form-id").resetForm();
            $("#add-dictionary-info-dialog-id").dialog("open");

        }

        function deleteDictionaryInfo() {
            var dictId = $("#mod-dic-id").val();
            if(dictId == null ||"" === dictId){
                return;
            }
            $.ajax({
                type: 'post',
                async: false,
                dataType : 'json',
                url: '${pageContext.request.contextPath}/dictionary/deleteDictionaryInfo',
                data: [{name:"dictId",value:dictId}],
                success: function (data) {
                    message(data.msg);
                    $("#modify-dictionary-info-form-id").resetForm();
                    leftDictionaryLoad();
                },
                error: function(msg){
                    message(' <spring:message code="common.error"></spring:message>');
                }
            });
        }

        function editTable(trId) {
            $("#" + trId).addClass("table-edit-tr");
            
        }


        function saveTable(trId) {
            var trDom = $("#" + trId);
            var form = $("<form></form>");
            form.appendTo($("body"));
            trDom.find(".edit").cloneObj().appendTo(form);
            form.ajaxForm({
                dataType: "json",
                success : function(data) {
                    //message(data.msg);
                    form.remove();
                    $("#table-i18n-edit-id").tableEditReload();
                },
                error : function() {
                    message(' <spring:message code="common.error"></spring:message>');
                },
                complete : function(response, status) {

                }
            });

            var dictIdDom = form.find(".edit[name='dictId']");
            if(dictIdDom.val() === ''){
                dictIdDom.val($("#mod-dic-id").val());
                form.attr({
                    method:"post",
                    action:"${pageContext.request.contextPath}/dictionary/addI18nData"
                });
            }else{
                form.attr({
                    method:"post",
                    action:"${pageContext.request.contextPath}/dictionary/saveI18nData"
                });
            }
            form.submit();
        }

        function deleteTable(trId) {
            var trDom = $("#" + trId);
            var form = $("<form></form>");
            form.appendTo($("body"));
            trDom.find(".edit").cloneObj().appendTo(form);
            form.ajaxForm({
                dataType: "json",
                success : function(data) {
                    //message(data.msg);
                    form.remove();
                    $("#table-i18n-edit-id").tableEditReload();
                },
                error : function() {
                    message(' <spring:message code="common.error"></spring:message>');
                },
                complete : function(response, status) {

                }
            });
            form.attr({
                method:"post",
                action:"${pageContext.request.contextPath}/dictionary/deleteI18nData"
            });
            form.submit();

        }


    </script>
</head>
<body style="display: flex;flex-direction: row">

            <nav>
                <spring:message code="dictionary.info.tip"></spring:message>
                <ul id="dictionary-left-tree-ul-id" class="ztree"></ul>
            </nav>

            <article style="flex-grow: 1">

                <nav>
                    <a href="#" class="btn" onclick="modifyDictionaryInfo()">
                        <i class="glyphicon glyphicon-pencil"></i>
                        <span><spring:message code="common.modify"></spring:message></span>
                    </a>

                    <a href="#" class="btn" onclick="addDictionaryInfo()">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span><spring:message code="common.add"></spring:message></span>
                    </a>

                    <a href="#" class="btn" onclick="deleteDictionaryInfo()">
                        <i class="glyphicon glyphicon-trash"></i>
                        <span><spring:message code="common.delete"></spring:message></span>
                    </a>
                </nav>

                <form id="modify-dictionary-info-form-id" action="${pageContext.request.contextPath}/dictionary/modifyDictionaryInfoById" method="post">
                    <fieldset>
                        <legend>
                            <span><spring:message code="common.base"></spring:message></span>
                        </legend>

                        <div class="form-tow-column">
                            <div class="form-group" >
                                <span for="mod-dic-id"><spring:message code="dictionary.id"></spring:message></span>
                                <input name="dictId" id="mod-dic-id" class="form-control" readonly>
                            </div>

                            <div class="form-group">
                                <span for="mod-dic-value-id"><spring:message code="dictionary.datavalue.id"></spring:message></span>
                                <input name="dicCode" id="mod-dic-value-id" class="form-control" regex="^.{1,50}$" tip="input 1 to 50 char">
                            </div>

                            <div class="form-group" >
                                <span for="mod-dic-groupName-id"><spring:message code="dictionary.groupName.id"></spring:message></span>
                                <input name="dictGroup" id="mod-dic-groupName-id" class="form-control" regex="^.{1,50}$" tip="input 1 to 50 char">
                                <%--                        <gdmt:dicData group="organ.type" id="mod-dic-groupName-id" classez="form-control" name="dictGroup"></gdmt:dicData>--%>
                            </div>

                            <div class="form-group" >
                                <span for="mod-dic-describe-id"><spring:message code="dictionary.describe.id"></spring:message></span>
                                <input name="description" id="mod-dic-describe-id" class="form-control" regex="^.{1,50}$" tip="input 1 to 50 char">
                            </div>

                            <div class="form-group" >
                                <span for="mod-dic-weight-id"><spring:message code="dictionary.weight.id"></spring:message></span>
                                <input name="dicSort" id="mod-dic-weight-id" class="form-control"regex="^.{1,50}$" tip="input 1 to 50 char">
                            </div>
                        </div>


                    </fieldset>
                </form>

                <ul id="tab-ul-dictionary-id" class="nav nav-tabs">
                    <li tab="tab-i18n-id">
                        <a href="#">
                            <i class="fa fa-university"></i>
                            <spring:message code="common.i18n.name"></spring:message>
                        </a>
                    </li>

                    <li tab="tab-son-dictionary-id">
                        <a href="#">
                            <i class="fa fa-trello"></i>
                            <spring:message code="dictionary.son.name"></spring:message>
                        </a>
                    </li>
                </ul>
                <section class="tab-content">
                    <article id="tab-i18n-id" class="tab-pane fade in active">
                        <table id="table-i18n-edit-id" class="table-edit-data"></table>
                    </article>

                    <article id="tab-son-dictionary-id" class="tab-pane fade">
                        <spring:message code="dictionary.son.name"></spring:message>
                    </article>
                </section>


            </article>

            <div id="add-dictionary-info-dialog-id" style="display: none">
                <form id="add-dictionary-info-form-id" action="${pageContext.request.contextPath}/dictionary/addDictionaryInfo" method="post">
                    <div class="form-group">
                        <span for="add-dic-value-id"><spring:message code="dictionary.datavalue.id"></spring:message></span>
                        <input name="dicCode" id="add-dic-value-id" class="form-control" regex="^.{1,50}$" tip="input 1 to 50 char">
                    </div>

                    <div class="form-group" >
                        <span for="add-dic-groupName-id"><spring:message code="dictionary.groupName.id"></spring:message></span>
                        <input name="dictGroup" id="add-dic-groupName-id" class="form-control" regex="^.{1,50}$" tip="input 1 to 50 char">
                    </div>

                    <div class="form-group" >
                        <span for="add-dic-describe-id"><spring:message code="dictionary.describe.id"></spring:message></span>
                        <input name="description" id="add-dic-describe-id" class="form-control" regex="^.{1,50}$" tip="input 1 to 50 char">
                    </div>

                    <div class="form-group" >
                        <span for="add-dic-weight-id"><spring:message code="dictionary.weight.id"></spring:message></span>
                        <input name="dicSort" id="add-dic-weight-id" class="form-control"regex="^.{1,50}$" tip="input 1 to 50 char">
                    </div>
                </form>
            </div>

</body>
</html>
