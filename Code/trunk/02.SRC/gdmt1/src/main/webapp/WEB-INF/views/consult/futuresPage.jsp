<%@include file="../common/common.jsp"%>
<html>
<head>
    <script src="${pageContext.request.contextPath}/amcharts/amcharts.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/amcharts/serial.js" type="text/javascript"></script>
    <title>
        <spring:message code="futuresprice"></spring:message>
    </title>
    <script type="text/javascript">
        var _gridWidth;
        var _gridHeight;
        //页面自适应
        function resizePageSize(){
            _gridWidth = $(document).width()-20;/*  -189 是去掉左侧 菜单的宽度，   -12 是防止浏览器缩小页面 出现滚动条 恢复页面时  折行的问题 */
            _gridHeight = $(document).height()-10; /* -32 顶部主菜单高度，   -90 查询条件高度*/
        }

        $(function () {
            resizePageSize();
            var _columnWidth = _gridWidth/8-5;
            $("#flexigrid-id").flexigrid({
                width : _gridWidth,
                height : _gridHeight,
                url : "${pageContext.request.contextPath}/consult/getFuturesInfolistByPage",
                dataType : 'json',
                colModel : [
                    {display : 'futurespriceId',name : 'futurespriceId',width : 150,sortable : false,align : 'center',hide : 'true'},
                    {display : "<spring:message code='futurespriceDate'/>",name : 'futurespriceDate',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='futurespriceMonth'/>",name : 'futurespriceMonth',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='futurespriceOprice'/>",name : 'futurespriceOprice',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='futurespriceCprice'/>",name : 'futurespriceCprice',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='futurespriceSettlement'/>",name : 'futurespriceSettlement',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='futurespriceCjl'/>",name : 'futurespriceCjl',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='futurespriceKpl'/>",name : 'futurespriceKpl',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='futurespriceHb'/>",name : 'futurespriceHb',width : _columnWidth, sortable : true,align : 'center'}
                ],
                resizable : false, //resizable table是否可伸缩
                useRp : true,
                usepager : true, //是否分页
                autoload : false, //自动加载，即第一次发起ajax请求
                hideOnSubmit : true, //是否在回调时显示遮盖
                showcheckbox : true, //是否显示多选框
                //rowhandler : rowDbclick, //是否启用行的扩展事情功能,在生成行时绑定事件，如双击，右键等
                rowbinddata : true
            });

            $("#flexigrid-id").flexReload();

            $("ul#tab-ul-menu-id > li").click(function(event) {
                $("ul#tab-ul-menu-id .active").removeClass("active");
                $(this).addClass("active");

                $("section.tab-content .in").removeClass("in");
                $("section.tab-content .active").removeClass("active");
                var tab = $(this).attr("tab");
                $("#" + tab).addClass("in");
                $("#" + tab).addClass("active")
            });
            $("ul#tab-ul-menu-id > li:first").click();
            queryFutures();

        })//ready

        function queryFutures() {
            $.ajax({
                    type: 'post',
                    async: false,
                    dataType : 'json',
                    url: '${pageContext.request.contextPath}/consult/getFutures',
                    data: [],
                    success: function (data) {
                        loadChart(data);
                    },
                    error: function(msg){
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );

        }

        function loadChart(chartData) {
            var chart;
            // SERIAL CHART
            chart = new AmCharts.AmSerialChart();
            chart.pathToImages = "../amcharts/images/";
            chart.dataProvider = chartData;
            chart.categoryField = "futurespriceDate";
            chart.dataDateFormat = "YYYY-MM-DD";
            chart.startDuration = 1;

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
            categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to DD
            categoryAxis.dashLength = 1;
            categoryAxis.gridAlpha = 0.15;
            categoryAxis.axisColor = "#DADADA";
            categoryAxis.title="<spring:message code='futurespriceDate'/>"

            // first value axis (on the left)
            var valueAxis1 = new AmCharts.ValueAxis();
            valueAxis1.axisColor = "#FF6600";
            valueAxis1.axisThickness = 2;
            valueAxis1.gridAlpha = 0;
            chart.addValueAxis(valueAxis1);

            // second value axis (on the right)
            var valueAxis2 = new AmCharts.ValueAxis();
            valueAxis2.position = "right"; // this line makes the axis to appear on the right
            valueAxis2.axisColor = "#FCD202";
            valueAxis2.gridAlpha = 0;
            valueAxis2.axisThickness = 2;
            chart.addValueAxis(valueAxis2);

            // third value axis (on the left, detached)
            valueAxis3 = new AmCharts.ValueAxis();
            valueAxis3.offset = 70; // this line makes the axis to appear detached from plot area
            valueAxis3.gridAlpha = 0;
            valueAxis3.axisColor = "#B0DE09";
            valueAxis3.axisThickness = 2;
            chart.addValueAxis(valueAxis3);

            // GRAPH
            var graph3 = new AmCharts.AmGraph();
            graph3.valueAxis = valueAxis3; // we have to indicate which value axis should be used
            graph3.type = "smoothedLine";
            graph3.lineAlpha = 1;
            graph3.bullet = "triangleUp";
            graph3.hideBulletsCount = 30;
            graph3.bulletBorderThickness = 1;
            graph3.hidden = false;
            graph3.title = "<spring:message code='futurespriceCjl'/>";
            graph3.valueField = "futurespriceCjl";
            graph3.lineThickness = 2;
            chart.addGraph(graph3);

            // GRAPH
            var graph1 = new AmCharts.AmGraph();
            graph1.valueAxis = valueAxis1; // we have to indicate which value axis should be used
            graph1.type = "smoothedLine";
            graph1.bullet = "round";
            graph1.hideBulletsCount = 30;
            graph1.bulletBorderThickness = 1;
            graph1.hidden = false;
            graph1.title = "<spring:message code='futurespriceKpl'/>";
            graph1.valueField = "futurespriceKpl";
            graph1.lineThickness = 2;
            chart.addGraph(graph1);

            // GRAPH
            var graph2 = new AmCharts.AmGraph();
            graph2.valueAxis = valueAxis2; // we have to indicate which value axis should be used
            graph2.type = "smoothedLine";
            graph2.bullet = "square";
            graph2.hideBulletsCount = 30;
            graph2.bulletBorderThickness = 1;
            graph2.hidden = false;
            graph2.title = "<spring:message code='futurespriceSettlement'/>";
            graph2.valueField = "futurespriceSettlement";
            graph2.lineThickness = 2;
            chart.addGraph(graph2);

            // CURSOR
            var chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorPosition = "mouse";
            chartCursor.cursorAlpha = 0;
            chart.addChartCursor(chartCursor);

            // SCROLLBAR
            var chartScrollbar = new AmCharts.ChartScrollbar();
            chart.addChartScrollbar(chartScrollbar);

            // LEGEND
            var legend = new AmCharts.AmLegend();
            legend.valueWidth = 120;
            legend.useGraphSettings = true;
            chart.addLegend(legend);

            // WRITE
            chart.write("chartdiv");
        }

        function queryBtn() {
            var startDate = $("#startDate").val();
            var endDate = $("#endDate").val();
            $("#flexigrid-id").flexOptions({
                extParam:[
                    {name:"startDate",value:startDate},
                    {name:"endDate",value:endDate}
                ]
            }).flexReload();

        }
    
    </script>

</head>
<body>
<ul id="tab-ul-menu-id" class="nav nav-tabs">
    <security:authorize access="hasRole('ROLE_gdmt_data_fp')">
        <li tab="fp-chart-id">
            <a href="#">
                <i class="fa fa-area-chart"></i>
                <spring:message code="chart"></spring:message>
            </a>
        </li>
    </security:authorize>
    <security:authorize access="hasRole('ROLE_gdmt_data_fp')">
        <li tab="fp-table-id">
            <a href="#">
                <i class="fa fa-calendar"></i>
                <spring:message code="table"></spring:message>
            </a>
        </li>
    </security:authorize>
</ul>
<section class="tab-content">
    <article id="fp-chart-id" class="tab-pane fade in active">
        <div id="chartdiv" style="width:100%; height:400px;"></div>
    </article>
    <article id="fp-table-id" class="tab-pane fade">
        <div>
            <label> <spring:message code='startdate'/>:</label> <input type="date" id="startDate"/>
            <label> <spring:message code='enddate'/>:</label> <input type="date" id="endDate"/>
            <a class="btn" href="#" onclick="queryBtn();">
                <i class="glyphicon glyphicon-zoom-in"></i>
                <span><spring:message code='common.query'/></span>
            </a>
        </div>
        <table id="flexigrid-id"></table>
    </article>
</section>
</body>
</html>
