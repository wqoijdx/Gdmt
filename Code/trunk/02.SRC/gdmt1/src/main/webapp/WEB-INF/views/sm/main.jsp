<%@include file="../common/common.jsp"%>

<html>
<head>
    <title>
        <spring:message code="main.tile.tip"></spring:message>
    </title>
    <style type="text/css">

        .sm-page-menu{
            margin-left: 20px;
        }
        #contents-id{
            border: 1px solid #ddd;
        }

    </style>

    <script type="text/javascript">

        function resizePageSize() {
            $("body").css("height",$(document).height()+ "px");
        }

        $(function () {

            resizePageSize();

            //contents-id
            $("#user-info-id").click(function (event) {
                event.stopPropagation();
                $(".to-link").css("display","block");
                // console.info($(".to-link"));
            });

            $("body").click(function (event) {
                event.stopPropagation();
                $(".to-link").hide();
            });
            $(".to-link").hide();
            $("#gd-sys-menu-id > li").click(function (event) {
                $(this).parent().find(".active").removeClass("active");
                $(this).addClass("active");
                var menuId = $(this).attr("menuId");
                $.ajax({
                        type: 'post',
                        async: false,
                        dataType : 'json',
                        url: '${pageContext.request.contextPath}/getServiceMenu',
                        data: [{name:"menuId",value:menuId}],
                        success: function (data) {
                            //console.info(data);
                            var htmlContents ='';
                            $(data).each(function (index,item) {
                                htmlContents +='<li><a href="#" ><i class="'+item.menuIcon+'"></i><span>'+item.menuName+'</span></a>';
                                htmlContents +='<ul class="sm-page-menu nav navbar-btn">';
                                //console.info(item.childrenList);
                                $(item.childrenList).each(function (index1,item1) {
                                    htmlContents +='<li><a href="${pageContext.request.contextPath}/'+ item1.menuUrl +'#" target="contents-name"><i class="'+item1.menuIcon+'"></i><span>'+item1.menuName+'</span></a></li>';
                                });
                                htmlContents +="</ul>";
                                htmlContents += '</li>';
                            });
                            $("#gd-server-menu-id").html(htmlContents);

                        },
                        error: function(msg){
                            message(' <spring:message code="common.error"></spring:message>');
                        }
                    }
                );
            });

            $("#gd-sys-menu-id > li:first").click();

        });//ready

    </script>
</head>
<body style="display: flex;flex-direction: column;">
    <div style="display: flex;flex-direction: row;">

        <c:if test="${siteLanguage != 'en'}">
            <img src="${pageContext.request.contextPath}/assets/images/logo_zh.png">
        </c:if>
        <c:if test="${siteLanguage == 'en'}">
            <img src="${pageContext.request.contextPath}/assets/images/logo_en.png">
        </c:if>
        <nav style="    position: absolute;right: 100px; top: 6px;width: 132px;">
            <ul class="nav navbar-btn">
                <li id="user-info-id">
                    <a href="#">
                        <i class="glyphicon glyphicon-user"></i>
                        <security:authentication property="principal.niceName"></security:authentication>
                    </a>
                </li>
                <li class="to-link">
                    <a href="${pageContext.request.contextPath}/logout">
                        <i class="glyphicon glyphicon-off"></i>
                        <spring:message code="main.logout"></spring:message>
                    </a>
                </li>
                <li class="to-link">
                    <a href="/login/userinfo#" target="contents-name">
                        <i class="glyphicon glyphicon-th-large"></i>
                        <spring:message code="main.modify.user"></spring:message>
                    </a>
                </li>
                <li class="to-link">
                    <a href="/login/modpass#" target="contents-name">
                        <i class="glyphicon glyphicon-road"></i>
                        <spring:message code="main.modify.passwd"></spring:message>
                    </a>
                </li>
            </ul>
        </nav>
    </div>

    <nav>
        <ul id="gd-sys-menu-id" class="nav nav-tabs">
            <c:forEach items="${menu}" var="firstMenu">
                <li menuId="${firstMenu.menuId}">
                    <a href="#" >
                        <i class="${firstMenu.menuIcon}"></i>
                        <span>${firstMenu.menuName}</span>
                    </a>
                </li>
            </c:forEach>

        </ul>
    </nav>

    <article style="display: flex;flex-direction: row;flex-grow: 1">
        <ul id="gd-server-menu-id" class="nav navbar-btn" style="width: 144px">

        </ul>
        <iframe id="contents-id" name="contents-name" width="100%" height="100%" src="${pageContext.request.contextPath}/menu/menuPage" ></iframe>

        <section class="tab-content">
            <article id="tab-organlist-id" class="tab-pane fade in active">
                <div id="adjust-depart-info-dialog-id" style="display: none">
                    <form id="adjust-depart-info-form-id" action="${pageContext.request.contextPath}/department/getDepartmentIdInfolistByPage"
                          method="post">
                        <div class="form-group">
                            <span for="adjust-depart-duty-id"><spring:message code="adjust.dutyId"></spring:message> </span>
                            <input name="dutyId" id="adjust-depart-duty-id" class="form-control" regex="^.{1,50}$" tip="input 1 to 50 char">
                        </div>
                        <div class="form-group">
                            <span for="adjust-depart-duty-id"><spring:message code="adjust.dutyName"></spring:message> </span>
                            <input name="dutyName"  id="adjust-depart-duty-name" class="form-control" regex="^.{1,50}$" tip="input 1 to 50 char">
                        </div>
                        <div class="form-group">
                            <span for="adjust-depart-role-name"><spring:message code="adjust.roleName"></spring:message> </span>
                            <input name="roleName"  id="adjust-depart-role-name" class="form-control" regex="^.{1,50}$" tip="input 1 to 50 char">
                        </div>
                        <div class="form-group">
                            <span for="adjust-depart-email-id"><spring:message code="adjust.operation"></spring:message> </span>
                            <input name="operation"  id="adjust-depart-operation" class="form-control" regex="^.{1,50}$" tip="input 1 to 50 char">
                        </div>
                    </form>
                </div>
            </article>
        </section>

    </article>


</body>
</html>
