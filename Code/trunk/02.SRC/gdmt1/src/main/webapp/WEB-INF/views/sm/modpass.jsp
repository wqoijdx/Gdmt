<%@include file="../common/common.jsp"%>
<html>
<head>
    <title>
        <spring:message code="menu.info.tip"></spring:message>
    </title>
    <style type="text/css">
        .form-tow-column{
            display:grid;
            grid-template-columns:auto auto;

        }
        .form-group{
            margin: 0 10px;
        }
        fieldset>legend{
            font-size: 10px;
            border-bottom:none;
            margin-bottom: 0;
        }
        fieldset {
            display: block;
            margin-inline-start: 2px;
            margin-inline-end: 2px;
            padding-block-start: 0.35em;
            padding-inline-start: 0.75em;
            padding-inline-end: 0.75em;
            padding-block-end: 0.625em;
            min-inline-size: min-content;
            border: 1px solid #e5e5e5;;
            border-image: initial;
        }

        .gdmt-tb-th{
            height: 32px;
            background-color: #b5b5b5;
            background: #f5f5f5;
            border-top: #d4d4d4 1px solid;
            background-color: #fff;
        }
        .gdmt-tb-th > th{
            font-weight: normal;
            cursor: default;
            white-space: nowrap;
            overflow: hidden;
            text-align: center;
            vertical-align: middle !important;
            padding: 0px;
            height: 32px;
            background-color: #b5b5b5;
            box-sizing: border-box;
            color: #FFF;
            font-size: 12px;
        }
        .table-edit-data td{
            text-align: center;
            vertical-align: middle !important;
            padding: 0px;
            height: 32px;
            font-size: 12px;
            background-color: #f5f5f5;
        }
        .table-erow td{
            background-color: #ededed;
        }
        .table-edit-data .read{
            display: inline-block;
            margin-right: 10px;
        }
        .table-edit-data .edit{
            display: none;
        }
        .table-edit-tr .edit{
            display: inline-block;
        }
        .table-edit-tr .read{
            display: none;
        }
    </style>
    <script type="text/javascript">
        //页面自适应
        function resizePageSize(){
            _gridWidth = $(document).width()-12;/*  -189 是去掉左侧 菜单的宽度，   -12 是防止浏览器缩小页面 出现滚动条 恢复页面时  折行的问题 */
            _gridHeight = $(document).height()-32-85; /* -32 顶部主菜单高度，   -90 查询条件高度*/
        }
        $(function() {
            resizePageSize();
            $("#submit-id").click(function (event) {
                var flag = true;

                $("#modify-pass-form-id").submit();

            });
            $("#modify-pass-form-id").ajaxForm({
                dataType: "json",
                success : function(data) {
                    message(data.msg);
                    if (data.success == true) {
                        setTimeout(function () {
                            top.location.href = '${pageContext.request.contextPath}/logout'
                        }, 1000)
                    }
                },
                error : function() {
                    message('<spring:message code="common.error"></spring:message>');
                },
                complete : function(response, status) {

                }
            });
            $("#add-menu-info-dialog-id").dialog({
                autoOpen : false,
                width : 400,
                modal : true,
                resizable : false,
                title: '<spring:message code='menu.add'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function() {
                            var flag = true;
                            var dom = $("#add-menu-info-form-id > div > .form-control");
                            dom.each(function(index,item) {
                                flag &= $(item).verification();
                                console.info(flag);
                            });
                            if(!flag){
                                return;
                            }
                            $("#add-menu-info-form-id").submit();
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });

            $("#add-menu-info-form-id").ajaxForm({
                dataType: "json",
                success : function(data) {
                    message(data.msg);
                    leftMenuLoad();
                    $("#add-menu-info-dialog-id").dialog('close');
                },
                error : function() {
                    message(' <spring:message code="common.error"></spring:message>');
                },
                complete : function(response, status) {

                }

            });

            $("#bound-menu-dialog-id").dialog({
                autoOpen : false,
                width : 400,
                modal : true,
                resizable : false,
                title: '<spring:message code='common.bound'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function() {
                            var parentId = "";
                            var ztreeObj = $.fn.zTree.getZTreeObj("bound-menu-ztree-id");
                            var nodes = ztreeObj.getCheckedNodes(true);
                            $(nodes).each(function (index,item) {
                                parentId =item.id;
                            });
                            var menuId = $("#mod-menu-id").val();

                            $.ajax({
                                type: 'post',
                                async: false,
                                dataType : 'json',
                                url: '${pageContext.request.contextPath}/menu/boundMenu',
                                data: [
                                    {name:"parentId",value:parentId},
                                    {name:"menuId",value:menuId}
                                ],
                                success: function (data) {
                                    message(data.msg);
                                    $("#bound-menu-dialog-id").dialog('close');
                                    leftMenuLoad();
                                },
                                error: function(msg){
                                    message(' <spring:message code="common.error"></spring:message>');
                                }
                            });
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });

            $("ul#tab-ul-menu-id > li").click(function(event) {
                $("ul#tab-ul-menu-id .active").removeClass("active");
                $(this).addClass("active");

                $("section.tab-content .in").removeClass("in");
                $("section.tab-content .active").removeClass("active");
                var tab = $(this).attr("tab");
                $("#" + tab).addClass("in");
                $("#" + tab).addClass("active")
            });
            $("ul#tab-ul-menu-id > li:first").click();
            _gridWidth =  _gridWidth - 153;
            var _columnWidth = _gridWidth/3;
            $("#table-i18n-edit-id").tableEditData({
                width : _gridWidth,
                height:400,
                url:"${pageContext.request.contextPath}/menu/getI18nByMenuId",
                colModel : [
                    {display : 'menuId',name : 'menuId',width : 150,align : 'center',hide : 'true'},
                    {display : "<spring:message code='menu.lang'/>",name : 'langId',width : _columnWidth,dom: function(obj,v,_trid,_row){
                            var select=$('<gdmt:dicData group="lang.type" classez="edit" name="langId"></gdmt:dicData>');
                            select.val(v);
                            select.css("width","43%");
                            select.appendTo(obj);
                            select.attr("readonly",true);
                            if(_row){
                                select.attr("disabled","disabled");
                            }



                            var span = $("<span></span>");
                            span.addClass("read");
                            var vl  =  select.find("option[value='" + v + "']").text();
                            span.text(vl);
                            span.appendTo(obj);


                        }},
                    {display : "<spring:message code='menu.name'/>",name : 'menuName',width : _columnWidth},
                    {display : "<spring:message code='common.operation'/>",name : 'menuName',width : _columnWidth,process: function(v,_trid,_row) {
                            var html='<a href="#" class="read" onclick="editTable(\''+ _trid +'\');"> <i class="fa fa-pencil"></i> </a>';
                            html +='<a href="#" class="edit" onclick="saveTable(\''+ _trid +'\');"> <i class="fa fa-floppy-o"></i> </a>';
                            html +='<a href="#" class="read" onclick="deleteTable(\''+ _trid +'\');"> <i class="fa fa-trash-o"></i> </a>';

                            return html;
                        }}
                ]
            });

        });//ready
        var leftMenuSetting = {
            check: {
                enable: false
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback : {
                onClick : onClickNode
            }
        };
        function leftMenuLoad() {
            $.ajax({
                    type: 'post',
                    async: false,
                    dataType : 'json',
                    url: '${pageContext.request.contextPath}/menu/getMenuTree',
                    data: [],
                    success: function (data) {
                        $.fn.zTree.init($("#menu-left-tree-ul-id"), leftMenuSetting, data);
                    },
                    error: function(msg){
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );
        }

        //ztree回调事件
        function onClickNode(event, treeId, treeNode){
            //console.info(treeNode.id);
            $.ajax({
                    type: 'post',
                    async: false,
                    dataType : 'json',
                    url: '${pageContext.request.contextPath}/menu/getMenuInfoById',
                    data: [{name:"menuId",value:treeNode.id}],
                    success: function (data) {
                        var dom = $("#modify-menu-info-form-id .form-control");
                        dom.each(function(index,item) {
                            var nameAttr = $(item).attr("name");
                            // console.info(data[nameAttr]);
                            $(item).val(data[nameAttr]);
                        });

                    },
                    error: function(msg){
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );
            $("#table-i18n-edit-id").tableEditOptions({
                extParam:[
                    {name:"menuId",value:treeNode.id}
                ]
            }).tableEditReload();
        }
        function modifyMenuInfo() {
            var flag = true;
            var dom = $("#modify-menu-info-form-id > div > .form-control");
            dom.each(function(index,item) {
                flag &= $(item).verification();
                console.info(flag);
            });
            if(!flag){
                return;
            }
            $("#modify-menu-info-form-id").submit();
        }

        var BoundMenuSetting = {
            check: {
                enable: true,
                chkStyle: "radio",
                radioType: "all"
            },
            data: {
                simpleData: {
                    enable: true
                }
            }
        };

        function boundMenuInfo () {
            var modMenuId = $("#mod-menu-id").val();
            if(modMenuId == null || '' === modMenuId){
                return;
            }
            $.ajax({
                    type: 'post',
                    async: false,
                    dataType : 'json',
                    url: '${pageContext.request.contextPath}/menu/getMenuTreeByMenuId',
                    data: [{name:"menuId",value:modMenuId}],
                    success: function (data) {
                        $.fn.zTree.init($("#bound-menu-ztree-id"), BoundMenuSetting, data);
                        var ztreeObj = $.fn.zTree.getZTreeObj("bound-menu-ztree-id");
                        var nodes = ztreeObj.getNodesByParam("id",modMenuId,null);
                        ztreeObj.removeNode(nodes[0]);
                        $("#bound-menu-dialog-id").dialog('open');

                    },
                    error: function(msg){
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );
        }

        function unboundMenuInfo() {
            var modMenuId = $("#mod-menu-id").val();
            if(modMenuId == null || '' === modMenuId){
                return;
            }
            $.ajax({
                type: 'post',
                async: false,
                dataType : 'json',
                url: '${pageContext.request.contextPath}/menu/unBoundByMenuId',
                data: [{name:"menuId",value:modMenuId}],
                success: function (data) {
                    leftMenuLoad();
                    message(data.msg);
                },
                error: function(msg){
                    message(' <spring:message code="common.error"></spring:message>');
                }
            });
        }
        function editTable(trId) {
            $("#" + trId).addClass("table-edit-tr");
        }


        function saveTable(trId) {
            var trDom = $("#" + trId);
            var form = $("<form></form>");
            form.appendTo($("body"));
            trDom.find(".edit").cloneObj().appendTo(form);
            form.ajaxForm({
                dataType: "json",
                success : function(data) {
                    //message(data.msg);
                    form.remove();
                    $("#table-i18n-edit-id").tableEditReload();
                },
                error : function() {
                    message(' <spring:message code="common.error"></spring:message>');
                },
                complete : function(response, status) {

                }
            });

            var menuIdDom = form.find(".edit[name='menuId']");
            if(menuIdDom.val() === ''){
                menuIdDom.val($("#mod-menu-id").val());
                form.attr({
                    method:"post",
                    action:"${pageContext.request.contextPath}/menu/addI18nData"
                });
            }else{
                form.attr({
                    method:"post",
                    action:"${pageContext.request.contextPath}/menu/saveI18nData"
                });
            }
            form.submit();
        }

        function deleteTable(trId) {
            var trDom = $("#" + trId);
            var form = $("<form></form>");
            form.appendTo($("body"));
            trDom.find(".edit").cloneObj().appendTo(form);
            form.ajaxForm({
                dataType: "json",
                success : function(data) {
                    //message(data.msg);
                    form.remove();
                    $("#table-i18n-edit-id").tableEditReload();
                },
                error : function() {
                    message(' <spring:message code="common.error"></spring:message>');
                },
                complete : function(response, status) {

                }
            });
            form.attr({
                method:"post",
                action:"${pageContext.request.contextPath}/menu/deleteI18nData"
            });
            form.submit();
        }
    </script>
</head>
<body style="display: flex;flex-direction: row">

<article style="flex-grow: 1">

    <form id="modify-pass-form-id" action="${pageContext.request.contextPath}/user/modPass"
          method="post">
        <fieldset >
            <legend>
                <span><spring:message code="main.modify.passwd" /></span>
            </legend>
                <div class="form-group">
                    <span for="oldPass"><spring:message code="old.pass"></spring:message> </span>
                    <input type="password" name="oldPass" id="oldPass" class="form-control" >
                </div>
                <div class="form-group">
                    <span for="oldPass"><spring:message code="new.pass"></spring:message> </span>
                    <input type="password" name="newPass" id="newPass" class="form-control" regex="^.{1,50}$" tip="input 1 to 50 char">
                </div>
            <div class="form-group" style="display: flex;margin-top: 10px;">
                <a class="form-control btn-success" id="submit-id" href="#" style="text-align: center;justify-content: center;width: 50%" >
                    <spring:message code="common.confirm"></spring:message>
                </a>
            </div>
        </fieldset>
    </form>

</article>

<div id="bound-menu-dialog-id" style="display: none">
    <ul id="bound-menu-ztree-id" class="ztree"></ul>
</div>


</body>
</html>
