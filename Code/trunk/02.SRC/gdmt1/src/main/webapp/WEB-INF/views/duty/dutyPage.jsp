<%@include file="../common/common.jsp"%>

<html>
<head>
    <title><spring:message code="duty.tip"></spring:message> </title>
    <style type="text/css">
        .form-tow-column{
            display:grid;
            grid-template-columns:auto auto;

        }
        .form-group{
            margin: 0 10px;
        }
        fieldset>legend{
            font-size: 10px;
            border-bottom:none;
            margin-bottom: 0;
        }
        fieldset {
            display: block;
            margin-inline-start: 2px;
            margin-inline-end: 2px;
            padding-block-start: 0.35em;
            padding-inline-start: 0.75em;
            padding-inline-end: 0.75em;
            padding-block-end: 0.625em;
            min-inline-size: min-content;
            border: 1px solid #e5e5e5;;
            border-image: initial;
        }

        .gdmt-tb-th{
            height: 32px;
            background-color: #b5b5b5;
            background: #f5f5f5;
            border-top: #d4d4d4 1px solid;
            background-color: #fff;
        }
        .gdmt-tb-th > th{
            font-weight: normal;
            cursor: default;
            white-space: nowrap;
            overflow: hidden;
            text-align: center;
            vertical-align: middle !important;
            padding: 0px;
            height: 32px;
            background-color: #b5b5b5;
            box-sizing: border-box;
            color: #FFF;
            font-size: 12px;
        }
        .table-edit-data td{
            text-align: center;
            vertical-align: middle !important;
            padding: 0px;
            height: 32px;
            font-size: 12px;
            background-color: #f5f5f5;
        }
        .table-erow td{
            background-color: #ededed;
        }
        .table-edit-data .read{
            display: inline-block;
            margin-right: 10px;
        }
        .table-edit-data .edit{
            display: none;
        }
        .table-edit-tr .edit{
            display: inline-block;
        }
        .table-edit-tr .read{
            display: none;
        }
    </style>
    <script type="text/javascript">
        //页面自适应
        function resizePageSize(){
            _gridWidth = $(document).width()-12;/*  -189 是去掉左侧 菜单的宽度，   -12 是防止浏览器缩小页面 出现滚动条 恢复页面时  折行的问题 */
            _gridHeight = $(document).height()-98; /* -32 顶部主菜单高度，   -90 查询条件高度*/
        }
        $(function() {
            resizePageSize();
            leftMenuLoad();

            var _columnWidth = _gridWidth/6-4;
            $("#flexigrid-id").flexigrid({
                width : _gridWidth-190,
                height : _gridHeight,

                url : "${pageContext.request.contextPath}/post/queryDutyListPage",
                dataType : 'json',
                colModel : [
                    {display : 'dutyId',name : 'dutyId',width : 150,sortable : false,align : 'center',hide : 'true'},
                                {display : "<spring:message code='duty.id'/>",name : 'dutyid',width : _columnWidth, sortable : true,align : 'center',dom: function(v){
                                    var v=dutyid;
                                    return v;
                                    }},
                                {display : "<spring:message code='duty.name'/>",name : 'name',width : _columnWidth, sortable : true,align : 'center'},
                                {display : "<spring:message code='organ.name'/>",name : 'organName',width : _columnWidth, sortable : true,align : 'center'},
                                {display : "<spring:message code='role.name'/>",name : 'roleName',width : _columnWidth, sortable : true,align : 'center'},
                              {display : "<spring:message code='common.operation'/>",name : 'dutyid',width : _columnWidth,process: function(v,_trid,_row) {
                            var html='<a href="#" class="read" onclick="editTable(\''+v+'\');"> <spring:message code='common.details'/> </a>';
                            return html;
                        }}
                ],
                resizable : false, //resizable table是否可伸缩
                useRp : true,
                usepager : true, //是否分页
                autoload : false, //自动加载，即第一次发起ajax请求
                hideOnSubmit : true, //是否在回调时显示遮盖
                showcheckbox : true, //是否显示多选框
                //rowhandler : rowDbclick, //是否启用行的扩展事情功能,在生成行时绑定事件，如双击，右键等
                rowbinddata : true
            });
            $("#flexigrid-id").flexReload();


            $("#duty-organ-mod-dialog-id").dialog({
                autoOpen : false,
                width : 400,
                modal : true,
                resizable : false,
                title: '<spring:message code='duty.organ.mod'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function() {
                            var dutyid = $("#duty-organ-mod").attr("dutyid");
                            var ztreeObj = $.fn.zTree.getZTreeObj("duty-organ-mod");
                            var nodes = ztreeObj.getCheckedNodes(true);
                            var organIds =[];
                            $(nodes).each(function (index,item) {
                                organIds.push(item.id);
                            });


                            $.ajax({
                                type: 'post',
                                async: false,
                                dataType : 'json',
                                url: '${pageContext.request.contextPath}/post/setOrganByDutyId',
                                data: [{name:"dutyid",value:dutyid},
                                    {name:"organUuidArray",value:organIds}],
                                success: function (data) {
                                    message(data.msg);
                                    $("#duty-organ-mod-dialog-id").dialog('close');

                                },

                                error: function(msg){
                                    message(' <spring:message code="common.error"></spring:message>');
                                }
                            });
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });
            $("#flexigrid-id").flexReload();

            $("#duty-role-mod-dialog-id").dialog({
                autoOpen : false,
                width : 400,
                modal : true,
                resizable : false,
                title: '<spring:message code='duty.role.mod'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function() {
                            var roleIds = [];
                            var ztreeObj = $.fn.zTree.getZTreeObj("duty-role-mod");
                            var nodes = ztreeObj.getCheckedNodes(true);
                            $(nodes).each(function (index,item) {
                                roleIds.push(item.id);
                            });

                            var dutyid = $("#duty-role-mod").attr("dutyid");
                            $.ajax({
                                type: 'post',
                                async: false,
                                dataType : 'json',
                                url: '${pageContext.request.contextPath}/post/setRoleByDutyId',
                                data: [{name:"dutyid",value:dutyid},
                                    {name:"roleUuidArray",value:roleIds}],
                                success: function (data) {
                                    message(data.msg);
                                    $("#duty-role-mod-dialog-id").dialog('close');
                                },
                                error: function(msg){
                                    message(' <spring:message code="common.error"></spring:message>');
                                }
                            });


                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });
            $("#flexigrid-id").flexReload();


            $("#add-duty-dialog-id").dialog({
                autoOpen : false,
                width : 400,
                modal : true,
                resizable : false,
                title: '<spring:message code='duty.add'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function() {
                            var flag = true;
                            var dom = $("#add-duty> div > .form-control");
                            dom.each(function(index,item) {
                                flag &= $(item).verification();
                                console.info(flag);
                            });
                            if(!flag){
                                return;
                            }
                            $("#add-duty-name ").submit();
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]

            });
            $("#add-duty").ajaxForm({
                dataType: "json",
                success : function(data) {

                    message(data.msg);

                    $("#add-duty-dialog-id").dialog('close');

                },
                error : function() {
                    message(' <spring:message code="common.error"></spring:message>');
                },
                complete : function(response, status) {

                }

            });

            $("ul#tab-ul-menu-id > li").click(function(event) {
                $("ul#tab-ul-menu-id .active").removeClass("active");
                $(this).addClass("active");

                var tab = $(this).attr("tab");
                $("#" + tab).addClass("in");
                $("#" + tab).addClass("active")
            });
            $("ul#tab-ul-menu-id > li:first").click();

            $("#common-operation").flexigrid({
                width : _gridWidth-660,
                height:440,
                modal : true,
                resizable : false,
                url:"${pageContext.request.contextPath}/post/queryDutyListPage2",
                colModel : [
                    {display : 'dutyid',name : 'dutyid',width : 50,sortable : false,align : 'center',hide : 'true'},
                    {display : "<spring:message code='duty.name'/>",name : 'name',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='user.name'/>",name : 'userName',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='role.remarks'/>",name : 'remarks',width : _columnWidth+200, sortable : true,align : 'center'},
                ],
                resizable : false, //resizable table是否可伸缩
                useRp : true,
                usepager : true, //是否分页
                autoload : false, //自动加载，即第一次发起ajax请求
                hideOnSubmit : true, //是否在回调时显示遮盖
                showcheckbox : true, //是否显示多选框
                //rowhandler : rowDbclick, //是否启用行的扩展事情功能,在生成行时绑定事件，如双击，右键等
                rowbinddata : true
            });

            $("#duty-common-dialog").dialog({
                autoOpen : false,
                width : _gridWidth-620,
                height:600,
                modal : true,
                resizable : false,
                title: '<spring:message code='duty.common'/>',


            });

        });//ready
        function editTable(v) {

            $("#common-operation").flexOptions({
                extParam:[
                    {name:"dutyId",value:v}
                ]
            }).flexReload();
            $("#duty-common-dialog").dialog('open');
        }




        var leftMenuSetting = {
            check: {
                enable: false
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback : {
                onClick : onClickNode
            }
        };

        function leftMenuLoad() {
            $.ajax({
                    type: 'post',
                    async: false,
                    dataType : 'json',
                    url: '${pageContext.request.contextPath}/post/getDutyTree',
                    data: [],
                    success: function (data) {
                        $.fn.zTree.init($("#duty-left-tree-ul-id"), leftMenuSetting, data);
                    },
                    error: function(msg){
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );
        }          //ztree

        function queryBtn() {
            var searchDutyName = $("#searchDutyName").val();
            $("#flexigrid-id").flexOptions({
                extParam:[
                    {name:"searchDutyName",value:searchDutyName}
                ]
            }).flexReload();

        }    //查询

        function onClickNode(event, treeId, treeNode){
            $("#flexigrid-id").flexOptions({
                extParam:[
                    {name:"organUuid",value:treeNode.id}
                ]
            }).flexReload();

        }

        var modifyDutySetting = {
            check: {
                enable: true
            },
            data: {
                simpleData: {
                    enable: true
                }
            }
        };

        function modifyorgan() {
            var ids = $("#flexigrid-id").flexiData();
            if(ids.length !== 1){
                message('<spring:message code="common.select.one"></spring:message>');
                return;
            }

            $("#duty-organ-mod").attr("dutyid",ids);

            $.ajax({
                    type: 'post',
                    async: false,
                    dataType : 'json',
                    url: '${pageContext.request.contextPath}/post/getOrganByDutyId',
                    data: [{name:"dutyid",value:ids}],
                    success: function (data) {
                        $.fn.zTree.init($("#duty-organ-mod"), modifyDutySetting, data);
                        $("#duty-organ-mod-dialog-id").dialog('open');
                    },
                    error: function(msg){
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );
        }

        function modifyrole() {
            var ids = $("#flexigrid-id").flexiData();
            if(ids.length !== 1){
                message('<spring:message code="common.select.one"></spring:message>');
                return;
            }

            $("#duty-role-mod").attr("dutyid",ids);

            $.ajax({
                    type: 'post',
                    async: false,
                    dataType : 'json',
                    url: '${pageContext.request.contextPath}/post/getRoleByDutyId',
                    data: [{name:"dutyid",value:ids}],
                    success: function (data) {
                        $.fn.zTree.init($("#duty-role-mod"), modifyDutySetting, data);
                        $("#duty-role-mod-dialog-id").dialog('open');
                    },
                    error: function(msg){
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );


        }

        function addDuty() {
            $("#add-duty").resetForm();
            $("#add-duty-dialog-id").dialog('open');
        }

        function deleteDutyInfo() {
            var ids = $("#flexigrid-id").flexiData();
            if(ids.length !== 1){
                message('<spring:message code="common.select.one"></spring:message>');
                return;
            }

            $("#duty-role-mod").attr("dutyid",ids);
            $.ajax({
                type: 'post',
                async: false,
                dataType : 'json',
                url: '${pageContext.request.contextPath}/post/deleteDutyInfoById',
                data: [{name:"dutyid",value:ids}],
                success: function (data) {
                    message(data.msg);

                },
                error: function(msg){
                    message(' <spring:message code="common.error"></spring:message>');
                }
            });
            $("#flexigrid-id").flexReload();

        }




    </script>
</head>
<body style="display: flex;flex-direction: row">

<nav>
    <spring:message code="duty.tip"></spring:message>
    <ul id="duty-left-tree-ul-id" class="ztree" ></ul>

</nav>
<article style="flex-grow: 1">
    <nav>



        <security:authorize access="hasRole('ROLE_gdmt_data_post')">
            <label> <spring:message code='duty.name'/>:</label> <input type="text" id="searchDutyName"/>
            <a class="btn" href="#" onclick="queryBtn();">
                <i class="glyphicon glyphicon-zoom-in"></i>
                <span><spring:message code='common.query'/></span>
            </a>
        </security:authorize>


        <a href="#" class="btn" onclick="deleteDutyInfo();">
            <i class="fa fa-trash-o"></i>
            <span> <spring:message code="common.delete"></spring:message> </span>
        </a>

        <a href="#" class="btn" onclick="addDuty();">
            <i class="fa fa-plus"></i>
            <span> <spring:message code="common.add"></spring:message> </span>
        </a>
        <a href="#" class="btn"  onclick="modifyorgan();">
            <i class="glyphicon glyphicon-modal-window"></i>
            <span> <spring:message code="organ.modify"></spring:message> </span>
        </a>
        <a href="#" class="btn"  onclick="modifyrole();">
            <i class="glyphicon glyphicon-modal-window"></i>
            <span> <spring:message code="role.modify"></spring:message> </span>
        </a>
        <table id="flexigrid-id" class="table-edit-data"></table>




        <div id="duty-common-dialog" ><table id="common-operation" class="table-edit-data"></table></div>








    </nav>

</article>
<div >

</div>


<div id="duty-organ-mod-dialog-id" style="display: none">
    <ul id="duty-organ-mod" class="ztree"></ul>
</div>

<div id="duty-role-mod-dialog-id" style="display: none">
    <ul id="duty-role-mod" class="ztree"></ul>
</div>

<div id="add-duty-dialog-id" style="display: none">
        <form id="add-duty" action="${pageContext.request.contextPath}/post/addDutyInfoById"
              method="post">
            <div class="form-group">
                <span for="add-duty-span"><spring:message code="duty.name"></spring:message> </span>
                <input name="name" id="add-duty-name" class="form-control" regex="^.{1,50}$" tip="input 1 to 50 char">
                <p></p>
                <p><spring:message code="duty.organ.add"></spring:message></p>

                <gdmt:dataselect name="organUuid" id="search-organ-id" ref="organService"></gdmt:dataselect>
                <p></p>
                <p><spring:message code="duty.role.add"></spring:message></p>
                <gdmt:dataselect name="roleUuid" id="search-role-id" ref="roleService"></gdmt:dataselect>
                <p></p>
            </div>
            <div class="form-group">

            </div>

        </form>
</div>
</body>
</html>
