<%@include file="../common/common.jsp"%>
<html>
<head>
    <title>
        <spring:message code="user.title.tip"></spring:message>
    </title>
    <script type="text/javascript">
        var _gridWidth;
        var _gridHeight;
        //页面自适应
        function resizePageSize(){
            _gridWidth = $(document).width()-12;/*  -189 是去掉左侧 菜单的宽度，   -12 是防止浏览器缩小页面 出现滚动条 恢复页面时  折行的问题 */
            _gridHeight = $(document).height()-32-85; /* -32 顶部主菜单高度，   -90 查询条件高度*/
        }

        $(function () {
            resizePageSize();
            var _columnWidth = _gridWidth/5-12;
            $("#flexigrid-id").flexigrid({
                width : _gridWidth,
                height : _gridHeight,
                url : "${pageContext.request.contextPath}/user/getUserInfolistByPage",
                dataType : 'json',
                colModel : [
                    {display : 'userUuid',name : 'userUuid',width : 150,sortable : false,align : 'center',hide : 'true'},
                    {display : "<spring:message code='user.name'/>",name : 'userName',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='user.nick'/>",name : 'niceName',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='user.mobile'/>",name : 'mobile',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='user.email'/>",name : 'email',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='user.status'/>",name : 'status',width : _columnWidth, align : 'center'}
                ],
                resizable : false, //resizable table是否可伸缩
                useRp : true,
                usepager : true, //是否分页
                autoload : false, //自动加载，即第一次发起ajax请求
                hideOnSubmit : true, //是否在回调时显示遮盖
                showcheckbox : true, //是否显示多选框
                //rowhandler : rowDbclick, //是否启用行的扩展事情功能,在生成行时绑定事件，如双击，右键等
                rowbinddata : true
            });

            $("#flexigrid-id").flexReload();

            $("#allocate-role-user-dialog-id").dialog({
                autoOpen : false,
                width : 400,
                modal : true,
                resizable : false,
                title: '<spring:message code='user.role.allocate'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function() {
                            var roleIds = [];
                            var ztreeObj = $.fn.zTree.getZTreeObj("allocate-role-user-ztree-id");
                            var nodes = ztreeObj.getCheckedNodes(true);
                            $(nodes).each(function (index,item) {
                                roleIds.push(item.id);
                            });

                            var userUUid = $("#allocate-role-user-ztree-id").attr("userUUid");
                            $.ajax({
                                type: 'post',
                                async: false,
                                dataType : 'json',
                                url: '${pageContext.request.contextPath}/user/setRoleUuidByUserUuid',
                                data: [{name:"userUuid",value:userUUid},
                                    {name:"roleUuidArray",value:roleIds}],
                                success: function (data) {
                                    message(data.msg);
                                    $("#allocate-role-user-dialog-id").dialog('close');
                                },
                                error: function(msg){
                                    message(' <spring:message code="common.error"></spring:message>');
                                }
                            });


                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });

            //传入增加用户的log对象
            $("#add-user-info-dialog-id").dialog({
                autoOpen : false,
                width : 400,
                modal : true,
                resizable : false,
                title: '<spring:message code='user.add'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function() {
                            var flag = true;
                            var dom = $("#add-user-info-form-id > div > .form-control");
                            dom.each(function(index,item) {
                                flag &= $(item).verification();
                                console.info(flag);
                            });
                            var passwordId = $("#add-user-password").val();
                            var confirmPasswordId = $("#add-user-confirmPassword").val();
                            if(passwordId !== confirmPasswordId){
                                message('<spring:message code="confirm.password.error"></spring:message>');
                                return;
                            }
                            if(!flag){
                                return;
                            }
                            $("#add-user-info-form-id").submit();
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });

            $("#add-user-info-form-id").ajaxForm({
                dataType: "json",
                success : function(data) {
                    $("#flexigrid-id").flexReload();
                    message(data.msg);
                    $("#add-user-info-dialog-id").dialog('close');
                },
                error : function() {
                    message(' <spring:message code="common.error"></spring:message>');
                },
                complete : function(response, status) {

                }

            });
            //传入修改用户的log对象
            $("#modify-user-info-dialog-id").dialog({
                autoOpen : false,
                width : 400,
                modal : true,
                resizable : false,
                title: '<spring:message code='user.modify'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function() {
                            var flag = true;
                            var dom = $("#modify-user-info-form-id > div > .form-control");
                            dom.each(function(index,item) {
                                flag &= $(item).verification();
                                console.info(flag);
                            });
                            if(!flag){
                                return;
                            }
                            $("#modify-user-info-form-id").submit();
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });

            $("#modify-user-info-form-id").ajaxForm({
                dataType: "json",
                success : function(data) {
                    $("#flexigrid-id").flexReload();
                    message(data.msg);
                    $("#modify-user-info-dialog-id").dialog('close');
                },
                error : function() {
                    message(' <spring:message code="common.error"></spring:message>');
                },
                complete : function(response, status) {

                }

            });
            //传入分配岗位的log对象
            $("#allocate-duty-user-dialog-id").dialog({
                autoOpen : false,
                width : 400,
                modal : true,
                resizable : false,
                title: '<spring:message code='user.duty.allocate'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function() {
                            var dutyIds = [];
                            var ztreeObj = $.fn.zTree.getZTreeObj("allocate-duty-user-ztree-id");
                            var nodes = ztreeObj.getCheckedNodes(true);
                            $(nodes).each(function (index,item) {
                                dutyIds.push(item.id);
                            });

                            var userUUid = $("#allocate-duty-user-ztree-id").attr("userUUid");
                            $.ajax({
                                type: 'post',
                                async: false,
                                dataType : 'json',
                                url: '${pageContext.request.contextPath}/user/setDutyIdByUserUuid',
                                data: [{name:"userUuid",value:userUUid},
                                    {name:"dutyIdArray",value:dutyIds}],
                                success: function (data) {
                                    message(data.msg);
                                    $("#allocate-duty-user-dialog-id").dialog('close');
                                },
                                error: function(msg){
                                    message(' <spring:message code="common.error"></spring:message>');
                                }
                            });


                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });
            //传入分配部门的log对象
            $("#allocate-dep-user-dialog-id").dialog({
                autoOpen : false,
                width : 400,
                modal : true,
                resizable : false,
                title: '<spring:message code='user.dep.allocate'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function() {
                            // var depIds = [];
                            var depId = 0;
                            var ztreeObj = $.fn.zTree.getZTreeObj("allocate-dep-user-ztree-id");
                            var nodes = ztreeObj.getCheckedNodes(true);
                            $(nodes).each(function (index,item) {
                                depId = item.id;
                            });

                            var userUUid = $("#allocate-dep-user-ztree-id").attr("userUUid");
                            $.ajax({
                                type: 'post',
                                async: false,
                                dataType : 'json',
                                url: '${pageContext.request.contextPath}/user/setDepUuIdByUserUuid',
                                data: [{name:"userUuid",value:userUUid},
                                    // {name:"depUuId",value:depIds}],
                                    {name:"depUuId",value:depId}],
                                success: function (data) {
                                    message(data.msg);
                                    $("#allocate-dep-user-dialog-id").dialog('close');
                                },
                                error: function(msg){
                                    message(' <spring:message code="common.error"></spring:message>');
                                }
                            });


                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });
        });//ready

        function queryBtn() {
            var searchUserName = $("#searchUserName").val();
            var searchNiceName = $("#searchNiceName").val();
            var searchPhone = $("#searchPhone").val();
            var searchEmail = $("#searchEmail").val();
            $("#flexigrid-id").flexOptions({
                extParam:[
                    {name:"searchUserName",value:searchUserName},
                    {name:"searchNiceName",value:searchNiceName},
                    {name:"searchPhone",value:searchPhone},
                    {name:"searchEmail",value:searchEmail}
                ]
            }).flexReload();

        }
        var allocateSetting = {
            check: {
                enable: true
            },
            data: {
                simpleData: {
                    enable: true
                }
            }
        };

        function allocateRoleBtn() {
            var ids = $("#flexigrid-id").flexiData();
            if(ids.length !== 1){
                message('<spring:message code="common.select.one"></spring:message>');
                return;
            }

            $("#allocate-role-user-ztree-id").attr("userUUid",ids);

            $.ajax({
                    type: 'post',
                    async: false,
                    dataType : 'json',
                    url: '${pageContext.request.contextPath}/user/getRoleByUserId',
                    data: [{name:"userUuid",value:ids}],
                    success: function (data) {
                        $.fn.zTree.init($("#allocate-role-user-ztree-id"), allocateSetting, data);
                        $("#allocate-role-user-dialog-id").dialog('open');
                    },
                    error: function(msg){
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );
        }

        function allocateDutyBtn() {
            var ids = $("#flexigrid-id").flexiData();
            if(ids.length !== 1){
                message('<spring:message code="common.select.one"></spring:message>');
                return;
            }

            $("#allocate-duty-user-ztree-id").attr("userUUid",ids);

            $.ajax({
                    type: 'post',
                    async: false,
                    dataType : 'json',
                    url: '${pageContext.request.contextPath}/user/getDutyByUserId',
                    data: [{name:"userUuid",value:ids}],
                    success: function (data) {
                        $.fn.zTree.init($("#allocate-duty-user-ztree-id"), allocateSetting, data);
                        $("#allocate-duty-user-dialog-id").dialog('open');
                    },
                    error: function(msg){
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );

        }

        function addUserInfo() {
            $("#add-user-info-form-id").resetForm();
            $("#add-user-info-dialog-id").dialog('open');
        }

        function deleteBtn() {
            var ids = $("#flexigrid-id").flexiData();
            if(ids.length !== 1){
                message('<spring:message code="common.select.one"></spring:message>');
                return;
            }
            $.ajax({
                    type: 'post',
                    async: false,
                    dataType : 'json',
                    url: '${pageContext.request.contextPath}/user/deleteUserInfoById',
                    data: [{name:"ids",value:ids}],
                    success: function (data) {
                        $("#flexigrid-id").flexReload();
                        message(data.msg);
                        $("#delete-user-info-form-id").resetForm();
                    },
                    error: function(msg){
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );

        }
        //修改用户函数
        function modifyBtn() {
            $("#modify-user-info-form-id").resetForm();
            var ids = $("#flexigrid-id").flexiData();
            if(ids.length !== 1){
                message('<spring:message code="common.select.one"></spring:message>');
                return;
            }
            $.ajax({
                    type: 'post',
                    async: false,
                    dataType : 'json',
                    url: '${pageContext.request.contextPath}/user/queryUserListById',
                    data: [{name:"userUuid",value:ids[0]}],
                    success: function (data) {
                        var dom = $("#modify-user-info-form-id .form-control");
                        dom.each(function(index,item) {
                            var nameAttr = $(item).attr("name");
                            // console.info(data[nameAttr]);
                            $(item).val(data[nameAttr]);
                        });
                        $("#modify-user-info-dialog-id").dialog('open');
                    },
                    error: function(msg){
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );
        }

        function unlockBtn() {
            var ids = $("#flexigrid-id").flexiData();
            if(ids.length !== 1){
                message('<spring:message code="common.select.one"></spring:message>');
                return;
            }
            $.ajax({
                url: '/user/unlock?id=' + ids,
                method: 'get',
                dataType: 'json',
                success: function (resp) {
                    message(resp.msg)
                    queryBtn()
                }
            })
        }

        // 为用户分配部门
        function allocateDepBtn() {
            var ids = $("#flexigrid-id").flexiData();
            if(ids.length !== 1){
                message('<spring:message code="common.select.one"></spring:message>');
                return;
            }

            $("#allocate-dep-user-ztree-id").attr("userUUid",ids);

            $.ajax({
                    type: 'post',
                    async: false,
                    dataType : 'json',
                    url: '${pageContext.request.contextPath}/user/getDepByUserUuid',
                    data: [{name:"userUuid",value:ids}],
                    success: function (data) {
                        $.fn.zTree.init($("#allocate-dep-user-ztree-id"), allocateRadioSetting, data);
                        $("#allocate-dep-user-dialog-id").dialog('open');
                    },
                    error: function(msg){
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );

        }
    </script>
</head>
<body>
<spring:message code="user.title.tip"></spring:message>

<div>
    <security:authorize access="hasRole('ROLE_cldk_data_user_query')">
        <label> <spring:message code='user.name'/>:</label> <input type="text" id="searchUserName"/>
        <label> <spring:message code='user.nick'/>:</label> <input type="text" id="searchNiceName"/>
        <label> <spring:message code='user.mobile'/>:</label> <input type="text" id="searchPhone"/>
        <label> <spring:message code='user.email'/>:</label> <input type="text" id="searchEmail"/>
        <a class="btn" href="#" onclick="queryBtn();">
            <i class="glyphicon glyphicon-zoom-in"></i>
            <span><spring:message code='common.query'/></span>
        </a>
    </security:authorize>
    <security:authorize access="hasRole('ROLE_cldk_data_user_role')">
        <a class="btn" href="#" onclick="allocateRoleBtn();">
            <i class="glyphicon glyphicon-wrench"></i>
            <span><spring:message code='user.role.allocate'/></span>
        </a>
    </security:authorize>
    <security:authorize access="hasRole('ROLE_cldk_data_user_role')">
        <a class="btn" href="#" onclick="unlockBtn();">
            <i class="glyphicon glyphicon-wrench"></i>
            <span><spring:message code='user.unlock'/></span>
        </a>
    </security:authorize>
    <security:authorize access="hasRole('ROLE_cldk_data_user_duty')">
        <a class="btn" href="#" onclick="allocateDutyBtn();">
            <i class="glyphicon glyphicon-wrench"></i>
            <span><spring:message code='user.duty.allocate'/></span>
        </a>
    </security:authorize>
    <security:authorize access="hasRole('ROLE_cldk_data_user_dep')">
        <a class="btn" href="#" onclick="allocateDepBtn();">
            <i class="glyphicon glyphicon-wrench"></i>
            <span><spring:message code='user.dep.allocate'/></span>
        </a>
    </security:authorize>
    <security:authorize access="hasRole('ROLE_cldk_data_user_add')">
        <%--增加用户图标--%>
        <a href="#" class="btn" onclick="addUserInfo();">
            <i class="fa fa-plus"></i>
            <span> <spring:message code="common.add.user"></spring:message> </span>
        </a>
    </security:authorize>
    <security:authorize access="hasRole('ROLE_cldk_data_user_del')">
        <%--删除用户图标--%>
        <a href="#" class="btn" onclick="deleteBtn();">
            <i class="fa fa-trash-o"></i>
            <span> <spring:message code="common.delete.user"></spring:message> </span>
        </a>
    </security:authorize>
    <security:authorize access="hasRole('ROLE_cldk_data_user_mod')">
        <%--修改用户图标--%>
        <a href="#" class="btn" onclick="modifyBtn();">
            <i class="glyphicon glyphicon-modal-window"></i>
            <span> <spring:message code="common.modify.user"></spring:message> </span>
        </a>
    </security:authorize>
</div>

<table id="flexigrid-id"></table>

<%--导入列表插件--%>
<table id="flexigrid-id"></table>
<%--分配角色对话框--%>
<div id="allocate-role-user-dialog-id" style="display: none">
    <ul id="allocate-role-user-ztree-id" class="ztree"></ul>
</div>
<%--添加用户的form组件--%>
<div id="add-user-info-dialog-id" style="display: none">
    <form id="add-user-info-form-id" action="${pageContext.request.contextPath}/user/addUserInfoById"
          method="post">
        <div class="form-group">
            <span for="add-user-name"><spring:message code="user.name"></spring:message> </span>
            <input name="userName" id="add-user-name" class="form-control" regex="^\w+$" tip="please input your username">
        </div>
        <div class="form-group">
            <span for="add-user-password"><spring:message code="user.password"></spring:message> </span>
            <input name="password" id="add-user-password" class="form-control" regex="^.+$" tip="please input your password">
        </div>
        <div class="form-group">
            <span for="add-user-confirmPassword"><spring:message code="user.confirmPassword"></spring:message> </span>
            <input name="confirmPassword" id="add-user-confirmPassword" class="form-control" regex="^.+$" tip="please confirm your password">
        </div>
        <div class="form-group">
            <span for="add-user-niceName"><spring:message code="user.nick"></spring:message> </span>
            <input name="niceName" id="add-user-niceName" class="form-control" regex="^.+$" tip="please input your nicename">
        </div>
        <div class="form-group">
            <span for="add-user-mobile"><spring:message code="user.mobile"></spring:message> </span>
            <input name="mobile" id="add-user-mobile" class="form-control" regex="^[0-9]{1,11}$" tip="input 1 to 11 number">
        </div>
        <div class="form-group">
            <span for="add-user-email"><spring:message code="user.email"></spring:message> </span>
            <input name="email" id="add-user-email" class="form-control" regex="^.{1,50}$" tip="input 1 to 50 char">
        </div>
    </form>
</div>
<%--删除用户的form组件--%>
<div id="delete-user-info-dialog-id" style="display: none">
    <form id="delete-user-info-form-id" action="${pageContext.request.contextPath}/user/deleteUserInfoById"
          method="post">
        <div class="form-group">
            <span for="delete-user-name-id"><spring:message code="user.delete"></spring:message> </span>
        </div>
    </form>
</div>
<%--修改用户的form组件--%>
<div id="modify-user-info-dialog-id" style="display: none">
    <form id="modify-user-info-form-id" action="${pageContext.request.contextPath}/user/modifyUserListById"
          method="post">

        <input class="form-control" name="userUuid" id="modify-user-uuid" type="hidden"/>

        <div class="form-group">
            <span for="modify-user-name"><spring:message code="user.name.modify"></spring:message> </span>
            <input name="userName" id="modify-user-name" class="form-control" regex="^\w+$" tip="please input your username">
        </div>
        <div class="form-group">
            <span for="modify-user-password"><spring:message code="user.password.modify"></spring:message> </span>
            <input name="password" id="modify-user-password" class="form-control" readonly >
        </div>
        <div class="form-group">
            <span for="modify-user-niceName"><spring:message code="user.niceName.modify"></spring:message> </span>
            <input name="niceName" id="modify-user-niceName" class="form-control" regex="^.+$" tip="please input your nicename">
        </div>
        <div class="form-group">
            <span for="modify-user-mobile"><spring:message code="user.mobile.modify"></spring:message> </span>
            <input name="mobile" id="modify-user-mobile" class="form-control" regex="^[0-9]{1,11}$" tip="input 1 to 11 number">
        </div>
        <div class="form-group">
            <span for="modify-user-email"><spring:message code="user.email.modify"></spring:message> </span>
            <input name="email" id="modify-user-email" class="form-control" regex="^.{1,50}$" tip="input 1 to 50 char">
        </div>
    </form>
</div>
<div id="allocate-duty-user-dialog-id" style="display: none">
    <ul id="allocate-duty-user-ztree-id" class="ztree"></ul>
</div>

<div id="allocate-dep-user-dialog-id" style="display: none">
    <ul id="allocate-dep-user-ztree-id" class="ztree"></ul>
</div>

</body>
</html>
