<%@include file="../common/common.jsp"%>
<html>
<head>
    <script src="${pageContext.request.contextPath}/amcharts/amcharts.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/amcharts/serial.js" type="text/javascript"></script>
    <title>
        <spring:message code="electric"></spring:message>
    </title>
    <script type="text/javascript">
        var _gridWidth;
        var _gridHeight;
        //页面自适应
        function resizePageSize(){
            _gridWidth = $(document).width()-20;/*  -189 是去掉左侧 菜单的宽度，   -12 是防止浏览器缩小页面 出现滚动条 恢复页面时  折行的问题 */
            _gridHeight = $(document).height()-10; /* -32 顶部主菜单高度，   -90 查询条件高度*/
        }

        $(function () {
            resizePageSize();
            var _columnWidth = _gridWidth/3-14;
            $("#flexigrid-id").flexigrid({
                width : _gridWidth,
                height : _gridHeight,
                url : "${pageContext.request.contextPath}/consult/getElectricInfolistByPage",
                dataType : 'json',
                colModel : [
                    {display : 'elcId',name : 'elcId',width : 150,sortable : false,align : 'center',hide : 'true'},
                    {display : "<spring:message code='elcDate'/>",name : 'elcDate',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='elcNation'/>",name : 'elcNation',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='elcNationYoy'/>",name : 'elcNationYoy',width : _columnWidth, sortable : true,align : 'center'}
                ],
                resizable : false, //resizable table是否可伸缩
                useRp : true,
                usepager : true, //是否分页
                autoload : false, //自动加载，即第一次发起ajax请求
                hideOnSubmit : true, //是否在回调时显示遮盖
                showcheckbox : true, //是否显示多选框
                //rowhandler : rowDbclick, //是否启用行的扩展事情功能,在生成行时绑定事件，如双击，右键等
                rowbinddata : true
            });

            $("#flexigrid-id").flexReload();

            $("ul#tab-ul-menu-id > li").click(function(event) {
                $("ul#tab-ul-menu-id .active").removeClass("active");
                $(this).addClass("active");

                $("section.tab-content .in").removeClass("in");
                $("section.tab-content .active").removeClass("active");
                var tab = $(this).attr("tab");
                $("#" + tab).addClass("in");
                $("#" + tab).addClass("active")
            });
            $("ul#tab-ul-menu-id > li:first").click();
            queryElectric();

        })//ready

        function queryElectric() {
            $.ajax({
                    type: 'post',
                    async: false,
                    dataType : 'json',
                    url: '${pageContext.request.contextPath}/consult/getElectric',
                    data: [],
                    success: function (data) {
                        loadChart(data);
                    },
                    error: function(msg){
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );

        }

        function loadChart(chartData) {
            var chart;
            // SERIAL CHART
            chart = new AmCharts.AmSerialChart();
            chart.pathToImages = "../amcharts/images/";
            chart.dataProvider = chartData;
            chart.categoryField = "elcDate";
            chart.dataDateFormat = "YYYY-MM-DD";
            chart.startDuration = 1;

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
            categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to DD
            categoryAxis.gridAlpha = 0.15;
            categoryAxis.axisColor = "#DADADA";
            categoryAxis.title="<spring:message code='elcDate'/>"

            // first value axis (on the left)
            var valueAxis1 = new AmCharts.ValueAxis();
            valueAxis1.axisColor = "#DADADA";
            valueAxis1.axisThickness = 2;
            valueAxis1.gridAlpha = 0;
            valueAxis1.inside=false;
            chart.addValueAxis(valueAxis1);
            
            // second value axis (on the right)
            var valueAxis2 = new AmCharts.ValueAxis();
            valueAxis2.position = "right";
            valueAxis2.gridAlpha = 0;
            valueAxis2.axisThickness = 2;
            valueAxis2.axisColor = "#DADADA";
            valueAxis2.dashLength = 1;
            chart.addValueAxis(valueAxis2);

            // GRAPH1
            var graph1 = new AmCharts.AmGraph();
            graph1.title = "<spring:message code='elcNation'/>";
            graph1.valueField = "elcNation";
            graph1.balloonText = "<spring:message code='elcNation'/> : [[value]]";
            graph1.type = "column";
            graph1.hidden = false;
            graph1.lineAlpha = 0;
            graph1.fillAlphas = 1;
            graph1.lineColor = "#b5030d";
            graph1.valueAxis=valueAxis1;
            chart.addGraph(graph1);
            
            // GRAPH2
            var graph2 = new AmCharts.AmGraph();
            graph2.type = "smoothedLine";
            graph2.bullet = "round";
            graph2.hideBulletsCount = 30;
            graph2.bulletBorderThickness = 1;
            graph2.hidden = false;
            graph2.title = "<spring:message code='elcNationYoy'/>";
            graph2.valueField = "elcNationYoy";
            graph2.balloonText = "<spring:message code='elcNationYoy'/> : [[value]]";
            graph2.lineThickness = 2;
            graph2.lineColor = "#00BBCC";
            graph2.valueAxis=valueAxis2;
            chart.addGraph(graph2);

            // CURSOR
            var chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorPosition = "mouse";
            chart.addChartCursor(chartCursor);

            // SCROLLBAR
            var chartScrollbar = new AmCharts.ChartScrollbar();
            chartCursor.cursorAlpha = 0;
            chart.addChartScrollbar(chartScrollbar);

            // LEGEND
            var legend = new AmCharts.AmLegend();
            legend.bulletType = "round";
            legend.useGraphSettings = true;
            chart.addLegend(legend);

            // WRITE
            chart.write("chartdiv");
        }

        function queryBtn() {
            var startDate = $("#startDate").val();
            var endDate = $("#endDate").val();
            $("#flexigrid-id").flexOptions({
                extParam:[
                    {name:"startDate",value:startDate},
                    {name:"endDate",value:endDate}
                ]
            }).flexReload();

        }
    
    </script>

</head>
<body>
<ul id="tab-ul-menu-id" class="nav nav-tabs">
    <security:authorize access="hasRole('ROLE_gdmt_data_electric')">
        <li tab="electric-chart-id">
            <a href="#">
                <i class="fa fa-area-chart"></i>
                <spring:message code="chart"></spring:message>
            </a>
        </li>
    </security:authorize>
    <security:authorize access="hasRole('ROLE_gdmt_data_electric')">
        <li tab="electric-table-id">
            <a href="#">
                <i class="fa fa-calendar"></i>
                <spring:message code="table"></spring:message>
            </a>
        </li>
    </security:authorize>
</ul>
<section class="tab-content">
    <article id="electric-chart-id" class="tab-pane fade in active">
        <div id="chartdiv" style="width:100%; height:400px;"></div>
    </article>
    <article id="electric-table-id" class="tab-pane fade">
        <div>
            <label> <spring:message code='startdate'/>:</label> <input type="date" id="startDate"/>
            <label> <spring:message code='enddate'/>:</label> <input type="date" id="endDate"/>
            <a class="btn" href="#" onclick="queryBtn();">
                <i class="glyphicon glyphicon-zoom-in"></i>
                <span><spring:message code='common.query'/></span>
            </a>
        </div>
        <table id="flexigrid-id"></table>
    </article>
</section>
</body>
</html>
