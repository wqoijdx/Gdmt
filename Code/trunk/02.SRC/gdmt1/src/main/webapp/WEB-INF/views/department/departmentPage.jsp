<%@include file="../common/common.jsp" %>
<html>
<head>
    <title>
        <spring:message code="depart.info.tip"></spring:message>
    </title>
    <style type="text/css">
        .form-tow-column {
            display: grid;
            grid-template-columns: auto auto;

        }

        .form-group {
            margin: 0 10px;
        }

        fieldset > legend {
            font-size: 10px;
            border-bottom: none;
            margin-bottom: 0;
        }

        fieldset {
            display: block;
            margin-inline-start: 2px;
            margin-inline-end: 2px;
            padding-block-start: 0.35em;
            padding-inline-start: 0.75em;
            padding-inline-end: 0.75em;
            padding-block-end: 0.625em;
            min-inline-size: min-content;
            border: 1px solid #e5e5e5;;
            border-image: initial;
        }

        .gdmt-tb-th > th {
            font-weight: normal;
            cursor: default;
            white-space: nowrap;
            overflow: hidden;
            text-align: center;
            vertical-align: middle !important;
            padding: 0px;
            height: 32px;
            background-color: #b5b5b5;
            box-sizing: border-box;
            color: #FFF;
            font-size: 12px;
        }

        .table-edit-data td {
            text-align: center;
            vertical-align: middle !important;
            padding: 0px;
            height: 32px;
            font-size: 12px;
            background-color: #f5f5f5;
        }

        .table-erow td {
            background-color: #ededed;
        }

        .table-edit-data .read {
            display: inline-block;
            margin-right: 10px;
        }

        .table-edit-data .edit {
            display: none;
        }
    </style>
    <script type="text/javascript">
        //页面自适应
        function resizePageSize() {
            _gridWidth = $(document).width() - 12;/*  -189 是去掉左侧 菜单的宽度，   -12 是防止浏览器缩小页面 出现滚动条 恢复页面时  折行的问题 */
            _gridHeight = $(document).height() - 32 - 85; /* -32 顶部主菜单高度，   -90 查询条件高度*/
        }

        $(function () {
            leftDepartmentLoad()
            resizePageSize();
            leftDepartmentLoad();
            $("#modify-depart-info-form-id").ajaxForm({
                dataType: "json",
                success: function (data) {
                    message(data.msg);
                    leftDepartmentLoad();
                },
                error: function () {
                    message(' <spring:message code="common.error"></spring:message>');
                },
                complete: function (response, status) {

                }
            });
            $("#add-depart-info-dialog-id").dialog({
                autoOpen: false,
                width: 400,
                modal: true,
                resizable: false,
                title: '<spring:message code='depart.add'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function () {
                            var flag = true;
                            var dom = $("#add-depart-info-form-id > div > .form-control");
                            dom.each(function (index, item) {
                                flag &= $(item).verification();
                                console.info(flag);
                            });
                            if (!flag) {
                                return;
                            }
                            $("#add-depart-info-form-id").submit();
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ]
            });

            $("#add-depart-info-form-id").ajaxForm({
                dataType: "json",
                success: function (data) {
                    message(data.msg);
                    leftDepartmentLoad();
                    $("#add-depart-info-dialog-id").dialog('close');
                },
                error: function () {
                    message(' <spring:message code="common.error"></spring:message>');
                },
                complete: function (response, status) {

                }

            });

            $("#modify-organ-dialog-id").dialog({
                autoOpen: false,
                width: 400,
                modal: true,
                resizable: false,
                title: '<spring:message code='organ.adjust'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function () {
                            var organId = "";
                            var ztreeObj = $.fn.zTree.getZTreeObj("modify-organ-ztree-id");
                            var nodes = ztreeObj.getCheckedNodes(true);
                            $(nodes).each(function (index, item) {
                                organId = item.id;
                            });
                            var departId = $("#mod-depart-id").val();

                            $.ajax({
                                type: 'post',
                                async: false,
                                dataType: 'json',
                                url: '${pageContext.request.contextPath}/department/adjustDepartment',
                                data: [
                                    {name: "organId", value: organId},
                                    {name: "departId", value: departId}
                                ],
                                success: function (data) {
                                    message(data.msg);
                                    $("#modify-organ-dialog-id").dialog('close');
                                    leftDepartmentLoad();
                                },
                                error: function (msg) {
                                    message(' <spring:message code="common.error"></spring:message>');
                                }
                            });
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ]
            });




            $("#bound-depart-dialog-id").dialog({
                autoOpen: false,
                width: 400,
                modal: true,
                resizable: false,
                title: '<spring:message code='common.bound'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function () {
                            var belongCenter = "";
                            var ztreeObj = $.fn.zTree.getZTreeObj("bound-depart-ztree-id");
                            var nodes = ztreeObj.getCheckedNodes(true);
                            $(nodes).each(function (index, item) {
                                belongCenter = item.id;
                            });
                            var departId = $("#mod-depart-id").val();

                            $.ajax({
                                type: 'post',
                                async: false,
                                dataType: 'json',
                                url: '${pageContext.request.contextPath}/department/boundDepart',
                                data: [
                                    {name: "belongCenter", value: belongCenter},
                                    {name: "departId", value: departId}
                                    // {name: "organId", value: organId}
                                ],
                                success: function (data) {
                                    message(data.msg);
                                    $("#bound-depart-dialog-id").dialog('close');
                                    leftDepartmentLoad();
                                },
                                error: function (msg) {
                                    message(' <spring:message code="common.error"></spring:message>');
                                }
                            });
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ]
            });

            $("ul#tab-ul-duty-id > li").click(function (event) {
                $("ul#tab-ul-duty-id .active").removeClass("active");
                $(this).addClass("active");

                $("section.tab-content .in").removeClass("in");
                $("section.tab-content .active").removeClass("active");
                var tab = $(this).attr("tab");
                $("#" + tab).addClass("in");
                $("#" + tab).addClass("active")
            });
            $("ul#tab-ul-duty-id > li:first").click();
            _gridWidth = _gridWidth - 153;
            var _columnWidth = _gridWidth / 3;
            $("ul#tab-ul-depart-id > li").click(function (event) {
                $("ul#tab-ul-depart-id .active").removeClass("active");
                $(this).addClass("active");

                $("section.tab-content .in").removeClass("in");
                $("section.tab-content .active").removeClass("active");
                var tab = $(this).attr("tab");
                $("#" + tab).addClass("in");
                $("#" + tab).addClass("active")
            });
            $("ul#tab-ul-depart-id > li:first").click();

            resizePageSize();
            _gridWidth = _gridWidth - 173;
            var _columnWidth = _gridWidth / 4;
            $("#table-post-edit-id").tableEditData({
                width: _gridWidth,
                height: 400,
                url: "${pageContext.request.contextPath}/department/getDepartmentDutyInfoById",
                colModel: [
                    {display: "<spring:message code='duty.id'/>", name: 'dutyid', width: _columnWidth},
                    {display: "<spring:message code='duty.name'/>", name: 'name', width: _columnWidth},
                    {display: "<spring:message code='role.name'/>", name: 'roleName', width: _columnWidth},
                    {
                        display: "<spring:message code='common.operation'/>",
                        name: 'dutyid',
                        width: _columnWidth,
                        process: function (v, _trid, _row) {
                            var html = '<a href="#" class="btn" onclick="details(\'' + v + '\');"> <spring:message code='common.details'/> </a>';
                            return html;
                        }
                    }
                ]
            });
// 详情页面
            $("#department-detail-dialog-id").dialog({
                autoOpen : false,
                width : 400,
                modal : true,
                resizable : false,
                title: '<spring:message code='user.info'/>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function() {
                            $("#department-detail-dialog-id").dialog('close');
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });
            $("#department-detail-dialog-id").flexigrid({
                width : _gridWidth-660,
                height:440,
                modal : true,
                resizable : false,
                url:"${pageContext.request.contextPath}/department/getUserInfoByDutyid",
                colModel : [
                    {display : 'userName',name : 'userName',width : 50,sortable : false,align : 'center',hide : 'true'},
                    {display : "<spring:message code='user.name'/>",name : 'userName',width : _columnWidth, sortable : true,align : 'center'}


                ],
                resizable : false, //resizable table是否可伸缩
                useRp : true,
                usepager : true, //是否分页
                autoload : false, //自动加载，即第一次发起ajax请求
                hideOnSubmit : true, //是否在回调时显示遮盖
                showcheckbox : true, //是否显示多选框
                //rowhandler : rowDbclick, //是否启用行的扩展事情功能,在生成行时绑定事件，如双击，右键等
                rowbinddata : true
            });

        });//ready
        var leftDepartmentSetting = {
            check: {
                enable: false
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback: {
                onClick: onClickNode
            }
        };

        function leftDepartmentLoad() {
            $.ajax({
                    type: 'post',
                    async: false,
                    dataType: 'json',
                    url: '${pageContext.request.contextPath}/department/getDepartmentTree',
                    data: [],
                    success: function (data) {
                        $.fn.zTree.init($("#depart-left-tree-ul-id"), leftDepartmentSetting, data);
                    },
                    error: function (msg) {
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );
        }

        //ztree回调事件
        function onClickNode(event, treeId, treeNode) {
            $.ajax({
                    type: 'post',
                    async: false,
                    dataType: 'json',
                    url: '${pageContext.request.contextPath}/department/getDepartmentInfoById',
                    data: [{name: "departId", value: treeNode.id}],
                    success: function (data) {
                        var dom = $("#modify-depart-info-form-id .form-control");
                        dom.each(function (index, item) {
                            var nameAttr = $(item).attr("name");
                            console.info(data[nameAttr]);
                            $(item).val(data[nameAttr]);
                        });
                    },
                    error: function (msg) {
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );
            $("#table-post-edit-id").tableEditOptions({
                extParam: [
                    {name: "departId", value: treeNode.id}
                ]
            }).tableEditReload();
        }

        function deleteDepartInfo() {
            var modDepartId = $("#mod-depart-id").val();
            if (modDepartId == null || "" === modDepartId) {
                return;
            }
            $.ajax({
                type: 'post',
                async: false,
                dataType: 'json',
                url: '${pageContext.request.contextPath}/department/deleteDepartInfoById',
                data: [{name: "departId", value: modDepartId}],
                success: function (data) {
                    message(data.msg);
                    $("#modify-depart-info-form-id").resetForm();
                    leftDepartmentLoad();
                },
                error: function (msg) {
                    message(' <spring:message code="common.error"></spring:message>');
                }
            });
        }

        function modifyDepartInfo() {
            var flag = true;
            var dom = $("#modify-depart-info-form-id > div > .form-control");
            dom.each(function (index, item) {
                flag &= $(item).verification();
                console.info(flag);
            });

            if (!flag) {
                return;
            }
            $("#modify-depart-info-form-id").submit();
        }

        function addDepartInfo() {
            $("#add-depart-info-form-id").resetForm();
            $("#add-depart-info-dialog-id").dialog('open');
        }

        var BoundDepartSetting = {
            check: {
                enable: true,
                chkStyle: "radio",
                radioType: "all"
            },
            data: {
                simpleData: {
                    enable: true
                }
            }
        };

        function boundDepartInfo() {
            var departId = $("#mod-depart-id").val();
            if (departId == null || '' === departId) {
                return;
            }
            $.ajax({
                    type: 'post',
                    async: false,
                    dataType: 'json',
                    url: '${pageContext.request.contextPath}/department/getDepartTreeByDepartId',
                    data: [{name: "departId", value: departId}],
                    success: function (data) {
                        $.fn.zTree.init($("#bound-depart-ztree-id"), BoundDepartSetting, data);
                        var ztreeObj = $.fn.zTree.getZTreeObj("bound-depart-ztree-id");
                        var nodes = ztreeObj.getNodesByParam("id", departId, null);
                        ztreeObj.removeNode(nodes[0]);
                        $("#bound-depart-dialog-id").dialog('open');

                    },
                    error: function (msg) {
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );
        }

        function unboundDepartInfo() {
            var modDepartId = $("#mod-depart-id").val();
            if (modDepartId == null || '' === modDepartId) {
                return;
            }
            $.ajax({
                type: 'post',
                async: false,
                dataType: 'json',
                url: '${pageContext.request.contextPath}/department/unBoundByDepartId',
                data: [{name: "departId", value: modDepartId}],
                success: function (data) {
                    leftDepartmentLoad();
                    message(data.msg);
                },
                error: function (msg) {
                    message(' <spring:message code="common.error"></spring:message>');
                }
            });
        }

        var ModifyOrganSetting = {
            check: {
                enable: true,
                chkStyle: "radio",
                radioType: "all"
            },
            data: {
                simpleData: {
                    enable: true
                }
            }
        };

        function modifyOrganInfo() {
            var modDepartId = $("#mod-depart-id").val();
            if (modDepartId == null || '' === modDepartId) {
                return;
            }
            $.ajax({
                    type: 'post',
                    async: false,
                    dataType: 'json',
                    url: '${pageContext.request.contextPath}/department/getDepartTreeByDepartId',
                    data: [{name: "departId", value: modDepartId}],
                    success: function (data) {
                        $.fn.zTree.init($("#modify-organ-ztree-id"), ModifyOrganSetting, data);
                        var ztreeObj = $.fn.zTree.getZTreeObj("modify-organ-ztree-id");
                        var nodes = ztreeObj.getNodesByParam("id", modDepartId, null);
                        ztreeObj.removeNode(nodes[0]);
                        $("#modify-organ-dialog-id").dialog('open');

                    },
                    error: function (msg) {
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );
        }

        function details(dutyid) {
            $("#common-operation").flexOptions({
                extParam:[
                    {name:"dutyid",value:dutyid}
                ]
            }).flexReload();
            $.ajax({
                type: 'post',
                async: false,
                dataType : 'json',
                url: '${pageContext.request.contextPath}/department/getUserInfoByDutyid',
                data: [
                    {name:"dutyid",value:dutyid}
                ],
                success: function (data) {
                    // message(data.msg);
                    $("#department-detail-dialog-id").dialog('open');
                    leftDepartmentLoad();
                },
                error: function(msg){
                    message(' <spring:message code="common.error"></spring:message>');
                }
            });
        }

    </script>

</head>
<body style="display: flex;flex-direction: row">


<nav>
    <spring:message code="depart.info.tip"></spring:message>
    <ul id="depart-left-tree-ul-id" class="ztree"></ul>
</nav>


<article style="flex-grow: 1">
    <nav>
        <a href="#" class="btn" onclick="deleteDepartInfo();">
            <i class="fa fa-trash-o"></i>
            <span> <spring:message code="common.delete"></spring:message> </span>
        </a>

        <a href="#" class="btn" onclick="modifyDepartInfo();">
            <i class="glyphicon glyphicon-modal-window"></i>
            <span> <spring:message code="common.modify"></spring:message> </span>
        </a>
        <a href="#" class="btn" onclick="addDepartInfo();">
            <i class="fa fa-plus"></i>
            <span> <spring:message code="common.add"></spring:message> </span>
        </a>
        <a href="#" class="btn" onclick="boundDepartInfo();">
            <i class="fa fa-chain-broken"></i>
            <span> <spring:message code="common.bound"></spring:message> </span>
        </a>
<%--        <a href="#" class="btn" onclick="modifyOrganInfo();">--%>
<%--            <i class="fa fa-caret-square-o-up"></i>--%>
<%--            <span> <spring:message code="common.modOrgan"></spring:message> </span>--%>
<%--        </a>--%>
        <a href="#" class="btn" onclick="unboundDepartInfo();">
            <i class="fa fa-unlock"></i>
            <span> <spring:message code="common.unbound"></spring:message> </span>
        </a>

    </nav>
    <form id="modify-depart-info-form-id"
          action="${pageContext.request.contextPath}/department/modifyDepartInfoById"
          method="post">
        <fieldset>
            <legend>
                <span><spring:message code="common.base"/></span>
            </legend>

            <div class="form-tow-column">

                <div class="form-group">
                    <span for="mod-depart-id"><spring:message code="depart.id"></spring:message> </span>
                    <input name="depUuid" id="mod-depart-id" class="form-control" readonly>
                </div>

                <div class="form-group">
                    <span for="mod-depart-name-id"><spring:message code="depart.name"></spring:message> </span>
                    <input name="branchName" id="mod-depart-name-id" class="form-control" regex="^.{1,50}$"
                           tip="input 1 to 50 char">
                </div>

                <div class="form-group">
                    <span for="mod-belong-id"><spring:message code="belong.id"></spring:message> </span>
                    <input name="organUuid" id="mod-belong-id" class="form-control" readonly>
                </div>

                <div class="form-group">
                    <span for="mod-belong-type-id"><spring:message code="belong.center"></spring:message> </span>
                    <input name="belongCenter" id="mod-belong-type-id" class="form-control" readonly>
                </div>

            </div>
        </fieldset>
    </form>

    <ul id="tab-ul-depart-id" class="nav nav-tabs">
        <li tab="tab-post-id">
            <a href="#">
                <i class="fa fa-pied-piper-alt"></i>
                <spring:message code="duty.name"></spring:message>
            </a>
        </li>
    </ul>

    <section class="tab-content">
        <article id="tab-post-id" class="tab-pane fade in active">
            <table id="table-post-edit-id" class="table-edit-data"></table>
        </article>
    </section>
</article>

<div id="add-depart-info-dialog-id" style="display: none">

    <form id="add-depart-info-form-id" action="${pageContext.request.contextPath}/department/addDepartInfoById"
          method="post">

        <div class="form-group">
            <span for="add-depart-name-id"><spring:message code="depart.name"></spring:message> </span>
            <input name="branchName" id="add-depart-name-id" class="form-control" regex="^.{1,50}$"
                   tip="input 1 to 50 char">
        </div>

        <div class="form-group">
            <span for="add-belong-id"><spring:message code="belong.id"></spring:message> </span>
            <input name="organUuid" id="add-belong-id" class="form-control" regex="^\w*$"
                   tip="please input char 、number、_ ">
        </div>

    </form>
</div>

<div id="bound-depart-dialog-id" style="display: none">
    <ul id="bound-depart-ztree-id" class="ztree"></ul>
</div>

<div id="modify-organ-dialog-id" style="display: none">
    <ul id="modify-organ-ztree-id" class="ztree"></ul>
</div>

<div id="department-detail-dialog-id" style="display: none">
    <ul id="department-detail-ztree-id" class="ztree"></ul>
</div>

</body>
</html>