<%@include file="../common/common.jsp"%>
<html>
<head>
    <title>
        <spring:message code="login.tile.tip"></spring:message>
    </title>
    <script type="text/javascript">
        $(function () {
            $("#submit-id").click(function (event) {
                var flag = true;

                flag &= $("#user-name-id").verification();
                flag &= $("#password-id").verification();
                if(!flag){
                    return;
                }

                $("#login-form-id").submit();

            });

        });//ready
        /**
         * 覆盖form表单提交的逻辑
         */
        $("#login-form-id").ajaxForm({
            dataType: "json",
            success : function(data) {
                console.log(data)
                message(data.msg)
                setTimeout(function () {
                    if (data.success) {
                        window.location = '${pageContext.request.contextPath}/login/login'
                    } else {
                        window.location = '${pageContext.request.contextPath}/login/register'
                    }
                }, 1200)
            },
            error : function() {
                message(' <spring:message code="common.error"></spring:message>');
            },
            complete : function(response, status) {

            }
        });
    </script>

</head>
<body style="display: flex;flex-direction: column">
<div style="display: flex;flex-direction: row">
    <c:if test="${siteLanguage != 'en'}">
        <img src="${pageContext.request.contextPath}/assets/images/logo_zh.png">
    </c:if>
    <c:if test="${siteLanguage == 'en'}">
        <img src="${pageContext.request.contextPath}/assets/images/logo_en.png">
    </c:if>
    <div style="    position: absolute;right: 100px; top: 34px;">
        <a class="btn-toolbar" href="${pageContext.request.contextPath}/login/login?siteLanguage=zh" target="_self"><spring:message code="login.chinese"></spring:message></a>
        <a class="btn-toolbar" href="${pageContext.request.contextPath}/login/login?siteLanguage=en" target="_self"><spring:message code="login.english"></spring:message></a>
    </div>
</div>
<div style="flex-grow: 1;display: flex;flex-direction: row">
    <div style="flex-grow: 1">
        <img src="${pageContext.request.contextPath}/assets/images/pic.png"></img>
    </div>

    <form style="width: 400px;" id="login-form-id" action="${pageContext.request.contextPath}/login/doRegister" method="post">
        <div class="form-group">
                <span for="user-name-id">
                    <spring:message code="login.username"></spring:message>
                    :</span>
            <input class="form-control" type="text" name="userName" id="user-name-id" regex="^.{1,40}$" tip="input 1 to 40 char">
        </div>
        <div class="form-group">
            <span for="password-id"><spring:message code="login.password"></spring:message>:</span>
            <input class="form-control" type="password" name="password" id="password-id" regex="^.{1,40}$" tip="input 1 to 40 char">
        </div>
        <div class="form-group">
            <span for="password-id"><spring:message code="user.email"></spring:message>:</span>
            <input class="form-control" type="text" name="email" id="email-id" regex="^.{1,40}$" tip="input 1 to 40 char">
        </div>
        <div class="form-group">
            <span for="password-id"><spring:message code="user.mobile"></spring:message>:</span>
            <input class="form-control" type="text" name="mobile" id="mobile-id" regex="^.{1,40}$" tip="input 1 to 40 char">
        </div>
        <div class="form-group">
            <span for="password-id"><spring:message code="user.nick"></spring:message>:</span>
            <input class="form-control" type="text" name="niceName" id="nick-id" regex="^.{1,40}$" tip="input 1 to 40 char">
        </div>
        <div class="form-group">
            <a class="form-control btn-success" id="submit-id" href="#" style="text-align: center" >
                <spring:message code="login.register"></spring:message>
            </a>
        </div>
        <div>
            <a id="login-register-id" href="/login/login">
                <spring:message code="login.login"></spring:message>
            </a>
        </div>
    </form>
    <div style="width: 200px;">

    </div>

</div>


</body>
</html>
