<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/8/24
  Time: 13:52
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="gdmt" uri="http://www.isoftstone.com/gdmt" %>

<html>
<head>
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/favicon.ico?v=2" />

    <link href="${pageContext.request.contextPath}/assets/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/grid/css/flexigrid2.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/zTree3.3/css/zTreeStyle/zTreeStyle.css" type="text/css"/>
    <link href="${pageContext.request.contextPath}/assets/jquery-ui-1.9.2.custom/css/jquery.ui.all.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/assets/css/common.css" rel="stylesheet"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/assets/css/page.css">


    <script src="${pageContext.request.contextPath}/static/assets/amcharts_3.1.1/amcharts/amcharts.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/static/assets/amcharts_3.1.1/amcharts/serial.js" type="text/javascript"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/grid/js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/grid/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/grid/js/jquery.flexigrid.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/zTree3.3/js/jquery.ztree.core-3.5.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/zTree3.3/js/jquery.ztree.excheck-3.5.js"> </script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/common.js"> </script>
    <script type="text/javascript">
        $(function () {
            $("#dialog-id").dialog({
                autoOpen : false,
                width : 400,
                modal : true,
                resizable : false,
                title: '<spring:message code="common.message.tip"></spring:message>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });
        });//ready
        function message(contents) {
            $("#dialog-id").html(contents);
            $("#dialog-id").dialog('open');
        }

        var i18n_flexigrid_queryFail = "<spring:message code='flexigrid.tip.queryFail' />";
        var i18n_flexigrid_recordDisplayFromTo = "<spring:message code='flexigrid.tip.recordDisplayFromTo' />";
        var i18n_flexigrid_loading = "<spring:message code='flexigrid.tip.loading' />";
        var i18n_flexigrid_noRecord = "<spring:message code='flexigrid.tip.noRecord' />";
        var i18n_flexigrid_toFirstPage = "<spring:message code='flexigrid.tip.toFirstPage' />";
        var i18n_flexigrid_toPreviousPage = "<spring:message code='flexigrid.tip.toPreviousPage' />";
        var i18n_flexigrid_currentPage = "<spring:message code='flexigrid.tip.currentPage' />";
        var i18n_flexigrid_totalPage = "<spring:message code='flexigrid.tip.totalPage' />";
        var i18n_flexigrid_toNextPage = "<spring:message code='flexigrid.tip.toNextPage' />";
        var i18n_flexigrid_toLastPage = "<spring:message code='flexigrid.tip.toLastPage' />";
        var i18n_flexigrid_refresh = "<spring:message code='flexigrid.tip.refresh' />";
        var i18n_flexigrid_everyPageCount = "<spring:message code='flexigrid.tip.everyPageCount' />";
        var i18n_flexigrid_quickSearch = "<spring:message code='flexigrid.tip.quickSearch' />";
        var i18n_flexigrid_queryError = "<spring:message code='flexigrid.tip.queryError' />";
        var i18n_flexigrid_selectAll = "<spring:message code='flexigrid.tip.selectall' />";

    </script>

</head>
<body>

    <div id="dialog-id" style="display: none;">

    </div>

</body>
</html>
