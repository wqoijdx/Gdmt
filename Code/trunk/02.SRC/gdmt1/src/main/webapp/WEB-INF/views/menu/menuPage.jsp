<%@include file="../common/common.jsp"%>
<html>
<head>
    <title>
    <spring:message code="menu.info.tip"></spring:message>
   </title>
    <style type="text/css">
        .form-tow-column{
            display:grid;
            grid-template-columns:auto auto;

         }
        .form-group{
             margin: 0 10px;
        }
        fieldset>legend{
            font-size: 10px;
            border-bottom:none;
            margin-bottom: 0;
        }
        fieldset {
            display: block;
            margin-inline-start: 2px;
            margin-inline-end: 2px;
            padding-block-start: 0.35em;
            padding-inline-start: 0.75em;
            padding-inline-end: 0.75em;
            padding-block-end: 0.625em;
            min-inline-size: min-content;
            border: 1px solid #e5e5e5;;
            border-image: initial;
        }

        .gdmt-tb-th{
            height: 32px;
            background-color: #b5b5b5;
            background: #f5f5f5;
            border-top: #d4d4d4 1px solid;
            background-color: #fff;
        }
        .gdmt-tb-th > th{
            font-weight: normal;
            cursor: default;
            white-space: nowrap;
            overflow: hidden;
            text-align: center;
            vertical-align: middle !important;
            padding: 0px;
            height: 32px;
            background-color: #b5b5b5;
            box-sizing: border-box;
            color: #FFF;
            font-size: 12px;
        }
        .table-edit-data td{
            text-align: center;
            vertical-align: middle !important;
            padding: 0px;
            height: 32px;
            font-size: 12px;
            background-color: #f5f5f5;
        }
        .table-erow td{
            background-color: #ededed;
        }
        .table-edit-data .read{
            display: inline-block;
            margin-right: 10px;
        }
        .table-edit-data .edit{
         display: none;
        }
        .table-edit-tr .edit{
         display: inline-block;
        }
        .table-edit-tr .read{
         display: none;
        }
    </style>
    <script type="text/javascript">
    //页面自适应
    function resizePageSize(){
        _gridWidth = $(document).width()-12;/*  -189 是去掉左侧 菜单的宽度，   -12 是防止浏览器缩小页面 出现滚动条 恢复页面时  折行的问题 */
        _gridHeight = $(document).height()-32-85; /* -32 顶部主菜单高度，   -90 查询条件高度*/
    }
    $(function() {
        resizePageSize();
        leftMenuLoad();
        $("#modify-menu-info-form-id").ajaxForm({
            dataType: "json",
                success : function(data) {
                    message(data.msg);
                    leftMenuLoad();
                 },
                 error : function() {
                     message(' <spring:message code="common.error"></spring:message>');
                 },
                complete : function(response, status) {

                }
        });
        $("#add-menu-info-dialog-id").dialog({
                    autoOpen : false,
                    width : 400,
                    modal : true,
                    resizable : false,
                    title: '<spring:message code='menu.add'/>',
                    buttons: [
                        {
                            text: '<spring:message code="common.confirm"></spring:message>',
                            click: function() {
                                var flag = true;
                                var dom = $("#add-menu-info-form-id > div > .form-control");
                                dom.each(function(index,item) {
                                    flag &= $(item).verification();
                                    console.info(flag);
                                });
                                if(!flag){
                                    return;
                                }
                                $("#add-menu-info-form-id").submit();
                            }
                        },
                        {
                            text: '<spring:message code="common.cancel"></spring:message>',
                            click: function() {
                                $( this ).dialog( "close" );
                            }
                        }
                    ]
        });

        $("#add-menu-info-form-id").ajaxForm({
                    dataType: "json",
                    success : function(data) {
                        message(data.msg);
                        leftMenuLoad();
                        $("#add-menu-info-dialog-id").dialog('close');
                     },
                     error : function() {
                         message(' <spring:message code="common.error"></spring:message>');
                     },
                    complete : function(response, status) {

                    }

        });

        $("#bound-menu-dialog-id").dialog({
            autoOpen : false,
            width : 400,
            modal : true,
            resizable : false,
            title: '<spring:message code='common.bound'/>',
            buttons: [
                {
                    text: '<spring:message code="common.confirm"></spring:message>',
                    click: function() {
                        var parentId = "";
                        var ztreeObj = $.fn.zTree.getZTreeObj("bound-menu-ztree-id");
                        var nodes = ztreeObj.getCheckedNodes(true);
                        $(nodes).each(function (index,item) {
                            parentId =item.id;
                        });
                        var menuId = $("#mod-menu-id").val();

                        $.ajax({
                            type: 'post',
                            async: false,
                            dataType : 'json',
                            url: '${pageContext.request.contextPath}/menu/boundMenu',
                            data: [
                                {name:"parentId",value:parentId},
                                {name:"menuId",value:menuId}
                            ],
                            success: function (data) {
                                message(data.msg);
                                $("#bound-menu-dialog-id").dialog('close');
                                leftMenuLoad();
                            },
                            error: function(msg){
                                message(' <spring:message code="common.error"></spring:message>');
                            }
                        });
                    }
                },
                {
                    text: '<spring:message code="common.cancel"></spring:message>',
                    click: function() {
                        $( this ).dialog( "close" );
                    }
                }
            ]
        });

        $("ul#tab-ul-menu-id > li").click(function(event) {
            $("ul#tab-ul-menu-id .active").removeClass("active");
            $(this).addClass("active");

            $("section.tab-content .in").removeClass("in");
            $("section.tab-content .active").removeClass("active");
            var tab = $(this).attr("tab");
            $("#" + tab).addClass("in");
            $("#" + tab).addClass("active")
        });
        $("ul#tab-ul-menu-id > li:first").click();
        _gridWidth =  _gridWidth - 153;
        var _columnWidth = _gridWidth/3;
        $("#table-i18n-edit-id").tableEditData({
            width : _gridWidth,
            height:400,
            url:"${pageContext.request.contextPath}/menu/getI18nByMenuId",
            colModel : [
                    {display : 'menuId',name : 'menuId',width : 150,align : 'center',hide : 'true'},
                    {display : "<spring:message code='menu.lang'/>",name : 'langId',width : _columnWidth,dom: function(obj,v,_trid,_row){
                        var select=$('<gdmt:dicData group="lang.type" classez="edit" name="langId"></gdmt:dicData>');
                        select.val(v);
                        select.css("width","43%");
                        select.appendTo(obj);
                        select.attr("readonly",true);
                        if(_row){
                            select.attr("disabled","disabled");
                        }



                        var span = $("<span></span>");
                        span.addClass("read");
                        var vl  =  select.find("option[value='" + v + "']").text();
                        span.text(vl);
                        span.appendTo(obj);


                    }},
                    {display : "<spring:message code='menu.name'/>",name : 'menuName',width : _columnWidth},
                    {display : "<spring:message code='common.operation'/>",name : 'menuName',width : _columnWidth,process: function(v,_trid,_row) {
                        var html='<a href="#" class="read" onclick="editTable(\''+ _trid +'\');"> <i class="fa fa-pencil"></i> </a>';
                        html +='<a href="#" class="edit" onclick="saveTable(\''+ _trid +'\');"> <i class="fa fa-floppy-o"></i> </a>';
                        html +='<a href="#" class="read" onclick="deleteTable(\''+ _trid +'\');"> <i class="fa fa-trash-o"></i> </a>';

                        return html;
                    }}
            ]
        });

    });//ready
    var leftMenuSetting = {
        check: {
            enable: false
        },
        data: {
                simpleData: {
                enable: true
            }
        },
        callback : {
  			onClick : onClickNode
  		}
    };
    function leftMenuLoad() {
        $.ajax({
                type: 'post',
                async: false,
                dataType : 'json',
                url: '${pageContext.request.contextPath}/menu/getMenuTree',
                data: [],
                success: function (data) {
                    $.fn.zTree.init($("#menu-left-tree-ul-id"), leftMenuSetting, data);
                },
                error: function(msg){
                    message(' <spring:message code="common.error"></spring:message>');
                }
            }
        );
    }

    //ztree回调事件
    function onClickNode(event, treeId, treeNode){
    	//console.info(treeNode.id);
        $.ajax({
                type: 'post',
                async: false,
                dataType : 'json',
                url: '${pageContext.request.contextPath}/menu/getMenuInfoById',
                data: [{name:"menuId",value:treeNode.id}],
                success: function (data) {
                    var dom = $("#modify-menu-info-form-id .form-control");
                    dom.each(function(index,item) {
                        var nameAttr = $(item).attr("name");
                        // console.info(data[nameAttr]);
                        $(item).val(data[nameAttr]);
                    });

                },
                error: function(msg){
                    message(' <spring:message code="common.error"></spring:message>');
                }
            }
        );
        $("#table-i18n-edit-id").tableEditOptions({
            extParam:[
                {name:"menuId",value:treeNode.id}
            ]
        }).tableEditReload();
    }
    function modifyMenuInfo() {
        var flag = true;
        var dom = $("#modify-menu-info-form-id > div > .form-control");
        dom.each(function(index,item) {
            flag &= $(item).verification();
            console.info(flag);
        });
        if(!flag){
            return;
        }
        $("#modify-menu-info-form-id").submit();
    }
    
    function addMenuInfo() {
        $("#add-menu-info-form-id").resetForm();
        $("#add-menu-info-dialog-id").dialog('open');
    }
    
    function deleteMenuInfo() {
        var modMenuId = $("#mod-menu-id").val();
        if(modMenuId == null || "" === modMenuId){
            return;
         }
        $.ajax({
            type: 'post',
            async: false,
            dataType : 'json',
            url: '${pageContext.request.contextPath}/menu/deleteMenuInfoById',
            data: [{name:"menuId",value:modMenuId}],
            success: function (data) {
                message(data.msg);
                $("#modify-menu-info-form-id").resetForm();
                leftMenuLoad();
            },
            error: function(msg){
                message(' <spring:message code="common.error"></spring:message>');
            }
        });
    
    }

    var BoundMenuSetting = {
            check: {
                enable: true,
                chkStyle: "radio",
 			    radioType: "all"
            },
            data: {
                    simpleData: {
                    enable: true
                }
            }
    };

    function boundMenuInfo () {
        var modMenuId = $("#mod-menu-id").val();
        if(modMenuId == null || '' === modMenuId){
            return;
        }
        $.ajax({
                type: 'post',
                async: false,
                dataType : 'json',
                url: '${pageContext.request.contextPath}/menu/getMenuTreeByMenuId',
                data: [{name:"menuId",value:modMenuId}],
                success: function (data) {
                    $.fn.zTree.init($("#bound-menu-ztree-id"), BoundMenuSetting, data);
                    var ztreeObj = $.fn.zTree.getZTreeObj("bound-menu-ztree-id");
                    var nodes = ztreeObj.getNodesByParam("id",modMenuId,null);
                    ztreeObj.removeNode(nodes[0]);
                    $("#bound-menu-dialog-id").dialog('open');

                },
                error: function(msg){
                    message(' <spring:message code="common.error"></spring:message>');
                }
            }
        );
    }
    
    function unboundMenuInfo() {
        var modMenuId = $("#mod-menu-id").val();
        if(modMenuId == null || '' === modMenuId){
            return;
        }
        $.ajax({
            type: 'post',
            async: false,
            dataType : 'json',
            url: '${pageContext.request.contextPath}/menu/unBoundByMenuId',
            data: [{name:"menuId",value:modMenuId}],
            success: function (data) {
                leftMenuLoad();
                message(data.msg);
            },
            error: function(msg){
                message(' <spring:message code="common.error"></spring:message>');
            }
        });
    }
    function editTable(trId) {
        $("#" + trId).addClass("table-edit-tr");
    }


    function saveTable(trId) {
       var trDom = $("#" + trId);
       var form = $("<form></form>");
       form.appendTo($("body"));
       trDom.find(".edit").cloneObj().appendTo(form);
       form.ajaxForm({
            dataType: "json",
            success : function(data) {
                //message(data.msg);
                form.remove();
                $("#table-i18n-edit-id").tableEditReload();
             },
             error : function() {
                 message(' <spring:message code="common.error"></spring:message>');
             },
            complete : function(response, status) {

            }
        });

        var menuIdDom = form.find(".edit[name='menuId']");
        if(menuIdDom.val() === ''){
            menuIdDom.val($("#mod-menu-id").val());
            form.attr({
                 method:"post",
                 action:"${pageContext.request.contextPath}/menu/addI18nData"
            });
         }else{
            form.attr({
                 method:"post",
                 action:"${pageContext.request.contextPath}/menu/saveI18nData"
            });
         }
       form.submit();
    }
    
    function deleteTable(trId) {
    var trDom = $("#" + trId);
           var form = $("<form></form>");
           form.appendTo($("body"));
           trDom.find(".edit").cloneObj().appendTo(form);
           form.ajaxForm({
                dataType: "json",
                success : function(data) {
                    //message(data.msg);
                    form.remove();
                    $("#table-i18n-edit-id").tableEditReload();
                 },
                 error : function() {
                     message(' <spring:message code="common.error"></spring:message>');
                 },
                complete : function(response, status) {

                }
            });
            form.attr({
                 method:"post",
                 action:"${pageContext.request.contextPath}/menu/deleteI18nData"
            });
           form.submit();
    }
    </script>
</head>
<body style="display: flex;flex-direction: row">

    <nav>
        <spring:message code="menu.info.tip"></spring:message>
        <ul id="menu-left-tree-ul-id" class="ztree" ></ul>
    </nav>


    <article style="flex-grow: 1">
        <nav>
            <a href="#" class="btn" onclick="deleteMenuInfo();">
                <i class="fa fa-trash-o"></i>
                <span> <spring:message code="common.delete"></spring:message> </span>
            </a>

            <a href="#" class="btn" onclick="modifyMenuInfo();">
                <i class="glyphicon glyphicon-modal-window"></i>
                <span> <spring:message code="common.modify"></spring:message> </span>
            </a>
            <a href="#" class="btn" onclick="addMenuInfo();">
                <i class="fa fa-plus"></i>
                <span> <spring:message code="common.add"></spring:message> </span>
            </a>
            <a href="#" class="btn" onclick="boundMenuInfo();">
                <i class="fa fa-chain-broken"></i>
                <span> <spring:message code="common.bound"></spring:message> </span>
            </a>
            <a href="#" class="btn" onclick="unboundMenuInfo();">
                <i class="fa fa-unlock"></i>
                <span> <spring:message code="common.unbound"></spring:message> </span>
            </a>

        </nav>

        <form id="modify-menu-info-form-id" action="${pageContext.request.contextPath}/menu/modifyMenuInfoById"
           method="post">
            <fieldset >
                    <legend>
                        <span><spring:message code="common.base" /></span>
                    </legend>
                    <div class="form-tow-column">
                        <div class="form-group">
                            <span for="mod-menu-id"><spring:message code="menu.id"></spring:message> </span>
                            <input name="menuId" id="mod-menu-id" class="form-control" readonly >
                        </div>
                        <div class="form-group">
                            <span for="mod-menu-name-id"><spring:message code="menu.name"></spring:message> </span>
                            <input name="menuName" id="mod-menu-name-id" class="form-control" regex="^.{1,50}$" tip="input 1 to 50 char">
                        </div>
                        <div class="form-group">
                            <span for="mod-menu-type-id"><spring:message code="menu.type"></spring:message> </span>
                            <gdmt:dicData group="menu.type" classez="form-control" id="mod-menu-type-id" name="menuType" regex="^\d$" tip="please choose"></gdmt:dicData>
                        </div>
                        <div class="form-group">
                            <span for="mod-menu-type-id"><spring:message code="menu.level"></spring:message> </span>
                             <gdmt:dicData group="menu.level" classez="form-control" id="mod-menu-level-id" name="menuLevel"></gdmt:dicData>
                        </div>
                        <div class="form-group">
                            <span for="mod-menu-url-id">URL</span>
                            <input name="menuUrl" id="mod-menu-url-id" class="form-control">
                        </div>
                        <div class="form-group">
                            <span for="mod-menu-privilege-code-id"><spring:message code="menu.privilege.code"></spring:message> </span>
                            <input name="privilegeCode" id="mod-menu-privilege-code-id" class="form-control">
                        </div>
                        <div class="form-group">
                            <span for="mod-menu-icon-id"><spring:message code="menu.menu.icon"></spring:message> </span>
                            <input name="menuIcon" id="mod-menu-icon-id" class="form-control">
                        </div>
                        <div class="form-group">
                            <span for="mod-menu-http-id"><spring:message code="menu.menu.http"></spring:message> </span>
                            <gdmt:dicData group="menu.https" classez="form-control" id="mod-menu-http-id" name="isHttps" value="0"></gdmt:dicData>
                        </div>
                    </div>
            </fieldset>
        </form>
        <ul id="tab-ul-menu-id" class="nav nav-tabs">
            <li tab="tab-i18n-id">
                <a href="#">
                    <i class="fa fa-university"></i>
                    <spring:message code="common.i18n.name"></spring:message>
                </a>
            </li>
            <li tab="tab-son-menu-id">
                <a href="#">
                    <i class="fa fa-th"></i>
                    <spring:message code="menu.son.name"></spring:message>
                </a>
            </li>
        </ul>
        <section class="tab-content">
            <article id="tab-i18n-id" class="tab-pane fade in active">
                <table id="table-i18n-edit-id" class="table-edit-data"></table>
            </article>
            <article id="tab-son-menu-id" class="tab-pane fade">
                <spring:message code="menu.son.name"></spring:message>
            </article>
        </section>

    </article>

    <div id="add-menu-info-dialog-id" style="display: none">
        <form id="add-menu-info-form-id" action="${pageContext.request.contextPath}/menu/addMenuInfoById"
           method="post">
            <div class="form-group">
                <span for="add-menu-name-id"><spring:message code="menu.name"></spring:message> </span>
                <input name="menuName" id="add-menu-name-id" class="form-control" regex="^.{1,50}$" tip="input 1 to 50 char">
            </div>
            <div class="form-group">
                <span for="add-menu-type-id"><spring:message code="menu.type"></spring:message> </span>
                <gdmt:dicData group="menu.type" classez="form-control" id="add-menu-type-id" name="menuType" regex="^\d$" tip="please choose"></gdmt:dicData>
            </div>
            <div class="form-group">
                <span for="add-menu-type-id"><spring:message code="menu.level"></spring:message> </span>
                 <gdmt:dicData group="menu.level" classez="form-control" id="add-menu-level-id" name="menuLevel" regex="^\d$" tip="please choose"></gdmt:dicData>
            </div>
            <div class="form-group">
                <span for="add-menu-url-id">URL</span>
                <input name="menuUrl" id="add-menu-url-id" class="form-control" regex="^([a-z,A-Z]([a-z,A-Z,0-9]|_)*(/[a-z,A-Z]([a-z,A-Z,0-9]|_)*)*)?$" tip="please input url">
            </div>
            <div class="form-group">
                <span for="add-menu-privilege-code-id"><spring:message code="menu.privilege.code"></spring:message> </span>
                <input name="privilegeCode" id="add-menu-privilege-code-id" class="form-control" regex="^\w*$" tip="please input char 、number、_ ">
            </div>
            <div class="form-group">
                <span for="add-menu-icon-id"><spring:message code="menu.menu.icon"></spring:message> </span>
                <input name="menuIcon" id="add-menu-icon-id" class="form-control" regex="^.+$" tip="please input icon class ">
            </div>
            <div class="form-group">
                <span for="add-menu-http-id"><spring:message code="menu.menu.http"></spring:message> </span>
                <gdmt:dicData group="menu.https" classez="form-control" id="add-menu-http-id" name="isHttps" regex="^\d$" tip="please choose"></gdmt:dicData>
            </div>
        </form>
    </div>

    <div id="bound-menu-dialog-id" style="display: none">
        <ul id="bound-menu-ztree-id" class="ztree"></ul>
    </div>


</body>
</html>
