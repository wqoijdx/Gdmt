<%@include file="../common/common.jsp"%>
<html>
<head>
    <title>
        <spring:message code="file.show.page"></spring:message>
    </title>
</head>
<body>
    <security:authorize access="hasRole('ROLE_cldk_file_add')">
        <a href="#">
            <spring:message code="file.upload"></spring:message>
        </a>
    </security:authorize>
    <security:authorize access="hasRole('ROLE_cldk_file_del')">
        <a href="#">
            <spring:message code="file.delete"></spring:message>
        </a>
    </security:authorize>

</body>
</html>
