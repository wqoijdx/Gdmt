<%@include file="../common/common.jsp"%>
<html>
<head>
    <script src="${pageContext.request.contextPath}/amcharts/amcharts.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/amcharts/serial.js" type="text/javascript"></script>
    <title>
        <spring:message code="pmi"></spring:message>
    </title>
    <script type="text/javascript">
        var _gridWidth;
        var _gridHeight;
        //页面自适应
        function resizePageSize(){
            _gridWidth = $(document).width()-20;/*  -189 是去掉左侧 菜单的宽度，   -12 是防止浏览器缩小页面 出现滚动条 恢复页面时  折行的问题 */
            _gridHeight = $(document).height()-10; /* -32 顶部主菜单高度，   -90 查询条件高度*/
        }

        $(function () {
            resizePageSize();
            var _columnWidth = _gridWidth/2-20;
            $("#flexigrid-id").flexigrid({
                width : _gridWidth,
                height : _gridHeight,
                url : "${pageContext.request.contextPath}/consult/getPmiInfolistByPage",
                dataType : 'json',
                colModel : [
                    {display : 'pmiId',name : 'pmiId',width : 150,sortable : false,align : 'center',hide : 'true'},
                    {display : "<spring:message code='myDate'/>",name : 'myDate',width : _columnWidth, sortable : true,align : 'center'},
                    {display : "<spring:message code='pmiValue'/>",name : 'pmiValue',width : _columnWidth, sortable : true,align : 'center'}
                ],
                resizable : false, //resizable table是否可伸缩
                useRp : true,
                usepager : true, //是否分页
                autoload : false, //自动加载，即第一次发起ajax请求
                hideOnSubmit : true, //是否在回调时显示遮盖
                showcheckbox : true, //是否显示多选框
                //rowhandler : rowDbclick, //是否启用行的扩展事情功能,在生成行时绑定事件，如双击，右键等
                rowbinddata : true
            });

            $("#flexigrid-id").flexReload();

            $("ul#tab-ul-menu-id > li").click(function(event) {
                $("ul#tab-ul-menu-id .active").removeClass("active");
                $(this).addClass("active");

                $("section.tab-content .in").removeClass("in");
                $("section.tab-content .active").removeClass("active");
                var tab = $(this).attr("tab");
                $("#" + tab).addClass("in");
                $("#" + tab).addClass("active")
            });
            $("ul#tab-ul-menu-id > li:first").click();
            queryPmi();

        })//ready

        function queryPmi() {
            $.ajax({
                    type: 'post',
                    async: false,
                    dataType : 'json',
                    url: '${pageContext.request.contextPath}/consult/getPmi',
                    data: [],
                    success: function (data) {
                        loadChart(data);
                    },
                    error: function(msg){
                        message(' <spring:message code="common.error"></spring:message>');
                    }
                }
            );

        }

        function loadChart(chartData) {
            var chart;
            // SERIAL CHART
            chart = new AmCharts.AmSerialChart();
            chart.pathToImages = "../amcharts/images/";
            chart.dataProvider = chartData;
            chart.categoryField = "myDate";
            chart.dataDateFormat = "YYYY-MM-DD";
            chart.startDuration = 1;

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
            categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to DD
            categoryAxis.dashLength = 1;
            categoryAxis.gridAlpha = 0.15;
            categoryAxis.axisColor = "#DADADA";
            categoryAxis.title="<spring:message code='myDate'/>"

            // value
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.axisColor = "#DADADA";
            valueAxis.dashLength = 1;
            valueAxis.logarithmic = true; // this line makes axis logarithmic
            chart.addValueAxis(valueAxis);

            // GRAPH
            var graph = new AmCharts.AmGraph();
            graph.type = "smoothedLine";
            graph.bullet = "round";
            graph.bulletColor = "#FFFFFF";
            graph.bulletBorderColor = "#00BBCC";
            graph.bulletBorderAlpha = 1;
            graph.bulletBorderThickness = 2;
            graph.bulletSize = 7;
            graph.title = "<spring:message code='pmiValue'/>";
            graph.balloonText = "<spring:message code='pmiValue'/> : [[value]]";
            graph.valueField = "pmiValue";
            graph.lineThickness = 2;
            graph.lineColor = "#00BBCC";
            chart.addGraph(graph);

            // CURSOR
            var chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorPosition = "mouse";
            chart.addChartCursor(chartCursor);

            // SCROLLBAR
            var chartScrollbar = new AmCharts.ChartScrollbar();
            chart.addChartScrollbar(chartScrollbar);

            // LEGEND
            var legend = new AmCharts.AmLegend();
            legend.useGraphSettings = true;
            chart.addLegend(legend);

            // WRITE
            chart.write("chartdiv");
        }

        function queryBtn() {
            var startDate = $("#startDate").val();
            var endDate = $("#endDate").val();
            $("#flexigrid-id").flexOptions({
                extParam:[
                    {name:"startDate",value:startDate},
                    {name:"endDate",value:endDate}
                ]
            }).flexReload();

        }
    
    </script>

</head>
<body>
<ul id="tab-ul-menu-id" class="nav nav-tabs">
    <security:authorize access="hasRole('ROLE_gdmt_data_pmi')">
        <li tab="pmi-chart-id">
            <a href="#">
                <i class="fa fa-area-chart"></i>
                <spring:message code="chart"></spring:message>
            </a>
        </li>
    </security:authorize>
    <security:authorize access="hasRole('ROLE_gdmt_data_pmi')">
        <li tab="pmi-table-id">
            <a href="#">
                <i class="fa fa-calendar"></i>
                <spring:message code="table"></spring:message>
            </a>
        </li>
    </security:authorize>
</ul>
<section class="tab-content">
    <article id="pmi-chart-id" class="tab-pane fade in active">
        <div id="chartdiv" style="width:100%; height:400px;"></div>
    </article>
    <article id="pmi-table-id" class="tab-pane fade">
        <div>
            <label> <spring:message code='startdate'/>:</label> <input type="date" id="startDate"/>
            <label> <spring:message code='enddate'/>:</label> <input type="date" id="endDate"/>
            <a class="btn" href="#" onclick="queryBtn();">
                <i class="glyphicon glyphicon-zoom-in"></i>
                <span><spring:message code='common.query'/></span>
            </a>
        </div>
        <table id="flexigrid-id"></table>
    </article>
</section>
</body>
</html>
