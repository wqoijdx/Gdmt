(function ($) {
    $.fn.verification =  function (p){
        var operation = $.extend({},p);
        var  regexStr = $(this).attr("regex");
        if(regexStr){
            var regExp = new RegExp( regexStr, "i" );
            $(this).removeClass("ui-state-error")
            if(regExp.test($(this).val())){
                return true;
            }else {
                $(this).addClass("ui-state-error")
                $(this).focus();
                var tipInfo =  $(this).attr("tip");
                if(tipInfo){
                    var tipDom = $("<div></div>");
                    tipDom.text(tipInfo);
                    tipDom.appendTo($(this).parent());
                    setTimeout(function () {
                        tipDom.remove();
                    },1000);
                    // $(this).parent().append(tipDom);
                }

                return false;
            }

        }else {
            return true;
        }
    };


    $.fn.flexiData = function (p) {
        var operation = $.extend({
            index:0
        },p);
        var selectTrList = $(this).find("tr.trSelected");
        var columnList = [];
        $(selectTrList).each(function (index,item) {
            var ch = $(item).attr("ch");
            var trlist = ch.split('_FG$SP_');
            columnList[columnList.length] = trlist[operation.index];
        });
        return columnList;
    };
    $.fn.tableEditData = function (p) {
        var p = $.extend({
            width : 400,
            height:400,
            url:null,
            extParam:[],
            colModel : []
        },p);
        if(this.length < 1){
            return;
        }
        var t = this[0];
        t.p = p;

        var table = $("<table></table>");

        $(table).css("width",p.width + "px");
        var thTR = $("<tr></tr>");
        thTR.addClass("gdmt-tb-th");
        for(var index in p.colModel){
            var th = $("<th></th>")
            var item = p.colModel[index];
            th.text(item.display);
            th.css("width",item.width + "px");
            if(item.hide && item.hide === 'true'){
                th.css("display","none");
            }
            th.appendTo(thTR);
            //console.info(item);
        }
        thTR.appendTo(table);

        t.table = table;
        $(this).before(table);
    };
    $.fn.tableEditOptions = function (p) {
        if(this.length < 1){
            return;
        }
        var t = this[0];
        $.extend(t.p,p);
        return this;
    };

    $.fn.tableEditReload = function () {
        if(this.length < 1){
            return;
        }
        var that = this;
        var tableId = this.attr("id");

        var t = this[0];
        var p = t.p;
        that.html("");
        $.ajax({
            type: 'post',
            async: false,
            dataType : 'json',
            url: p.url,
            data: p.extParam,
            success: function (data) {
                var tr = $("<tr></tr>");
                $(data).each(function (key,itemTr) {
                    tr = $("<tr></tr>");
                    if((key % 2) == 0){
                        tr.addClass("table-erow");
                    }
                    var trid = tableId + "-tr-" + key;
                    tr.attr("id",trid);
                    for(var index in p.colModel){
                        var td = $("<td></td>")
                        var itemCl = p.colModel[index];
                        var vl = itemTr[itemCl.name];

                        if(itemCl.process) {
                            td.html(itemCl.process(vl, trid, itemTr));
                        }else if(itemCl.dom){
                            itemCl.dom(td,vl, trid, itemTr)
                        }else{
                            var span = $("<span></span>");
                            span.text(vl);
                            span.appendTo(td);
                            span.addClass("read");

                            var input=$("<input type='text'>");
                            input.attr("name",itemCl.name);
                            input.appendTo(td);
                            input.val(vl);
                            input.addClass("edit");
                        }


                        td.css("width",itemCl.width + "px");
                        if(itemCl.hide && itemCl.hide === 'true'){
                            td.css("display","none");
                        }
                        td.appendTo(tr);
                    }
                    tr.appendTo(that);
                });

                tr = $("<tr></tr>");
                tr.addClass("table-edit-tr");
                var key = data.length + 1;
                var trid = tableId + "-tr-" + key;
                tr.attr("id",trid);
                for(var index in p.colModel){
                    var td = $("<td></td>")
                    var itemCl = p.colModel[index];

                    if(itemCl.process){
                        td.html(itemCl.process('',trid,null));
                    }else if(itemCl.dom){
                        itemCl.dom(td,'', trid, null)
                    }else{

                        var input=$("<input type='text'>");
                        input.attr("name",itemCl.name);
                        input.appendTo(td);
                        input.addClass("edit");
                    }
                    td.css("width",itemCl.width + "px");
                    if(itemCl.hide && itemCl.hide === 'true'){
                        td.css("display","none");
                    }
                    td.appendTo(tr);
                }
                tr.appendTo(that);
            },
            error: function(msg){
                message(' <spring:message code="common.error"></spring:message>');
            }
        });

        return this;
    };

    $.fn.cloneObj = function () {
        var objList = this;
        var newList = objList.clone();
        for(var i = 0 ; i  < objList.length; i ++){
             $(newList[i]).val($(objList[i]).val());
             $(newList[i]).removeAttr("disabled");
         }
        return newList;
    }



})(jQuery);