#database & user

use mysql;

#delete database & user
drop database if exists  cldk_data;
#delete from user where user='cldk_data';
#drop user if exists  cldk_data;
flush privileges;

#create database & user
create database  cldk_data default character set utf8 collate utf8_general_ci;
grant all on  cldk_data.* to 'cldk_data'@'localhost' identified by 'Huawei_123' with grant option;
grant all on  cldk_data.* to 'cldk_data'@'%' identified by 'Huawei_123' with grant option;

flush privileges;
use  cldk_data;

-- ----------------------------
-- Table structure for `pt_dictionary`
-- ----------------------------
DROP TABLE IF EXISTS `pt_dictionary`;
CREATE TABLE `pt_dictionary` (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_parent_id` bigint(20) DEFAULT '0',
  `dict_group` varchar(64) NOT NULL COMMENT '字典分组',
  `dic_code` varchar(32) NOT NULL COMMENT '代码',
  `description` varchar(64) DEFAULT NULL COMMENT '备注',
  `dic_sort` int(11) DEFAULT NULL COMMENT '显示排序',
  PRIMARY KEY (`dict_id`)
) ENGINE=InnoDB AUTO_INCREMENT=303 DEFAULT CHARSET=utf8 COMMENT='字典数据表';

-- ----------------------------
-- Records of pt_dictionary
-- ----------------------------
INSERT INTO `pt_dictionary` VALUES ('5', '0', 'sex', '0', '女', '1');
INSERT INTO `pt_dictionary` VALUES ('6', '0', 'sex', '1', '男', '2');
INSERT INTO `pt_dictionary` VALUES ('7', '0', 'group', '0', '全部', '1');
INSERT INTO `pt_dictionary` VALUES ('8', '0', 'group', '1', '区组', '2');
INSERT INTO `pt_dictionary` VALUES ('9', '0', 'menu.https', '0', '0 http', '1');
INSERT INTO `pt_dictionary` VALUES ('10', '0', 'menu.https', '1', '1 https', '2');
INSERT INTO `pt_dictionary` VALUES ('11', '0', 'organ.type', '0', '组织类型  集团侧', '1');
INSERT INTO `pt_dictionary` VALUES ('12', '0', 'organ.type', '1', '组织类型  阳光专区供应商', '2');
INSERT INTO `pt_dictionary` VALUES ('13', '0', 'organ.type', '2', '组织类型  电厂侧', '3');
INSERT INTO `pt_dictionary` VALUES ('14', '0', 'organ.type', '3', '组织类型  分公司侧', '4');
INSERT INTO `pt_dictionary` VALUES ('15', '0', 'menu.level', '1', '一级菜单', '1');
INSERT INTO `pt_dictionary` VALUES ('16', '0', 'menu.level', '2', '二级菜单', '2');
INSERT INTO `pt_dictionary` VALUES ('17', '0', 'menu.level', '3', '三级菜单', '3');
INSERT INTO `pt_dictionary` VALUES ('18', '0', 'menu.level', '4', '四级菜单', '4');
INSERT INTO `pt_dictionary` VALUES ('110', '0', 'file.type', '0', '文件', '1');
INSERT INTO `pt_dictionary` VALUES ('111', '0', 'file.type', '1', '文件夹', '2');
INSERT INTO `pt_dictionary` VALUES ('201', '0', 'menu.type', '0', '菜单目录', '1');
INSERT INTO `pt_dictionary` VALUES ('202', '0', 'menu.type', '1', '菜单', '2');
INSERT INTO `pt_dictionary` VALUES ('203', '0', 'menu.type', '2', '功能', '3');
INSERT INTO `pt_dictionary` VALUES ('301', '0', 'lang.type', '1', '英文', '1');
INSERT INTO `pt_dictionary` VALUES ('302', '0', 'lang.type', '2', '中文', '2');

-- ----------------------------
-- Table structure for `pt_dictionary_i18n`
-- ----------------------------
DROP TABLE IF EXISTS `pt_dictionary_i18n`;
CREATE TABLE `pt_dictionary_i18n` (
  `dict_id` bigint(20) NOT NULL COMMENT '字典编码',
  `dict_name` varchar(64) DEFAULT NULL COMMENT '字典名称',
  `language_id` bigint(20) NOT NULL COMMENT '语言ID',
  PRIMARY KEY (`dict_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典数据国际化表';

-- ----------------------------
-- Records of pt_dictionary_i18n
-- ----------------------------
INSERT INTO `pt_dictionary_i18n` VALUES ('5', 'female', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('5', '女', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('6', 'male', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('6', '男', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('7', 'all', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('7', '全部', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('8', 'group', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('8', '区组', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('9', 'http', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('9', 'http', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('10', 'https', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('10', 'https', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('11', 'Group side', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('11', '集团侧', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('12', 'Suppliers of sunshine zone', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('12', '阳光专区供应商', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('13', 'Power plant side', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('13', '电厂侧', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('14', 'Branch side', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('14', '分公司侧', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('15', 'level 1', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('15', '1 级', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('16', 'level 2', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('16', '2 级', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('17', 'level 3', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('17', '3 级', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('18', 'level 4', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('18', '4 级', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('110', 'fileName', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('110', '文件名', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('111', 'dir', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('111', '文件夹', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('201', 'dir', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('201', '目录', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('202', 'menu', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('202', '菜单', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('203', 'fuction', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('203', '功能', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('301', 'english', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('301', '英文', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('302', 'chinese', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('302', '中文', '2');

-- ----------------------------
-- Table structure for `pt_file`
-- ----------------------------
DROP TABLE IF EXISTS `pt_file`;
CREATE TABLE `pt_file` (
  `file_uuid` varchar(36) NOT NULL COMMENT '文件id',
  `parent_uuid` varchar(36) NOT NULL DEFAULT '0' COMMENT '父文件id',
  `file_type` int(11) NOT NULL DEFAULT '0' COMMENT '文件类型 0:文件 1:文件夹',
  `file_capacity` varchar(36) DEFAULT NULL COMMENT '文件容量',
  `file_name` varchar(100) NOT NULL COMMENT '文件名',
  `user_uuid` varchar(36) DEFAULT NULL COMMENT '用户id',
  `file_path` varchar(516) DEFAULT NULL COMMENT '用户id',
  `remarks` varchar(516) DEFAULT NULL COMMENT '备注',
  `MODTIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEL_FLAG` int(11) NOT NULL DEFAULT '0' COMMENT '是否删除标识 0:正常 1:删除',
  PRIMARY KEY (`file_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件信息表';

-- ----------------------------
-- Records of pt_file
-- ----------------------------
INSERT INTO `pt_file` VALUES ('0fdd5198-5455-44f1-a470-a170bb148eda', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '4871.44', '许嵩 - 两种悲剧.mp3', null, '分类1/许嵩 - 两种悲剧.mp3', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('11a2838d-0ddf-41e2-a7b6-ea1b415ed0fb', 'de425e53-3a26-4eda-8c70-f55b0f33ecbc', '0', '53.71', '00.png', null, '分类2/00.png', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('13a67756-e7fc-4e82-ac17-842e2d5a0248', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '227501.44', '软通大学 宣传1.mp4', null, '分类1/软通大学 宣传1.mp4', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('1420c7c5-af35-4b9f-9d02-253cf596cc67', '0', '0', '39.06', '网络工程师计算题_6分.docx', null, '网络工程师计算题_6分.docx', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('1518d87a-fd16-4360-be37-2dea900a1246', '0', '0', '8.76', 'zhixing.jpg', null, 'zhixing.jpg', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('17ce792f-3f0e-49fd-b845-38113a0ddec1', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '125.46', '实训的要求484848.docx', null, '分类1/实训的要求484848.docx', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('1bf2e58b-bcf0-4079-9b8f-ab05eb3f2c62', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '757.77', '晨会2021_08_18.doc', null, '分类1/晨会2021_08_18.doc', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('1fd94e88-ccb5-4280-a384-6a0ecdd54582', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '9663.34', '许嵩1 - 断桥残雪.mp3', null, '分类1/许嵩1 - 断桥残雪.mp3', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('24e3aeee-8f1c-4e03-bb97-65c30067f513', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '8.28', '碟.jpg', null, '分类1/碟.jpg', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('250115fd-7fcb-41c9-b801-38b1066c0ab5', 'de425e53-3a26-4eda-8c70-f55b0f33ecbc', '0', '5219.63', 'idea使用.docx', null, '分类2/idea使用.docx', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('2d0491d0-5e28-4507-a721-674dcfce2641', '0', '0', '47.29', '5.PNG', null, '5.PNG', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('2d4335cc-794b-4c39-be05-c517cf642097', '0', '0', '138.07', '8.18晨会截图 - 副本.png', null, '8.18晨会截图 - 副本.png', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('2dc14e0f-7a34-42c6-9a03-56bee8ae1833', '0', '0', '138.07', '8.18第二组晨会截图.png', null, '8.18第二组晨会截图.png', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('2eb7e983-f470-413a-981c-d652a95ea631', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '3425.83', '开环境搭建总结1.docx', null, '分类1/开环境搭建总结1.docx', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('360bed89-c0f2-42a3-9106-0085c3b6286c', '0', '0', '44.98', '2.PNG', null, '2.PNG', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('38e4bc83-c070-471f-a55f-f147f561391a', '0', '0', '1.76', 'pt_cl.sql', null, 'pt_cl.sql', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('3ea212a0-0f48-467c-ad29-8830cea8aa45', '0', '1', '0.00', 'bbb1/', null, 'bbb1/', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('422f07ff-68f9-4066-af82-c06d5d4fd2c2', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '597.00', '新系统缴费方式(1).doc', null, '分类1/新系统缴费方式(1).doc', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('42e5f5c4-c9be-4240-a3b4-871854513392', '0', '0', '69.81', '1.txt', null, '1.txt', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('4439eba9-f816-42cf-af81-c809b7f27acd', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '2376.52', '视频2.mp4', null, '分类1/视频2.mp4', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('448308a4-dd6d-4464-ba81-054408b458c9', 'de425e53-3a26-4eda-8c70-f55b0f33ecbc', '0', '212.56', 'Thymeleaf入门.docx', null, '分类2/Thymeleaf入门.docx', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('44e9fc41-2b90-4706-9048-bc7573095209', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '801.24', '学生日志.doc', null, '分类1/学生日志.doc', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('4630a172-778f-4576-b28c-1121ea9b6716', '0', '0', '308.62', '交换机与路由器的配置1.docx', null, '交换机与路由器的配置1.docx', null, '2021-08-19 22:25:26', '0');
INSERT INTO `pt_file` VALUES ('4702f504-8add-46ee-825f-7b7bf5c18349', '0', '1', '0.00', '12.txt/', null, '12.txt/', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('486b520a-1d6a-491f-9ad4-6869e5204baa', '0', '0', '138.07', '8.18第二组晨会截图 - 副本.png', null, '8.18第二组晨会截图 - 副本.png', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('4b45c0d1-66c7-4a1d-93e2-df6ede261f4f', '0', '0', '44.98', '2 - 副本 (2) - 副本.PNG', null, '2 - 副本 (2) - 副本.PNG', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('4daf8504-3192-4c41-88c7-3a1691f5bb5b', '0', '0', '44.98', '2 - 副本 (3).PNG', null, '2 - 副本 (3).PNG', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('4e294a7a-5772-4003-adaf-8f27d53fbcfb', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '0.06', '陈梓杰.txt', null, '分类1/陈梓杰.txt', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('509807e4-2c1f-45d1-b82d-2c1e35011ca3', '0', '0', '61.06', '1.PNG', null, '1.PNG', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '1', '0.00', '分类1/', null, '分类1/', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('565012ef-9a46-4f4f-a34d-8911b21086c1', '0', '0', '56454.41', 'zhumu_0(1).mp4', null, 'zhumu_0(1).mp4', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('5a1109b9-02c8-41bc-bf97-eb254fb690d0', '0', '0', '44.98', '2 - 副本.PNG', null, '2 - 副本.PNG', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('5dd0f13a-ca28-49a2-9b38-424f35f7e5fd', '0', '0', '195.66', 'apache_shiro.docx', null, 'apache_shiro.docx', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('5f991d67-8450-4a47-a374-75516e62734c', 'de425e53-3a26-4eda-8c70-f55b0f33ecbc', '0', '212.56', 'Thymeleaf入.docx', null, '分类2/Thymeleaf入.docx', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('60d91be9-f8b9-4cea-b99b-49a656ccc75b', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '11968.73', '视频1.mp4', null, '分类1/视频1.mp4', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('68e3cbd6-3bd4-4ef6-9220-845de379c823', '0', '0', '98.17', '3.png', null, '3.png', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('6a181b4f-5bbf-4006-b8af-88a697a88cc5', 'e13b2873-3495-458b-9042-d7bb28fc35ee', '0', '862.90', 'obs-sdk-java-devag.pdf', null, 'aaa/obs-sdk-java-devag.pdf', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('71226d20-4943-4be9-ab53-fab64d415cc9', '53ae4867-1301-46c6-b2d5-12760fee6d28', '1', '0.00', '张毅豪.txt/', null, '分类1/张毅豪.txt/', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('7241561a-7672-4522-b18e-f67d113be78f', '0', '0', '322.55', 'walj.png', null, 'walj.png', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('7397ae8f-04b0-443b-8531-d8c26d11b576', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '3.96', 't.java', null, '分类1/t.java', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('86aadad4-6134-4c8c-ad83-46f85f397ffd', '0', '0', '141.29', 'pt_user.sql', null, 'pt_user.sql', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('8723744f-9205-41ba-ad89-0cf488a887bb', '0', '0', '7965.32', '许嵩22 - 南山忆.mp3', null, '许嵩22 - 南山忆.mp3', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('883be6d2-427b-49a9-9a3d-91ca94a40a65', '0', '0', '136.02', 'flexigrid_2.docx', null, 'flexigrid_2.docx', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('8ec08428-0fef-43d2-9b7f-100ffc782c73', 'de425e53-3a26-4eda-8c70-f55b0f33ecbc', '0', '0.29', '4.txt', null, '分类2/4.txt', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('94ce4bd7-11c4-4d4d-9d08-055610cd0b59', 'de425e53-3a26-4eda-8c70-f55b0f33ecbc', '0', '12.89', '18.png', null, '分类2/18.png', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('98c07e46-7f8e-4bdb-8aff-5e75391b737d', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '4303.73', '李梦尹 - 突然好你.mp3', null, '分类1/李梦尹 - 突然好你.mp3', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('9ba7fa6b-e670-48ba-bbad-2660fc0cbff8', '0', '0', '120039.77', 'zhumu_0(33).mp4', null, 'zhumu_0(33).mp4', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('a2815073-f840-4024-9b4c-3e4f81605e6f', '0', '0', '3425.83', '开环境搭建总结.docx', null, '开环境搭建总结.docx', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('ada5dc81-f4cb-4e85-9989-187ef55b8ea1', 'de425e53-3a26-4eda-8c70-f55b0f33ecbc', '0', '336.40', '5.png', null, '分类2/5.png', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('b05252f2-d8ac-4d99-9cbb-d469443ed1ef', '0', '0', '44.98', '2 - 副本 (2).PNG', null, '2 - 副本 (2).PNG', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('b1977e4b-2172-4ceb-a39a-f411e3cfd33b', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '122.70', '实训的要求哈哈哈啊哈哈.docx', null, '分类1/实训的要求哈哈哈啊哈哈.docx', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('b2b06560-a20a-4a9c-87a0-1e9f9cf29fed', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '83.74', '电子龙次代星.jpg', null, '分类1/电子龙次代星.jpg', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('b3cb81af-3612-4b8f-8f38-d4afce616f5a', '0', '0', '2.02', 'gdp.sql', null, 'gdp.sql', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('b6cb3bd0-abec-418c-b4b9-a843fe1ead5a', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '862.90', 'obs-sdk-java-devag.pdf', null, '分类1/obs-sdk-java-devag.pdf', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('cb14b75c-0755-4b5f-91a9-27a766abd362', '53ae4867-1301-46c6-b2d5-12760fee6d28', '1', '0.00', '新建文本文档.txt/', null, '分类1/新建文本文档.txt/', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('d1f7ae2a-2434-4c5b-b6ba-5c0c57ab459f', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '0.57', '测试.txt', null, '分类1/测试.txt', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('dc44b8cc-577b-406d-808b-424fe8d7cab9', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '2448.14', '贝瓦儿歌 - 小小兔子乖乖.mp3', null, '分类1/贝瓦儿歌 - 小小兔子乖乖.mp3', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('de425e53-3a26-4eda-8c70-f55b0f33ecbc', '0', '1', '0.00', '分类2/', null, '分类2/', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('e13b2873-3495-458b-9042-d7bb28fc35ee', '0', '1', '0.00', 'aaa/', null, 'aaa/', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('e336aa2b-a405-4495-ac70-84b72c7b8c60', '0', '0', '322.55', 'walj - 副本 - 副本.png', null, 'walj - 副本 - 副本.png', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('e6532cc4-d221-4928-9dd9-73ae511f8740', '0', '0', '130.44', 'flexigrid.docx', null, 'flexigrid.docx', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('e80706da-19a8-4603-a3c3-30563548e420', 'de425e53-3a26-4eda-8c70-f55b0f33ecbc', '0', '116.50', '8.png', null, '分类2/8.png', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('e884d354-3b73-49c2-a980-08cb1128092e', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '2821.17', 'java基础编程s2.docx', null, '分类1/java基础编程s2.docx', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('e8f9091c-548b-4582-a3e1-28091151e55b', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '0.02', '徐泽群1810506226.txt', null, '分类1/徐泽群1810506226.txt', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('eeb15585-3053-4665-898d-f3b4db3527d7', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '14.13', '晨会2021_08_06.docx', null, '分类1/晨会2021_08_06.docx', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('f08f1fc2-ce17-4770-a095-96c49b172947', 'e13b2873-3495-458b-9042-d7bb28fc35ee', '0', '5710.08', '基于centos 7操作系统 linuxa是编程.docx', null, 'aaa/基于centos 7操作系统 linuxa是编程.docx', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('f0f8351a-1935-467b-93ba-9ca15a132e93', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '19489.29', '听德纲网_053《全德报》岳云鹏 孙越1.mp3', null, '分类1/听德纲网_053《全德报》岳云鹏 孙越1.mp3', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('f8d38088-2f7b-4f92-8c37-954317fd1909', '53ae4867-1301-46c6-b2d5-12760fee6d28', '0', '0.02', '改名袁圆.txt', null, '分类1/改名袁圆.txt', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('fbfc2f05-f7c5-4e23-8664-5cfa8854b0d0', '0', '0', '322.55', 'walj - 副本.png', null, 'walj - 副本.png', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('fd0c5176-a5c7-45fd-9fda-433981378be8', '0', '0', '125.46', '实训的要求.docx', null, '实训的要求.docx', null, '2021-08-19 22:23:59', '0');
INSERT INTO `pt_file` VALUES ('feaa30b2-2c0c-4281-a289-0d59a81e5c50', '3ea212a0-0f48-467c-ad29-8830cea8aa45', '0', '61.06', '1.PNG', null, 'bbb1/1.PNG', null, '2021-08-19 22:23:59', '0');

-- ----------------------------
-- Table structure for `pt_language`
-- ----------------------------
DROP TABLE IF EXISTS `pt_language`;
CREATE TABLE `pt_language` (
  `lang_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '语言ID',
  `lang_type` varchar(30) DEFAULT NULL COMMENT '语言类型',
  `lang_desc` varchar(20) DEFAULT NULL COMMENT '语言描述',
  PRIMARY KEY (`lang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='语言表';

-- ----------------------------
-- Records of pt_language
-- ----------------------------
INSERT INTO `pt_language` VALUES ('1', 'en', 'English');
INSERT INTO `pt_language` VALUES ('2', 'zh', '汉语');

-- ----------------------------
-- Table structure for `pt_menu`
-- ----------------------------
DROP TABLE IF EXISTS `pt_menu`;
CREATE TABLE `pt_menu` (
  `menu_id` varchar(32) NOT NULL,
  `menu_name` varchar(32) DEFAULT NULL COMMENT '菜单名称',
  `parent_id` varchar(32) DEFAULT NULL COMMENT '父菜单ID',
  `menu_type` int(10) DEFAULT NULL COMMENT '菜单类型，0:菜单目录 1:菜单 2:页面功能',
  `menu_level` int(11) DEFAULT NULL COMMENT '菜单目录级数 1 2 3',
  `menu_url` varchar(256) DEFAULT NULL COMMENT '菜单url',
  `privilege_code` varchar(32) DEFAULT NULL COMMENT '菜单权限',
  `is_https` int(11) DEFAULT NULL,
  `menu_icon` varchar(255) DEFAULT NULL COMMENT '菜单图标类名',
  `menu_seq` varchar(10) DEFAULT NULL COMMENT '排序',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pt_menu
-- ----------------------------
INSERT INTO `pt_menu` VALUES ('1000', '业务管理', '0', '0', '1', '', '', '0', 'glyphicon glyphicon-align-justify', '2', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('1100', '文件管理', '1000', '0', '2', '', '', '0', 'fa fa-credit-card', '1', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('1110', '全部文件', '1100', '1', '3', 'disk/initPage', 'cldk_data_show_file', '0', 'fa fa-file-text-o', '1', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('1111', '文件上传', '1110', '2', '4', null, 'cldk_file_add', '0', null, null, '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('1112', '删除文件', '1110', '2', '4', null, 'cldk_file_del', '0', null, null, '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('1120', '回收站', '1100', '1', '3', 'recycle/file', 'cldk_data_offer', '0', 'fa fa-trash-o', '1', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('1130', '收到分享', '1100', '0', '3', 'quoteinfo/page', 'cldk_data_quote', '0', 'fa fa-location-arrow', '1', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('1140', '我的收藏', '1100', '1', '3', 'gdpinfo/page', 'cldk_data_gdp', '0', 'fa fa-star-half-o', '1', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('1150', '我的分享', '1100', '1', '3', 'sto/page', 'cldk_data_sto', '0', 'fa fa-paperclip', '1', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('2000', '系统管理', '0', '0', '1', 'temp.jsp', '', '0', 'glyphicon glyphicon-align-justify', '1', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('2100', '后台管理', '2000', '0', '2', '', '', '0', 'fa fa-credit-card', '2', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('2110', '角色管理', '2100', '0', '3', 'role/rolePage', 'cldk_data_role', '0', 'fa fa-file-text-o', '6', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('2111', '角色查询', '2110', '2', '4', null, 'cldk_data_role_query', '0', null, '7', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('2112', '角色增加', '2110', '2', '4', null, 'cldk_data_role_add', '0', null, '8', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('2113', '角色修改', '2110', '2', '4', null, 'cldk_data_role_mod', '0', null, '9', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('2114', '分配权限', '2110', '2', '4', null, 'cldk_data_role_menu', '0', null, '10', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('2115', '角色删除', '2110', '2', '4', null, 'cldk_data_role_del', '0', null, '11', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('2120', '用户管理', '2100', '0', '3', 'user/userPage', 'cldk_data_user', '0', 'fa fa-file-text-o', '0', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('2121', '用户查询', '2120', '2', '4', null, 'cldk_data_user_query', '0', null, '1', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('2122', '用户增加', '2120', '2', '4', null, 'cldk_data_user_add', '0', null, '2', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('2123', '用户修改', '2120', '2', '4', null, 'cldk_data_user_mod', '0', null, '3', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('2124', '用户删除', '2120', '2', '4', null, 'cldk_data_user_del', '0', null, '4', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('2125', '角色分配', '2120', '2', '4', null, 'cldk_data_user_role', '0', null, '5', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('2200', '系统配置', '2000', '0', '2', '', '', '0', 'fa fa-credit-card', '1', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('2210', '菜单', '2200', '0', '3', 'menu/menuPage', 'cldk_data_menu', '0', 'fa fa-file-text-o', '', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('2220', '配置', '2200', '0', '3', 'temp.jsp', 'cldk_data_config', '0', 'fa fa-file-text-o', '', '2021-09-03 11:21:05');
INSERT INTO `pt_menu` VALUES ('2230', '日志', '2200', '0', '3', 'temp.jsp', 'cldk_data_log', '0', 'fa fa-file-text-o', '', '2021-09-03 11:21:05');

-- ----------------------------
-- Table structure for `pt_menu_i18n`
-- ----------------------------
DROP TABLE IF EXISTS `pt_menu_i18n`;
CREATE TABLE `pt_menu_i18n` (
  `menu_id` varchar(32) NOT NULL DEFAULT '0' COMMENT '菜单ID',
  `lang_id` int(10) NOT NULL DEFAULT '0' COMMENT '语言ID',
  `menu_name` varchar(64) DEFAULT NULL COMMENT '国际化 菜单',
  PRIMARY KEY (`menu_id`,`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='国际化表';

-- ----------------------------
-- Records of pt_menu_i18n
-- ----------------------------
INSERT INTO `pt_menu_i18n` VALUES ('1000', '1', 'serviceManager');
INSERT INTO `pt_menu_i18n` VALUES ('1000', '2', '业务管理');
INSERT INTO `pt_menu_i18n` VALUES ('1100', '1', 'fileManager');
INSERT INTO `pt_menu_i18n` VALUES ('1100', '2', '文件管理');
INSERT INTO `pt_menu_i18n` VALUES ('1110', '1', 'AllFile');
INSERT INTO `pt_menu_i18n` VALUES ('1110', '2', '全部文件');
INSERT INTO `pt_menu_i18n` VALUES ('1120', '1', 'recycleBin');
INSERT INTO `pt_menu_i18n` VALUES ('1120', '2', '回收站');
INSERT INTO `pt_menu_i18n` VALUES ('1130', '1', 'receiveShare');
INSERT INTO `pt_menu_i18n` VALUES ('1130', '2', '收到分享');
INSERT INTO `pt_menu_i18n` VALUES ('1140', '1', 'myCollection');
INSERT INTO `pt_menu_i18n` VALUES ('1140', '2', '我的收藏');
INSERT INTO `pt_menu_i18n` VALUES ('1150', '1', 'myShare');
INSERT INTO `pt_menu_i18n` VALUES ('1150', '2', '我的分享');
INSERT INTO `pt_menu_i18n` VALUES ('2000', '1', 'management');
INSERT INTO `pt_menu_i18n` VALUES ('2000', '2', '系统管理');
INSERT INTO `pt_menu_i18n` VALUES ('2100', '1', 'userRole');
INSERT INTO `pt_menu_i18n` VALUES ('2100', '2', '后台管理');
INSERT INTO `pt_menu_i18n` VALUES ('2110', '1', 'role');
INSERT INTO `pt_menu_i18n` VALUES ('2110', '2', '角色管理');
INSERT INTO `pt_menu_i18n` VALUES ('2120', '1', 'user');
INSERT INTO `pt_menu_i18n` VALUES ('2120', '2', '用户管理');
INSERT INTO `pt_menu_i18n` VALUES ('2200', '1', 'system');
INSERT INTO `pt_menu_i18n` VALUES ('2200', '2', '系统配置');
INSERT INTO `pt_menu_i18n` VALUES ('2210', '1', 'menu');
INSERT INTO `pt_menu_i18n` VALUES ('2210', '2', '菜单');
INSERT INTO `pt_menu_i18n` VALUES ('2220', '1', 'config');
INSERT INTO `pt_menu_i18n` VALUES ('2220', '2', '配置');
INSERT INTO `pt_menu_i18n` VALUES ('2230', '1', 'log');
INSERT INTO `pt_menu_i18n` VALUES ('2230', '2', '日志');

-- ----------------------------
-- Table structure for `pt_role`
-- ----------------------------
DROP TABLE IF EXISTS `pt_role`;
CREATE TABLE `pt_role` (
  `role_uuid` varchar(255) NOT NULL COMMENT '角色资源ID',
  `role_name` varchar(32) DEFAULT NULL COMMENT '角色名称',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of pt_role
-- ----------------------------
INSERT INTO `pt_role` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '管理员', '管理员');
INSERT INTO `pt_role` VALUES ('1111', '普通人员', '普通人员');
INSERT INTO `pt_role` VALUES ('25f79a2bced711eabd63f430b915d5f2', '研发人员', '研发人员');
INSERT INTO `pt_role` VALUES ('33c25a3e003f48a397ef65c223009d26', '项目经理', '项目经理');
INSERT INTO `pt_role` VALUES ('3995161cced711eabd63f430b915d5f2', '测试人员', '测试人员');
INSERT INTO `pt_role` VALUES ('4a323fb1ceeb11eabd63f430b915d5f2', '资料', '资料');

-- ----------------------------
-- Table structure for `pt_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `pt_role_menu`;
CREATE TABLE `pt_role_menu` (
  `ROLE_UUID` varchar(255) NOT NULL COMMENT '角色资源ID',
  `menu_id` int(20) NOT NULL DEFAULT '0' COMMENT '菜单ID',
  PRIMARY KEY (`ROLE_UUID`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色资源配置';

-- ----------------------------
-- Records of pt_role_menu
-- ----------------------------
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2000');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2100');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2110');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2111');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2112');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2113');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2114');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2115');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2120');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2121');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2122');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2123');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2124');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2125');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2200');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2210');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2220');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2230');
INSERT INTO `pt_role_menu` VALUES ('1111', '1000');
INSERT INTO `pt_role_menu` VALUES ('1111', '1100');
INSERT INTO `pt_role_menu` VALUES ('1111', '1110');
INSERT INTO `pt_role_menu` VALUES ('1111', '1111');
INSERT INTO `pt_role_menu` VALUES ('4a323fb1ceeb11eabd63f430b915d5f2', '2000');
INSERT INTO `pt_role_menu` VALUES ('4a323fb1ceeb11eabd63f430b915d5f2', '2200');
INSERT INTO `pt_role_menu` VALUES ('4a323fb1ceeb11eabd63f430b915d5f2', '2210');
INSERT INTO `pt_role_menu` VALUES ('4a323fb1ceeb11eabd63f430b915d5f2', '2220');
INSERT INTO `pt_role_menu` VALUES ('4a323fb1ceeb11eabd63f430b915d5f2', '2230');

-- ----------------------------
-- Table structure for `pt_role_user`
-- ----------------------------
DROP TABLE IF EXISTS `pt_role_user`;
CREATE TABLE `pt_role_user` (
  `USER_UUID` varchar(255) NOT NULL,
  `ROLE_UUID` varchar(255) NOT NULL,
  PRIMARY KEY (`USER_UUID`,`ROLE_UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色表';

-- ----------------------------
-- Records of pt_role_user
-- ----------------------------
INSERT INTO `pt_role_user` VALUES ('001fddf8608c4cd1bbac81dde4e0c701', '0');
INSERT INTO `pt_role_user` VALUES ('001fddf8608c4cd1bbac81dde4e0c701', '10ac1faf2f0c4e8c84a8366178699c8c');
INSERT INTO `pt_role_user` VALUES ('02d7938e06934f48b92baec5348a8609', '0');
INSERT INTO `pt_role_user` VALUES ('02d7938e06934f48b92baec5348a8609', '10ac1faf2f0c4e8c84a8366178699c8c');
INSERT INTO `pt_role_user` VALUES ('0311e42abab4467a80aefd2ebd18ab19', '0');
INSERT INTO `pt_role_user` VALUES ('0311e42abab4467a80aefd2ebd18ab19', '4a323fb1ceeb11eabd63f430b915d5f2');

-- ----------------------------
-- Table structure for `pt_security_log`
-- ----------------------------
DROP TABLE IF EXISTS `pt_security_log`;
CREATE TABLE `pt_security_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `user_uuid` varchar(255) NOT NULL,
  `log_system` int(11) NOT NULL COMMENT '子系统',
  `log_module` int(11) NOT NULL COMMENT '模块',
  `log_action` int(11) NOT NULL COMMENT '操作动作',
  `log_contents` varchar(256) NOT NULL COMMENT '操作内容',
  `create_time` datetime NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='安全日志';

-- ----------------------------
-- Records of pt_security_log
-- ----------------------------

-- ----------------------------
-- Table structure for `pt_sys_config`
-- ----------------------------
DROP TABLE IF EXISTS `pt_sys_config`;
CREATE TABLE `pt_sys_config` (
  `cfg_id` int(11) NOT NULL AUTO_INCREMENT,
  `cfg_name` varchar(64) NOT NULL,
  `cfg_value` varchar(64) NOT NULL,
  `remarks` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`cfg_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of pt_sys_config
-- ----------------------------
INSERT INTO `pt_sys_config` VALUES ('20', 'isAuthCode', 'false', '开启登录的验证码');
INSERT INTO `pt_sys_config` VALUES ('21', 'siteLanguage', 'zh', '默认语方类型');

-- ----------------------------
-- Table structure for `pt_user`
-- ----------------------------
DROP TABLE IF EXISTS `pt_user`;
CREATE TABLE `pt_user` (
  `USER_UUID` varchar(255) NOT NULL,
  `USER_NAME` varchar(32) DEFAULT NULL COMMENT '登录名',
  `PASSWORD` varchar(32) DEFAULT NULL COMMENT '密码',
  `EMAIL` varchar(32) DEFAULT NULL COMMENT '邮件地址',
  `MOBILE` varchar(255) DEFAULT NULL COMMENT '电话',
  `NICE_NAME` varchar(64) DEFAULT NULL COMMENT '真实姓名',
  `REGISTERDATE` datetime DEFAULT NULL COMMENT '注册日期',
  `REMARK` varchar(512) DEFAULT NULL COMMENT '备注',
  `DEL_FLAG` int(11) NOT NULL COMMENT '是否删除标识 0:正常 1:删除',
  `MODTIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`USER_UUID`),
  UNIQUE KEY `mobileIndex` (`MOBILE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pt_user
-- ----------------------------
INSERT INTO `pt_user` VALUES ('001fddf8608c4cd1bbac81dde4e0c701', 'zhaowei', 'ae00cfce6cf6566c7dc53015ca184c1a', 'zhaowei@isoftstone.com', '13131363553', '赵伟', '2020-07-30 16:16:20', null, '0', '2020-07-30 16:20:25');
INSERT INTO `pt_user` VALUES ('02d7938e06934f48b92baec5348a8609', 'yanwenyi', 'ae00cfce6cf6566c7dc53015ca184c1a', 'yanwenyi@isoftstone.com', '13897145171', '严文毅', '2020-07-30 16:16:20', null, '0', '2020-07-30 16:20:25');
INSERT INTO `pt_user` VALUES ('0311e42abab4467a80aefd2ebd18ab19', 'zhangjifang', 'ae00cfce6cf6566c7dc53015ca184c1a', 'zhangjifang@isoftstone.com', '13124524956', '张吉方', '2020-07-30 16:16:20', null, '0', '2020-07-30 16:20:25');
INSERT INTO `pt_user` VALUES ('04325342590745fdad116befc6e812a8', 'lijuan', 'ae00cfce6cf6566c7dc53015ca184c1a', 'lijuan@isoftstone.com', '13213283541', '李娟', '2020-07-30 16:16:20', null, '0', '2020-07-30 16:20:25');
INSERT INTO `pt_user` VALUES ('1c5bd24fe2e54b8daee18ce10a92c4a2', 'yueheng', 'ae00cfce6cf6566c7dc53015ca184c1a', 'yueheng@isoftstone.com', '13212705634', '岳衡', '2020-07-30 16:16:20', null, '0', '2020-07-30 16:20:25');
INSERT INTO `pt_user` VALUES ('1cb3253577f948e3b4287a8014bda341', 'wangpu', 'ae00cfce6cf6566c7dc53015ca184c1a', 'wangpu@isoftstone.com', '13126495609', '王璞', '2020-07-30 16:16:20', null, '0', '2020-07-30 16:20:25');
INSERT INTO `pt_user` VALUES ('999', 'admin', 'ae00cfce6cf6566c7dc53015ca184c1a', 'admin@isoftstone.com', '13461106251', '管理员', '2020-07-30 16:16:20', null, '0', '2020-07-30 16:20:25');
DROP TRIGGER IF EXISTS `pt_user_create_trigger`;
DELIMITER ;;
CREATE TRIGGER `pt_user_create_trigger` BEFORE INSERT ON `pt_user` FOR EACH ROW BEGIN
	if (NEW.USER_UUID = '' or NEW.USER_UUID is null)  then
		set NEW.USER_UUID = REPLACE(UUID(),'-','');
  end if;
END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `pt_user_delete_trigger`;
DELIMITER ;;
CREATE TRIGGER `pt_user_delete_trigger` BEFORE DELETE ON `pt_user` FOR EACH ROW BEGIN
 DECLARE cout int(11);
    set cout=(select count(1) from  pt_role_user where USER_UUID = OLD.USER_UUID);
    if( cout > 0 ) THEN
         set @tip_msg =  CONCAT('角色授权正在使用:',OLD.USER_NAME);
         signal sqlstate 'ERROR' set message_text =  @tip_msg ;
      end if;
END
;;
DELIMITER ;
