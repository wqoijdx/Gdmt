package com.isoftstone.gdmt.consult.service.impl;

import com.isoftstone.gdmt.consult.entity.SearchStoEntity;
import com.isoftstone.gdmt.consult.repository.StoDao;
import com.isoftstone.gdmt.consult.service.StoService;
import com.isoftstone.gdmt.mybatis.entity.CementProductionEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("stoService")
public class StoServiceImpl implements StoService {
    @Resource
    private StoDao stoDao;

    @Override
    public List<CementProductionEntity> getSto() {
        return stoDao.getSto();
    }

    @Override
    public PadingRstType<CementProductionEntity> getStoInfolistByPage(SearchStoEntity entity, PagingBean paging) {
        paging.deal(CementProductionEntity.class);
        PadingRstType<CementProductionEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(paging.getPage());
        List<CementProductionEntity> list = stoDao.queryStoInfoByPage(entity,paging);
        padingRstType.setRawRecords(list);
        Integer total = stoDao.queryStoInfoTotal(entity);
        padingRstType.setTotal(total);
        padingRstType.putItems();
        return padingRstType;
    }

}
