package com.isoftstone.gdmt.consult.service;


import com.isoftstone.gdmt.consult.entity.SearchQuoteEntity;
import com.isoftstone.gdmt.mybatis.entity.InternationalQuoteEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;

import java.util.List;

public interface QuoteService {
    List<InternationalQuoteEntity> getQuote();

    PadingRstType<InternationalQuoteEntity> getQuoteInfolistByPage(SearchQuoteEntity entity, PagingBean paging);


}