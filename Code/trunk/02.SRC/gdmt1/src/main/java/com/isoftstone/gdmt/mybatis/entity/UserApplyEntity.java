package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class UserApplyEntity implements Serializable
{
	@Columns("apply_id")
	private Integer applyId;

	@Columns("user_id")
	private String userId;

	@Columns("user_name")
	private String userName;

	@Columns("old_department")
	private String oldDepartment;

	@Columns("new_department")
	private String newDepartment;

	@Columns("change_reason")
	private String changeReason;
	
	public Integer getApplyId() 
	{
		return applyId;
	}
	
	public void setApplyId(Integer applyId) 
	{
		this.applyId = applyId;
	}
	
	public String getUserId() 
	{
		return userId;
	}
	
	public void setUserId(String userId) 
	{
		this.userId = userId;
	}
	
	public String getUserName() 
	{
		return userName;
	}
	
	public void setUserName(String userName) 
	{
		this.userName = userName;
	}
	
	public String getOldDepartment() 
	{
		return oldDepartment;
	}
	
	public void setOldDepartment(String oldDepartment) 
	{
		this.oldDepartment = oldDepartment;
	}
	
	public String getNewDepartment() 
	{
		return newDepartment;
	}
	
	public void setNewDepartment(String newDepartment) 
	{
		this.newDepartment = newDepartment;
	}
	
	public String getChangeReason() 
	{
		return changeReason;
	}
	
	public void setChangeReason(String changeReason) 
	{
		this.changeReason = changeReason;
	}

}
