package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class IntegraldailEntity implements Serializable
{
	@Columns("id")
	private Integer id;

	@Columns("member_id")
	private String memberId;

	@Columns("integral")
	private Double integral;

	@Columns("update_time")
	private String updateTime;
	
	public Integer getId() 
	{
		return id;
	}
	
	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public String getMemberId() 
	{
		return memberId;
	}
	
	public void setMemberId(String memberId) 
	{
		this.memberId = memberId;
	}
	
	public Double getIntegral() 
	{
		return integral;
	}
	
	public void setIntegral(Double integral) 
	{
		this.integral = integral;
	}
	
	public String getUpdateTime() 
	{
		return updateTime;
	}
	
	public void setUpdateTime(String updateTime) 
	{
		this.updateTime = updateTime;
	}

}
