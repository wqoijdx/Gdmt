package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class GdpEntity implements Serializable
{
	@Columns("my_date")
	private String myDate;

	@Columns("gdp_value")
	private Double gdpValue;

	@Columns("gdp_up")
	private Double gdpUp;

	@Columns("gdp_id")
	private Integer gdpId;
	
	public String getMyDate() 
	{
		return myDate;
	}
	
	public void setMyDate(String myDate) 
	{
		this.myDate = myDate;
	}
	
	public Double getGdpValue() 
	{
		return gdpValue;
	}
	
	public void setGdpValue(Double gdpValue) 
	{
		this.gdpValue = gdpValue;
	}
	
	public Double getGdpUp() 
	{
		return gdpUp;
	}
	
	public void setGdpUp(Double gdpUp) 
	{
		this.gdpUp = gdpUp;
	}
	
	public Integer getGdpId() 
	{
		return gdpId;
	}
	
	public void setGdpId(Integer gdpId) 
	{
		this.gdpId = gdpId;
	}

}
