package com.isoftstone.gdmt.consult.repository;

import com.isoftstone.gdmt.consult.entity.SearchQuoteEntity;
import com.isoftstone.gdmt.mybatis.entity.InternationalQuoteEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface QuoteDao {
    List<InternationalQuoteEntity> getQuote();

    List<InternationalQuoteEntity> queryQuoteInfoByPage(@Param("entity") SearchQuoteEntity entity, @Param("paging") PagingBean paging);

    Integer queryQuoteInfoTotal(@Param("entity") SearchQuoteEntity entity);

}
