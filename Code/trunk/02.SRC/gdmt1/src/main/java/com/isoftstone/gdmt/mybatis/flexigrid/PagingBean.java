package com.isoftstone.gdmt.mybatis.flexigrid;

import com.isoftstone.gdmt.mybatis.annotation.Columns;

import java.io.Serializable;
import java.lang.reflect.Field;

public class PagingBean implements Serializable {
    /**
     * 当前页面
     */
    private Integer page = null;

    /**
     * 容量
     */
    private Integer rp = null;

    /**
     * 数据的开始
     */
    private Integer start = null;

    /**
     * 排序  asc
     */
    private String sortorder = null;

    /**
     * 按某个字段来排序
     */
    private String sortname = null;

    public Integer getPage() {

        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getRp() {
        return rp;
    }

    public void setRp(Integer rp) {
        this.rp = rp;
    }

    public Integer getStart() {
        if ((this.page != null) && (this.rp != null))
        {
            this.start = (Integer.valueOf(page) - 1) * Integer.valueOf(rp);
        }

        return start;
    }


    public String getSortorder() {
        return sortorder;
    }

    public void setSortorder(String sortorder) {
        this.sortorder = sortorder;
    }

    public String getSortname() {
        return sortname;
    }

    public void setSortname(String sortname) {
        this.sortname = sortname;
    }

    /**
     * 基于反射，根据需要排序的成员变量名，利用@Columns注解，得到对应的数据库表的字段名
     * @param <T>
     * @param class1
     */
     public <T>void deal(Class<T> class1)
    {
        if (sortname == null || "undefined".equals(sortname)) {
            return;
        }

        Field field = null;
        try {
            field = class1.getDeclaredField(sortname);
            if (field.isAnnotationPresent(Columns.class))
            {
                Columns columns = field.getAnnotation(Columns.class);
                sortname = columns.value();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

    }

    @Override
    public String toString() {
        return "PagingBean{" +
                "page=" + page +
                ", rp=" + rp +
                ", start=" + start +
                ", sortorder='" + sortorder + '\'' +
                ", sortname='" + sortname + '\'' +
                '}';
    }
}
