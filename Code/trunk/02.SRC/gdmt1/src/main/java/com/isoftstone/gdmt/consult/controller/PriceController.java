package com.isoftstone.gdmt.consult.controller;

import com.isoftstone.gdmt.consult.entity.SearchPriceEntity;
import com.isoftstone.gdmt.consult.service.PriceService;
import com.isoftstone.gdmt.mybatis.entity.InternationalPriceEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import javax.annotation.Resource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping({"/consult"})
public class PriceController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private PriceService priceService;

    @Secured("ROLE_gdmt_data_price")
    @RequestMapping({"/pricePage"})
    public String pricePage() {
        return "consult/pricePage";
    }

    @Secured("ROLE_gdmt_price_chart")
    @RequestMapping({"/getPrice"})
    @ResponseBody
    public List<InternationalPriceEntity> getPrice() {
        return priceService.getPrice();
    }

    @Secured("ROLE_gdmt_price_from")
    @RequestMapping({"/getPriceInfolistByPage"})
    @ResponseBody
    public PadingRstType<InternationalPriceEntity> getPriceInfolistByPage(SearchPriceEntity entity, PagingBean paging) {
        this.logger.info(entity);
        this.logger.info(paging);
        PadingRstType<InternationalPriceEntity> padingRstType = priceService.getPriceInfolistByPage(entity,paging);
        return padingRstType;
    }

}
