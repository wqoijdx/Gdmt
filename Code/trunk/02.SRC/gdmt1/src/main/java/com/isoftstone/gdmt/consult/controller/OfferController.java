package com.isoftstone.gdmt.consult.controller;

import com.isoftstone.gdmt.consult.entity.SearchOfferEntity;
import com.isoftstone.gdmt.consult.service.OfferService;
import com.isoftstone.gdmt.mybatis.entity.InternationalOfferEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import javax.annotation.Resource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping({"/consult"})
public class OfferController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private OfferService offerService;

    @Secured("ROLE_gdmt_data_offer")
    @RequestMapping({"/offerPage"})
    public String offerPage() {
        return "consult/offerPage";
    }

    @Secured("ROLE_gdmt_data_offer")
    @RequestMapping({"/getOffer"})
    @ResponseBody
    public List<InternationalOfferEntity> getOffer() {
        return offerService.getOffer();
    }

    @Secured("ROLE_gdmt_data_offer")
    @RequestMapping({"/getOfferInfolistByPage"})
    @ResponseBody
    public PadingRstType<InternationalOfferEntity> getOfferInfolistByPage(SearchOfferEntity entity, PagingBean paging) {
        this.logger.info(entity);
        this.logger.info(paging);
        PadingRstType<InternationalOfferEntity> padingRstType = offerService.getOfferInfolistByPage(entity,paging);
        return padingRstType;
    }

}
