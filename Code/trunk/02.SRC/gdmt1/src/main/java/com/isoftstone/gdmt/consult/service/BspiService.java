package com.isoftstone.gdmt.consult.service;


import com.isoftstone.gdmt.consult.entity.SearchBspiEntity;
import com.isoftstone.gdmt.mybatis.entity.CoalBspiEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;

import java.util.List;

public interface BspiService {
    List<CoalBspiEntity> getBspi();

    PadingRstType<CoalBspiEntity> getBspiInfolistByPage(SearchBspiEntity entity, PagingBean paging);


}