package com.isoftstone.gdmt.platform.menu.controller;

import com.isoftstone.gdmt.mybatis.entity.PtMenuEntity;
import com.isoftstone.gdmt.mybatis.entity.PtMenuI18nEntity;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.controller.BaseConctroller;
import com.isoftstone.gdmt.platform.menu.service.MenuService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/menu")
public class MenuController extends BaseConctroller {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private MenuService menuService;
    @Secured("ROLE_cldk_data_menu")
    @RequestMapping("/menuPage")
    public String menuPage(){
        return "menu/menuPage";
    }
    @Secured("ROLE_cldk_data_menu")
    @RequestMapping("/getMenuTree")
    @ResponseBody
    public List<ZtreeStrEntity> getMenuTree(){
        return menuService.getMenuTree();
    }
    @Secured("ROLE_cldk_data_menu")
    @RequestMapping("/getMenuInfoById")
    @ResponseBody
    public PtMenuEntity getMenuInfoById(@RequestParam("menuId") String menuId){
        logger.info("menuId:" + menuId);
        return menuService.getMenuInfoById(menuId);
    }
    @Secured("ROLE_cldk_data_menu")
    @RequestMapping("/modifyMenuInfoById")
    @ResponseBody
    public String modifyMenuInfoById(PtMenuEntity entity){
        logger.info( entity);
        menuService.modifyMenuInfoById(entity);
        return getSuccess("menu.modify.success");
    }
    @Secured("ROLE_cldk_data_menu")
    @RequestMapping("/addMenuInfoById")
    @ResponseBody
    public String addMenuInfoById(PtMenuEntity entity){
        logger.info( entity);
        menuService.addMenuInfoById(entity);
        return getSuccess("menu.add.success");
    }

    @Secured("ROLE_cldk_data_menu")
    @RequestMapping("/deleteMenuInfoById")
    @ResponseBody
    public String deleteMenuInfoById(@RequestParam("menuId") String menuId){
        logger.info("menuId:" +  menuId);
        menuService.deleteMenuInfoById(menuId);
        return getSuccess("menu.delete.success");
    }
    @Secured("ROLE_cldk_data_menu")
    @RequestMapping("/getMenuTreeByMenuId")
    @ResponseBody
    public List<ZtreeStrEntity> getMenuTreeByMenuId(@RequestParam("menuId") String menuId){
        logger.info("menuId:" +  menuId);
        return menuService.getMenuTreeByMenuId(menuId);
    }

    @Secured("ROLE_cldk_data_menu")
    @RequestMapping("/boundMenu")
    @ResponseBody
    public String boundMenu(@RequestParam("menuId") String menuId,@RequestParam("parentId") String parentId){
        logger.info("menuId:" +  menuId + " parentId:" + parentId);
        menuService.boundMenu(menuId,parentId);
        return getSuccess("menu.bound.success");
    }

    @Secured("ROLE_cldk_data_menu")
    @RequestMapping("/unBoundByMenuId")
    @ResponseBody
    public String unBoundByMenuId(@RequestParam("menuId") String menuId){
        logger.info("menuId:" +  menuId);
        menuService.unBoundByMenuId(menuId);
        return getSuccess("menu.unbound.success");
    }

    @Secured("ROLE_cldk_data_menu")
    @RequestMapping("/getI18nByMenuId")
    @ResponseBody
    public List<PtMenuI18nEntity> getI18nByMenuId(HttpServletRequest request){
        String menuId = request.getParameter("menuId");
        logger.info("menuId:" +  menuId);
        List<PtMenuI18nEntity> list = menuService.getI18nByMenuId(menuId);
        return list;
    }
    @Secured("ROLE_cldk_data_menu")
    @RequestMapping("/saveI18nData")
    @ResponseBody
    public String saveI18nData(PtMenuI18nEntity entity){
        logger.info(entity);
        menuService.saveI18nData(entity);
        return getSuccess("menu.save.i18n.success");
    }

    @Secured("ROLE_cldk_data_menu")
    @RequestMapping("/addI18nData")
    @ResponseBody
    public String addI18nData(PtMenuI18nEntity entity){
        logger.info(entity);
        menuService.addI18nData(entity);
        return getSuccess("menu.add.i18n.success");
    }
    @Secured("ROLE_cldk_data_menu")
    @RequestMapping("/deleteI18nData")
    @ResponseBody
    public String deleteI18nData(PtMenuI18nEntity entity){
        logger.info(entity);
        menuService.deleteI18nData(entity);
        return getSuccess("menu.delete.i18n.success");
    }



}
