package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class MemberEntity implements Serializable
{
	@Columns("id")
	private Integer id;

	@Columns("member_id")
	private String memberId;

	@Columns("name")
	private String name;

	@Columns("integral")
	private Double integral;

	@Columns("remarks")
	private String remarks;

	@Columns("update_time")
	private String updateTime;

	@Columns("tel")
	private String tel;
	
	public Integer getId() 
	{
		return id;
	}
	
	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public String getMemberId() 
	{
		return memberId;
	}
	
	public void setMemberId(String memberId) 
	{
		this.memberId = memberId;
	}
	
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public Double getIntegral() 
	{
		return integral;
	}
	
	public void setIntegral(Double integral) 
	{
		this.integral = integral;
	}
	
	public String getRemarks() 
	{
		return remarks;
	}
	
	public void setRemarks(String remarks) 
	{
		this.remarks = remarks;
	}
	
	public String getUpdateTime() 
	{
		return updateTime;
	}
	
	public void setUpdateTime(String updateTime) 
	{
		this.updateTime = updateTime;
	}
	
	public String getTel() 
	{
		return tel;
	}
	
	public void setTel(String tel) 
	{
		this.tel = tel;
	}

}
