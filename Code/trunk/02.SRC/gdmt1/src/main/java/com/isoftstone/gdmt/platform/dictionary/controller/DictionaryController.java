package com.isoftstone.gdmt.platform.dictionary.controller;

import com.isoftstone.gdmt.mybatis.entity.PtDictionaryEntity;
import com.isoftstone.gdmt.mybatis.entity.PtDictionaryI18nEntity;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.controller.BaseConctroller;
import com.isoftstone.gdmt.platform.dictionary.service.DictionaryService;
import org.apache.ibatis.annotations.Param;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/dictionary")
public class DictionaryController extends BaseConctroller {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private DictionaryService dictionaryService;

    @Secured("ROLE_cldk_data_dictionary")
    @RequestMapping("/dictionaryPage")
    public String dictionaryPage() {
        return "dictionary/dictionaryPage";
    }

    @Secured("ROLE_cldk_data_dictionary")
    @RequestMapping("/getDictionaryTree")
    @ResponseBody
    public List<ZtreeStrEntity> getDictionaryTree(){

        return dictionaryService.getDictionaryTree();
    }

    @Secured("ROLE_cldk_data_dictionary")
    @RequestMapping("/getDictionaryInfoById")
    @ResponseBody
    public PtDictionaryEntity getDictionaryInfoById(@RequestParam("dictId") String dictId){
        logger.info("dictId" + dictId);
        return dictionaryService.getDictionaryInfoById(dictId);
    }

    @Secured("ROLE_cldk_data_dictionary")
    @RequestMapping("/modifyDictionaryInfoById")
    @ResponseBody
    public String modifyDictionaryInfoById(PtDictionaryEntity entity){
        logger.info(entity);
        dictionaryService.modifyDictionaryInfoById(entity);
        return getSuccess("dictionary.modify.success");
    }

    @Secured("ROLE_cldk_data_dictionary")
    @RequestMapping("/addDictionaryInfo")
    @ResponseBody
    public String addDictionaryInfo(PtDictionaryEntity entity){
        logger.info(entity);
        dictionaryService.addDictionaryInfo(entity);
        return getSuccess("dictionary.add.success");
    }

    @Secured("ROLE_cldk_data_dictionary")
    @RequestMapping("/deleteDictionaryInfo")
    @ResponseBody
    public String deleteDictionaryInfo(@RequestParam("dictId") String dictId){
        logger.info("dictId" + dictId);
        dictionaryService.deleteDictionaryInfo(dictId);
        return getSuccess("dictionary.delete.success");
    }

    @Secured("ROLE_cldk_data_dictionary")
    @RequestMapping("/getI18nByDictionaryId")
    @ResponseBody
    public List<PtDictionaryI18nEntity> getI18nByDictionaryId(HttpServletRequest request){
        String dictId = request.getParameter("dictId");
        logger.info("dictId" + dictId);
        List<PtDictionaryI18nEntity> list = dictionaryService.getI18nByDictionaryId(dictId);
        return list;
    }

    @Secured("ROLE_cldk_data_dictionary")
    @RequestMapping("/saveI18nData")
    @ResponseBody
    public String saveI18nData(PtDictionaryI18nEntity entity){
        logger.info(entity);
        dictionaryService.saveI18nData(entity);
        return getSuccess("dictionary.save.i18nData.success");
    }

    @Secured("ROLE_cldk_data_dictionary")
    @RequestMapping("/addI18nData")
    @ResponseBody
    public String addI18nData(PtDictionaryI18nEntity entity){
        logger.info(entity);
        dictionaryService.addI18nData(entity);
        return getSuccess("dictionary.add.i18nData.success");
    }

    @Secured("ROLE_cldk_data_dictionary")
    @RequestMapping("/deleteI18nData")
    @ResponseBody
    public String deleteI18nData(PtDictionaryI18nEntity entity){
        logger.info(entity);
        dictionaryService.deleteI18nData(entity);
        return getSuccess("dictionary.delete.i18nData.success");
    }

}
