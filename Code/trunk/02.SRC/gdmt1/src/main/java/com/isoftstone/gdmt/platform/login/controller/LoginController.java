package com.isoftstone.gdmt.platform.login.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.isoftstone.gdmt.mybatis.entity.PtUserEntity;
import com.isoftstone.gdmt.platform.controller.BaseConctroller;
import com.isoftstone.gdmt.user.service.UserService;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.UUID;

@Controller
@RequestMapping("/login")
public class LoginController extends BaseConctroller {
    @Resource
    private UserService userService;

    @RequestMapping("/login")
    public String login(HttpServletRequest request){
        String  siteLanguage = request.getParameter("siteLanguage");
        request.getSession().setAttribute("siteLanguage",siteLanguage);
        return "sm/login";
    }

    /**
     * 跳转到注册页面
     * @return
     */
    @RequestMapping("/register")
    public String register() {
        return "sm/register";
    }

    @PostMapping("/doRegister")
    @ResponseBody
    public String doRegister(PtUserEntity user) {
        PtUserEntity temp = userService.queryUserListByName(user.getUserName());
        if (temp != null) {
            return getFail("username.exist");
        }
        user.setDelFlag(0);
        user.setModtime(DateUtil.now());
        user.setRegisterdate(DateUtil.now());
        user.setUserUuid(UUID.randomUUID().toString().replaceAll("-", ""));
        userService.save(user);
        return getSuccess("register.success");
    }

    @RequestMapping("/verification")
    public String verification(){
        return "redirect:/";
    }
    @RequestMapping("/main")
    public String main(){
        return "redirect:/";
    }
    @Secured("ROLE_cldk_data_offer")
    @RequestMapping("/test1")
    public String test1(){
        return "test/test1";
    }
    @Secured("ROLE_cldk_data_quote")
    @RequestMapping("/test2")
    public String test2(){
        return "test/test2";
    }
    @Resource
    private MessageSource messageSource;
    @RequestMapping("/test3")
    @ResponseBody
    public String test3(){
        HashMap<String,Object> message = new HashMap<>();
        Locale locale = LocaleContextHolder.getLocale();
        String msgStr = messageSource.getMessage("test.nav.tip", null, locale);
        message.put("success", true);
        message.put("msg", msgStr);
        message.put("description", msgStr);
        String rst = JSON.toJSONString(message);
        return rst;
    }

    @GetMapping("/captcha")
    public void getCaptcha(HttpSession session, HttpServletResponse response) throws IOException {
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(200, 100);
        lineCaptcha.write(response.getOutputStream());
        session.setAttribute("captcha", lineCaptcha.getCode());
    }

    @GetMapping("/userinfo")
    public String userinfo() {
        return "sm/userinfo";
    }

    @GetMapping("/modpass")
    public String modpass() {
        return "sm/modpass";
    }
}
