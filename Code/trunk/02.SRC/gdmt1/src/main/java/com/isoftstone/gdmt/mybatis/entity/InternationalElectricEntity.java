package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class InternationalElectricEntity implements Serializable
{
	@Columns("elc_id")
	private Integer elcId;

	@Columns("elc_date")
	private String elcDate;

	@Columns("elc_nation")
	private Double elcNation;

	@Columns("elc_nation_yoy")
	private Double elcNationYoy;
	
	public Integer getElcId() 
	{
		return elcId;
	}
	
	public void setElcId(Integer elcId) 
	{
		this.elcId = elcId;
	}
	
	public String getElcDate() 
	{
		return elcDate;
	}
	
	public void setElcDate(String elcDate) 
	{
		this.elcDate = elcDate;
	}
	
	public Double getElcNation() 
	{
		return elcNation;
	}
	
	public void setElcNation(Double elcNation) 
	{
		this.elcNation = elcNation;
	}
	
	public Double getElcNationYoy() 
	{
		return elcNationYoy;
	}
	
	public void setElcNationYoy(Double elcNationYoy) 
	{
		this.elcNationYoy = elcNationYoy;
	}

}
