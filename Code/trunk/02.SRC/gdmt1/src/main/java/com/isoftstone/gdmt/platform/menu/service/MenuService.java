package com.isoftstone.gdmt.platform.menu.service;

import com.isoftstone.gdmt.mybatis.entity.PtMenuEntity;
import com.isoftstone.gdmt.mybatis.entity.PtMenuI18nEntity;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;

import java.util.List;

public interface MenuService {
    List<ZtreeStrEntity> getMenuTree();


    PtMenuEntity getMenuInfoById(String menuId);

    void modifyMenuInfoById(PtMenuEntity entity);

    void addMenuInfoById(PtMenuEntity entity);

    void deleteMenuInfoById(String menuId);

    List<ZtreeStrEntity> getMenuTreeByMenuId(String menuId);

    void boundMenu(String menuId, String parentId);

    void unBoundByMenuId(String menuId);

    List<PtMenuI18nEntity> getI18nByMenuId(String menuId);

    void saveI18nData(PtMenuI18nEntity entity);

    void addI18nData(PtMenuI18nEntity entity);

    void deleteI18nData(PtMenuI18nEntity entity);
}
