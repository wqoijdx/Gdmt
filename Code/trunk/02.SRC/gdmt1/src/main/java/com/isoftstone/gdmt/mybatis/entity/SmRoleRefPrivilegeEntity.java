package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class SmRoleRefPrivilegeEntity implements Serializable
{
	@Columns("privilege_id")
	private Integer privilegeId;

	@Columns("role_id")
	private String roleId;
	
	public Integer getPrivilegeId() 
	{
		return privilegeId;
	}
	
	public void setPrivilegeId(Integer privilegeId) 
	{
		this.privilegeId = privilegeId;
	}
	
	public String getRoleId() 
	{
		return roleId;
	}
	
	public void setRoleId(String roleId) 
	{
		this.roleId = roleId;
	}

}
