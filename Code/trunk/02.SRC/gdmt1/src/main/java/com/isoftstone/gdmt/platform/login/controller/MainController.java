package com.isoftstone.gdmt.platform.login.controller;

import com.isoftstone.gdmt.platform.login.entity.MenuEntity;
import com.isoftstone.gdmt.platform.login.service.LoginService;
import com.isoftstone.gdmt.spring.security.entity.SecurityUserEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class MainController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private LoginService loginService;
    @RequestMapping("/")
    public String main(HttpServletRequest request){


        HttpSession session = request.getSession();



        SecurityUserEntity userDetails = (SecurityUserEntity) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        String userUuid = userDetails.getUserUuid();
        session.setAttribute("userId",userUuid);

        List<MenuEntity> list =  loginService.queryMenuByUserId(userUuid);

        session.setAttribute("menu",list);

        return "sm/main";
    }

    @RequestMapping("/getServiceMenu")
    @ResponseBody
    public List<MenuEntity> getServiceMenu(HttpServletRequest request){
        HttpSession session = request.getSession();
        String id = request.getParameter("menuId");
        logger.info("menuId:" + id);
        List<MenuEntity> list = (List<MenuEntity>) session.getAttribute("menu");
        if(id == null){
            return null;
        }
        for(MenuEntity menuEntity: list){
            if(id.equals(menuEntity.getMenuId())){
                return menuEntity.getChildrenList();
            }
        }
        
        return null;
    }
}
