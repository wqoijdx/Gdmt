package com.isoftstone.gdmt.platform.util;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author czx
 * @date 2021/05/06
 */
public class HttpServletUtil {
    /**
     * 得到当前线程的request
     * @return
     */
    public static HttpServletRequest currentServletRequest() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        return request;
    }
    /**
     * 得到session
     * @return
     */
    public static HttpSession currentHttpSession() {
        return currentServletRequest().getSession();
    }

    /**
     * 获取session中的某一项值
     * @param key
     * @return
     */
    public static Object getSessionAttribute(String key) {
        return currentHttpSession().getAttribute(key);
    }

    /**
     * 得到当前线程的响应对象
     * @return
     */
    public static HttpServletResponse currentServletResponse() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
    }

}
