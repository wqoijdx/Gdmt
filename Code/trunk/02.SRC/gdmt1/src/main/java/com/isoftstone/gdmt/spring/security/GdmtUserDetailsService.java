package com.isoftstone.gdmt.spring.security;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.isoftstone.gdmt.mybatis.entity.PtUserEntity;
import com.isoftstone.gdmt.platform.util.HttpServletUtil;
import com.isoftstone.gdmt.spring.security.entity.SecurityUserEntity;
import com.isoftstone.gdmt.user.service.UserService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service("userDetailsService")
public class GdmtUserDetailsService implements UserDetailsService {
    /**
     * 密码错误缓存，超时时间3分钟
     */
    public static final Cache<String, Integer> passErr = Caffeine.newBuilder()
            .expireAfterWrite(3, TimeUnit.MINUTES)
            .removalListener((k, v, cause) -> {
                System.out.println("密码错误缓存剔除：" + k + "剔除原因：" + cause);
            })
            .build();
    public static final Cache<String, Object> lockUser = Caffeine.newBuilder()
            .expireAfterWrite(30, TimeUnit.MINUTES)
            .removalListener((k, v, cause) -> {
                System.out.println("锁定用户缓存剔除：" + k + "剔除原因：" + cause);
            })
            .build();

    @Resource
    private PasswordEncoder passwordEncoder;
    @Resource
    private UserService userService;


    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        //校验验证码
        HttpServletRequest request = HttpServletUtil.currentServletRequest();
        String captcha = request.getParameter("captcha");
        String sessionCap = (String) request.getSession().getAttribute("captcha");
        if (!sessionCap.equalsIgnoreCase(captcha)) {
            throw  new UsernameNotFoundException("验证码错误 :" + captcha);
        }
        PtUserEntity ptUserEntitie =  userService.queryUserListByName(userName);
        if(ptUserEntitie == null){
            System.out.println("用户名错误");
            throw  new UsernameNotFoundException("user name is not exit :" + userName);
        }

        List<String> privilegeCodes=userService.queryPrivilegeCode(ptUserEntitie.getUserUuid());
        List<GrantedAuthority> collection = new ArrayList<>();
        for(String item:privilegeCodes){
            collection.add(new SimpleGrantedAuthority("ROLE_" + item));
        }
        //检查密码是否正确，如果密码不正确，就放入用户输错密码缓存
        String password = request.getParameter("password");
        final boolean matches = passwordEncoder.matches(password, ptUserEntitie.getPassword());
        if (!"admin".equals(userName) && !matches) {
            Integer errCount = passErr.getIfPresent(ptUserEntitie.getUserUuid());
            if (errCount == null) {
                errCount = 0;
            }
            errCount++;
            passErr.put(ptUserEntitie.getUserUuid(), errCount);
            //如果密码错误次数超过3次，就锁定用户
            if (errCount >= 3) {
                lockUser.put(ptUserEntitie.getUserUuid(), new Object());
            }
        }
        Object userTemp = lockUser.getIfPresent(ptUserEntitie.getUserUuid());
        if (userTemp != null) {
            System.out.println("用户锁定");
            throw  new UsernameNotFoundException("用户锁定：" + userName);
        }
        // 参数分别是：用户名，密码，用户权限
        SecurityUserEntity user = new SecurityUserEntity(userName, ptUserEntitie.getPassword(), collection);
        user.setEmail(ptUserEntitie.getEmail());
        user.setMobile(ptUserEntitie.getMobile());
        user.setNiceName(ptUserEntitie.getNiceName());
        user.setUserUuid(ptUserEntitie.getUserUuid());
        return user;

    }
}
