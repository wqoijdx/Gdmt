package com.isoftstone.gdmt.consult.controller;

import com.isoftstone.gdmt.consult.entity.SearchQuoteEntity;
import com.isoftstone.gdmt.consult.service.QuoteService;
import com.isoftstone.gdmt.mybatis.entity.InternationalQuoteEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping({"/consult"})
public class QuoteController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private QuoteService quoteService;

    @Secured("ROLE_gdmt_data_quote")
    @RequestMapping({"/quotePage"})
    public String quotePage() {
        return "consult/quotePage";
    }

    @Secured("ROLE_gdmt_data_quote")
    @RequestMapping({"/getQuote"})
    @ResponseBody
    public List<InternationalQuoteEntity> getQuote() {
        return quoteService.getQuote();
    }

    @Secured("ROLE_gdmt_data_quote")
    @RequestMapping({"/getQuoteInfolistByPage"})
    @ResponseBody
    public PadingRstType<InternationalQuoteEntity> getQuoteInfolistByPage(SearchQuoteEntity entity, PagingBean paging) {
        this.logger.info(entity);
        this.logger.info(paging);
        PadingRstType<InternationalQuoteEntity> padingRstType = quoteService.getQuoteInfolistByPage(entity,paging);
        return padingRstType;
    }

}
