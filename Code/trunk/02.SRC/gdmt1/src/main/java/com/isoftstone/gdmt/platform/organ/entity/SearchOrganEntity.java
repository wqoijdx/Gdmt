package com.isoftstone.gdmt.platform.organ.entity;



import java.io.Serializable;


public class SearchOrganEntity implements Serializable {
    private String searchOrganId ;

    public String getSearchOrganId() {
        return searchOrganId;
    }

    public void setsearchOrganId(String searchOrganId) {
        this.searchOrganId = searchOrganId;
    }

    @Override
    public String toString() {
        return "SearchOrganEntity{" +
                "searchOrganId='" + searchOrganId + '\'' +
                '}';
    }

}
