package com.isoftstone.gdmt.platform.tag.service;

import com.isoftstone.gdmt.platform.tag.entity.DictTagEntity;

import java.util.List;

public interface DicDataTagService {
    List<DictTagEntity> queryDicInfoByGroupId(String group);
}
