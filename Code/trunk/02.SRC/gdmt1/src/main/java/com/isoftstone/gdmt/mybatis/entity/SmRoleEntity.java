package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class SmRoleEntity implements Serializable
{
	@Columns("role_id")
	private String roleId;

	@Columns("name")
	private String name;

	@Columns("description")
	private String description;

	@Columns("creator")
	private String creator;

	@Columns("update_time")
	private String updateTime;
	
	public String getRoleId() 
	{
		return roleId;
	}
	
	public void setRoleId(String roleId) 
	{
		this.roleId = roleId;
	}
	
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String description) 
	{
		this.description = description;
	}
	
	public String getCreator() 
	{
		return creator;
	}
	
	public void setCreator(String creator) 
	{
		this.creator = creator;
	}
	
	public String getUpdateTime() 
	{
		return updateTime;
	}
	
	public void setUpdateTime(String updateTime) 
	{
		this.updateTime = updateTime;
	}

}
