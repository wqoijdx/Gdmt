package com.isoftstone.gdmt.platform.department.controller;

import com.isoftstone.gdmt.mybatis.entity.PtDepartmentEntity;
import com.isoftstone.gdmt.mybatis.entity.PtRRoleOrganEntity;
import com.isoftstone.gdmt.mybatis.entity.PtUserEntity;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.controller.BaseConctroller;
import com.isoftstone.gdmt.platform.department.entity.PtDepartmentDutyEntity;
import com.isoftstone.gdmt.platform.department.service.DepartmentService;
import org.apache.ibatis.annotations.Param;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/department")
public class DepartmentController extends BaseConctroller {
    private Logger logger = LogManager.getLogger(this.getClass());

    @Resource
    private DepartmentService departmentService;

    @RequestMapping("/departmentPage")
    public String department() {
        return "department/departmentPage";
    }

    //部门树列表
    @RequestMapping("/getDepartmentTree")
    @ResponseBody
    public List<ZtreeStrEntity> getDepartmentTree() {
        return departmentService.getDepartmentTree();

    }

    //获取部门信息
    @RequestMapping("/getDepartmentInfoById")
    @ResponseBody
    public PtDepartmentEntity getDepartmentInfoById(@RequestParam("departId") String departId) {
        logger.info("departId" + departId);
        return departmentService.getDepartmentInfoById(departId);
    }

    @RequestMapping("/deleteDepartInfoById")
    @ResponseBody
    public String deleteDepartInfoById(@RequestParam("departId") String departId) {
        logger.info("departId:" + departId);
        departmentService.deleteMenuInfoById(departId);
        return getSuccess("depart.delete.success");
    }

    @RequestMapping("/modifyDepartInfoById")
    @ResponseBody
    public String modifyDepartInfoById(PtDepartmentEntity entity) {
        logger.info(entity);
        departmentService.modifyDepartInfoById(entity);
        return getSuccess("depart.modify.success");
    }

    @RequestMapping("/addDepartInfoById")
    @ResponseBody
    public String addDepartInfoById(PtDepartmentEntity entity) {
        logger.info(entity);
        departmentService.addDepartInfoById(entity);
        return getSuccess("depart.add.success");
    }

    @RequestMapping("/getDepartTreeByDepartId")
    @ResponseBody
    public List<ZtreeStrEntity> getDepartTreeByDepartId(@RequestParam("departId") String departId){
        logger.info("departId:" +  departId);
        return departmentService.getDepartTreeByDepartId(departId);
    }

    @RequestMapping("/boundDepart")
    @ResponseBody
    public String boundMenu(@RequestParam("departId") String departId,@RequestParam("belongCenter") String belongCenter){
        logger.info("departId:" +  departId + " belongCenter:" + belongCenter);
        departmentService.boundDepart(departId,belongCenter);
        return getSuccess("depart.bound.success");
    }

    @RequestMapping("/unBoundByDepartId")
    @ResponseBody
    public String unBoundByDepartId(@RequestParam("departId") String departId){
        logger.info("departId:" +  departId);
        departmentService.unBoundByDepartId(departId);
        return getSuccess("depart.unbound.success");
    }

    @RequestMapping("/getDepartmentDutyInfoById")
    @ResponseBody
    public List<PtDepartmentDutyEntity> getDepartmentDutyInfoById(HttpServletRequest request){
        String departId = request.getParameter("departId");
        logger.info("departId:" +  departId);
        List<PtDepartmentDutyEntity> list = departmentService.getDepartmentDutyInfoById(departId);
        return list;
    }

//    @RequestMapping("/adjustDepartment")
//    @ResponseBody
//    public String adjustDepartment(@RequestParam("departId") String departId,@RequestParam("organId") String organId){
//        logger.info("departId:" +  departId + " organId:" + organId);
//        departmentService.adjustDepartment(departId,organId);
//        return getSuccess("department.adjust.success");
//    }

    @RequestMapping("/getUserInfoByDutyid")
    @ResponseBody
    public List<PtUserEntity> getUserInfoByDutyid(HttpServletRequest request){
        String dutyid = request.getParameter("dutyid");
        logger.info("dutyid:" +  dutyid);
        List<PtUserEntity> list = departmentService.getUserInfoByDutyid(dutyid);
        return list;
    }

}
