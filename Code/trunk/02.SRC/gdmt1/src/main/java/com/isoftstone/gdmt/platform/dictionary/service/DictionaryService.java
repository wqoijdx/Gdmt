package com.isoftstone.gdmt.platform.dictionary.service;


import com.isoftstone.gdmt.mybatis.entity.PtDictionaryEntity;
import com.isoftstone.gdmt.mybatis.entity.PtDictionaryI18nEntity;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;

import java.util.List;

public interface DictionaryService {
    List<ZtreeStrEntity> getDictionaryTree();

    PtDictionaryEntity getDictionaryInfoById(String dictId);

    void modifyDictionaryInfoById(PtDictionaryEntity entity);

    void addDictionaryInfo(PtDictionaryEntity entity);

    void deleteDictionaryInfo(String dictId);

    List<PtDictionaryI18nEntity> getI18nByDictionaryId(String dictId);

    void saveI18nData(PtDictionaryI18nEntity entity);

    void addI18nData(PtDictionaryI18nEntity entity);

    void deleteI18nData(PtDictionaryI18nEntity entity);
}
