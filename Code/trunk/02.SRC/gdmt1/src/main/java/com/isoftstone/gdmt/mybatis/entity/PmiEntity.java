package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class PmiEntity implements Serializable
{
	@Columns("my_date")
	private String myDate;

	@Columns("pmi_value")
	private Double pmiValue;

	@Columns("pmi_id")
	private Integer pmiId;
	
	public String getMyDate() 
	{
		return myDate;
	}
	
	public void setMyDate(String myDate) 
	{
		this.myDate = myDate;
	}
	
	public Double getPmiValue() 
	{
		return pmiValue;
	}
	
	public void setPmiValue(Double pmiValue) 
	{
		this.pmiValue = pmiValue;
	}
	
	public Integer getPmiId() 
	{
		return pmiId;
	}
	
	public void setPmiId(Integer pmiId) 
	{
		this.pmiId = pmiId;
	}

}
