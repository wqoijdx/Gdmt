package com.isoftstone.gdmt.consult.controller;

import com.isoftstone.gdmt.consult.entity.SearchBspiEntity;
import com.isoftstone.gdmt.consult.entity.SearchGdpEntity;
import com.isoftstone.gdmt.consult.service.BspiService;
import com.isoftstone.gdmt.consult.service.GdpService;
import com.isoftstone.gdmt.mybatis.entity.CoalBspiEntity;
import com.isoftstone.gdmt.mybatis.entity.GdpEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping({"/consult"})
public class GdpController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private GdpService gdpService;

    @Secured("ROLE_gdmt_data_gdp")
    @RequestMapping({"/gdpPage"})
    public String gdpPage() {
        return "consult/gdpPage";
    }

    @Secured("ROLE_gdmt_data_gdp")
    @RequestMapping({"/getGdp"})
    @ResponseBody
    public List<GdpEntity> getGdp() {
        return gdpService.getGdp();
    }

    @Secured("ROLE_gdmt_data_gdp")
    @RequestMapping({"/getGdpInfolistByPage"})
    @ResponseBody
    public PadingRstType<GdpEntity> getGdpInfolistByPage(SearchGdpEntity entity, PagingBean paging) {
        this.logger.info(entity);
        this.logger.info(paging);
        PadingRstType<GdpEntity> padingRstType = gdpService.getGdpInfolistByPage(entity,paging);
        return padingRstType;
    }

}
