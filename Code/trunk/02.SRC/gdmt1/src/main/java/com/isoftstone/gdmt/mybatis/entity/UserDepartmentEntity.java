package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class UserDepartmentEntity implements Serializable
{
	@Columns("user_id")
	private Integer userId;

	@Columns("user_name")
	private String userName;

	@Columns("department")
	private String department;
	
	public Integer getUserId() 
	{
		return userId;
	}
	
	public void setUserId(Integer userId) 
	{
		this.userId = userId;
	}
	
	public String getUserName() 
	{
		return userName;
	}
	
	public void setUserName(String userName) 
	{
		this.userName = userName;
	}
	
	public String getDepartment() 
	{
		return department;
	}
	
	public void setDepartment(String department) 
	{
		this.department = department;
	}

}
