package com.isoftstone.gdmt.consult.controller;

import com.isoftstone.gdmt.consult.entity.SearchBspiEntity;
import com.isoftstone.gdmt.consult.entity.SearchQuoteEntity;
import com.isoftstone.gdmt.consult.service.BspiService;
import com.isoftstone.gdmt.consult.service.QuoteService;
import com.isoftstone.gdmt.mybatis.entity.CoalBspiEntity;
import com.isoftstone.gdmt.mybatis.entity.InternationalQuoteEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping({"/consult"})
public class BspiController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private BspiService bspiService;

    @Secured("ROLE_gdmt_data_bspi")
    @RequestMapping({"/bspiPage"})
    public String bspiPage() {
        return "consult/bspiPage";
    }

    @Secured("ROLE_gdmt_data_bspi")
    @RequestMapping({"/getBspi"})
    @ResponseBody
    public List<CoalBspiEntity> getBspi() {
        return bspiService.getBspi();
    }

    @Secured("ROLE_gdmt_data_bspi")
    @RequestMapping({"/getBspiInfolistByPage"})
    @ResponseBody
    public PadingRstType<CoalBspiEntity> getBspiInfolistByPage(SearchBspiEntity entity, PagingBean paging) {
        this.logger.info(entity);
        this.logger.info(paging);
        PadingRstType<CoalBspiEntity> padingRstType = bspiService.getBspiInfolistByPage(entity,paging);
        return padingRstType;
    }

}
