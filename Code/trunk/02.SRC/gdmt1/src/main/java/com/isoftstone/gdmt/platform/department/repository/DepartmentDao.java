package com.isoftstone.gdmt.platform.department.repository;

import com.isoftstone.gdmt.mybatis.entity.PtDepartmentEntity;
import com.isoftstone.gdmt.mybatis.entity.PtRRoleOrganEntity;
import com.isoftstone.gdmt.mybatis.entity.PtUserEntity;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.department.entity.PtDepartmentDutyEntity;
import com.isoftstone.gdmt.platform.tag.entity.DictTagEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DepartmentDao {
    List<ZtreeStrEntity> queryDepartmentTree();

    PtDepartmentEntity getDepartmentInfoById(@Param("departId") String departId);

    void deleteDepartInfoById(@Param("departId") String departId);

    void modifyDepartInfoById(@Param("entity") PtDepartmentEntity entity);

    void addDepartInfoById(@Param("entity") PtDepartmentEntity entity);

    String queryDepartTreeByDepartId(@Param("departId") String departId);

    void updateOrganIdByDepartId(@Param("departId") String departId, @Param("belongCenter") String belongCenter);

    List<PtDepartmentDutyEntity> getDepartmentDutyInfoById(@Param("departId") String departId);

    List<DictTagEntity> queryTagElementList();

//    void adjustDepartment(@Param("departId") String departId,@Param("organId") String organId);

    List<PtUserEntity> getUserInfoByDutyid(@Param("dutyid") String dutyid);
}