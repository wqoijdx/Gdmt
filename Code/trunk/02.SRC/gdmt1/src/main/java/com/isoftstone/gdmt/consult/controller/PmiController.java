package com.isoftstone.gdmt.consult.controller;

import com.isoftstone.gdmt.consult.entity.SearchPmiEntity;
import com.isoftstone.gdmt.consult.service.PmiService;
import com.isoftstone.gdmt.mybatis.entity.PmiEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping({"/consult"})
public class PmiController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private PmiService pmiService;

    @Secured("ROLE_gdmt_data_pmi")
    @RequestMapping({"/pmiPage"})
    public String pmiPage() {
        return "consult/pmiPage";
    }

    @Secured("ROLE_gdmt_data_pmi")
    @RequestMapping({"/getPmi"})
    @ResponseBody
    public List<PmiEntity> getPmi() {
        return pmiService.getPmi();
    }

    @Secured("ROLE_gdmt_data_pmi")
    @RequestMapping({"/getPmiInfolistByPage"})
    @ResponseBody
    public PadingRstType<PmiEntity> getPmiInfolistByPage(SearchPmiEntity entity, PagingBean paging) {
        this.logger.info(entity);
        this.logger.info(paging);
        PadingRstType<PmiEntity> padingRstType = pmiService.getPmiInfolistByPage(entity,paging);
        return padingRstType;
    }
    
}
