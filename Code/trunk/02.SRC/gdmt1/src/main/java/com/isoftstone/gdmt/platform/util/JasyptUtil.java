package com.isoftstone.gdmt.platform.util;

import org.jasypt.util.text.BasicTextEncryptor;

public final class JasyptUtil {
    private  JasyptUtil(){

    }
    public static String Encrypt(String plaintext,String textKey){
        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword(textKey);
        return textEncryptor.encrypt(plaintext);
    }

    public static String Decrypt(String ciphertext, String textKey){
        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword(textKey);
        return textEncryptor.decrypt(ciphertext);
    }

    public static void main(String[] args) {
        String tips=" <0:Encrypt | 1:Decrypt> <code> <key>";
        if(args.length < 3){
            System.out.println(tips);
            return;
        }
        if("0".equals(args[0])){
            System.out.println(Encrypt(args[1], args[2]));
        }else{
            System.out.println(Decrypt(args[1], args[2]));
        }
//
//        String rst = Encrypt("Huawei_123", "0123456789ABCDEF");
//
//        System.out.println(rst);
//        System.out.println(Decrypt("d/+McO/+fRCJsGlHFg0Yzrz1Pluxm4L5", "0123456789ABCDEF"));
    }



}
