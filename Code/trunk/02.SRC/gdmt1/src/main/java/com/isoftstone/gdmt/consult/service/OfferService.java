package com.isoftstone.gdmt.consult.service;


import com.isoftstone.gdmt.consult.entity.SearchOfferEntity;
import com.isoftstone.gdmt.consult.entity.SearchPriceEntity;
import com.isoftstone.gdmt.mybatis.entity.InternationalOfferEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;

import java.util.List;

public interface OfferService {
    List<InternationalOfferEntity> getOffer();

    PadingRstType<InternationalOfferEntity> getOfferInfolistByPage(SearchOfferEntity entity, PagingBean paging);


}