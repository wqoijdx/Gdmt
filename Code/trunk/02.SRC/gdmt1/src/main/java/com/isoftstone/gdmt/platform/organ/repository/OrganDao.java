package com.isoftstone.gdmt.platform.organ.repository;

import com.isoftstone.gdmt.mybatis.entity.PtDepartmentInfoEntity;
import com.isoftstone.gdmt.mybatis.entity.PtOrganEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.department.entity.SearchDepartmentEntity;
import com.isoftstone.gdmt.platform.organ.entity.SearchOrganEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface OrganDao {

    PtOrganEntity getOrganInfoById(@Param("organId") String organId);
     //依靠id修改organ
    void modifyOrganInfoById(@Param("entity") PtOrganEntity entity);
    ////组织增加
    void addOrganInfoById(@Param("entity") PtOrganEntity entity);
    //依靠id组织删除 （有子结构，岗位，国电不能删）
    void deleteOrganInfoById(@Param("organId") String organId);
     //依靠id查询相关子id是否有组织
     List<PtOrganEntity> selectOrganParentIdById(@Param("organId") String organId);

    void updateParentIdByOrganId(@Param("organId") String organId, @Param("parentId") String parentId);

    String queryOrganTreeByOrganId(@Param("organId") String organId);


    //查询有无岗位
    Integer selectStationById(@Param("organId") String organId);

    Integer selectCountDepartmentByOrganId(@Param("organId") String organId);

//    List<PtDepartmentInfoEntity> getDepartByOrganId(@Param("organId") String organId);

    List<ZtreeStrEntity> queryOrganTree();
    List<PtDepartmentInfoEntity> queryDepListPage(@Param("search") SearchDepartmentEntity search, @Param("paging") PagingBean paging);

    Integer queryDepTotal(@Param("search") SearchDepartmentEntity search);

}
