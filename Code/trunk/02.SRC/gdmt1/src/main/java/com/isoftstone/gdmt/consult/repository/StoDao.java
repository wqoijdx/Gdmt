package com.isoftstone.gdmt.consult.repository;

import com.isoftstone.gdmt.consult.entity.SearchStoEntity;
import com.isoftstone.gdmt.mybatis.entity.CementProductionEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface StoDao {
    List<CementProductionEntity> getSto();

    List<CementProductionEntity> queryStoInfoByPage(@Param("entity") SearchStoEntity entity, @Param("paging") PagingBean paging);

    Integer queryStoInfoTotal(@Param("entity") SearchStoEntity entity);

}
