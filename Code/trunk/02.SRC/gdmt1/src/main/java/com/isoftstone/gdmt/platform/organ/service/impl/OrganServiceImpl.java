package com.isoftstone.gdmt.platform.organ.service.impl;

import com.isoftstone.gdmt.mybatis.entity.PtDepartmentInfoEntity;
import com.isoftstone.gdmt.mybatis.entity.PtOrganEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.common.util.DataUtil;
import com.isoftstone.gdmt.platform.department.entity.SearchDepartmentEntity;
import com.isoftstone.gdmt.platform.organ.entity.SearchOrganEntity;
import com.isoftstone.gdmt.platform.organ.repository.OrganDao;
import com.isoftstone.gdmt.platform.organ.service.OrganService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


@Service("organService")
public class OrganServiceImpl implements OrganService {

    @Resource
    private OrganDao organDao;
    @Value("${ubound.uuid}")
    private String uboundUuid;

    @Value("${root.uuid}")
    private String rootUuid;

    @Resource
    private MessageSource messageSource;

    @Override
    public List<ZtreeStrEntity> getOrganTree() {
        List<ZtreeStrEntity> list = organDao.queryOrganTree();
        ZtreeStrEntity ztreeStrEntity = new ZtreeStrEntity();
        ztreeStrEntity.setId(uboundUuid);
        Locale locale = LocaleContextHolder.getLocale();
        String unboundName = messageSource.getMessage("common.ubound.name",null,locale);
        ztreeStrEntity.setName(unboundName);
        list.add(ztreeStrEntity);
        Integer index = 0;
        for(ZtreeStrEntity item:list){
            if(index < 10){
                item.setOpen(true);
            }else{
                item.setOpen(false);
            }
            index++;
        }
        return list;
    }

    @Override
    public PtOrganEntity getOrganInfoById(String organId) {
        return organDao.getOrganInfoById(organId);
    }

    @Override
    public void modifyOrganInfoById(PtOrganEntity entity) {
        organDao.modifyOrganInfoById(entity);
    }

    @Override
    public void addOrganInfoById(PtOrganEntity entity) {
        entity.setOrganId(DataUtil.getUUIDShort());//随机生成id
        entity.setParentId(uboundUuid);//父id=0
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        entity.setModtime(date.toString());
        organDao.addOrganInfoById(entity);
    }

    @Override
    //删除时要判断
    public void deleteOrganInfoById(String organId) {
            //判断有无子组织
        List<PtOrganEntity> sonList = organDao.selectOrganParentIdById(organId);
        if(sonList.size()==0)
            if(organDao.selectStationById(organId)==0)//无岗位
                if(organDao.selectCountDepartmentByOrganId(organId) == 0)//无国电部门
                     organDao.deleteOrganInfoById(organId);
        }

    @Override
    public List<ZtreeStrEntity> getOrganTreeByOrganId(String organId) {
        String parentId = organDao.queryOrganTreeByOrganId(organId);
        List<ZtreeStrEntity> list = getOrganTree();
        ZtreeStrEntity ztreeStrEntity = new ZtreeStrEntity();
        ztreeStrEntity.setId(rootUuid);
        Locale locale = LocaleContextHolder.getLocale();
        String rootName = messageSource.getMessage("common.root.node.name",null,locale);
        ztreeStrEntity.setName(rootName);
        list.add(ztreeStrEntity);
        if(parentId != null){
            for(ZtreeStrEntity item:list){
                if(parentId.equals(item.getId())){
                    item.setChecked(true);
                }
            }
        }
        return list;
    }

    @Override
    public void boundOrgan(String organId, String parentId) {
        organDao.updateParentIdByOrganId(organId,parentId);
    }



    @Override
    public void unBoundByOrganId(String organId) {
        organDao.updateParentIdByOrganId(organId,uboundUuid);
    }

    @Override
    public PadingRstType<PtOrganEntity> queryOrganListPage(SearchOrganEntity search, PagingBean paging) {
        return null;
    }


    @Override
    public PadingRstType<PtDepartmentInfoEntity> queryDepListPage(SearchDepartmentEntity search, PagingBean paging) {
        paging.deal(PtDepartmentInfoEntity.class);
        PadingRstType<PtDepartmentInfoEntity> ptDepEntityPadingRstType = new PadingRstType<PtDepartmentInfoEntity>();
        ptDepEntityPadingRstType.setPage(paging.getPage());
        List<PtDepartmentInfoEntity> list = organDao.queryDepListPage(search, paging);
        ptDepEntityPadingRstType.setRawRecords(list);
        Integer total = organDao.queryDepTotal(search);
        ptDepEntityPadingRstType.setTotal(total);
        ptDepEntityPadingRstType.putItems();
        return ptDepEntityPadingRstType;
    }
}



