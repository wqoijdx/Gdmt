package com.isoftstone.gdmt.consult.repository;

import com.isoftstone.gdmt.consult.entity.SearchElectricEntity;
import com.isoftstone.gdmt.consult.entity.SearchElectricityEntity;
import com.isoftstone.gdmt.mybatis.entity.ElectricityEntity;
import com.isoftstone.gdmt.mybatis.entity.InternationalElectricEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ElectricityDao {
    List<ElectricityEntity> getElectricity();

    List<ElectricityEntity> queryElectricityInfoByPage(@Param("entity") SearchElectricityEntity entity, @Param("paging") PagingBean paging);

    Integer queryElectricityInfoTotal(@Param("entity") SearchElectricityEntity entity);

}
