package com.isoftstone.gdmt.user.repository;

import com.isoftstone.gdmt.mybatis.entity.PtUserEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.user.entity.SearchUserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserInfoDao {
    List<PtUserEntity> queryUserInfoList(@Param("search") SearchUserEntity search);

    void deleteUserInfoById(@Param("id") String id);

    List<String> queryPrivilegeCode(@Param("userUuid") String userUuid);

    List<PtUserEntity> queryUserInfoByPage(@Param("search") SearchUserEntity search,@Param("paging") PagingBean paging);

    Integer queryUserInfoTotal(@Param("search") SearchUserEntity search);

    List<ZtreeStrEntity> getRole();

    List<String> queryRoleByUserId(@Param("userUuid") String userUuid);

    void deleteRoleIdByUserId(@Param("userUuid") String userUuid);

    void insertRoleIdAndUserId(@Param("userUuid") String userUuid,@Param("roleUuid") String roleUuid);

    void insert(PtUserEntity user);

    PtUserEntity selectById(String id);

    void updateById(PtUserEntity user);

    void addUserInfoById(@Param("entity") PtUserEntity entity);

    PtUserEntity queryUserListById(@Param("userUuid") String userUuid);

    void modifyUserListById(@Param("entity") PtUserEntity entity);

    List<ZtreeStrEntity> getDuty();

    List<String> queryDutyByUserId(@Param("userUuid") String userUuid);

    void deleteDutyIdByUserId(@Param("userUuid") String userUuid);

    void insertDutyIdAndUserId(@Param("userUuid") String userUuid,@Param("dutyId") String dutyId);

    List<ZtreeStrEntity> getDep();

    String queryDepByUserUuid(@Param("userUuid") String userUuid);

    void modifyDepIdAndUserId(@Param("userUuid") String userUuid,@Param("depUuId") String depUuId);
}
