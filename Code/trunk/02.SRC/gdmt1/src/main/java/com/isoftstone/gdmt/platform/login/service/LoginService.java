package com.isoftstone.gdmt.platform.login.service;

import com.isoftstone.gdmt.platform.login.entity.MenuEntity;

import java.util.List;

public interface LoginService {
    List<MenuEntity> queryMenuByUserId(String userUuid);
}
