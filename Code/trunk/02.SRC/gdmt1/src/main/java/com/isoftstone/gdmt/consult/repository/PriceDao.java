package com.isoftstone.gdmt.consult.repository;

import com.isoftstone.gdmt.consult.entity.SearchPriceEntity;
import com.isoftstone.gdmt.mybatis.entity.InternationalPriceEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface PriceDao {

    List<InternationalPriceEntity> getPrice();

    List<InternationalPriceEntity> queryPriceInfoByPage(@Param("entity") SearchPriceEntity entity, @Param("paging") PagingBean paging);

    Integer queryPriceInfoTotal(@Param("entity") SearchPriceEntity entity);

}
