package com.isoftstone.gdmt.consult.controller;

import com.isoftstone.gdmt.consult.entity.SearchElectricEntity;
import com.isoftstone.gdmt.consult.service.ElectricService;
import com.isoftstone.gdmt.mybatis.entity.InternationalElectricEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping({"/consult"})
public class ElectricController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private ElectricService electricService;

    @Secured("ROLE_gdmt_data_electric")
    @RequestMapping({"/electricPage"})
    public String electricPage() {
        return "consult/electricPage";
    }

    @Secured("ROLE_gdmt_data_electric")
    @RequestMapping({"/getElectric"})
    @ResponseBody
    public List<InternationalElectricEntity> getElectric() {
        return electricService.getElectric();
    }

    @Secured("ROLE_gdmt_data_electric")
    @RequestMapping({"/getElectricInfolistByPage"})
    @ResponseBody
    public PadingRstType<InternationalElectricEntity> getElectricInfolistByPage(SearchElectricEntity entity, PagingBean paging) {
        this.logger.info(entity);
        this.logger.info(paging);
        PadingRstType<InternationalElectricEntity> padingRstType = electricService.getElectricInfolistByPage(entity,paging);
        return padingRstType;
    }

}
