package com.isoftstone.gdmt.consult.repository;

import com.isoftstone.gdmt.consult.entity.SearchElectricEntity;
import com.isoftstone.gdmt.mybatis.entity.InternationalElectricEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ElectricDao {
    List<InternationalElectricEntity> getElectric();

    List<InternationalElectricEntity> queryElectricInfoByPage(@Param("entity") SearchElectricEntity entity, @Param("paging") PagingBean paging);

    Integer queryElectricInfoTotal(@Param("entity") SearchElectricEntity entity);

}
