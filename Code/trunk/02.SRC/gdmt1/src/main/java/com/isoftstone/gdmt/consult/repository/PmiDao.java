package com.isoftstone.gdmt.consult.repository;

import com.isoftstone.gdmt.consult.entity.SearchPmiEntity;
import com.isoftstone.gdmt.mybatis.entity.PmiEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PmiDao {
    List<PmiEntity> getPmi();

    List<PmiEntity> queryPmiInfoByPage(@Param("entity") SearchPmiEntity entity, @Param("paging") PagingBean paging);

    Integer queryPmiInfoTotal(@Param("entity") SearchPmiEntity entity);

}
