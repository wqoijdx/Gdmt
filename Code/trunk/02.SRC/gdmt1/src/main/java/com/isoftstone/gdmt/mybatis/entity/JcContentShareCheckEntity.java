package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class JcContentShareCheckEntity implements Serializable
{
	@Columns("share_check_id")
	private Integer shareCheckId;

	@Columns("content_id")
	private String contentId;

	@Columns("channel_id")
	private String channelId;

	@Columns("check_status")
	private Integer checkStatus;

	@Columns("check_opinion")
	private String checkOpinion;

	@Columns("share_valid")
	private Integer shareValid;
	
	public Integer getShareCheckId() 
	{
		return shareCheckId;
	}
	
	public void setShareCheckId(Integer shareCheckId) 
	{
		this.shareCheckId = shareCheckId;
	}
	
	public String getContentId() 
	{
		return contentId;
	}
	
	public void setContentId(String contentId) 
	{
		this.contentId = contentId;
	}
	
	public String getChannelId() 
	{
		return channelId;
	}
	
	public void setChannelId(String channelId) 
	{
		this.channelId = channelId;
	}
	
	public Integer getCheckStatus() 
	{
		return checkStatus;
	}
	
	public void setCheckStatus(Integer checkStatus) 
	{
		this.checkStatus = checkStatus;
	}
	
	public String getCheckOpinion() 
	{
		return checkOpinion;
	}
	
	public void setCheckOpinion(String checkOpinion) 
	{
		this.checkOpinion = checkOpinion;
	}
	
	public Integer getShareValid() 
	{
		return shareValid;
	}
	
	public void setShareValid(Integer shareValid) 
	{
		this.shareValid = shareValid;
	}

}
