package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class CoalBspiEntity implements Serializable
{
	@Columns("bspi_id")
	private Integer bspiId;

	@Columns("bspi_date")
	private String bspiDate;

	@Columns("bspi_bspi")
	private Double bspiBspi;

	@Columns("bspi_hbzj")
	private Double bspiHbzj;

	@Columns("bspi_hb")
	private Double bspiHb;

	@Columns("bspi_tbzj")
	private Double bspiTbzj;

	@Columns("bspi_tb")
	private Double bspiTb;
	
	public Integer getBspiId() 
	{
		return bspiId;
	}
	
	public void setBspiId(Integer bspiId) 
	{
		this.bspiId = bspiId;
	}
	
	public String getBspiDate() 
	{
		return bspiDate;
	}
	
	public void setBspiDate(String bspiDate) 
	{
		this.bspiDate = bspiDate;
	}
	
	public Double getBspiBspi() 
	{
		return bspiBspi;
	}
	
	public void setBspiBspi(Double bspiBspi) 
	{
		this.bspiBspi = bspiBspi;
	}
	
	public Double getBspiHbzj() 
	{
		return bspiHbzj;
	}
	
	public void setBspiHbzj(Double bspiHbzj) 
	{
		this.bspiHbzj = bspiHbzj;
	}
	
	public Double getBspiHb() 
	{
		return bspiHb;
	}
	
	public void setBspiHb(Double bspiHb) 
	{
		this.bspiHb = bspiHb;
	}
	
	public Double getBspiTbzj() 
	{
		return bspiTbzj;
	}
	
	public void setBspiTbzj(Double bspiTbzj) 
	{
		this.bspiTbzj = bspiTbzj;
	}
	
	public Double getBspiTb() 
	{
		return bspiTb;
	}
	
	public void setBspiTb(Double bspiTb) 
	{
		this.bspiTb = bspiTb;
	}

}
