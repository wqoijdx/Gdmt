package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class SmUserRefRoleEntity implements Serializable
{
	@Columns("role_id")
	private String roleId;

	@Columns("user_id")
	private String userId;
	
	public String getRoleId() 
	{
		return roleId;
	}
	
	public void setRoleId(String roleId) 
	{
		this.roleId = roleId;
	}
	
	public String getUserId() 
	{
		return userId;
	}
	
	public void setUserId(String userId) 
	{
		this.userId = userId;
	}

}
