package com.isoftstone.gdmt.consult.service;

import com.isoftstone.gdmt.consult.entity.SearchElectricEntity;
import com.isoftstone.gdmt.mybatis.entity.InternationalElectricEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;

import java.util.List;

public interface ElectricService {
    List<InternationalElectricEntity> getElectric();

    PadingRstType<InternationalElectricEntity> getElectricInfolistByPage(SearchElectricEntity entity, PagingBean paging);


}