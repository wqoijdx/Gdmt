package com.isoftstone.gdmt.duty.controller;

import com.isoftstone.gdmt.duty.entity.SearchDutyEntity;
import com.isoftstone.gdmt.duty.service.DutyService;

import com.isoftstone.gdmt.mybatis.entity.PtRRoleOrganEntity;

import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.controller.BaseConctroller;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;



@Controller
@RequestMapping("/post")
public class DutyController extends BaseConctroller {
    private Log logger = LogFactory.getLog(this.getClass());

    @Resource                              //跳转界面
    private DutyService dutyService;
    @Secured("ROLE_gdmt_data_post")
    @RequestMapping("/page")
    public String dutyPage(){
        return "duty/dutyPage";
    }
    @Secured("ROLE_gdmt_data_post")         //ztree
    @RequestMapping("/getDutyTree")
    @ResponseBody
    public List<ZtreeStrEntity> getDutyTree(){
        return dutyService.getDutyTree();
    }
    @Secured("ROLE_gdmt_data_post")         //ztree
    @RequestMapping("/queryRoleTree")
    @ResponseBody
    public List<ZtreeStrEntity> queryRoleTree(){
        return dutyService.queryRoleTree();
    }
    @Secured("ROLE_gdmt_data_post")
    @RequestMapping("/getDutyInfoById")
    @ResponseBody
    public PtRRoleOrganEntity getDutyInfoById(@RequestParam("organ_uuId") String organ_uuId){
        logger.info("organ_uuId:" + organ_uuId);
        return dutyService.getDutyInfoById(organ_uuId);
    }
    @Secured("ROLE_gdmt_data_post")
    @RequestMapping("/queryDutyListPage")
    @ResponseBody
    public PadingRstType<PtRRoleOrganEntity> queryDutyListPage(SearchDutyEntity search, PagingBean paging){
        logger.info(search);
        logger.info(paging);
        PadingRstType<PtRRoleOrganEntity> ptRoleEntityPadingRstType = dutyService.queryDutyListPage(search,paging);


        return ptRoleEntityPadingRstType;
    }

    @Secured("ROLE_gdmt_data_post")
    @RequestMapping("/getOrganByDutyId")
    @ResponseBody
    public List<ZtreeStrEntity> getOrganByDutyId(@RequestParam("dutyid") String dutyid){
                logger.info("dutyid:" + dutyid);
        List<ZtreeStrEntity> list = dutyService.getOrganByDutyId(dutyid);
        return list;
    }
    @Secured("ROLE_gdmt_data_post")
    @RequestMapping("/getRoleByDutyId")
    @ResponseBody
    public List<ZtreeStrEntity> getRoleByDutyId(@RequestParam("dutyid") String dutyid){
        logger.info("dutyid:" + dutyid);
        List<ZtreeStrEntity> list = dutyService.getRoleByDutyId(dutyid);
        return list;
    }
    @Secured("ROLE_gdmt_data_post")
    @RequestMapping("/setOrganByDutyId")
    @ResponseBody
    public String setOrganByDutyId(@RequestParam("dutyid") String dutyid,
                                        @RequestParam("organUuidArray") String organUuidArray){
        logger.info("dutyid:" + dutyid);
        logger.info("organUuidArray:" + organUuidArray);
        dutyService.setOrganByDutyId(dutyid,organUuidArray);
        return getSuccess("organ.mod.success");
    }
    @Secured("ROLE_gdmt_data_post")
    @RequestMapping("/setRoleByDutyId")
    @ResponseBody
    public String setRoleByDutyId(@RequestParam("dutyid") String dutyid,
                                   @RequestParam("roleUuidArray") String roleUuidArray){
        logger.info("dutyid:" + dutyid);
        logger.info("roleUuidArray:" + roleUuidArray);
        dutyService.setRoleByDutyId(dutyid,roleUuidArray);
        return getSuccess("role.mod.success");
    }

    @Secured("ROLE_gdmt_data_post")
    @RequestMapping("/addDutyInfoById")
    @ResponseBody
    public String addDutyInfoById(PtRRoleOrganEntity entity){
        logger.info(entity);
        dutyService.addDutyInfoById(entity);
        return getSuccess("duty.add.success");
    }
    @Secured("ROLE_gdmt_data_post")
    @RequestMapping("/deleteDutyInfoById")
    @ResponseBody
    public String deleteDutyInfoById(@RequestParam("dutyid") String dutyid){
        int flag=1;
        logger.info("dutyid:" +  dutyid);
        flag=dutyService.deleteDutyInfoById(dutyid);
        if(flag==0)
            return getSuccess("duty.delete.success");
        else
            return getFail("duty.delete.fail");
    }



    @Secured("ROLE_gdmt_data_post")
    @RequestMapping("/queryDutyListPage2")
    @ResponseBody
    public PadingRstType<PtRRoleOrganEntity> queryDutyListPage2(SearchDutyEntity search, PagingBean paging){
        logger.info(search);
        logger.info(paging);
        PadingRstType<PtRRoleOrganEntity> ptRoleEntityPadingRstType = dutyService.queryDutyListPage2(search,paging);

        return ptRoleEntityPadingRstType;
    }
}
