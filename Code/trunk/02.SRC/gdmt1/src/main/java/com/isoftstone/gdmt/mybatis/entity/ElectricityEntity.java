package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class ElectricityEntity implements Serializable
{
	@Columns("electricity_id")
	private Integer electricityId;

	@Columns("electricity_date")
	private String electricityDate;

	@Columns("electricity_fire")
	private Double electricityFire;

	@Columns("electricity_ftb")
	private Double electricityFtb;

	@Columns("electricity_flj")
	private Double electricityFlj;

	@Columns("electricity_fljtb")
	private Double electricityFljtb;

	@Columns("electricity_water")
	private Double electricityWater;

	@Columns("electricity_wtb")
	private Double electricityWtb;

	@Columns("electricity_wlj")
	private Double electricityWlj;

	@Columns("electricity_wljtb")
	private Double electricityWljtb;
	
	public Integer getElectricityId() 
	{
		return electricityId;
	}
	
	public void setElectricityId(Integer electricityId) 
	{
		this.electricityId = electricityId;
	}
	
	public String getElectricityDate() 
	{
		return electricityDate;
	}
	
	public void setElectricityDate(String electricityDate) 
	{
		this.electricityDate = electricityDate;
	}
	
	public Double getElectricityFire() 
	{
		return electricityFire;
	}
	
	public void setElectricityFire(Double electricityFire) 
	{
		this.electricityFire = electricityFire;
	}
	
	public Double getElectricityFtb() 
	{
		return electricityFtb;
	}
	
	public void setElectricityFtb(Double electricityFtb) 
	{
		this.electricityFtb = electricityFtb;
	}
	
	public Double getElectricityFlj() 
	{
		return electricityFlj;
	}
	
	public void setElectricityFlj(Double electricityFlj) 
	{
		this.electricityFlj = electricityFlj;
	}
	
	public Double getElectricityFljtb() 
	{
		return electricityFljtb;
	}
	
	public void setElectricityFljtb(Double electricityFljtb) 
	{
		this.electricityFljtb = electricityFljtb;
	}
	
	public Double getElectricityWater() 
	{
		return electricityWater;
	}
	
	public void setElectricityWater(Double electricityWater) 
	{
		this.electricityWater = electricityWater;
	}
	
	public Double getElectricityWtb() 
	{
		return electricityWtb;
	}
	
	public void setElectricityWtb(Double electricityWtb) 
	{
		this.electricityWtb = electricityWtb;
	}
	
	public Double getElectricityWlj() 
	{
		return electricityWlj;
	}
	
	public void setElectricityWlj(Double electricityWlj) 
	{
		this.electricityWlj = electricityWlj;
	}
	
	public Double getElectricityWljtb() 
	{
		return electricityWljtb;
	}
	
	public void setElectricityWljtb(Double electricityWljtb) 
	{
		this.electricityWljtb = electricityWljtb;
	}

}
