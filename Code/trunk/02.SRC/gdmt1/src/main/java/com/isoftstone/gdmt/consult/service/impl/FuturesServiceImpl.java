package com.isoftstone.gdmt.consult.service.impl;

import com.isoftstone.gdmt.consult.entity.SearchFuturesEntity;
import com.isoftstone.gdmt.consult.repository.FuturesDao;
import com.isoftstone.gdmt.consult.service.FuturesService;
import com.isoftstone.gdmt.mybatis.entity.FuturesPriceEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("futuresService")
public class FuturesServiceImpl implements FuturesService {
    @Resource
    private FuturesDao futuresDao;

    @Override
    public List<FuturesPriceEntity> getFutures() {
        return futuresDao.getFutures();
    }

    @Override
    public PadingRstType<FuturesPriceEntity> getFuturesInfolistByPage(SearchFuturesEntity entity, PagingBean paging) {
        paging.deal(FuturesPriceEntity.class);
        PadingRstType<FuturesPriceEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(paging.getPage());
        List<FuturesPriceEntity> list = futuresDao.queryFuturesInfoByPage(entity,paging);
        padingRstType.setRawRecords(list);
        Integer total = futuresDao.queryFuturesInfoTotal(entity);
        padingRstType.setTotal(total);
        padingRstType.putItems();
        return padingRstType;
    }

}
