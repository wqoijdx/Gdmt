package com.isoftstone.gdmt.platform.role.entity;

import java.io.Serializable;

public class SearchRoleEntity implements Serializable {
    private String searchRoleName ;

    public String getSearchRoleName() {
        return searchRoleName;
    }

    public void setSearchRoleName(String searchRoleName) {
        this.searchRoleName = searchRoleName;
    }

    @Override
    public String toString() {
        return "SearchRoleEntity{" +
                "searchRoleName='" + searchRoleName + '\'' +
                '}';
    }
}
