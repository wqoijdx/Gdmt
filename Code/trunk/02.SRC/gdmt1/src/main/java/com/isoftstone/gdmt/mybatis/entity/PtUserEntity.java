package com.isoftstone.gdmt.mybatis.entity;
import com.isoftstone.gdmt.mybatis.annotation.Columns;

import java.io.Serializable;


public class PtUserEntity implements Serializable
{
	@Columns("USER_UUID")
	private String userUuid;

	@Columns("USER_NAME")
	private String userName;

	@Columns("PASSWORD")
	private String password;

	@Columns("EMAIL")
	private String email;

	@Columns("MOBILE")
	private String mobile;

	@Columns("NICE_NAME")
	private String niceName;

	@Columns("REGISTERDATE")
	private String registerdate;

	@Columns("REMARK")
	private String remark;

	@Columns("DEL_FLAG")
	private Integer delFlag;

	@Columns("MODTIME")
	private String modtime;
	/**
	 * 是否锁定
	 */
	private String status;
	
	public String getUserUuid() 
	{
		return userUuid;
	}
	
	public void setUserUuid(String userUuid) 
	{
		this.userUuid = userUuid;
	}
	
	public String getUserName() 
	{
		return userName;
	}
	
	public void setUserName(String userName) 
	{
		this.userName = userName;
	}
	
	public String getPassword() 
	{
		return password;
	}
	
	public void setPassword(String password) 
	{
		this.password = password;
	}
	
	public String getEmail() 
	{
		return email;
	}
	
	public void setEmail(String email) 
	{
		this.email = email;
	}
	
	public String getMobile() 
	{
		return mobile;
	}
	
	public void setMobile(String mobile) 
	{
		this.mobile = mobile;
	}
	
	public String getNiceName() 
	{
		return niceName;
	}
	
	public void setNiceName(String niceName) 
	{
		this.niceName = niceName;
	}
	
	public String getRegisterdate() 
	{
		return registerdate;
	}
	
	public void setRegisterdate(String registerdate) 
	{
		this.registerdate = registerdate;
	}
	
	public String getRemark() 
	{
		return remark;
	}
	
	public void setRemark(String remark) 
	{
		this.remark = remark;
	}
	
	public Integer getDelFlag() 
	{
		return delFlag;
	}
	
	public void setDelFlag(Integer delFlag) 
	{
		this.delFlag = delFlag;
	}
	
	public String getModtime() 
	{
		return modtime;
	}
	
	public void setModtime(String modtime) 
	{
		this.modtime = modtime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
