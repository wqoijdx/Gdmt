package com.isoftstone.gdmt.user.service.impl;

import com.isoftstone.gdmt.mybatis.entity.PtUserEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.common.util.DataUtil;
import com.isoftstone.gdmt.spring.security.GdmtPasswordEncoder;
import com.isoftstone.gdmt.spring.security.GdmtUserDetailsService;
import com.isoftstone.gdmt.user.entity.SearchUserEntity;
import com.isoftstone.gdmt.user.repository.UserInfoDao;
import com.isoftstone.gdmt.user.service.UserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Locale;

@Service("UserService")
public class UserServiceImpl  implements UserService {
    @Resource
    private UserInfoDao userInfoDao;

    @Resource
    private MessageSource messageSource;
    @Resource
    private GdmtPasswordEncoder passwordEncoder;

    @Value("${root.uuid}")
    private String rootUuid;

    @Override
    public List<PtUserEntity> queryUserListByName(SearchUserEntity entity) {
        return userInfoDao.queryUserInfoList(entity);
    }

    @Override
    public void deleteUserInfoById(String ids) {
        String[] idArray = ids.split(",");
        for(String id : idArray){
            userInfoDao.deleteUserInfoById(id);
        }

    }

    @Override
    public PtUserEntity queryUserListByName(String userName) {
        SearchUserEntity entity = new SearchUserEntity();
        entity.setUserName(userName);
        List<PtUserEntity> list = userInfoDao.queryUserInfoList(entity);
        if(list.size() > 0){
            return list.get(0);
        }
        return null;
    }

    @Override
    public List<String> queryPrivilegeCode(String userUuid) {
        return userInfoDao.queryPrivilegeCode(userUuid);
    }

    @Override
    public PadingRstType<PtUserEntity> getUserInfolistByPage(SearchUserEntity entity, PagingBean paging) {
        paging.deal(PtUserEntity.class);
        PadingRstType<PtUserEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(paging.getPage());
        List<PtUserEntity> list = userInfoDao.queryUserInfoByPage(entity,paging);
        //判断用户是否锁定
        for (PtUserEntity userEntity : list) {
            String userUuid = userEntity.getUserUuid();
            Object temp = GdmtUserDetailsService.lockUser.getIfPresent(userUuid);
            if (temp != null) {
                userEntity.setStatus("锁定");
            } else {
                userEntity.setStatus("正常");
            }
        }
        padingRstType.setRawRecords(list);
        Integer total = userInfoDao.queryUserInfoTotal(entity);
        padingRstType.setTotal(total);
        padingRstType.putItems();
        return padingRstType;
    }

    @Override
    public List<ZtreeStrEntity> getRoleByUserId(String userUuid) {
        Locale locale = LocaleContextHolder.getLocale();
        String msgStr = messageSource.getMessage("role.manage.info", null, locale);
        List<ZtreeStrEntity> list = userInfoDao.getRole();



        ZtreeStrEntity ztreeStrEntity = new ZtreeStrEntity();
        ztreeStrEntity.setId("0");
        ztreeStrEntity.setName(msgStr);
        ztreeStrEntity.setpId("-1");
        list.add(ztreeStrEntity);

        List<String> roleList = userInfoDao.queryRoleByUserId(userUuid);
        for(ZtreeStrEntity item:list){
            for(String roleUuid: roleList){
                if(roleUuid.equals(item.getId())){
                    item.setChecked(true);
                }
            }
        }

        return list;
    }

    @Override
    public void setRoleUuidByUserUuid(String userUuid, String roleUuidArray) {
        userInfoDao.deleteRoleIdByUserId(userUuid);
        String[] roleUuids = roleUuidArray.split(",");
        for(String roleUuid:roleUuids){
            userInfoDao.insertRoleIdAndUserId(userUuid,roleUuid);
        }

    }

    @Override
    public void addUserInfoById(PtUserEntity entity) {
        entity.setUserUuid(DataUtil.getUUIDShort());
        userInfoDao.addUserInfoById(entity);
    }

    @Override
    public PtUserEntity queryUserListById(String userUuid) {
        return userInfoDao.queryUserListById(userUuid);
    }

    @Override
    public void modifyUserListById(PtUserEntity entity) {
        userInfoDao.modifyUserListById(entity);
    }

    @Override
    public List<ZtreeStrEntity> getDutyByUserId(String userUuid) {
        Locale locale = LocaleContextHolder.getLocale();
        String msgStr = messageSource.getMessage("duty.manage.info", null, locale);
        List<ZtreeStrEntity> list = userInfoDao.getDuty();

        ZtreeStrEntity ztreeStrEntity = new ZtreeStrEntity();
        ztreeStrEntity.setId("0");
        ztreeStrEntity.setName(msgStr);
        ztreeStrEntity.setpId("-1");
        list.add(ztreeStrEntity);

        List<String> dutyList = userInfoDao.queryDutyByUserId(userUuid);
        for(ZtreeStrEntity item:list){
            for(String DUTYID: dutyList){
                if(DUTYID.equals(item.getId())){
                    item.setChecked(true);
                }
            }
        }

        return list;
    }

    @Override
    public void setDutyIdByUserUuid(String userUuid, String dutyIdArray) {
        userInfoDao.deleteDutyIdByUserId(userUuid);
        String[] dutyIds = dutyIdArray.split(",");
        for(String dutyId:dutyIds){
            userInfoDao.insertDutyIdAndUserId(userUuid,dutyId);
        }
    }

    @Override
    public List<ZtreeStrEntity> getDepByUserUuid(String userUuid) {
        Locale locale = LocaleContextHolder.getLocale();
        String msgStr = messageSource.getMessage("dep.manage.info", null, locale);
        String depUuid = userInfoDao.queryDepByUserUuid(userUuid);
        List<ZtreeStrEntity> list = userInfoDao.getDep();

        ZtreeStrEntity ztreeStrEntity = new ZtreeStrEntity();
        ztreeStrEntity.setId(rootUuid);
        ztreeStrEntity.setName(msgStr);
        ztreeStrEntity.setpId("-2");
        list.add(ztreeStrEntity);

        if(depUuid!=null){
            for(ZtreeStrEntity item:list){
                if(depUuid.equals(item.getId())){
                    item.setChecked(true);
                }
            }
        }

        return list;
    }

    @Override
    public void setDepUuIdByUserUuid(String userUuid, String depUuId) {
//        String[] depUuIds = depUuIdArray.split(",");
//        for(String depUuId:depUuIds){
        userInfoDao.modifyDepIdAndUserId(userUuid,depUuId);
//        }
    }
    @Override
    public void save(PtUserEntity user) {
        final String password = user.getPassword();
        String encode = passwordEncoder.encode(password);
        user.setPassword(encode);
        userInfoDao.insert(user);
    }

    @Override
    public PtUserEntity getById(String id) {
        return userInfoDao.selectById(id);
    }

    @Override
    public void updateById(PtUserEntity user) {
        userInfoDao.updateById(user);
    }
}
