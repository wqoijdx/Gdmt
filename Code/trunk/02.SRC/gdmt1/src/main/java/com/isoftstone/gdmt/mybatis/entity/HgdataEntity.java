package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class HgdataEntity implements Serializable
{
	@Columns("hgsj_id")
	private Integer hgsjId;

	@Columns("date")
	private String date;

	@Columns("zj")
	private String zj;

	@Columns("zjh")
	private String zjh;

	@Columns("zjt")
	private String zjt;

	@Columns("zc")
	private String zc;

	@Columns("zch")
	private String zch;

	@Columns("zct")
	private String zct;

	@Columns("rj")
	private String rj;

	@Columns("rh")
	private String rh;

	@Columns("rt")
	private String rt;

	@Columns("hj")
	private String hj;

	@Columns("hh")
	private String hh;

	@Columns("ht")
	private String ht;
	
	public Integer getHgsjId() 
	{
		return hgsjId;
	}
	
	public void setHgsjId(Integer hgsjId) 
	{
		this.hgsjId = hgsjId;
	}
	
	public String getDate() 
	{
		return date;
	}
	
	public void setDate(String date) 
	{
		this.date = date;
	}
	
	public String getZj() 
	{
		return zj;
	}
	
	public void setZj(String zj) 
	{
		this.zj = zj;
	}
	
	public String getZjh() 
	{
		return zjh;
	}
	
	public void setZjh(String zjh) 
	{
		this.zjh = zjh;
	}
	
	public String getZjt() 
	{
		return zjt;
	}
	
	public void setZjt(String zjt) 
	{
		this.zjt = zjt;
	}
	
	public String getZc() 
	{
		return zc;
	}
	
	public void setZc(String zc) 
	{
		this.zc = zc;
	}
	
	public String getZch() 
	{
		return zch;
	}
	
	public void setZch(String zch) 
	{
		this.zch = zch;
	}
	
	public String getZct() 
	{
		return zct;
	}
	
	public void setZct(String zct) 
	{
		this.zct = zct;
	}
	
	public String getRj() 
	{
		return rj;
	}
	
	public void setRj(String rj) 
	{
		this.rj = rj;
	}
	
	public String getRh() 
	{
		return rh;
	}
	
	public void setRh(String rh) 
	{
		this.rh = rh;
	}
	
	public String getRt() 
	{
		return rt;
	}
	
	public void setRt(String rt) 
	{
		this.rt = rt;
	}
	
	public String getHj() 
	{
		return hj;
	}
	
	public void setHj(String hj) 
	{
		this.hj = hj;
	}
	
	public String getHh() 
	{
		return hh;
	}
	
	public void setHh(String hh) 
	{
		this.hh = hh;
	}
	
	public String getHt() 
	{
		return ht;
	}
	
	public void setHt(String ht) 
	{
		this.ht = ht;
	}

}
