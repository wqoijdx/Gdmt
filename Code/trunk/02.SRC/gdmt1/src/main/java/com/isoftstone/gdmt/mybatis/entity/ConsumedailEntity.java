package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class ConsumedailEntity implements Serializable
{
	@Columns("id")
	private Integer id;

	@Columns("member_id")
	private String memberId;

	@Columns("amount")
	private Double amount;

	@Columns("update_time")
	private String updateTime;
	
	public Integer getId() 
	{
		return id;
	}
	
	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public String getMemberId() 
	{
		return memberId;
	}
	
	public void setMemberId(String memberId) 
	{
		this.memberId = memberId;
	}
	
	public Double getAmount() 
	{
		return amount;
	}
	
	public void setAmount(Double amount) 
	{
		this.amount = amount;
	}
	
	public String getUpdateTime() 
	{
		return updateTime;
	}
	
	public void setUpdateTime(String updateTime) 
	{
		this.updateTime = updateTime;
	}

}
