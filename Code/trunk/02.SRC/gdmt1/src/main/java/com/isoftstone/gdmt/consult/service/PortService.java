package com.isoftstone.gdmt.consult.service;

import com.isoftstone.gdmt.consult.entity.SearchPortEntity;
import com.isoftstone.gdmt.mybatis.entity.CoalHarborEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;

import java.util.List;

public interface PortService {
    List<CoalHarborEntity> getPort();

    PadingRstType<CoalHarborEntity> getPortInfolistByPage(SearchPortEntity entity, PagingBean paging);


}