package com.isoftstone.gdmt.platform.role.repository;

import com.isoftstone.gdmt.mybatis.entity.PtRoleEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.role.entity.SearchRoleEntity;
import com.isoftstone.gdmt.platform.tag.entity.DictTagEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RoleDao {
    List<PtRoleEntity> queryRoleListPage(@Param("search") SearchRoleEntity search,@Param("paging") PagingBean paging);

    Integer queryRoleTotal(@Param("search")SearchRoleEntity search);

    List<ZtreeStrEntity> queryMenuTree();

    List<String> queryMenuIdByRoleId(@Param("roleUuid")String roleUuid);

    void deleteMenuByRoleId(@Param("roleUuid")String roleUuid);

    void insertMenuAndRole(@Param("roleUuid")String roleUuid,@Param("menuId") String menuId);

    /**
     * 插入
     * @param role
     */
    void insert(PtRoleEntity role);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    PtRoleEntity queryById(String id);

    /**
     * 修改角色
     * @param role
     */
    void updateById(PtRoleEntity role);

    /**
     * 删除角色
     * @param id
     */
    void deleteById(String id);

    /**
     * 查询绑定角色的用户数量
     * @param roleId
     * @return
     */
    int queryUserCountByRoleId(String roleId);

    List<DictTagEntity> queryTagElementList();
}
