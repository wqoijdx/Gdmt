package com.isoftstone.gdmt.consult.service.impl;

import com.isoftstone.gdmt.consult.entity.SearchElectricEntity;
import com.isoftstone.gdmt.consult.entity.SearchElectricityEntity;
import com.isoftstone.gdmt.consult.repository.ElectricDao;
import com.isoftstone.gdmt.consult.repository.ElectricityDao;
import com.isoftstone.gdmt.consult.service.ElectricService;
import com.isoftstone.gdmt.consult.service.ElectricityService;
import com.isoftstone.gdmt.mybatis.entity.ElectricityEntity;
import com.isoftstone.gdmt.mybatis.entity.InternationalElectricEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("electricityService")
public class ElectricityServiceImpl implements ElectricityService {
    @Resource
    private ElectricityDao electricityDao;

    @Override
    public List<ElectricityEntity> getElectricity() {
        return electricityDao.getElectricity();
    }

    @Override
    public PadingRstType<ElectricityEntity> getElectricityInfolistByPage(SearchElectricityEntity entity, PagingBean paging) {
        paging.deal(ElectricityEntity.class);
        PadingRstType<ElectricityEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(paging.getPage());
        List<ElectricityEntity> list = electricityDao.queryElectricityInfoByPage(entity,paging);
        padingRstType.setRawRecords(list);
        Integer total = electricityDao.queryElectricityInfoTotal(entity);
        padingRstType.setTotal(total);
        padingRstType.putItems();
        return padingRstType;
    }

}
