package com.isoftstone.gdmt.consult.service.impl;

import com.isoftstone.gdmt.consult.entity.SearchPriceEntity;
import com.isoftstone.gdmt.consult.repository.PriceDao;
import com.isoftstone.gdmt.consult.service.PriceService;
import com.isoftstone.gdmt.mybatis.entity.InternationalPriceEntity;
import javax.annotation.Resource;

import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import io.micrometer.core.instrument.search.Search;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("priceService")
public class PriceServiceImpl implements PriceService {
    @Resource
    private PriceDao priceDao;

    @Override
    public List<InternationalPriceEntity> getPrice() {
        return priceDao.getPrice();
    }

    @Override
    public PadingRstType<InternationalPriceEntity> getPriceInfolistByPage(SearchPriceEntity entity, PagingBean paging) {
        paging.deal(InternationalPriceEntity.class);
        PadingRstType<InternationalPriceEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(paging.getPage());
        List<InternationalPriceEntity> list = priceDao.queryPriceInfoByPage(entity,paging);
        padingRstType.setRawRecords(list);
        Integer total = priceDao.queryPriceInfoTotal(entity);
        padingRstType.setTotal(total);
        padingRstType.putItems();
        return padingRstType;
    }

}