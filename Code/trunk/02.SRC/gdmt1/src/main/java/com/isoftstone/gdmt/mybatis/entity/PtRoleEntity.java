package com.isoftstone.gdmt.mybatis.entity;
import com.isoftstone.gdmt.mybatis.annotation.Columns;

import java.io.Serializable;


public class PtRoleEntity implements Serializable
{
	@Columns("role_uuid")
	private String roleUuid;

	@Columns("role_name")
	private String roleName;

	@Columns("remarks")
	private String remarks;
	
	public String getRoleUuid() 
	{
		return roleUuid;
	}
	
	public void setRoleUuid(String roleUuid) 
	{
		this.roleUuid = roleUuid;
	}
	
	public String getRoleName() 
	{
		return roleName;
	}
	
	public void setRoleName(String roleName) 
	{
		this.roleName = roleName;
	}
	
	public String getRemarks() 
	{
		return remarks;
	}
	
	public void setRemarks(String remarks) 
	{
		this.remarks = remarks;
	}

}
