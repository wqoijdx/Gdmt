package com.isoftstone.gdmt.consult.repository;

import com.isoftstone.gdmt.consult.entity.SearchBspiEntity;
import com.isoftstone.gdmt.consult.entity.SearchGdpEntity;
import com.isoftstone.gdmt.mybatis.entity.CoalBspiEntity;
import com.isoftstone.gdmt.mybatis.entity.GdpEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GdpDao {
    List<GdpEntity> getGdp();

    List<GdpEntity> queryGdpInfoByPage(@Param("entity") SearchGdpEntity entity, @Param("paging") PagingBean paging);

    Integer queryGdpInfoTotal(@Param("entity") SearchGdpEntity entity);

}
