package com.isoftstone.gdmt.platform.role.controller;

import com.isoftstone.gdmt.mybatis.entity.PtRoleEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.controller.BaseConctroller;
import com.isoftstone.gdmt.platform.role.entity.SearchRoleEntity;
import com.isoftstone.gdmt.platform.role.service.RoleService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@Controller
@RequestMapping("/role")
public class RoleController extends BaseConctroller {
    private Log logger = LogFactory.getLog(this.getClass());
    @Resource
    private RoleService roleService;
    @Secured("ROLE_cldk_data_role")
    @RequestMapping("/rolePage")
    public String rolePage(){
        return "role/rolePage";
    }
    @Secured("ROLE_cldk_data_role")
    @RequestMapping("/queryRoleListPage")
    @ResponseBody
    public PadingRstType<PtRoleEntity> queryRoleListPage(SearchRoleEntity search, PagingBean paging){
        logger.info(search);
        logger.info(paging);
        PadingRstType<PtRoleEntity> ptRoleEntityPadingRstType = roleService.queryRoleListPage(search,paging);

        
        return ptRoleEntityPadingRstType;
    }

    @Secured("ROLE_cldk_data_role_menu")
    @RequestMapping("/getMenuByRoleId")
    @ResponseBody
    public List<ZtreeStrEntity> getMenuByRoleId(@RequestParam("roleUuid") String roleUuid){
        logger.info("roleUuid:" + roleUuid);
        List<ZtreeStrEntity> ztreeStrEntities = roleService.queryMenuByRoleId(roleUuid);

        return ztreeStrEntities;
    }

    @Secured("ROLE_cldk_data_role_menu")
    @RequestMapping("/saveMenuIdAndRoleId")
    @ResponseBody
    public String saveMenuIdAndRoleId(@RequestParam("roleUuid") String roleUuid,
                                      @RequestParam("menuArray") String menuArray){
        logger.info("roleUuid:" + roleUuid);
        logger.info("menuArray:" + menuArray);
        roleService.saveMenuIdAndRoleId(roleUuid,menuArray);
        return getSuccess("role.menu.allocate.success");
    }
    //saveMenuIdAndRoleId

    /**
     * 创建角色
     */
    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ResponseBody
    public String create(PtRoleEntity role) {
        roleService.create(role);
        final String result = getSuccess("role.menu.add.success");
        return result;
    }

    @RequestMapping
    @ResponseBody
    public PtRoleEntity getById(String id) {
        PtRoleEntity role = roleService.getById(id);
        return role;
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public String update(PtRoleEntity role) {
        roleService.updateById(role);
        final String result = getSuccess("role.menu.mod.success");
        return result;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String delete(@PathVariable String id) {
        int userCount = roleService.getUserCountByRoleId(id);
        if (userCount > 0) {
            return getFail("role.menu.del.fail.user");
        }
        roleService.removeById(id);
        return getSuccess("role.menu.del.success");
    }
}
