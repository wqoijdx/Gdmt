package com.isoftstone.gdmt.consult.service;

import com.isoftstone.gdmt.consult.entity.SearchFuturesEntity;
import com.isoftstone.gdmt.mybatis.entity.FuturesPriceEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;

import java.util.List;

public interface FuturesService {
    List<FuturesPriceEntity> getFutures();

    PadingRstType<FuturesPriceEntity> getFuturesInfolistByPage(SearchFuturesEntity entity, PagingBean paging);


}