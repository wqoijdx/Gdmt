package com.isoftstone.gdmt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GdmtApplication {

    public static void main(String[] args) {
        SpringApplication.run(GdmtApplication.class, args);
    }

}
