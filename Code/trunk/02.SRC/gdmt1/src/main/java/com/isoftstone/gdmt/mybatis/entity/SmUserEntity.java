package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class SmUserEntity implements Serializable
{
	@Columns("user_id")
	private String userId;

	@Columns("user_name")
	private String userName;

	@Columns("password")
	private String password;

	@Columns("nick_name")
	private String nickName;

	@Columns("gender")
	private Integer gender;

	@Columns("telephone")
	private String telephone;

	@Columns("email")
	private String email;

	@Columns("description")
	private String description;

	@Columns("update_time")
	private String updateTime;

	@Columns("create_time")
	private String createTime;
	
	public String getUserId() 
	{
		return userId;
	}
	
	public void setUserId(String userId) 
	{
		this.userId = userId;
	}
	
	public String getUserName() 
	{
		return userName;
	}
	
	public void setUserName(String userName) 
	{
		this.userName = userName;
	}
	
	public String getPassword() 
	{
		return password;
	}
	
	public void setPassword(String password) 
	{
		this.password = password;
	}
	
	public String getNickName() 
	{
		return nickName;
	}
	
	public void setNickName(String nickName) 
	{
		this.nickName = nickName;
	}
	
	public Integer getGender() 
	{
		return gender;
	}
	
	public void setGender(Integer gender) 
	{
		this.gender = gender;
	}
	
	public String getTelephone() 
	{
		return telephone;
	}
	
	public void setTelephone(String telephone) 
	{
		this.telephone = telephone;
	}
	
	public String getEmail() 
	{
		return email;
	}
	
	public void setEmail(String email) 
	{
		this.email = email;
	}
	
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String description) 
	{
		this.description = description;
	}
	
	public String getUpdateTime() 
	{
		return updateTime;
	}
	
	public void setUpdateTime(String updateTime) 
	{
		this.updateTime = updateTime;
	}
	
	public String getCreateTime() 
	{
		return createTime;
	}
	
	public void setCreateTime(String createTime) 
	{
		this.createTime = createTime;
	}

}
