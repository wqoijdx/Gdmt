package com.isoftstone.gdmt.consult.service;

import com.isoftstone.gdmt.consult.entity.SearchPriceEntity;
import com.isoftstone.gdmt.mybatis.entity.InternationalPriceEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;

import java.util.List;

public interface PriceService {
    List<InternationalPriceEntity> getPrice();

    PadingRstType<InternationalPriceEntity> getPriceInfolistByPage(SearchPriceEntity entity, PagingBean paging);


}