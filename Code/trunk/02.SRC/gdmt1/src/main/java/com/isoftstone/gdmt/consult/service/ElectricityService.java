package com.isoftstone.gdmt.consult.service;

import com.isoftstone.gdmt.consult.entity.SearchElectricEntity;
import com.isoftstone.gdmt.consult.entity.SearchElectricityEntity;
import com.isoftstone.gdmt.mybatis.entity.ElectricityEntity;
import com.isoftstone.gdmt.mybatis.entity.InternationalElectricEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;

import java.util.List;

public interface ElectricityService {
    List<ElectricityEntity> getElectricity();

    PadingRstType<ElectricityEntity> getElectricityInfolistByPage(SearchElectricityEntity entity, PagingBean paging);


}