package com.isoftstone.gdmt.consult.service.impl;

import com.isoftstone.gdmt.consult.entity.SearchElectricEntity;
import com.isoftstone.gdmt.consult.repository.ElectricDao;
import com.isoftstone.gdmt.consult.service.ElectricService;
import com.isoftstone.gdmt.mybatis.entity.InternationalElectricEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("electricService")
public class ElectricServiceImpl implements ElectricService {
    @Resource
    private ElectricDao electricDao;

    @Override
    public List<InternationalElectricEntity> getElectric() {
        return electricDao.getElectric();
    }

    @Override
    public PadingRstType<InternationalElectricEntity> getElectricInfolistByPage(SearchElectricEntity entity, PagingBean paging) {
        paging.deal(InternationalElectricEntity.class);
        PadingRstType<InternationalElectricEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(paging.getPage());
        List<InternationalElectricEntity> list = electricDao.queryElectricInfoByPage(entity,paging);
        padingRstType.setRawRecords(list);
        Integer total = electricDao.queryElectricInfoTotal(entity);
        padingRstType.setTotal(total);
        padingRstType.putItems();
        return padingRstType;
    }

}
