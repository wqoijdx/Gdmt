package com.isoftstone.gdmt.spring.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

@Service("passwordEncoder")
public class GdmtPasswordEncoder implements PasswordEncoder {
    @Value("${login.passwd.key}")
    private String passwdKey;
    @Override
    public String encode(CharSequence charSequence) {
        if (charSequence == null) {
            throw new IllegalArgumentException("rawPassword cannot be null");
        }
        String rst = DigestUtils.md5DigestAsHex((charSequence + passwdKey).getBytes());
        return rst;

    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        if(rawPassword == null || encodedPassword==null){
            throw new IllegalArgumentException("rawPassword cannot be null");
        }
       return encodedPassword.equals(encode(rawPassword));
    }

}
