package com.isoftstone.gdmt.platform.organ.controller;

import com.isoftstone.gdmt.mybatis.entity.PtDepartmentInfoEntity;
import com.isoftstone.gdmt.mybatis.entity.PtOrganEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.controller.BaseConctroller;
import com.isoftstone.gdmt.platform.department.entity.SearchDepartmentEntity;
import com.isoftstone.gdmt.platform.organ.service.OrganService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;


@Controller
@RequestMapping("/organ")
public class OrganController extends BaseConctroller {

    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private OrganService organService;

    @RequestMapping("/getOrganTree")
    @ResponseBody
    @Secured("ROLE_gdmt_data_organ")
    public List<ZtreeStrEntity> getOrganTree(){
        return organService.getOrganTree();
    }

//    跳转到页面
    @RequestMapping("/organPage")
    @Secured("ROLE_gdmt_data_organ")
    public String organPage(){
        return "organ/organPage";
    }

//根据organid获取organ
    @RequestMapping("/getOrganInfoById")
    @ResponseBody
    @Secured("ROLE_gdmt_data_organ")
    public PtOrganEntity organId(@RequestParam("organId") String organId){
        logger.info("organId:" + organId);
        return organService.getOrganInfoById(organId);
    }

    //依靠id修改organ
    @Secured("ROLE_gdmt_data_organ")
    @RequestMapping("/modifyOrganInfoById")
    @ResponseBody
    public String modifyOrganInfoById(PtOrganEntity entity){
        logger.info(entity);
        organService.modifyOrganInfoById(entity);
        return getSuccess("organ.modify.success");
    }


    //组织增加
    @Secured("ROLE_gdmt_data_organ")
    @RequestMapping("/addOrganInfoById")
    @ResponseBody
    public String addOrganInfoById(PtOrganEntity entity){
        logger.info(entity);
        organService.addOrganInfoById(entity);
        return getSuccess("organ.add.success");
    }

   //依靠id组织删除 （有子结构，岗位，国电不能删）
    @Secured("ROLE_gdmt_data_organ")
    @RequestMapping("/deleteOrganInfoById")
    @ResponseBody
    public String deleteOrganInfoById(@RequestParam("organId") String organId){
        logger.info("organId:" +  organId);
        organService.deleteOrganInfoById(organId);
             return getSuccess("organ.delete.success");
    }

     //获得组织ztree靠id
    @Secured("ROLE_gdmt_data_organ")
    @RequestMapping("/getOrganTreeByOrganId")
    @ResponseBody
    public List<ZtreeStrEntity> getOrganTreeByOrganId(@RequestParam("organId") String organId){
        logger.info("organId:" +  organId);
        return organService.getOrganTreeByOrganId(organId);
    }
    //获得组织和相应父组织
    @Secured("ROLE_gdmt_data_organ")
    @RequestMapping("/boundOrgan")
    @ResponseBody
    public String boundOrgan(@RequestParam("organId") String organId, @RequestParam("parentId") String parentId){
        logger.info("organId:" +  organId + " parentId:" + parentId);
        organService.boundOrgan(organId,parentId);
        return getSuccess("organ.bound.success");
    }


    //解绑
    @Secured("ROLE_gdmt_data_organ")
    @RequestMapping("/unBoundByOrganId")
    @ResponseBody
    public String unBoundByOrganId(@RequestParam("organId") String organId){
        logger.info("organId:" +  organId);
        organService.unBoundByOrganId(organId);
        return getSuccess("organ.unbound.success");
    }



//    @Secured("ROLE_gdmt_data_organ")
//    @RequestMapping("/getDepartByOrganId")
//    @ResponseBody
//    public List<PtDepartmentInfoEntity> getDepartByOrganId(HttpServletRequest request){
//        String organId = request.getParameter("organId");
//        logger.info("organId:" +  organId);
//        List<PtDepartmentInfoEntity> list = (List<PtDepartmentInfoEntity>) organService.getDepartByOrganId(organId);
//        return list;
//    }

    @Secured("ROLE_gdmt_data_organ")
    @RequestMapping("/queryDepListPage")
    @ResponseBody
    public PadingRstType<PtDepartmentInfoEntity> queryDepListPage(SearchDepartmentEntity search, PagingBean paging){
        logger.info(search);
        logger.info(paging);
        PadingRstType<PtDepartmentInfoEntity> ptDepartmentEntityPadingRstType = organService.queryDepListPage(search,paging);
        return ptDepartmentEntityPadingRstType;
    }


}
