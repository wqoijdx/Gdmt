package com.isoftstone.gdmt.duty.repository;

import com.isoftstone.gdmt.duty.entity.SearchDutyEntity;
import com.isoftstone.gdmt.mybatis.entity.PtRRoleOrganEntity;
import com.isoftstone.gdmt.mybatis.entity.PtUserEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DutyDao {
    List<PtRRoleOrganEntity> queryDutyListPage(@Param("search") SearchDutyEntity search, @Param("paging") PagingBean paging);
    Integer queryDutyTotal(@Param("search") SearchDutyEntity search);
    List<ZtreeStrEntity> queryDutyTree();
    List<ZtreeStrEntity> queryRoleTree();

    PtRRoleOrganEntity getDutyInfoById(@Param("organUuid") String organUuid);
    List<String> getOrganByDutyId(@Param("dutyid") String dutyid);
    void insertOrganAndDutyId(@Param("dutyid") String dutyid,@Param("organUuid") String organUuid);
    void insertRoleAndDutyId(@Param("dutyid") String dutyid, @Param("roleUuid") String roleUuid);




    void addDutyInfoById(@Param("entity") PtRRoleOrganEntity entity);

   int deleteDutyInfoById(@Param("dutyid") String dutyid);


    List<PtRRoleOrganEntity> queryDutyListPage2(@Param("search") SearchDutyEntity search, @Param("paging") PagingBean paging);

    List<PtUserEntity> selectUserById(@Param("dutyid") String dutyid);
}
