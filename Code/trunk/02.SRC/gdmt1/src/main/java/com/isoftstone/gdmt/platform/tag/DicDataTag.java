package com.isoftstone.gdmt.platform.tag;

import com.isoftstone.gdmt.platform.tag.entity.DictTagEntity;
import com.isoftstone.gdmt.platform.tag.service.DicDataTagService;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.List;

public class DicDataTag extends SimpleTagSupport {
    private String id;
    private String name;
    private String style;
    private String classez;
    private String group;
    private String regex;
    private String tip;
    private String value;

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getClassez() {
        return classez;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setClassez(String classez) {
        this.classez = classez;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void doTag() throws JspException, IOException {
        PageContext pageContext = (PageContext) getJspContext();
        JspWriter out = pageContext.getOut();

        ServletContext sc = pageContext.getServletContext();
        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(sc);

        DicDataTagService dicDataTagService = webApplicationContext.getBean("dicDataTagService", DicDataTagService.class);
        List<DictTagEntity> dictTagEntities = dicDataTagService.queryDicInfoByGroupId(group);

//        MessageSource messageSource = webApplicationContext.getBean("dicDataTagService", MessageSource.class);

        out.write("<select");
        if(id != null){
            out.write(" id=\"" + id + "\" ");
        }
        if(name != null){
            out.write(" name=\"" + name + "\" ");
        }
        if(style != null){
            out.write(" style=\"" + style + "\" ");
        }
        if(classez != null){
            out.write(" class=\"" + classez + "\" ");
        }
        if(regex != null){
            out.write(" regex=\"" + regex + "\" ");
        }
        if(tip != null){
            out.write(" tip=\"" + tip + "\" ");
        }
        out.write(">");

//        out.write(" <option value=\"\">");
//        out.write(messageSource.getMessage("adfaa",null,locale));
//        out.write("</option>");
        for(DictTagEntity item:dictTagEntities){
            out.write(" <option value=\"");
            out.write(item.getDicCode());
            out.write("\" ");
            if(value != null && value.equals(item.getDicCode())){
                out.write(" selected ");
            }
            out.write(">");
            out.write(item.getDictName());
            out.write("</option>");
        }


        out.write("</select>");
    }


}
