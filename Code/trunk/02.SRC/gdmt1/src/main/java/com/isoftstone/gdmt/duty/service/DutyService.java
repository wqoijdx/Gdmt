package com.isoftstone.gdmt.duty.service;


import com.isoftstone.gdmt.duty.entity.SearchDutyEntity;

import com.isoftstone.gdmt.mybatis.entity.PtRRoleOrganEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;

import java.util.List;

public interface DutyService {

    PadingRstType<PtRRoleOrganEntity> queryDutyListPage(SearchDutyEntity search, PagingBean paging);
    List<ZtreeStrEntity> getDutyTree();
    List<ZtreeStrEntity> queryRoleTree();
    PtRRoleOrganEntity getDutyInfoById(String organ_uuId);


    
    List<ZtreeStrEntity> getOrganByDutyId(String dutyid);
    List<ZtreeStrEntity> getRoleByDutyId(String dutyid);
    void setOrganByDutyId(String dutyid, String organUuidArray);
    void setRoleByDutyId(String dutyid, String roleUuidArray);


    void addDutyInfoById(PtRRoleOrganEntity entity);

    int deleteDutyInfoById(String dutyid);



    PadingRstType<PtRRoleOrganEntity> queryDutyListPage2(SearchDutyEntity search, PagingBean paging);
}