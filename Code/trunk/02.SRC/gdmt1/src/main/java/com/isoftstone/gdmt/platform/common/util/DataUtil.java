package com.isoftstone.gdmt.platform.common.util;

import java.util.UUID;

public final class DataUtil {

    public static String getUUIDShort(){
        String uuid = UUID.randomUUID().toString();
        return uuid.replace("-","");
    }
}
