package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class PtMenuI18nEntity implements Serializable
{
	@Columns("menu_id")
	private String menuId;

	@Columns("lang_id")
	private Integer langId;

	@Columns("menu_name")
	private String menuName;
	
	public String getMenuId() 
	{
		return menuId;
	}
	
	public void setMenuId(String menuId) 
	{
		this.menuId = menuId;
	}
	
	public Integer getLangId() 
	{
		return langId;
	}
	
	public void setLangId(Integer langId) 
	{
		this.langId = langId;
	}
	
	public String getMenuName() 
	{
		return menuName;
	}
	
	public void setMenuName(String menuName) 
	{
		this.menuName = menuName;
	}

}
