package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class InternationalPriceEntity implements Serializable
{
	@Columns("price_id")
	private Integer priceId;

	@Columns("price_date")
	private String priceDate;

	@Columns("australia5500k")
	private Double australia5500k;

	@Columns("australia5500k_yoy")
	private Double australia5500kYoy;

	@Columns("indonesia4700k")
	private Double indonesia4700k;

	@Columns("indonesia4700k_yoy")
	private Double indonesia4700kYoy;

	@Columns("indonesia3800k")
	private Double indonesia3800k;

	@Columns("Indonesia3800k_yoy")
	private Double indonesia3800kYoy;
	
	public Integer getPriceId() 
	{
		return priceId;
	}
	
	public void setPriceId(Integer priceId) 
	{
		this.priceId = priceId;
	}
	
	public String getPriceDate() 
	{
		return priceDate;
	}
	
	public void setPriceDate(String priceDate) 
	{
		this.priceDate = priceDate;
	}
	
	public Double getAustralia5500k() 
	{
		return australia5500k;
	}
	
	public void setAustralia5500k(Double australia5500k) 
	{
		this.australia5500k = australia5500k;
	}
	
	public Double getAustralia5500kYoy() 
	{
		return australia5500kYoy;
	}
	
	public void setAustralia5500kYoy(Double australia5500kYoy) 
	{
		this.australia5500kYoy = australia5500kYoy;
	}
	
	public Double getIndonesia4700k() 
	{
		return indonesia4700k;
	}
	
	public void setIndonesia4700k(Double indonesia4700k) 
	{
		this.indonesia4700k = indonesia4700k;
	}
	
	public Double getIndonesia4700kYoy() 
	{
		return indonesia4700kYoy;
	}
	
	public void setIndonesia4700kYoy(Double indonesia4700kYoy) 
	{
		this.indonesia4700kYoy = indonesia4700kYoy;
	}
	
	public Double getIndonesia3800k() 
	{
		return indonesia3800k;
	}
	
	public void setIndonesia3800k(Double indonesia3800k) 
	{
		this.indonesia3800k = indonesia3800k;
	}
	
	public Double getIndonesia3800kYoy() 
	{
		return indonesia3800kYoy;
	}
	
	public void setIndonesia3800kYoy(Double indonesia3800kYoy) 
	{
		this.indonesia3800kYoy = indonesia3800kYoy;
	}

}
