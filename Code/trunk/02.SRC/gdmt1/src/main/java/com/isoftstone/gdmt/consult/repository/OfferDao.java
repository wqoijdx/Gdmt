package com.isoftstone.gdmt.consult.repository;

import com.isoftstone.gdmt.consult.entity.SearchOfferEntity;
import com.isoftstone.gdmt.mybatis.entity.InternationalOfferEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OfferDao {
    List<InternationalOfferEntity> getOffer();

    List<InternationalOfferEntity> queryOfferInfoByPage(@Param("entity") SearchOfferEntity entity, @Param("paging") PagingBean paging);

    Integer queryOfferInfoTotal(@Param("entity") SearchOfferEntity entity);

}
