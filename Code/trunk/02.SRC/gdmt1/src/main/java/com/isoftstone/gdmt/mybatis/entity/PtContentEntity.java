package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class PtContentEntity implements Serializable
{
	@Columns("id")
	private Integer id;

	@Columns("CHANNEL_ID")
	private String channelId;

	@Columns("TYPE_ID")
	private String typeId;

	@Columns("TOP_LEVEL")
	private String topLevel;

	@Columns("MODEL_ID")
	private String modelId;
	
	public Integer getId() 
	{
		return id;
	}
	
	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public String getChannelId() 
	{
		return channelId;
	}
	
	public void setChannelId(String channelId) 
	{
		this.channelId = channelId;
	}
	
	public String getTypeId() 
	{
		return typeId;
	}
	
	public void setTypeId(String typeId) 
	{
		this.typeId = typeId;
	}
	
	public String getTopLevel() 
	{
		return topLevel;
	}
	
	public void setTopLevel(String topLevel) 
	{
		this.topLevel = topLevel;
	}
	
	public String getModelId() 
	{
		return modelId;
	}
	
	public void setModelId(String modelId) 
	{
		this.modelId = modelId;
	}

}
