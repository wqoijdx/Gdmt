package com.isoftstone.gdmt.consult.repository;

import com.isoftstone.gdmt.consult.entity.SearchPortEntity;
import com.isoftstone.gdmt.mybatis.entity.CoalHarborEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PortDao {
    List<CoalHarborEntity> getPort();

    List<CoalHarborEntity> queryPortInfoByPage(@Param("entity") SearchPortEntity entity, @Param("paging") PagingBean paging);

    Integer queryPortInfoTotal(@Param("entity") SearchPortEntity entity);

}
