package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class SmRoleRefMenuEntity implements Serializable
{
	@Columns("menu_id")
	private Integer menuId;

	@Columns("role_id")
	private String roleId;
	
	public Integer getMenuId() 
	{
		return menuId;
	}
	
	public void setMenuId(Integer menuId) 
	{
		this.menuId = menuId;
	}
	
	public String getRoleId() 
	{
		return roleId;
	}
	
	public void setRoleId(String roleId) 
	{
		this.roleId = roleId;
	}

}
