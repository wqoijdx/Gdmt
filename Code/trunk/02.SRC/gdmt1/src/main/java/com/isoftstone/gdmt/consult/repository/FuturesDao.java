package com.isoftstone.gdmt.consult.repository;

import com.isoftstone.gdmt.consult.entity.SearchFuturesEntity;
import com.isoftstone.gdmt.mybatis.entity.FuturesPriceEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FuturesDao {
    List<FuturesPriceEntity> getFutures();

    List<FuturesPriceEntity> queryFuturesInfoByPage(@Param("entity") SearchFuturesEntity entity, @Param("paging") PagingBean paging);

    Integer queryFuturesInfoTotal(@Param("entity") SearchFuturesEntity entity);

}
