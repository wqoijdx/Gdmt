package com.isoftstone.gdmt.platform.dictionary.repository;

import com.isoftstone.gdmt.mybatis.entity.PtDictionaryEntity;
import com.isoftstone.gdmt.mybatis.entity.PtDictionaryI18nEntity;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DictionaryDao {
    List<ZtreeStrEntity> queryDictionaryTree();

    PtDictionaryEntity getDictionaryInfoById(@Param("dictId") String dictId);

    void modifyDictionaryInfoById(@Param("entity") PtDictionaryEntity entity);

    void addDictionaryInfo(@Param("entity") PtDictionaryEntity entity);

    void deleteDictionaryInfo(@Param("dictId") String dictId);

    List<PtDictionaryI18nEntity> getI18nByDictionaryId(@Param("dictId")String dictId);

    void saveI18nData(@Param("entity") PtDictionaryI18nEntity entity);

    void addI18nData(@Param("entity") PtDictionaryI18nEntity entity);

    void deleteI18nData(@Param("entity") PtDictionaryI18nEntity entity);
}
