package com.isoftstone.gdmt.user.service;

import com.isoftstone.gdmt.mybatis.entity.PtUserEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.user.entity.SearchUserEntity;

import java.util.List;

public interface UserService {
    List<PtUserEntity> queryUserListByName(SearchUserEntity entity);

    void deleteUserInfoById(String ids);

    PtUserEntity queryUserListByName(String userName);

    List<String> queryPrivilegeCode(String userUuid);

    PadingRstType<PtUserEntity> getUserInfolistByPage(SearchUserEntity entity, PagingBean paging);

    List<ZtreeStrEntity> getRoleByUserId(String userUuid);

    void setRoleUuidByUserUuid(String userUuid, String roleUuidArray);

    void save(PtUserEntity user);

    PtUserEntity getById(String id);

    void updateById(PtUserEntity user);

    void addUserInfoById(PtUserEntity entity);

    PtUserEntity queryUserListById(String userUuid);

    void modifyUserListById(PtUserEntity entity);

    List<ZtreeStrEntity> getDutyByUserId(String userUuid);

    void setDutyIdByUserUuid(String userUuid, String dutyIdArray);

    void setDepUuIdByUserUuid(String userUuid, String depUuId);

    List<ZtreeStrEntity> getDepByUserUuid(String userUuid);
}
