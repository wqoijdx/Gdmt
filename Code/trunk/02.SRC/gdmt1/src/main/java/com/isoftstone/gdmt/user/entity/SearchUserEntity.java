package com.isoftstone.gdmt.user.entity;

import java.io.Serializable;

public class SearchUserEntity implements Serializable {
    private String userName;

    private String searchUserName;
    private String searchNiceName;
    private String searchPhone;
    private String searchEmail;

    public String getSearchUserName() {
        return searchUserName;
    }

    public void setSearchUserName(String searchUserName) {
        this.searchUserName = searchUserName;
    }

    public String getSearchNiceName() {
        return searchNiceName;
    }

    public void setSearchNiceName(String searchNiceName) {
        this.searchNiceName = searchNiceName;
    }

    public String getSearchPhone() {
        return searchPhone;
    }

    public void setSearchPhone(String searchPhone) {
        this.searchPhone = searchPhone;
    }

    public String getSearchEmail() {
        return searchEmail;
    }

    public void setSearchEmail(String searchEmail) {
        this.searchEmail = searchEmail;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "SearchUserEntity{" +
                "userName='" + userName + '\'' +
                ", searchUserName='" + searchUserName + '\'' +
                ", searchNiceName='" + searchNiceName + '\'' +
                ", searchPhone='" + searchPhone + '\'' +
                ", searchEmail='" + searchEmail + '\'' +
                '}';
    }
}
