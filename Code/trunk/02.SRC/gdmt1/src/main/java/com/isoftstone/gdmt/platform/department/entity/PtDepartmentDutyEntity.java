package com.isoftstone.gdmt.platform.department.entity;


import com.isoftstone.gdmt.mybatis.annotation.Columns;

import java.io.Serializable;

public class PtDepartmentDutyEntity implements Serializable {
    @Columns("DUTYID")
    private String dutyid;

    @Columns("NAME")
    private String name;

    @Columns("ORGAN_UUID")
    private String organUuid;

    @Columns("ROLE_UUID")
    private String roleUuid;

    @Columns("ROLE_NAME")
    private String roleName;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDutyid()
    {
        return dutyid;
    }

    public void setDutyid(String dutyid)
    {
        this.dutyid = dutyid;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getOrganUuid()
    {
        return organUuid;
    }

    public void setOrganUuid(String organUuid)
    {
        this.organUuid = organUuid;
    }

    public String getRoleUuid()
    {
        return roleUuid;
    }

    public void setRoleUuid(String roleUuid)
    {
        this.roleUuid = roleUuid;
    }



}
