package com.isoftstone.gdmt.consult.service;

import com.isoftstone.gdmt.consult.entity.SearchPmiEntity;
import com.isoftstone.gdmt.mybatis.entity.PmiEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;

import java.util.List;

public interface PmiService {
    List<PmiEntity> getPmi();

    PadingRstType<PmiEntity> getPmiInfolistByPage(SearchPmiEntity entity, PagingBean paging);


}