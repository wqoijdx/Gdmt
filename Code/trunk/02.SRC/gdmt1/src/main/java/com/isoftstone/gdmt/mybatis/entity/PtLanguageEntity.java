package com.isoftstone.gdmt.mybatis.entity;
import com.isoftstone.gdmt.mybatis.annotation.Columns;

import java.io.Serializable;


public class PtLanguageEntity implements Serializable
{
	@Columns("lang_id")
	private Integer langId;

	@Columns("lang_type")
	private String langType;

	@Columns("lang_desc")
	private String langDesc;
	
	public Integer getLangId() 
	{
		return langId;
	}
	
	public void setLangId(Integer langId) 
	{
		this.langId = langId;
	}
	
	public String getLangType() 
	{
		return langType;
	}
	
	public void setLangType(String langType) 
	{
		this.langType = langType;
	}
	
	public String getLangDesc() 
	{
		return langDesc;
	}
	
	public void setLangDesc(String langDesc) 
	{
		this.langDesc = langDesc;
	}

}
