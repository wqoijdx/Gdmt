package com.isoftstone.gdmt.platform.department.service;

import com.isoftstone.gdmt.mybatis.entity.PtDepartmentEntity;
import com.isoftstone.gdmt.mybatis.entity.PtRRoleOrganEntity;
import com.isoftstone.gdmt.mybatis.entity.PtUserEntity;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.department.entity.PtDepartmentDutyEntity;

import java.util.List;

public interface DepartmentService {
    List<ZtreeStrEntity> getDepartmentTree();

    PtDepartmentEntity getDepartmentInfoById(String departId);

    void deleteMenuInfoById(String departId);

    void modifyDepartInfoById(PtDepartmentEntity entity);

    void addDepartInfoById(PtDepartmentEntity entity);

    List<ZtreeStrEntity> getDepartTreeByDepartId(String departId);

    void boundDepart(String departId, String belongCenter);

    void unBoundByDepartId(String departId);

    List<PtDepartmentDutyEntity> getDepartmentDutyInfoById(String departId);

//    void adjustDepartment(String departId, String organId);

    List<PtUserEntity> getUserInfoByDutyid(String dutyid);
}