package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class VoteUserEntity implements Serializable
{
	@Columns("vote_uuid")
	private String voteUuid;

	@Columns("user_id")
	private String userId;
	
	public String getVoteUuid() 
	{
		return voteUuid;
	}
	
	public void setVoteUuid(String voteUuid) 
	{
		this.voteUuid = voteUuid;
	}
	
	public String getUserId() 
	{
		return userId;
	}
	
	public void setUserId(String userId) 
	{
		this.userId = userId;
	}

}
