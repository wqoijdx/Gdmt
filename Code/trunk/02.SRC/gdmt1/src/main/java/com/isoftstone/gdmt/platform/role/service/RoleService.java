package com.isoftstone.gdmt.platform.role.service;

import com.isoftstone.gdmt.mybatis.entity.PtRoleEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.role.entity.SearchRoleEntity;

import java.util.List;

public interface RoleService {
    PadingRstType<PtRoleEntity> queryRoleListPage(SearchRoleEntity search, PagingBean paging);

    List<ZtreeStrEntity> queryMenuByRoleId(String roleUuid);

    void saveMenuIdAndRoleId(String roleUuid, String menuArray);

    /**
     * 创建角色
     * @param role
     */
    void create(PtRoleEntity role);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    PtRoleEntity getById(String id);

    /**
     * 修改角色
     * @param role
     */
    void updateById(PtRoleEntity role);

    /**
     * 删除角色
     * @param id
     */
    void removeById(String id);

    /**
     * 查询绑定角色的用户数量
     * @param id
     * @return
     */
    int getUserCountByRoleId(String id);
}
