package com.isoftstone.gdmt.consult.service;


import com.isoftstone.gdmt.consult.entity.SearchBspiEntity;
import com.isoftstone.gdmt.consult.entity.SearchGdpEntity;
import com.isoftstone.gdmt.mybatis.entity.CoalBspiEntity;
import com.isoftstone.gdmt.mybatis.entity.GdpEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;

import java.util.List;

public interface GdpService {
    List<GdpEntity> getGdp();

    PadingRstType<GdpEntity> getGdpInfolistByPage(SearchGdpEntity entity, PagingBean paging);


}