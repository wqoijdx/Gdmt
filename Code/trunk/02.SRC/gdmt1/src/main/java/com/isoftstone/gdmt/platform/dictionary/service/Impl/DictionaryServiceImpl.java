package com.isoftstone.gdmt.platform.dictionary.service.Impl;

import com.isoftstone.gdmt.mybatis.entity.PtDictionaryEntity;
import com.isoftstone.gdmt.mybatis.entity.PtDictionaryI18nEntity;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.dictionary.repository.DictionaryDao;
import com.isoftstone.gdmt.platform.dictionary.service.DictionaryService;
import com.isoftstone.gdmt.platform.role.repository.RoleDao;
import com.isoftstone.gdmt.platform.tag.entity.DictTagEntity;
import com.isoftstone.gdmt.platform.tag.service.DicDataTagService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

@Service("DictionaryService")
public class DictionaryServiceImpl implements DictionaryService {

    @Resource
    private DictionaryDao dictionaryDao;



    @Override
    public List<ZtreeStrEntity> getDictionaryTree() {
        List<ZtreeStrEntity> list = dictionaryDao.queryDictionaryTree();
        Set<ZtreeStrEntity> set = new HashSet<>();
        ZtreeStrEntity parent;
        for (ZtreeStrEntity item:list){
            parent = new ZtreeStrEntity();
            parent.setId(item.getpId());
            parent.setName(item.getpId());
            set.add(parent);
        }
        for(ZtreeStrEntity item:set){
            list.add(item);
        }

        return list;
    }

    @Override
    public PtDictionaryEntity getDictionaryInfoById(String dictId) {
        return dictionaryDao.getDictionaryInfoById(dictId);
    }

    @Override
    public void modifyDictionaryInfoById(PtDictionaryEntity entity) {
        dictionaryDao.modifyDictionaryInfoById(entity);
    }

    @Override
    public void addDictionaryInfo(PtDictionaryEntity entity) {
        dictionaryDao.addDictionaryInfo(entity);
    }

    @Override
    public void deleteDictionaryInfo(String dictId) {
        dictionaryDao.deleteDictionaryInfo(dictId);
    }

    @Override
    public List<PtDictionaryI18nEntity> getI18nByDictionaryId(String dictId) {
        return dictionaryDao.getI18nByDictionaryId(dictId);
    }

    @Override
    public void saveI18nData(PtDictionaryI18nEntity entity) {
        dictionaryDao.saveI18nData(entity);
    }

    @Override
    public void addI18nData(PtDictionaryI18nEntity entity) {
        dictionaryDao.addI18nData(entity);
    }

    @Override
    public void deleteI18nData(PtDictionaryI18nEntity entity) {
        dictionaryDao.deleteI18nData(entity);
    }


}