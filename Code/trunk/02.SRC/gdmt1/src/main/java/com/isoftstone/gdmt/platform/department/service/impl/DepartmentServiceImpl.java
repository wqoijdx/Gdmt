package com.isoftstone.gdmt.platform.department.service.impl;

import com.isoftstone.gdmt.mybatis.entity.PtDepartmentEntity;
import com.isoftstone.gdmt.mybatis.entity.PtRRoleOrganEntity;
import com.isoftstone.gdmt.mybatis.entity.PtUserEntity;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.common.util.DataUtil;
import com.isoftstone.gdmt.platform.department.entity.PtDepartmentDutyEntity;
import com.isoftstone.gdmt.platform.department.repository.DepartmentDao;
import com.isoftstone.gdmt.platform.department.service.DepartmentService;
import com.isoftstone.gdmt.platform.tag.entity.DictTagEntity;
import com.isoftstone.gdmt.platform.tag.service.DataSelectService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Locale;

@Service("departmentService")
public class DepartmentServiceImpl implements DepartmentService, DataSelectService {

    @Resource
    private DepartmentDao departmentDao;
    @Value("${ubound.uuid}")
    private String uboundUuid;
    @Value("${root.uuid}")
    private String rootUuid;
    @Resource
    private MessageSource messageSource;

    @Override
    public List<ZtreeStrEntity> getDepartmentTree() {
        List<ZtreeStrEntity> list = departmentDao.queryDepartmentTree();
        ZtreeStrEntity ztreeStrEntity = new ZtreeStrEntity();
        ztreeStrEntity.setId(uboundUuid);
        Locale locale = LocaleContextHolder.getLocale();
        String unboundName = messageSource.getMessage("common.unbound.name", null, locale);
        ztreeStrEntity.setName(unboundName);
        list.add(ztreeStrEntity);
        Integer index = 0;
        for (ZtreeStrEntity item : list) {
            if (index < 10) {
                item.setOpen(true);
            } else {
                item.setOpen(false);
            }
            index++;
        }
        return list;
    }

    @Override
    public PtDepartmentEntity getDepartmentInfoById(String departId) {
        return departmentDao.getDepartmentInfoById(departId);
    }

    @Override
    public void deleteMenuInfoById(String departId) {
        departmentDao.deleteDepartInfoById(departId);
    }

    @Override
    public void modifyDepartInfoById(PtDepartmentEntity entity) {
        departmentDao.modifyDepartInfoById(entity);
    }

    @Override
    public void addDepartInfoById(PtDepartmentEntity entity) {
        entity.setDepUuid(DataUtil.getUUIDShort());
        entity.setBelongCenter(uboundUuid);
        departmentDao.addDepartInfoById(entity);

    }

    @Override
    public List<ZtreeStrEntity> getDepartTreeByDepartId(String departId) {
        String parentId = departmentDao.queryDepartTreeByDepartId(departId);
        List<ZtreeStrEntity> list = getDepartmentTree();
        ZtreeStrEntity ztreeStrEntity = new ZtreeStrEntity();
        ztreeStrEntity.setId(rootUuid);
        Locale locale = LocaleContextHolder.getLocale();
        String rootName = messageSource.getMessage("common.root.node.name", null, locale);
        ztreeStrEntity.setName(rootName);
        list.add(ztreeStrEntity);

        if (departId != null) {
            for (ZtreeStrEntity item : list) {
                if (departId.equals(item.getId())) {
                    item.setChecked(true);
                }
            }
        }

        return list;
    }

    @Override
    public void boundDepart(String departId, String belongCenter) {
        departmentDao.updateOrganIdByDepartId(departId, belongCenter);
    }

    @Override
    public void unBoundByDepartId(String departId) {
        departmentDao.updateOrganIdByDepartId(departId, uboundUuid);
    }

    @Override
    public List<PtDepartmentDutyEntity> getDepartmentDutyInfoById(String departId) {
        return departmentDao.getDepartmentDutyInfoById(departId);
    }

//    @Override
//    public void adjustDepartment(String departId, String organId) {
//        departmentDao.adjustDepartment(departId,organId);
//    }

    @Override
    public List<PtUserEntity> getUserInfoByDutyid(String dutyid) {
        return departmentDao.getUserInfoByDutyid(dutyid);
    }


    @Override
    public List<DictTagEntity> queryTagElementList(String language) {
        return departmentDao.queryTagElementList();
    }
}