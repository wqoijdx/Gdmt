package com.isoftstone.gdmt.platform.login.repository;

import com.isoftstone.gdmt.mybatis.entity.PtMenuEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface LoginDao {
    List<PtMenuEntity> queryMenuByUserId(@Param("userUuid")String userUuid, @Param("langType") String langType);
}
