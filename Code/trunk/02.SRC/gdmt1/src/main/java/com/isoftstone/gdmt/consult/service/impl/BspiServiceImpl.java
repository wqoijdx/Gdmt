package com.isoftstone.gdmt.consult.service.impl;

import com.isoftstone.gdmt.consult.entity.SearchBspiEntity;
import com.isoftstone.gdmt.consult.repository.BspiDao;
import com.isoftstone.gdmt.consult.service.BspiService;
import com.isoftstone.gdmt.mybatis.entity.CoalBspiEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("bspiService")
public class BspiServiceImpl implements BspiService {
    @Resource
    private BspiDao bspiDao;

    @Override
    public List<CoalBspiEntity> getBspi() {
        return bspiDao.getBspi();
    }

    @Override
    public PadingRstType<CoalBspiEntity> getBspiInfolistByPage(SearchBspiEntity entity, PagingBean paging) {
        paging.deal(CoalBspiEntity.class);
        PadingRstType<CoalBspiEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(paging.getPage());
        List<CoalBspiEntity> list = bspiDao.queryBspiInfoByPage(entity,paging);
        padingRstType.setRawRecords(list);
        Integer total = bspiDao.queryBspiInfoTotal(entity);
        padingRstType.setTotal(total);
        padingRstType.putItems();
        return padingRstType;
    }

}
