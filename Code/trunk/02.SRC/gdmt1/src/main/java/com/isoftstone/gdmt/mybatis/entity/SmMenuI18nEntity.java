package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class SmMenuI18nEntity implements Serializable
{
	@Columns("menu_id")
	private Integer menuId;

	@Columns("language_id")
	private Integer languageId;

	@Columns("menu_name")
	private String menuName;
	
	public Integer getMenuId() 
	{
		return menuId;
	}
	
	public void setMenuId(Integer menuId) 
	{
		this.menuId = menuId;
	}
	
	public Integer getLanguageId() 
	{
		return languageId;
	}
	
	public void setLanguageId(Integer languageId) 
	{
		this.languageId = languageId;
	}
	
	public String getMenuName() 
	{
		return menuName;
	}
	
	public void setMenuName(String menuName) 
	{
		this.menuName = menuName;
	}

}
