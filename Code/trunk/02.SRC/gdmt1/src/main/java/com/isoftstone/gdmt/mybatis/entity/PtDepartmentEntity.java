package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class PtDepartmentEntity implements Serializable
{
	@Columns("DEP_UUID")
	private String depUuid;

	@Columns("ORGAN_UUID")
	private String organUuid;

	@Columns("BRANCH_NAME")
	private String branchName;

	@Columns("BELONG_CENTER")
	private String belongCenter;

	@Columns("MODIFIERID")
	private String modifierid;

	@Columns("MODTIME")
	private String modtime;

	@Columns("DEL_FLAG")
	private String delFlag;

	@Columns("REMARK")
	private String remark;

	public String getDepUuid()
	{
		return depUuid;
	}

	public void setDepUuid(String depUuid)
	{
		this.depUuid = depUuid;
	}

	public String getOrganUuid()
	{
		return organUuid;
	}

	public void setOrganUuid(String organUuid)
	{
		this.organUuid = organUuid;
	}

	public String getBranchName()
	{
		return branchName;
	}

	public void setBranchName(String branchName)
	{
		this.branchName = branchName;
	}

	public String getBelongCenter()
	{
		return belongCenter;
	}

	public void setBelongCenter(String belongCenter)
	{
		this.belongCenter = belongCenter;
	}

	public String getModifierid()
	{
		return modifierid;
	}

	public void setModifierid(String modifierid)
	{
		this.modifierid = modifierid;
	}

	public String getModtime()
	{
		return modtime;
	}

	public void setModtime(String modtime)
	{
		this.modtime = modtime;
	}

	public String getDelFlag()
	{
		return delFlag;
	}

	public void setDelFlag(String delFlag)
	{
		this.delFlag = delFlag;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

   @Override
   public String toString() {
      return "PtDepartmentEntity{" +
              "depUuid='" + depUuid + '\'' +
              ", organUuid='" + organUuid + '\'' +
              ", branchName='" + branchName + '\'' +
              ", belongCenter='" + belongCenter + '\'' +
              ", modifierid='" + modifierid + '\'' +
              ", modtime='" + modtime + '\'' +
              ", delFlag='" + delFlag + '\'' +
              ", remark='" + remark + '\'' +
              '}';
   }
}
