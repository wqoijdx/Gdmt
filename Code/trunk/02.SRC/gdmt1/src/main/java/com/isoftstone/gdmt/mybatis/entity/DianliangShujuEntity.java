package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class DianliangShujuEntity implements Serializable
{
	@Columns("dianliangdata_id")
	private Integer dianliangdataId;

	@Columns("dianliangdata_date")
	private String dianliangdataDate;

	@Columns("dianliangdata_fdv")
	private Double dianliangdataFdv;

	@Columns("dianliangdata_fdy")
	private Double dianliangdataFdy;

	@Columns("dianliangdata_acv")
	private Double dianliangdataAcv;

	@Columns("dianliangdata_acy")
	private Double dianliangdataAcy;
	
	public Integer getDianliangdataId() 
	{
		return dianliangdataId;
	}
	
	public void setDianliangdataId(Integer dianliangdataId) 
	{
		this.dianliangdataId = dianliangdataId;
	}
	
	public String getDianliangdataDate() 
	{
		return dianliangdataDate;
	}
	
	public void setDianliangdataDate(String dianliangdataDate) 
	{
		this.dianliangdataDate = dianliangdataDate;
	}
	
	public Double getDianliangdataFdv() 
	{
		return dianliangdataFdv;
	}
	
	public void setDianliangdataFdv(Double dianliangdataFdv) 
	{
		this.dianliangdataFdv = dianliangdataFdv;
	}
	
	public Double getDianliangdataFdy() 
	{
		return dianliangdataFdy;
	}
	
	public void setDianliangdataFdy(Double dianliangdataFdy) 
	{
		this.dianliangdataFdy = dianliangdataFdy;
	}
	
	public Double getDianliangdataAcv() 
	{
		return dianliangdataAcv;
	}
	
	public void setDianliangdataAcv(Double dianliangdataAcv) 
	{
		this.dianliangdataAcv = dianliangdataAcv;
	}
	
	public Double getDianliangdataAcy() 
	{
		return dianliangdataAcy;
	}
	
	public void setDianliangdataAcy(Double dianliangdataAcy) 
	{
		this.dianliangdataAcy = dianliangdataAcy;
	}

}
