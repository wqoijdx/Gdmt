package com.isoftstone.gdmt.consult.controller;

import com.isoftstone.gdmt.consult.entity.SearchFuturesEntity;
import com.isoftstone.gdmt.consult.service.FuturesService;
import com.isoftstone.gdmt.mybatis.entity.FuturesPriceEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping({"/consult"})
public class FuturesController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private FuturesService futuresService;

    @Secured("ROLE_gdmt_data_fp")
    @RequestMapping({"/futuresPage"})
    public String futuresPage() {
        return "consult/futuresPage";
    }

    @Secured("ROLE_gdmt_data_fp")
    @RequestMapping({"/getFutures"})
    @ResponseBody
    public List<FuturesPriceEntity> getFutures() {
        return futuresService.getFutures();
    }

    @Secured("ROLE_gdmt_data_fp")
    @RequestMapping({"/getFuturesInfolistByPage"})
    @ResponseBody
    public PadingRstType<FuturesPriceEntity> getFuturesInfolistByPage(SearchFuturesEntity entity, PagingBean paging) {
        this.logger.info(entity);
        this.logger.info(paging);
        PadingRstType<FuturesPriceEntity> padingRstType = futuresService.getFuturesInfolistByPage(entity,paging);
        return padingRstType;
    }

}
