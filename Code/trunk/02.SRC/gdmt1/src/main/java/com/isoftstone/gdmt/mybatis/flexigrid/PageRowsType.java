package com.isoftstone.gdmt.mybatis.flexigrid;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PageRowsType implements Serializable {

    private Integer id;

    private List<Object> cell =
            new ArrayList<Object>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Object> getCell() {
        return cell;
    }

    public void setCell(List<Object> cell) {
        this.cell = cell;
    }

    @Override
    public String toString() {
        return "PageRowsType{" +
                "id=" + id +
                ", cell=" + cell +
                '}';
    }

    public void addCell(Object o) {
        this.cell.add(o);
    }
}
