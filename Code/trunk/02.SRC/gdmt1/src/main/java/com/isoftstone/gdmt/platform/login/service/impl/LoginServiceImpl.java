package com.isoftstone.gdmt.platform.login.service.impl;

import com.isoftstone.gdmt.mybatis.entity.PtMenuEntity;
import com.isoftstone.gdmt.platform.login.entity.MenuEntity;
import com.isoftstone.gdmt.platform.login.repository.LoginDao;
import com.isoftstone.gdmt.platform.login.service.LoginService;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.LinkedList;
import java.util.List;

@Service("loginService")
public class LoginServiceImpl implements LoginService {
    @Resource
    private LoginDao loginDao;

    @Override
    public List<MenuEntity> queryMenuByUserId(String userUuid) {
        String langType = LocaleContextHolder.getLocale() + "";
        List<PtMenuEntity> ptMenuEntityList = loginDao.queryMenuByUserId(userUuid,langType);
        List<MenuEntity> menuEntityList = new LinkedList<>();
        MenuEntity  menuEntity1;
        MenuEntity  menuEntity2;
        MenuEntity  menuEntity3;
        for(PtMenuEntity menu1 :ptMenuEntityList){
            if(1 == menu1.getMenuLevel() ){
                menuEntity1 = MenuEntity.getFactory(menu1);
                menuEntityList.add(menuEntity1);
                for(PtMenuEntity menu2 :ptMenuEntityList){
                    if(menu1.getMenuId().equals(menu2.getParentId())){
                        menuEntity2 = MenuEntity.getFactory(menu2);
                        menuEntity1.getChildrenList().add(menuEntity2);
                        for(PtMenuEntity menu3 :ptMenuEntityList){
                            if(menu2.getMenuId().equals(menu3.getParentId())){
                                menuEntity3 = MenuEntity.getFactory(menu3);
                                menuEntity2.getChildrenList().add(menuEntity3);
                            }
                        }

                    }
                }
            }

        }
        return menuEntityList;
    }
}
