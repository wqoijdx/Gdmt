package com.isoftstone.gdmt.consult.controller;

import com.isoftstone.gdmt.consult.entity.SearchStoEntity;
import com.isoftstone.gdmt.consult.service.StoService;
import com.isoftstone.gdmt.mybatis.entity.CementProductionEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping({"/consult"})
public class StoController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private StoService stoService;

    @Secured("ROLE_gdmt_data_sto")
    @RequestMapping({"/stoPage"})
    public String stoPage() {
        return "consult/stoPage";
    }

    @Secured("ROLE_gdmt_data_sto")
    @RequestMapping({"/getSto"})
    @ResponseBody
    public List<CementProductionEntity> getSto() {
        return stoService.getSto();
    }

    @Secured("ROLE_gdmt_data_sto")
    @RequestMapping({"/getStoInfolistByPage"})
    @ResponseBody
    public PadingRstType<CementProductionEntity> getStoInfolistByPage(SearchStoEntity entity, PagingBean paging) {
        this.logger.info(entity);
        this.logger.info(paging);
        PadingRstType<CementProductionEntity> padingRstType = stoService.getStoInfolistByPage(entity,paging);
        return padingRstType;
    }

}
