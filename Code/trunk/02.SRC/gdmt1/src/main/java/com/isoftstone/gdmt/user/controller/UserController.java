package com.isoftstone.gdmt.user.controller;

import com.isoftstone.gdmt.mybatis.entity.PtUserEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.controller.BaseConctroller;
import com.isoftstone.gdmt.spring.security.GdmtPasswordEncoder;
import com.isoftstone.gdmt.spring.security.GdmtUserDetailsService;
import com.isoftstone.gdmt.spring.security.entity.SecurityUserEntity;
import com.isoftstone.gdmt.user.entity.SearchUserEntity;
import com.isoftstone.gdmt.user.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@Controller
@RequestMapping("/user")
public class UserController extends BaseConctroller {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private UserService userService;
    @Secured("ROLE_cldk_data_user")
    @RequestMapping("/userPage")
    public String userPage(){
        return "user/userPage";
    }
    @Secured("ROLE_cldk_data_user")
    @RequestMapping("/queryUserListByName")
    @ResponseBody
    public List<PtUserEntity> queryUserListByName(SearchUserEntity entity){
        logger.info(entity);
        return  userService.queryUserListByName(entity);
    }
    @Secured("ROLE_cldk_data_user")
    @RequestMapping("/deleteUserInfoById")
    @ResponseBody
    public String deleteUserInfoById(@RequestParam("ids") String ids){
        logger.info("ids:" + ids);
        userService.deleteUserInfoById(ids);
        return getSuccess("user.delete.success");
    }

    @Secured("ROLE_cldk_data_user")
    @RequestMapping("/getUserInfolistByPage")
    @ResponseBody
    public PadingRstType<PtUserEntity> getUserInfolistByPage(SearchUserEntity entity, PagingBean paging){
        logger.info(entity);
        logger.info(paging);
        PadingRstType<PtUserEntity> padingRstType = userService.getUserInfolistByPage(entity,paging);

        return padingRstType;
    }
    @Secured("ROLE_cldk_data_user")
    @RequestMapping("/getRoleByUserId")
    @ResponseBody
    public List<ZtreeStrEntity> getRoleByUserId(@RequestParam("userUuid") String userUuid){
        logger.info("userUuid:" + userUuid);
        List<ZtreeStrEntity> list = userService.getRoleByUserId(userUuid);
        return list;
    }
    @Secured("ROLE_cldk_data_user")
    @RequestMapping("/setRoleUuidByUserUuid")
    @ResponseBody
    public String setRoleUuidByUserUuid(@RequestParam("userUuid") String userUuid,
                                        @RequestParam("roleUuidArray") String roleUuidArray){
        logger.info("userUuid:" + userUuid);
        logger.info("roleUuidArray:" + roleUuidArray);
        userService.setRoleUuidByUserUuid(userUuid,roleUuidArray);
        return getSuccess("user.role.allocate.success");
    }
    @Secured("ROLE_cldk_data_user")
    @RequestMapping("/addUserInfoById")
    @ResponseBody
    public String addUserInfoById(PtUserEntity entity){
        logger.info( entity);
        userService.addUserInfoById(entity);
        return getSuccess("user.add.success");
    }

    @Secured("ROLE_cldk_data_user")
    @RequestMapping("/queryUserListById")
    @ResponseBody
    public PtUserEntity queryUserListById(String userUuid){
        return userService.queryUserListById(userUuid);
    }

    @Secured("ROLE_cldk_data_user")
    @RequestMapping("/modifyUserListById")
    @ResponseBody
    public String modifyUserListById(PtUserEntity entity){
        logger.info( entity);
        userService.modifyUserListById(entity);
        return getSuccess("user.modify.success");
    }

    @Secured("ROLE_cldk_data_user")
    @RequestMapping("/getDutyByUserId")
    @ResponseBody
    public List<ZtreeStrEntity> getDutyByUserId(@RequestParam("userUuid") String userUuid){
        logger.info("userUuid:" + userUuid);
        List<ZtreeStrEntity> list = userService.getDutyByUserId(userUuid);
        return list;
    }

    @Secured("ROLE_cldk_data_user")
    @RequestMapping("/setDutyIdByUserUuid")
    @ResponseBody
    public String setDutyIdByUserUuid(@RequestParam("userUuid") String userUuid,
                                      @RequestParam("dutyIdArray") String dutyIdArray){
        logger.info("userUuid:" + userUuid);
        logger.info("dutyIdArray:" + dutyIdArray);
        userService.setDutyIdByUserUuid(userUuid,dutyIdArray);
        return getSuccess("user.duty.allocate.success");
    }
    /**
     * 修改当前用户
     * @return
     */
    @GetMapping("/getCurrent")
    @ResponseBody
    public PtUserEntity getCurrent() {
        SecurityUserEntity principal = (SecurityUserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        PtUserEntity resultUser = userService.getById(principal.getUserUuid());
        return resultUser;
    }

    @PostMapping("/modifyCurrent")
    @ResponseBody
    public String modifyCurrent(PtUserEntity user) {
        SecurityUserEntity principal = (SecurityUserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        principal.setEmail(user.getEmail());
        principal.setMobile(user.getMobile());
        principal.setNiceName(user.getNiceName());
        userService.updateById(user);
        return getSuccess("user.update.success");
    }
    @Resource
    private GdmtPasswordEncoder passwordEncoder;

    @PostMapping("/modPass")
    @ResponseBody
    public String modPass(String oldPass, String newPass) {
        SecurityUserEntity principal = (SecurityUserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        PtUserEntity oldUser = userService.getById(principal.getUserUuid());
        String password = oldUser.getPassword();
        boolean matches = passwordEncoder.matches(oldPass, password);
        if (!matches) {
            return getFail("old.pass.invalid");
        }
        PtUserEntity user = new PtUserEntity();
        user.setUserUuid(principal.getUserUuid());
        user.setPassword(passwordEncoder.encode(newPass));
        userService.updateById(user);
        return getSuccess("mod.pass.success");
    }

    @RequestMapping("/unlock")
    @ResponseBody
    public String unlock(String id) {
        GdmtUserDetailsService.lockUser.invalidate(id);
        GdmtUserDetailsService.passErr.invalidate(id);
        return getSuccess("unlock.success");
    }

    //获取用户部门
    @Secured("ROLE_cldk_data_user")
    @RequestMapping("/getDepByUserUuid")
    @ResponseBody
    public List<ZtreeStrEntity> getDepByUserUuid(@RequestParam("userUuid") String userUuid){
        logger.info("userUuid:" + userUuid);
        List<ZtreeStrEntity> list = userService.getDepByUserUuid(userUuid);

        return list;
    }

    @Secured("ROLE_cldk_data_user")
    @RequestMapping("/setDepUuIdByUserUuid")
    @ResponseBody
    public String setDepUuIdByUserUuid(@RequestParam("userUuid") String userUuid,
                                       @RequestParam("depUuId") String depUuId){
        logger.info("userUuid:" + userUuid);
        logger.info("depUuId:" + depUuId);
        userService.setDepUuIdByUserUuid(userUuid,depUuId);
        return getSuccess("user.dep.allocate.success");
    }
}
