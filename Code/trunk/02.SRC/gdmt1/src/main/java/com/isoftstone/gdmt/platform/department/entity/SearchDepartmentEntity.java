package com.isoftstone.gdmt.platform.department.entity;

import java.io.Serializable;

public class SearchDepartmentEntity implements Serializable {
    private String searchdutyUuid;
    private String searchdutyName;

    public String getSearchdutyUuid() {
        return searchdutyUuid;
    }

    public void setSearchdutyUuid(String searchdutyUuid) {
        this.searchdutyUuid = searchdutyUuid;
    }

    public String getSearchdutyName() {
        return searchdutyName;
    }
    private String searchOrganId ;

    public String getSearchOrganId() {
        return searchOrganId;
    }

    public void setSearchOrganId(String searchOrganId) {
        this.searchOrganId = searchOrganId;
    }

    @Override
    public String toString() {
        return "SearchOrganEntity{" +
                "searchOrganId='" + searchOrganId + '\'' +
                '}';
    }

}


