package com.isoftstone.gdmt.consult.service.impl;

import com.isoftstone.gdmt.consult.entity.SearchPmiEntity;
import com.isoftstone.gdmt.consult.repository.PmiDao;
import com.isoftstone.gdmt.consult.service.PmiService;
import com.isoftstone.gdmt.mybatis.entity.PmiEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("pmiService")
public class PmiServiceImpl implements PmiService {
    @Resource
    private PmiDao pmiDao;

    @Override
    public List<PmiEntity> getPmi() {
        return pmiDao.getPmi();
    }

    @Override
    public PadingRstType<PmiEntity> getPmiInfolistByPage(SearchPmiEntity entity, PagingBean paging) {
        paging.deal(PmiEntity.class);
        PadingRstType<PmiEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(paging.getPage());
        List<PmiEntity> list = pmiDao.queryPmiInfoByPage(entity,paging);
        padingRstType.setRawRecords(list);
        Integer total = pmiDao.queryPmiInfoTotal(entity);
        padingRstType.setTotal(total);
        padingRstType.putItems();
        return padingRstType;
    }

}
