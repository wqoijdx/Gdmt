package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class SmDictionaryEntity implements Serializable
{
	@Columns("dict_id")
	private Integer dictId;

	@Columns("dict_group")
	private String dictGroup;

	@Columns("dic_code")
	private String dicCode;

	@Columns("description")
	private String description;

	@Columns("sort")
	private Integer sort;
	
	public Integer getDictId() 
	{
		return dictId;
	}
	
	public void setDictId(Integer dictId) 
	{
		this.dictId = dictId;
	}
	
	public String getDictGroup() 
	{
		return dictGroup;
	}
	
	public void setDictGroup(String dictGroup) 
	{
		this.dictGroup = dictGroup;
	}
	
	public String getDicCode() 
	{
		return dicCode;
	}
	
	public void setDicCode(String dicCode) 
	{
		this.dicCode = dicCode;
	}
	
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String description) 
	{
		this.description = description;
	}
	
	public Integer getSort() 
	{
		return sort;
	}
	
	public void setSort(Integer sort) 
	{
		this.sort = sort;
	}

}
