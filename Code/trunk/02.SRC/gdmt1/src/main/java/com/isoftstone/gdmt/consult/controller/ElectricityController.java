package com.isoftstone.gdmt.consult.controller;

import com.isoftstone.gdmt.consult.entity.SearchElectricityEntity;
import com.isoftstone.gdmt.consult.service.ElectricityService;
import com.isoftstone.gdmt.mybatis.entity.ElectricityEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping({"/consult"})
public class ElectricityController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private ElectricityService electricityService;

    @Secured("ROLE_gdmt_data_electricity")
    @RequestMapping({"/electricityPage"})
    public String electricityPage() {
        return "consult/electricityPage";
    }

    @Secured("ROLE_gdmt_data_electricity")
    @RequestMapping({"/getElectricity"})
    @ResponseBody
    public List<ElectricityEntity> getElectricity() {
        return electricityService.getElectricity();
    }

    @Secured("ROLE_gdmt_data_electricity")
    @RequestMapping({"/getElectricityInfolistByPage"})
    @ResponseBody
    public PadingRstType<ElectricityEntity> getElectricityInfolistByPage(SearchElectricityEntity entity, PagingBean paging) {
        this.logger.info(entity);
        this.logger.info(paging);
        PadingRstType<ElectricityEntity> padingRstType = electricityService.getElectricityInfolistByPage(entity,paging);
        return padingRstType;
    }

}
