package com.isoftstone.gdmt.consult.service.impl;

import com.isoftstone.gdmt.consult.entity.SearchOfferEntity;
import com.isoftstone.gdmt.consult.repository.OfferDao;
import com.isoftstone.gdmt.consult.service.OfferService;
import com.isoftstone.gdmt.mybatis.entity.InternationalOfferEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("offerService")
public class OfferServiceImpl implements OfferService {
    @Resource
    private OfferDao offerDao;

    @Override
    public List<InternationalOfferEntity> getOffer() {
        return offerDao.getOffer();
    }

    @Override
    public PadingRstType<InternationalOfferEntity> getOfferInfolistByPage(SearchOfferEntity entity, PagingBean paging) {
        paging.deal(InternationalOfferEntity.class);
        PadingRstType<InternationalOfferEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(paging.getPage());
        List<InternationalOfferEntity> list = offerDao.queryOfferInfoByPage(entity,paging);
        padingRstType.setRawRecords(list);
        Integer total = offerDao.queryOfferInfoTotal(entity);
        padingRstType.setTotal(total);
        padingRstType.putItems();
        return padingRstType;
    }

}
