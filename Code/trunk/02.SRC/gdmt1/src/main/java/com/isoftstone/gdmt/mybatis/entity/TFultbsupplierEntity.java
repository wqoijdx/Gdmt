package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class TFultbsupplierEntity implements Serializable
{
	@Columns("supplierid")
	private String supplierid;

	@Columns("membertypeid")
	private String membertypeid;

	@Columns("user_uuid")
	private String userUuid;

	@Columns("createuser")
	private String createuser;

	@Columns("createdate")
	private String createdate;

	@Columns("operuser")
	private String operuser;

	@Columns("operdate")
	private String operdate;

	@Columns("name")
	private String name;

	@Columns("code")
	private String code;

	@Columns("shortname")
	private String shortname;

	@Columns("corporation")
	private String corporation;

	@Columns("financing")
	private Double financing;

	@Columns("licence")
	private String licence;

	@Columns("coallicence")
	private String coallicence;

	@Columns("taxcode")
	private String taxcode;

	@Columns("linkman")
	private String linkman;

	@Columns("linktel")
	private String linktel;

	@Columns("faxcode")
	private String faxcode;

	@Columns("address")
	private String address;

	@Columns("postalcode")
	private String postalcode;

	@Columns("isstop")
	private Double isstop;

	@Columns("creditlevel")
	private String creditlevel;

	@Columns("norder")
	private Double norder;

	@Columns("organizationcode")
	private String organizationcode;

	@Columns("conemail")
	private String conemail;

	@Columns("mexplain")
	private String mexplain;

	@Columns("registerplace")
	private String registerplace;

	@Columns("corporationidentity")
	private String corporationidentity;

	@Columns("openbank")
	private String openbank;

	@Columns("bankaccount")
	private String bankaccount;

	@Columns("coalsource")
	private String coalsource;

	@Columns("transportmode")
	private String transportmode;

	@Columns("introduce")
	private String introduce;

	@Columns("achievement")
	private String achievement;

	@Columns("times")
	private Double times;

	@Columns("clicklogintime")
	private String clicklogintime;

	@Columns("nclicklogintime")
	private String nclicklogintime;

	@Columns("autoid")
	private Double autoid;

	@Columns("supplierkind")
	private String supplierkind;

	@Columns("password")
	private String password;

	@Columns("lastlogintime")
	private String lastlogintime;

	@Columns("stepnumber")
	private String stepnumber;

	@Columns("createuserid")
	private String createuserid;

	@Columns("createuserdeptid")
	private String createuserdeptid;

	@Columns("createuserdeptcode")
	private String createuserdeptcode;

	@Columns("minespecialcost")
	private Double minespecialcost;

	@Columns("railcarriage")
	private Double railcarriage;

	@Columns("datafrom")
	private String datafrom;

	@Columns("is_fb")
	private String isFb;

	@Columns("pt_audit")
	private String ptAudit;

	@Columns("is_caauth")
	private String isCaauth;
	
	public String getSupplierid() 
	{
		return supplierid;
	}
	
	public void setSupplierid(String supplierid) 
	{
		this.supplierid = supplierid;
	}
	
	public String getMembertypeid() 
	{
		return membertypeid;
	}
	
	public void setMembertypeid(String membertypeid) 
	{
		this.membertypeid = membertypeid;
	}
	
	public String getUserUuid() 
	{
		return userUuid;
	}
	
	public void setUserUuid(String userUuid) 
	{
		this.userUuid = userUuid;
	}
	
	public String getCreateuser() 
	{
		return createuser;
	}
	
	public void setCreateuser(String createuser) 
	{
		this.createuser = createuser;
	}
	
	public String getCreatedate() 
	{
		return createdate;
	}
	
	public void setCreatedate(String createdate) 
	{
		this.createdate = createdate;
	}
	
	public String getOperuser() 
	{
		return operuser;
	}
	
	public void setOperuser(String operuser) 
	{
		this.operuser = operuser;
	}
	
	public String getOperdate() 
	{
		return operdate;
	}
	
	public void setOperdate(String operdate) 
	{
		this.operdate = operdate;
	}
	
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public String getCode() 
	{
		return code;
	}
	
	public void setCode(String code) 
	{
		this.code = code;
	}
	
	public String getShortname() 
	{
		return shortname;
	}
	
	public void setShortname(String shortname) 
	{
		this.shortname = shortname;
	}
	
	public String getCorporation() 
	{
		return corporation;
	}
	
	public void setCorporation(String corporation) 
	{
		this.corporation = corporation;
	}
	
	public Double getFinancing() 
	{
		return financing;
	}
	
	public void setFinancing(Double financing) 
	{
		this.financing = financing;
	}
	
	public String getLicence() 
	{
		return licence;
	}
	
	public void setLicence(String licence) 
	{
		this.licence = licence;
	}
	
	public String getCoallicence() 
	{
		return coallicence;
	}
	
	public void setCoallicence(String coallicence) 
	{
		this.coallicence = coallicence;
	}
	
	public String getTaxcode() 
	{
		return taxcode;
	}
	
	public void setTaxcode(String taxcode) 
	{
		this.taxcode = taxcode;
	}
	
	public String getLinkman() 
	{
		return linkman;
	}
	
	public void setLinkman(String linkman) 
	{
		this.linkman = linkman;
	}
	
	public String getLinktel() 
	{
		return linktel;
	}
	
	public void setLinktel(String linktel) 
	{
		this.linktel = linktel;
	}
	
	public String getFaxcode() 
	{
		return faxcode;
	}
	
	public void setFaxcode(String faxcode) 
	{
		this.faxcode = faxcode;
	}
	
	public String getAddress() 
	{
		return address;
	}
	
	public void setAddress(String address) 
	{
		this.address = address;
	}
	
	public String getPostalcode() 
	{
		return postalcode;
	}
	
	public void setPostalcode(String postalcode) 
	{
		this.postalcode = postalcode;
	}
	
	public Double getIsstop() 
	{
		return isstop;
	}
	
	public void setIsstop(Double isstop) 
	{
		this.isstop = isstop;
	}
	
	public String getCreditlevel() 
	{
		return creditlevel;
	}
	
	public void setCreditlevel(String creditlevel) 
	{
		this.creditlevel = creditlevel;
	}
	
	public Double getNorder() 
	{
		return norder;
	}
	
	public void setNorder(Double norder) 
	{
		this.norder = norder;
	}
	
	public String getOrganizationcode() 
	{
		return organizationcode;
	}
	
	public void setOrganizationcode(String organizationcode) 
	{
		this.organizationcode = organizationcode;
	}
	
	public String getConemail() 
	{
		return conemail;
	}
	
	public void setConemail(String conemail) 
	{
		this.conemail = conemail;
	}
	
	public String getMexplain()
	{
		return mexplain;
	}
	
	public void setMexplain(String mexplain)
	{
		this.mexplain = mexplain;
	}
	
	public String getRegisterplace() 
	{
		return registerplace;
	}
	
	public void setRegisterplace(String registerplace) 
	{
		this.registerplace = registerplace;
	}
	
	public String getCorporationidentity() 
	{
		return corporationidentity;
	}
	
	public void setCorporationidentity(String corporationidentity) 
	{
		this.corporationidentity = corporationidentity;
	}
	
	public String getOpenbank() 
	{
		return openbank;
	}
	
	public void setOpenbank(String openbank) 
	{
		this.openbank = openbank;
	}
	
	public String getBankaccount() 
	{
		return bankaccount;
	}
	
	public void setBankaccount(String bankaccount) 
	{
		this.bankaccount = bankaccount;
	}
	
	public String getCoalsource()
	{
		return coalsource;
	}
	
	public void setCoalsource(String coalsource)
	{
		this.coalsource = coalsource;
	}
	
	public String getTransportmode()
	{
		return transportmode;
	}
	
	public void setTransportmode(String transportmode)
	{
		this.transportmode = transportmode;
	}
	
	public String getIntroduce()
	{
		return introduce;
	}
	
	public void setIntroduce(String introduce)
	{
		this.introduce = introduce;
	}
	
	public String getAchievement()
	{
		return achievement;
	}
	
	public void setAchievement(String achievement)
	{
		this.achievement = achievement;
	}
	
	public Double getTimes() 
	{
		return times;
	}
	
	public void setTimes(Double times) 
	{
		this.times = times;
	}
	
	public String getClicklogintime() 
	{
		return clicklogintime;
	}
	
	public void setClicklogintime(String clicklogintime) 
	{
		this.clicklogintime = clicklogintime;
	}
	
	public String getNclicklogintime() 
	{
		return nclicklogintime;
	}
	
	public void setNclicklogintime(String nclicklogintime) 
	{
		this.nclicklogintime = nclicklogintime;
	}
	
	public Double getAutoid() 
	{
		return autoid;
	}
	
	public void setAutoid(Double autoid) 
	{
		this.autoid = autoid;
	}
	
	public String getSupplierkind() 
	{
		return supplierkind;
	}
	
	public void setSupplierkind(String supplierkind) 
	{
		this.supplierkind = supplierkind;
	}
	
	public String getPassword() 
	{
		return password;
	}
	
	public void setPassword(String password) 
	{
		this.password = password;
	}
	
	public String getLastlogintime() 
	{
		return lastlogintime;
	}
	
	public void setLastlogintime(String lastlogintime) 
	{
		this.lastlogintime = lastlogintime;
	}
	
	public String getStepnumber() 
	{
		return stepnumber;
	}
	
	public void setStepnumber(String stepnumber) 
	{
		this.stepnumber = stepnumber;
	}
	
	public String getCreateuserid() 
	{
		return createuserid;
	}
	
	public void setCreateuserid(String createuserid) 
	{
		this.createuserid = createuserid;
	}
	
	public String getCreateuserdeptid() 
	{
		return createuserdeptid;
	}
	
	public void setCreateuserdeptid(String createuserdeptid) 
	{
		this.createuserdeptid = createuserdeptid;
	}
	
	public String getCreateuserdeptcode() 
	{
		return createuserdeptcode;
	}
	
	public void setCreateuserdeptcode(String createuserdeptcode) 
	{
		this.createuserdeptcode = createuserdeptcode;
	}
	
	public Double getMinespecialcost() 
	{
		return minespecialcost;
	}
	
	public void setMinespecialcost(Double minespecialcost) 
	{
		this.minespecialcost = minespecialcost;
	}
	
	public Double getRailcarriage() 
	{
		return railcarriage;
	}
	
	public void setRailcarriage(Double railcarriage) 
	{
		this.railcarriage = railcarriage;
	}
	
	public String getDatafrom() 
	{
		return datafrom;
	}
	
	public void setDatafrom(String datafrom) 
	{
		this.datafrom = datafrom;
	}
	
	public String getIsFb() 
	{
		return isFb;
	}
	
	public void setIsFb(String isFb) 
	{
		this.isFb = isFb;
	}
	
	public String getPtAudit() 
	{
		return ptAudit;
	}
	
	public void setPtAudit(String ptAudit) 
	{
		this.ptAudit = ptAudit;
	}
	
	public String getIsCaauth() 
	{
		return isCaauth;
	}
	
	public void setIsCaauth(String isCaauth) 
	{
		this.isCaauth = isCaauth;
	}

}
