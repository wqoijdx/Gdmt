package com.isoftstone.gdmt.platform.role.service.impl;

import com.isoftstone.gdmt.mybatis.entity.PtRoleEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.role.entity.SearchRoleEntity;
import com.isoftstone.gdmt.platform.role.repository.RoleDao;
import com.isoftstone.gdmt.platform.role.service.RoleService;
import com.isoftstone.gdmt.platform.tag.entity.DictTagEntity;
import com.isoftstone.gdmt.platform.tag.service.DataSelectService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("roleService")
public class RoleServiceImpl implements RoleService, DataSelectService {
    @Resource
    private RoleDao roleDao;

    @Override
    public PadingRstType<PtRoleEntity> queryRoleListPage(SearchRoleEntity search, PagingBean paging) {
        paging.deal(PtRoleEntity.class);
        PadingRstType<PtRoleEntity> ptRoleEntityPadingRstType = new PadingRstType<PtRoleEntity>();
        ptRoleEntityPadingRstType.setPage(paging.getPage());
        List<PtRoleEntity> list = roleDao.queryRoleListPage(search,paging);
        ptRoleEntityPadingRstType.setRawRecords(list);
        Integer total = roleDao.queryRoleTotal(search);
        ptRoleEntityPadingRstType.setTotal(total);
        ptRoleEntityPadingRstType.putItems();
        return ptRoleEntityPadingRstType;
    }

    @Override
    public List<ZtreeStrEntity> queryMenuByRoleId(String roleUuid) {

        List<ZtreeStrEntity> list = roleDao.queryMenuTree();
        List<String> menuIdList = roleDao.queryMenuIdByRoleId(roleUuid);
        int index = 0;
        for(ZtreeStrEntity item: list){
            if(index > 10 ){
                item.setOpen(false);
            }
            index++;
            for(String menuId : menuIdList){
                if(menuId.equals(item.getId())){
                    item.setChecked(true);
                }
            }
        }

        return list;
    }

    @Override
    public void saveMenuIdAndRoleId(String roleUuid, String menuArray) {
        roleDao.deleteMenuByRoleId(roleUuid);
        String[] menuIds = menuArray.split(",");
        for(String menuId: menuIds){
            roleDao.insertMenuAndRole(roleUuid,menuId);
        }
    }

    @Override
    public void create(PtRoleEntity role) {
        roleDao.insert(role);
    }

    @Override
    public PtRoleEntity getById(String id) {
        return roleDao.queryById(id);
    }

    @Override
    public void updateById(PtRoleEntity role) {
        roleDao.updateById(role);
    }

    @Override
    public void removeById(String id) {
        roleDao.deleteById(id);
    }

    @Override
    public int getUserCountByRoleId(String id) {
        return roleDao.queryUserCountByRoleId(id);
    }

    @Override
    public List<DictTagEntity> queryTagElementList(String language) {
        return roleDao.queryTagElementList();
    }
}
