package com.isoftstone.gdmt.consult.repository;

import com.isoftstone.gdmt.consult.entity.SearchBspiEntity;
import com.isoftstone.gdmt.mybatis.entity.CoalBspiEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BspiDao {
    List<CoalBspiEntity> getBspi();

    List<CoalBspiEntity> queryBspiInfoByPage(@Param("entity") SearchBspiEntity entity, @Param("paging") PagingBean paging);

    Integer queryBspiInfoTotal(@Param("entity") SearchBspiEntity entity);

}
