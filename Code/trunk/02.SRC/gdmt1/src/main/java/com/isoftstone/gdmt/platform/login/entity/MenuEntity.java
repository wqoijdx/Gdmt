package com.isoftstone.gdmt.platform.login.entity;

import com.isoftstone.gdmt.mybatis.entity.PtMenuEntity;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class MenuEntity implements Serializable {
    private String menuId;
    private String menuName;
    private String menuUrl;
    private String menuIcon;
    private List<MenuEntity> childrenList = new LinkedList<>();

    public static MenuEntity getFactory(PtMenuEntity ptMenuEntity){
        MenuEntity menuEntity = new MenuEntity();
        menuEntity.setMenuId(ptMenuEntity.getMenuId());
        menuEntity.setMenuName(ptMenuEntity.getMenuName());
        menuEntity.setMenuIcon(ptMenuEntity.getMenuIcon());
        menuEntity.setMenuUrl(ptMenuEntity.getMenuUrl());
        return  menuEntity;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    public List<MenuEntity> getChildrenList() {
        return childrenList;
    }

    public void setChildrenList(List<MenuEntity> childrenList) {
        this.childrenList = childrenList;
    }

    @Override
    public String toString() {
        return "MenuEntity{" +
                "menuId='" + menuId + '\'' +
                ", menuName='" + menuName + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", menuIcon='" + menuIcon + '\'' +
                ", childrenList=" + childrenList +
                '}';
    }
}
