package com.isoftstone.gdmt.platform.menu.service.impl;

import com.isoftstone.gdmt.mybatis.entity.PtMenuEntity;
import com.isoftstone.gdmt.mybatis.entity.PtMenuI18nEntity;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.common.util.DataUtil;
import com.isoftstone.gdmt.platform.dictionary.repository.DictionaryDao;
import com.isoftstone.gdmt.platform.menu.repository.MenuDao;
import com.isoftstone.gdmt.platform.menu.service.MenuService;
import com.isoftstone.gdmt.platform.role.repository.RoleDao;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Locale;

@Service("menuService")
public class MenuServiceImpl implements MenuService {

    @Resource
    private MenuDao menuDao;
    @Resource
    private RoleDao roleDao;
    @Value("${ubound.uuid}")
    private String uboundUuid;
    @Value("${root.uuid}")
    private String rootUuid;
    @Resource
    private MessageSource messageSource;

    @Override
    public List<ZtreeStrEntity> getMenuTree() {
        List<ZtreeStrEntity> list = roleDao.queryMenuTree();
        ZtreeStrEntity ztreeStrEntity = new ZtreeStrEntity();
        ztreeStrEntity.setId(uboundUuid);
        Locale locale = LocaleContextHolder.getLocale();
        String unboundName = messageSource.getMessage("common.unbound.name", null, locale);
        ztreeStrEntity.setName(unboundName);
        list.add(ztreeStrEntity);
        Integer index = 0;
        for (ZtreeStrEntity item : list) {
            if (index < 10) {
                item.setOpen(true);
            } else {
                item.setOpen(false);
            }
            index++;
        }

        return list;
    }

    @Override
    public PtMenuEntity getMenuInfoById(String menuId) {
        return menuDao.getMenuInfoById(menuId);
    }

    @Override
    public void modifyMenuInfoById(PtMenuEntity entity) {
        menuDao.modifyMenuInfoById(entity);
    }

    @Override
    public void addMenuInfoById(PtMenuEntity entity) {
        entity.setMenuId(DataUtil.getUUIDShort());
        entity.setParentId(uboundUuid);

        menuDao.addMenuInfoById(entity);
    }

    @Override
    public void deleteMenuInfoById(String menuId) {
        menuDao.deleteMenuInfoById(menuId);
    }

    @Override
    public List<ZtreeStrEntity> getMenuTreeByMenuId(String menuId) {
        String parentId = menuDao.queryMenuTreeByMenuId(menuId);
        List<ZtreeStrEntity> list = getMenuTree();
        ZtreeStrEntity ztreeStrEntity = new ZtreeStrEntity();
        ztreeStrEntity.setId(rootUuid);
        Locale locale = LocaleContextHolder.getLocale();
        String rootName = messageSource.getMessage("common.root.node.name", null, locale);
        ztreeStrEntity.setName(rootName);
        list.add(ztreeStrEntity);

        if (parentId != null) {
            for (ZtreeStrEntity item : list) {
                if (parentId.equals(item.getId())) {
                    item.setChecked(true);
                }
            }
        }

        return list;
    }

    @Override
    public void boundMenu(String menuId, String parentId) {
        menuDao.updateParentIdByMenuId(menuId, parentId);
    }

    @Override
    public void unBoundByMenuId(String menuId) {
        menuDao.updateParentIdByMenuId(menuId, uboundUuid);
    }

    @Override
    public List<PtMenuI18nEntity> getI18nByMenuId(String menuId) {
        return menuDao.getI18nByMenuId(menuId);
    }

    @Override
    public void saveI18nData(PtMenuI18nEntity entity) {
        menuDao.saveI18nData(entity);
    }

    @Override
    public void addI18nData(PtMenuI18nEntity entity) {
        menuDao.addI18nData(entity);
    }

    @Override
    public void deleteI18nData(PtMenuI18nEntity entity) {
        menuDao.deleteI18nData(entity);
    }

}
