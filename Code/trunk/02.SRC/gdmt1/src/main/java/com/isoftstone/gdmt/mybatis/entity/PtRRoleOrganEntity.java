package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class PtRRoleOrganEntity implements Serializable
{
	@Columns("DUTYID")
	private String dutyid;

	@Columns("NAME")
	private String name;

	@Columns("ORGAN_UUID")
	private String organUuid;

	@Columns("ROLE_UUID")
	private String roleUuid;


	@Columns("role_name")
	private String roleName;

	@Columns("ORGAN_NAME")
	private String organName;

	@Columns("USER_NAME")
	private String userName;

	@Columns("remarks")
	private String remarks;



	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getOrganName() {
		return organName;
	}

	public void setOrganName(String organName) {
		this.organName = organName;
	}


	public String getDutyid() 
	{
		return dutyid;
	}
	
	public void setDutyid(String dutyid) 
	{
		this.dutyid = dutyid;
	}

	public String getName()
	{
		return name;
	}

    public void setName(String name)
    {
        this.name = name;
    }

	public void setDutyname(String name) {
		this.name = name;
	}

	public String getOrganUuid()
	{
		return organUuid;
	}

	public void setOrganUuid(String organUuid)
	{
		this.organUuid = organUuid;
	}
	
	public String getRoleUuid() 
	{
		return roleUuid;
	}
	
	public void setRoleUuid(String roleUuid) 
	{
		this.roleUuid = roleUuid;
	}

}
