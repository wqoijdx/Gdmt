package com.isoftstone.gdmt.platform.tag.repository;

import com.isoftstone.gdmt.platform.tag.entity.DictTagEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DicDataTagDao {
    List<DictTagEntity> queryDicInfoByGroupId(@Param("group") String group,@Param("langType") String langType);
}
