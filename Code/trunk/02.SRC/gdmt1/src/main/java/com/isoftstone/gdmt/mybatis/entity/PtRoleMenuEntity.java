package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class PtRoleMenuEntity implements Serializable
{
	@Columns("ROLE_UUID")
	private String roleUuid;

	@Columns("menu_id")
	private Integer menuId;
	
	public String getRoleUuid() 
	{
		return roleUuid;
	}
	
	public void setRoleUuid(String roleUuid) 
	{
		this.roleUuid = roleUuid;
	}
	
	public Integer getMenuId() 
	{
		return menuId;
	}
	
	public void setMenuId(Integer menuId) 
	{
		this.menuId = menuId;
	}

}
