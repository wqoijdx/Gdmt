package com.isoftstone.gdmt.consult.service.impl;

import com.isoftstone.gdmt.consult.entity.SearchQuoteEntity;
import com.isoftstone.gdmt.consult.repository.QuoteDao;
import com.isoftstone.gdmt.consult.service.QuoteService;
import com.isoftstone.gdmt.mybatis.entity.InternationalQuoteEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("quoteService")
public class QuoteServiceImpl implements QuoteService {
    @Resource
    private QuoteDao quoteDao;

    @Override
    public List<InternationalQuoteEntity> getQuote() {
        return quoteDao.getQuote();
    }

    @Override
    public PadingRstType<InternationalQuoteEntity> getQuoteInfolistByPage(SearchQuoteEntity entity, PagingBean paging) {
        paging.deal(InternationalQuoteEntity.class);
        PadingRstType<InternationalQuoteEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(paging.getPage());
        List<InternationalQuoteEntity> list = quoteDao.queryQuoteInfoByPage(entity,paging);
        padingRstType.setRawRecords(list);
        Integer total = quoteDao.queryQuoteInfoTotal(entity);
        padingRstType.setTotal(total);
        padingRstType.putItems();
        return padingRstType;
    }

}
