package com.isoftstone.gdmt.platform.tag.service.impl;


import com.isoftstone.gdmt.platform.tag.entity.DictTagEntity;
import com.isoftstone.gdmt.platform.tag.repository.DicDataTagDao;
import com.isoftstone.gdmt.platform.tag.service.DicDataTagService;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

@Service("dicDataTagService")
public class DicDataTagServiceImpl implements DicDataTagService {
    @Resource
    private DicDataTagDao dicDataTagDao;
    @Resource
    private   MessageSource messageSource;
    @Override
    public List<DictTagEntity> queryDicInfoByGroupId(String group) {
        Locale locale = LocaleContextHolder.getLocale();
        List<DictTagEntity> list = dicDataTagDao.queryDicInfoByGroupId(group, locale.toString());

        List<DictTagEntity> dictTagEntities = new LinkedList<>();
        DictTagEntity dictTagEntity  = new DictTagEntity();
        dictTagEntity.setDictName(messageSource.getMessage("common.choose",null,locale));
        dictTagEntity.setDicCode("");
        dictTagEntities.add(dictTagEntity);
        dictTagEntities.addAll(list);
        return dictTagEntities;
    }
}
