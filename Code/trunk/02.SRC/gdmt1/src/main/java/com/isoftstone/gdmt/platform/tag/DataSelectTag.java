package com.isoftstone.gdmt.platform.tag;


import com.isoftstone.gdmt.platform.tag.entity.DictTagEntity;
import com.isoftstone.gdmt.platform.tag.service.DataSelectService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class DataSelectTag extends SimpleTagSupport {
    private Logger logger = LogManager.getLogger(DataSelectTag.class.getName());

    private String id;

    private String classez;

    private String style;

    private String name;

    private String ref;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClassez() {
        return classez;
    }

    public void setClassez(String classez) {
        this.classez = classez;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    private DataSelectService dataSelectService;


    @Override
    public void doTag()
            throws JspException, IOException {

        // session.setAttribute("siteLanguage",siteLanguage);
        //String language = (String)getJspContext().getAttribute("siteLanguage");
        //logger.info("language" + language);
        // ServletContext sc;

        PageContext pag = (PageContext) getJspContext();
        ServletContext sc = pag.getServletContext();

        String language = (String) pag.getSession().getAttribute("siteLanguage");
        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(sc);
        // webApplicationContext.getBean("roleService");

        // dctnrService = webApplicationContext.getBean("dctnrService123",
        // DctnrService.class);
        Locale locale = LocaleContextHolder.getLocale();
        MessageSource messageSource=webApplicationContext.getBean("messageSource", MessageSource.class);;
        dataSelectService = webApplicationContext.getBean(ref, DataSelectService.class);

        List<DictTagEntity> list = dataSelectService.queryTagElementList(language);
        logger.info("list" + list);
        JspWriter out = getJspContext().getOut();
        out.write("<select ");
        if (id != null) {
            out.write(" id='");
            out.write(id);
            out.write("'");
        }

        if (classez != null) {
            out.write(" class='");
            out.write(classez);
            out.write("'");
        }
        if (style != null) {
            out.write(" style='");
            out.write(style);
            out.write("'");
        }
        if (name != null) {
            out.write(" name='");
            out.write(name);
            out.write("'");
        }

        out.write(">'");
        out.write("<option value=''>");
        out.write(messageSource.getMessage("common.choose", null, locale));
        out.write("</option>");
        for (DictTagEntity SelectTagEntity : list) {
            logger.info(SelectTagEntity);
            out.write("<option value='");
            out.write(SelectTagEntity.getDicCode());
            out.write("'>");
            out.write(SelectTagEntity.getDictName());
            out.write("</option>");
        }
        out.write("</select>");
    }
}
