package com.isoftstone.gdmt.consult.service.impl;

import com.isoftstone.gdmt.consult.entity.SearchBspiEntity;
import com.isoftstone.gdmt.consult.entity.SearchGdpEntity;
import com.isoftstone.gdmt.consult.repository.BspiDao;
import com.isoftstone.gdmt.consult.repository.GdpDao;
import com.isoftstone.gdmt.consult.service.BspiService;
import com.isoftstone.gdmt.consult.service.GdpService;
import com.isoftstone.gdmt.mybatis.entity.CoalBspiEntity;
import com.isoftstone.gdmt.mybatis.entity.GdpEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("gdpService")
public class GdpServiceImpl implements GdpService {
    @Resource
    private GdpDao gdpDao;

    @Override
    public List<GdpEntity> getGdp() {
        return gdpDao.getGdp();
    }

    @Override
    public PadingRstType<GdpEntity> getGdpInfolistByPage(SearchGdpEntity entity, PagingBean paging) {
        paging.deal(GdpEntity.class);
        PadingRstType<GdpEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(paging.getPage());
        List<GdpEntity> list = gdpDao.queryGdpInfoByPage(entity,paging);
        padingRstType.setRawRecords(list);
        Integer total = gdpDao.queryGdpInfoTotal(entity);
        padingRstType.setTotal(total);
        padingRstType.putItems();
        return padingRstType;
    }

}
