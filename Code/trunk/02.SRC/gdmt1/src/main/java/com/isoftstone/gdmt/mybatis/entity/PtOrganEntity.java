package com.isoftstone.gdmt.mybatis.entity;

import com.isoftstone.gdmt.mybatis.annotation.Columns;

import java.io.Serializable;


public class PtOrganEntity implements Serializable {

    @Columns("ORGAN_UUID")
    private String organId;

    @Columns("ORGAN_NAME")
    private String organName;
//父组织ID
    @Columns("PARENT_UUID")
    private String parentId;

    @Columns("DEL_FLAG")
    private Integer delFlag;
//创建人id
    @Columns("MODIFIERID")
    private Integer modifierId;
//创建时间
    @Columns("MODTIME")
    private String modTime;

    @Columns("DESCRIPTION")
    private String description;
//组织类型
    @Columns("ORGAN_TYPE")
    private String organType;

    public String getOrganId() {
        return organId;
    }

    public void setOrganId(String organId) {
        this.organId = organId;
    }

    public String getOrganName() {
        return organName;
    }

    public void setOrganName(String organName) {
        this.organName = organName;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Integer getModifierId() {
        return modifierId;
    }

    public void setModifierid(Integer modifierId) {
        this.modifierId = modifierId;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public String getModtime() {
        return modTime;
    }

    public void setModtime(String modtime) {
        this.modTime = modtime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrganType() { return organType; }

    public void setOrganType(String organType) {
        this.organType = organType;
    }

    @Override
    public String toString() {
        return "PtOrganEntity{" +
                "organId='" + organId + '\'' +
                ", organName='" + organName + '\'' +
                ", parentId='" + parentId + '\'' +
                ", delFlag=" + delFlag +
                ", modifierId=" + modifierId +
                ", modTime='" + modTime + '\'' +
                ", description='" + description + '\'' +
                ", organType='" + organType + '\'' +
                '}';
    }
}
