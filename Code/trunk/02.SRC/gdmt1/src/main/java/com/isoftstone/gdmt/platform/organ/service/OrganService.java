package com.isoftstone.gdmt.platform.organ.service;

import com.isoftstone.gdmt.mybatis.entity.PtDepartmentInfoEntity;
import com.isoftstone.gdmt.mybatis.entity.PtOrganEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.department.entity.SearchDepartmentEntity;
import com.isoftstone.gdmt.platform.organ.entity.SearchOrganEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface OrganService {
    List<ZtreeStrEntity> getOrganTree();

    PtOrganEntity getOrganInfoById(@Param("organId") String organId);

    void modifyOrganInfoById(@Param("entity") PtOrganEntity entity);

    void addOrganInfoById(@Param("entity") PtOrganEntity entity);

    void deleteOrganInfoById(@Param("organId") String organId);

    List<ZtreeStrEntity> getOrganTreeByOrganId(@Param("organId") String organId);

    void boundOrgan(@Param("organId") String organId, @Param("parentId") String parentId);

    void unBoundByOrganId(@Param("organId") String organId);

//    PadingRstType<PtDepartmentInfoEntity> getDepartByOrganId(SearchOrganEntity search, PagingBean paging);
//
//    Object getDepartByOrganId(String organId);


    PadingRstType<PtOrganEntity> queryOrganListPage(SearchOrganEntity search, PagingBean paging);

    PadingRstType<PtDepartmentInfoEntity> queryDepListPage(SearchDepartmentEntity search, PagingBean paging);
}
