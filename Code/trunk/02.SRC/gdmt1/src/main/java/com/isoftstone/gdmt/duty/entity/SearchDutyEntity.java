package com.isoftstone.gdmt.duty.entity;


import java.io.Serializable;

public class SearchDutyEntity implements Serializable {
    private String searchDutyName;

    private String dutyId;

    private String organUuid;

    public String getOrganUuid() {
        return organUuid;
    }

    public void setOrganUuid(String organUuid) {
        this.organUuid = organUuid;
    }

    public String getSearchDutyName()
    {
        return searchDutyName;
    }

    public void setSearchDutyName(String searchDutyName)
    {
        this.searchDutyName = searchDutyName;
    }

    public String getDutyId()
    {
        return dutyId;
    }

    public void setDutyId(String dutyId)
    {
        this.dutyId = dutyId;
    }

    @Override
    public String toString()
    {
        return "SearchDutyEntity [searchDutyName=" + searchDutyName + ", dutyId=" + dutyId + "]";
    }
}
