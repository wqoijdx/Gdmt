package com.isoftstone.gdmt.consult.controller;

import com.isoftstone.gdmt.consult.entity.SearchPortEntity;
import com.isoftstone.gdmt.consult.service.PortService;
import com.isoftstone.gdmt.mybatis.entity.CoalHarborEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping({"/consult"})
public class PortController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private PortService portService;

    @Secured("ROLE_gdmt_data_port")
    @RequestMapping({"/portPage"})
    public String portPage() {
        return "consult/portPage";
    }

    @Secured("ROLE_gdmt_data_port")
    @RequestMapping({"/getPort"})
    @ResponseBody
    public List<CoalHarborEntity> getPort() {
        return portService.getPort();
    }

    @Secured("ROLE_gdmt_data_port")
    @RequestMapping({"/getPortInfolistByPage"})
    @ResponseBody
    public PadingRstType<CoalHarborEntity> getPortInfolistByPage(SearchPortEntity entity, PagingBean paging) {
        this.logger.info(entity);
        this.logger.info(paging);
        PadingRstType<CoalHarborEntity> padingRstType = portService.getPortInfolistByPage(entity,paging);
        return padingRstType;
    }

}
