package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class FuturesPriceEntity implements Serializable
{
	@Columns("futuresprice_id")
	private Integer futurespriceId;

	@Columns("futuresprice_date")
	private String futurespriceDate;

	@Columns("futuresprice_month")
	private Double futurespriceMonth;

	@Columns("futuresprice_oprice")
	private Double futurespriceOprice;

	@Columns("futuresprice_cprice")
	private Double futurespriceCprice;

	@Columns("futuresprice_settlement")
	private Double futurespriceSettlement;

	@Columns("futuresprice_cjl")
	private Double futurespriceCjl;

	@Columns("futuresprice_kpl")
	private Double futurespriceKpl;

	@Columns("futuresprice_hb")
	private Double futurespriceHb;
	
	public Integer getFuturespriceId() 
	{
		return futurespriceId;
	}
	
	public void setFuturespriceId(Integer futurespriceId) 
	{
		this.futurespriceId = futurespriceId;
	}
	
	public String getFuturespriceDate() 
	{
		return futurespriceDate;
	}
	
	public void setFuturespriceDate(String futurespriceDate) 
	{
		this.futurespriceDate = futurespriceDate;
	}
	
	public Double getFuturespriceMonth() 
	{
		return futurespriceMonth;
	}
	
	public void setFuturespriceMonth(Double futurespriceMonth) 
	{
		this.futurespriceMonth = futurespriceMonth;
	}
	
	public Double getFuturespriceOprice() 
	{
		return futurespriceOprice;
	}
	
	public void setFuturespriceOprice(Double futurespriceOprice) 
	{
		this.futurespriceOprice = futurespriceOprice;
	}
	
	public Double getFuturespriceCprice() 
	{
		return futurespriceCprice;
	}
	
	public void setFuturespriceCprice(Double futurespriceCprice) 
	{
		this.futurespriceCprice = futurespriceCprice;
	}
	
	public Double getFuturespriceSettlement() 
	{
		return futurespriceSettlement;
	}
	
	public void setFuturespriceSettlement(Double futurespriceSettlement) 
	{
		this.futurespriceSettlement = futurespriceSettlement;
	}
	
	public Double getFuturespriceCjl() 
	{
		return futurespriceCjl;
	}
	
	public void setFuturespriceCjl(Double futurespriceCjl) 
	{
		this.futurespriceCjl = futurespriceCjl;
	}
	
	public Double getFuturespriceKpl() 
	{
		return futurespriceKpl;
	}
	
	public void setFuturespriceKpl(Double futurespriceKpl) 
	{
		this.futurespriceKpl = futurespriceKpl;
	}
	
	public Double getFuturespriceHb() 
	{
		return futurespriceHb;
	}
	
	public void setFuturespriceHb(Double futurespriceHb) 
	{
		this.futurespriceHb = futurespriceHb;
	}

}
