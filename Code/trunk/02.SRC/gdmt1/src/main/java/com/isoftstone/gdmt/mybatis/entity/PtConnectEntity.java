package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class PtConnectEntity implements Serializable
{
	@Columns("connect_id")
	private String connectId;

	@Columns("connect_name")
	private String connectName;

	@Columns("connect_tel")
	private String connectTel;

	@Columns("supplierid")
	private String supplierid;
	
	public String getConnectId() 
	{
		return connectId;
	}
	
	public void setConnectId(String connectId) 
	{
		this.connectId = connectId;
	}
	
	public String getConnectName() 
	{
		return connectName;
	}
	
	public void setConnectName(String connectName) 
	{
		this.connectName = connectName;
	}
	
	public String getConnectTel() 
	{
		return connectTel;
	}
	
	public void setConnectTel(String connectTel) 
	{
		this.connectTel = connectTel;
	}
	
	public String getSupplierid() 
	{
		return supplierid;
	}
	
	public void setSupplierid(String supplierid) 
	{
		this.supplierid = supplierid;
	}

}
