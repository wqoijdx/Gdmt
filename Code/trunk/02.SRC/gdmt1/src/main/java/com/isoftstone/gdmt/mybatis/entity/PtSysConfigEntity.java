package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class PtSysConfigEntity implements Serializable
{
	@Columns("cfg_id")
	private Integer cfgId;

	@Columns("cfg_name")
	private String cfgName;

	@Columns("cfg_value")
	private String cfgValue;

	@Columns("remarks")
	private String remarks;
	
	public Integer getCfgId() 
	{
		return cfgId;
	}
	
	public void setCfgId(Integer cfgId) 
	{
		this.cfgId = cfgId;
	}
	
	public String getCfgName() 
	{
		return cfgName;
	}
	
	public void setCfgName(String cfgName) 
	{
		this.cfgName = cfgName;
	}
	
	public String getCfgValue() 
	{
		return cfgValue;
	}
	
	public void setCfgValue(String cfgValue) 
	{
		this.cfgValue = cfgValue;
	}
	
	public String getRemarks() 
	{
		return remarks;
	}
	
	public void setRemarks(String remarks) 
	{
		this.remarks = remarks;
	}

}
