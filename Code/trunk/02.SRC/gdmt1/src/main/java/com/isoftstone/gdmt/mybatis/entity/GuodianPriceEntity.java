package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class GuodianPriceEntity implements Serializable
{
	@Columns("price_id")
	private Integer priceId;

	@Columns("price_date")
	private String priceDate;

	@Columns("hot_value5000")
	private Double hotValue5000;

	@Columns("hot_value4000")
	private Double hotValue4000;
	
	public Integer getPriceId() 
	{
		return priceId;
	}
	
	public void setPriceId(Integer priceId) 
	{
		this.priceId = priceId;
	}
	
	public String getPriceDate() 
	{
		return priceDate;
	}
	
	public void setPriceDate(String priceDate) 
	{
		this.priceDate = priceDate;
	}
	
	public Double getHotValue5000() 
	{
		return hotValue5000;
	}
	
	public void setHotValue5000(Double hotValue5000) 
	{
		this.hotValue5000 = hotValue5000;
	}
	
	public Double getHotValue4000() 
	{
		return hotValue4000;
	}
	
	public void setHotValue4000(Double hotValue4000) 
	{
		this.hotValue4000 = hotValue4000;
	}

}
