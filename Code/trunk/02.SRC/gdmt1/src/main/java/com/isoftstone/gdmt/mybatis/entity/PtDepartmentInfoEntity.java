package com.isoftstone.gdmt.mybatis.entity;

import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class PtDepartmentInfoEntity {

    @Columns("DEP_UUID")
    private String depUuid;

    @Columns("ORGAN_UUID")
    private String organUuId;

    @Columns("BRANCH_NAME")
    private String branchName;

    @Columns("BELONG_CENTER")
    private String belongCenter;

    @Columns("DEP_TYPE")
    private String depType;

    public String getDepType() {
        return depType;
    }

    public void setDepType(String depType) {
        this.depType = depType;
    }



    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBelongCenter() {
        return belongCenter;
    }

    public void setBelongCenter(String belongCenter) {
        this.belongCenter = belongCenter;
    }

    public String getDepUuid() {
        return depUuid;
    }

    public void setDepUuid(String depUuid) {
        this.depUuid = depUuid;
    }

    public String getOrganUuId() {
        return organUuId;
    }

    public void setOrganUuId(String organUuId) {
        this.organUuId = organUuId;
    }

    @Override
    public String toString() {
        return "PtDepartmentInfoEntity{" +
                "depUuid='" + depUuid + '\'' +
                ", organUuId='" + organUuId + '\'' +
                ", branchName='" + branchName + '\'' +
                ", belongCenter='" + belongCenter + '\'' +
                ", depType='" + depType + '\'' +
                '}';
    }
}

