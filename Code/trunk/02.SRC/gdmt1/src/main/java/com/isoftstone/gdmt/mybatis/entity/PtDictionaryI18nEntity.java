package com.isoftstone.gdmt.mybatis.entity;
import com.isoftstone.gdmt.mybatis.annotation.Columns;

import java.io.Serializable;


public class PtDictionaryI18nEntity implements Serializable
{
	@Columns("dict_id")
	private Integer dictId;

	@Columns("dict_name")
	private String dictName;

	@Columns("language_id")
	private Integer languageId;
	
	public Integer getDictId() 
	{
		return dictId;
	}
	
	public void setDictId(Integer dictId) 
	{
		this.dictId = dictId;
	}
	
	public String getDictName() 
	{
		return dictName;
	}
	
	public void setDictName(String dictName) 
	{
		this.dictName = dictName;
	}
	
	public Integer getLanguageId() 
	{
		return languageId;
	}
	
	public void setLanguageId(Integer languageId) 
	{
		this.languageId = languageId;
	}

}
