package com.isoftstone.gdmt.mybatis.ztree;

import java.io.Serializable;
import java.util.Objects;

public class ZtreeStrEntity implements Serializable {
    private String id;
    private Boolean checked = false;
    private String pId="0";
    private Boolean open = true;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public Boolean getOpen() {
        return open;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ZtreeStrEntity{" +
                "id='" + id + '\'' +
                ", checked=" + checked +
                ", pId='" + pId + '\'' +
                ", open=" + open +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ZtreeStrEntity that = (ZtreeStrEntity) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
