package com.isoftstone.gdmt.platform.tag.entity;

import java.io.Serializable;

public class DictTagEntity implements Serializable {
    private String dictName;
    private String dicCode;

    public String getDictName() {
        return dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName;
    }

    public String getDicCode() {
        return dicCode;
    }

    public void setDicCode(String dicCode) {
        this.dicCode = dicCode;
    }

    @Override
    public String toString() {
        return "DictTagEntity{" +
                "dictName='" + dictName + '\'' +
                ", dicCode='" + dicCode + '\'' +
                '}';
    }
}
