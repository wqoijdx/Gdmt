package com.isoftstone.gdmt.platform.menu.repository;

import com.isoftstone.gdmt.mybatis.entity.PtMenuEntity;
import com.isoftstone.gdmt.mybatis.entity.PtMenuI18nEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MenuDao {
    PtMenuEntity getMenuInfoById(@Param("menuId") String menuId);

    void modifyMenuInfoById(@Param("entity") PtMenuEntity entity);

    void addMenuInfoById(@Param("entity")PtMenuEntity entity);

    void deleteMenuInfoById(@Param("menuId")String menuId);

    String queryMenuTreeByMenuId(@Param("menuId")String menuId);

    void updateParentIdByMenuId(@Param("menuId")String menuId, @Param("parentId")String parentId);

    List<PtMenuI18nEntity> getI18nByMenuId(@Param("menuId")String menuId);

    void saveI18nData(@Param("entity")PtMenuI18nEntity entity);

    void addI18nData(@Param("entity")PtMenuI18nEntity entity);

    void deleteI18nData(@Param("entity") PtMenuI18nEntity entity);
}
