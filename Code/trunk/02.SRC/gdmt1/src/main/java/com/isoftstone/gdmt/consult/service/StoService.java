package com.isoftstone.gdmt.consult.service;

import com.isoftstone.gdmt.consult.entity.SearchStoEntity;
import com.isoftstone.gdmt.mybatis.entity.CementProductionEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;

import java.util.List;

public interface StoService {
    List<CementProductionEntity> getSto();

    PadingRstType<CementProductionEntity> getStoInfolistByPage(SearchStoEntity entity, PagingBean paging);


}