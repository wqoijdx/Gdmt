package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class CementProductionEntity implements Serializable
{
	@Columns("cement_id")
	private Integer cementId;

	@Columns("cement_date")
	private String cementDate;

	@Columns("cement_production")
	private Double cementProduction;

	@Columns("cement_compare")
	private Double cementCompare;
	
	public Integer getCementId() 
	{
		return cementId;
	}
	
	public void setCementId(Integer cementId) 
	{
		this.cementId = cementId;
	}
	
	public String getCementDate() 
	{
		return cementDate;
	}
	
	public void setCementDate(String cementDate) 
	{
		this.cementDate = cementDate;
	}
	
	public Double getCementProduction() 
	{
		return cementProduction;
	}
	
	public void setCementProduction(Double cementProduction) 
	{
		this.cementProduction = cementProduction;
	}
	
	public Double getCementCompare() 
	{
		return cementCompare;
	}
	
	public void setCementCompare(Double cementCompare) 
	{
		this.cementCompare = cementCompare;
	}

}
