package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class CoalHarborEntity implements Serializable
{
	@Columns("harbor_id")
	private Integer harborId;

	@Columns("harbor_date")
	private String harborDate;

	@Columns("harbor_k5800")
	private Double harborK5800;

	@Columns("harbor_k5500")
	private Double harborK5500;

	@Columns("harbor_k5000")
	private Double harborK5000;

	@Columns("harbor_k4500")
	private Double harborK4500;
	
	public Integer getHarborId() 
	{
		return harborId;
	}
	
	public void setHarborId(Integer harborId) 
	{
		this.harborId = harborId;
	}
	
	public String getHarborDate() 
	{
		return harborDate;
	}
	
	public void setHarborDate(String harborDate) 
	{
		this.harborDate = harborDate;
	}
	
	public Double getHarborK5800() 
	{
		return harborK5800;
	}
	
	public void setHarborK5800(Double harborK5800) 
	{
		this.harborK5800 = harborK5800;
	}
	
	public Double getHarborK5500() 
	{
		return harborK5500;
	}
	
	public void setHarborK5500(Double harborK5500) 
	{
		this.harborK5500 = harborK5500;
	}
	
	public Double getHarborK5000() 
	{
		return harborK5000;
	}
	
	public void setHarborK5000(Double harborK5000) 
	{
		this.harborK5000 = harborK5000;
	}
	
	public Double getHarborK4500() 
	{
		return harborK4500;
	}
	
	public void setHarborK4500(Double harborK4500) 
	{
		this.harborK4500 = harborK4500;
	}

}
