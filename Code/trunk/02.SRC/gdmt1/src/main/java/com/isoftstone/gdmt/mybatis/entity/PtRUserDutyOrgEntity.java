package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class PtRUserDutyOrgEntity implements Serializable
{
	@Columns("USER_UUID")
	private String userUuid;

	@Columns("DUTYID")
	private String dutyid;
	
	public String getUserUuid() 
	{
		return userUuid;
	}
	
	public void setUserUuid(String userUuid) 
	{
		this.userUuid = userUuid;
	}
	
	public String getDutyid() 
	{
		return dutyid;
	}
	
	public void setDutyid(String dutyid) 
	{
		this.dutyid = dutyid;
	}

}
