package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class PtSecurityLogEntity implements Serializable
{
	@Columns("log_id")
	private Integer logId;

	@Columns("user_uuid")
	private String userUuid;

	@Columns("log_system")
	private Integer logSystem;

	@Columns("log_module")
	private Integer logModule;

	@Columns("log_action")
	private Integer logAction;

	@Columns("log_contents")
	private String logContents;

	@Columns("create_time")
	private String createTime;
	
	public Integer getLogId() 
	{
		return logId;
	}
	
	public void setLogId(Integer logId) 
	{
		this.logId = logId;
	}
	
	public String getUserUuid() 
	{
		return userUuid;
	}
	
	public void setUserUuid(String userUuid) 
	{
		this.userUuid = userUuid;
	}
	
	public Integer getLogSystem() 
	{
		return logSystem;
	}
	
	public void setLogSystem(Integer logSystem) 
	{
		this.logSystem = logSystem;
	}
	
	public Integer getLogModule() 
	{
		return logModule;
	}
	
	public void setLogModule(Integer logModule) 
	{
		this.logModule = logModule;
	}
	
	public Integer getLogAction() 
	{
		return logAction;
	}
	
	public void setLogAction(Integer logAction) 
	{
		this.logAction = logAction;
	}
	
	public String getLogContents() 
	{
		return logContents;
	}
	
	public void setLogContents(String logContents) 
	{
		this.logContents = logContents;
	}
	
	public String getCreateTime() 
	{
		return createTime;
	}
	
	public void setCreateTime(String createTime) 
	{
		this.createTime = createTime;
	}

}
