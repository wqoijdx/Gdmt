package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class GdpExEntity implements Serializable
{
	@Columns("gdp_id")
	private Integer gdpId;

	@Columns("gdp_depart")
	private String gdpDepart;

	@Columns("gdp_cycle")
	private String gdpCycle;
	
	public Integer getGdpId() 
	{
		return gdpId;
	}
	
	public void setGdpId(Integer gdpId) 
	{
		this.gdpId = gdpId;
	}
	
	public String getGdpDepart() 
	{
		return gdpDepart;
	}
	
	public void setGdpDepart(String gdpDepart) 
	{
		this.gdpDepart = gdpDepart;
	}
	
	public String getGdpCycle() 
	{
		return gdpCycle;
	}
	
	public void setGdpCycle(String gdpCycle) 
	{
		this.gdpCycle = gdpCycle;
	}

}
