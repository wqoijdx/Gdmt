package com.isoftstone.gdmt.consult.service.impl;

import com.isoftstone.gdmt.consult.entity.SearchPortEntity;
import com.isoftstone.gdmt.consult.repository.PortDao;
import com.isoftstone.gdmt.consult.service.PortService;
import com.isoftstone.gdmt.mybatis.entity.CoalHarborEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("portService")
public class PortServiceImpl implements PortService {
    @Resource
    private PortDao portDao;

    @Override
    public List<CoalHarborEntity> getPort() {
        return portDao.getPort();
    }

    @Override
    public PadingRstType<CoalHarborEntity> getPortInfolistByPage(SearchPortEntity entity, PagingBean paging) {
        paging.deal(CoalHarborEntity.class);
        PadingRstType<CoalHarborEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(paging.getPage());
        List<CoalHarborEntity> list = portDao.queryPortInfoByPage(entity,paging);
        padingRstType.setRawRecords(list);
        Integer total = portDao.queryPortInfoTotal(entity);
        padingRstType.setTotal(total);
        padingRstType.putItems();
        return padingRstType;
    }

}
