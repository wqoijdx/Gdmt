package com.isoftstone.gdmt.duty.service.impl;


import com.isoftstone.gdmt.duty.entity.SearchDutyEntity;
import com.isoftstone.gdmt.duty.repository.DutyDao;
import com.isoftstone.gdmt.duty.service.DutyService;

import com.isoftstone.gdmt.mybatis.entity.PtRRoleOrganEntity;
import com.isoftstone.gdmt.mybatis.entity.PtUserEntity;
import com.isoftstone.gdmt.mybatis.flexigrid.PadingRstType;
import com.isoftstone.gdmt.mybatis.flexigrid.PagingBean;
import com.isoftstone.gdmt.mybatis.ztree.ZtreeStrEntity;
import com.isoftstone.gdmt.platform.common.util.DataUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Locale;

@Service("dutyService")
public class DutyServiceImpl implements DutyService {
    @Resource
    private DutyDao dutyDao;
    @Value("${ubound.uuid}")
    private String uboundUuid;
    @Resource
    private MessageSource messageSource;
    @Value("${root.uuid}")
    private String rootUuid;

    @Override
    public PadingRstType<PtRRoleOrganEntity> queryDutyListPage(SearchDutyEntity search, PagingBean paging) {
        paging.deal(PtRRoleOrganEntity.class);
        PadingRstType<PtRRoleOrganEntity> PtRRoleOrganEntityPadingRstType =  new PadingRstType<PtRRoleOrganEntity>();
        PtRRoleOrganEntityPadingRstType.setPage(paging.getPage());
        List<PtRRoleOrganEntity> list = dutyDao.queryDutyListPage(search,paging);
        PtRRoleOrganEntityPadingRstType.setRawRecords(list);
        Integer total = dutyDao.queryDutyTotal(search);
        PtRRoleOrganEntityPadingRstType.setTotal(total);
        PtRRoleOrganEntityPadingRstType.putItems(PtRRoleOrganEntity.class,list);
        return PtRRoleOrganEntityPadingRstType;
    }


    @Override
    public List<ZtreeStrEntity> getDutyTree() {
        List<ZtreeStrEntity> list = dutyDao.queryDutyTree();
        ZtreeStrEntity ztreeStrEntity = new ZtreeStrEntity();
        ztreeStrEntity.setId(uboundUuid);
        Locale locale = LocaleContextHolder.getLocale();
        String unboundName = messageSource.getMessage("common.unbound.name",null,locale);
        ztreeStrEntity.setName(unboundName);
        list.add(ztreeStrEntity);
        Integer index = 0;
        for(ZtreeStrEntity item:list){
            if(index < 10){
                item.setOpen(true);
            }else{
                item.setOpen(false);
            }
            index++;
        }

        return list;
    }

    @Override
    public List<ZtreeStrEntity> queryRoleTree() {
        List<ZtreeStrEntity> list = dutyDao.queryRoleTree();
        ZtreeStrEntity ztreeStrEntity = new ZtreeStrEntity();
        ztreeStrEntity.setId(uboundUuid);
        Locale locale = LocaleContextHolder.getLocale();
        String unboundName = messageSource.getMessage("common.unbound.name",null,locale);
        ztreeStrEntity.setName(unboundName);
        list.add(ztreeStrEntity);
        Integer index = 0;
        for(ZtreeStrEntity item:list){
            if(index < 10){
                item.setOpen(true);
            }else{
                item.setOpen(false);
            }
            index++;
        }
        return list;
    }

    @Override
    public PtRRoleOrganEntity getDutyInfoById(String organUuid) {
        return dutyDao.getDutyInfoById(organUuid);
    }


    @Override
    public List<ZtreeStrEntity> getOrganByDutyId(String dutyid) {
        Locale locale = LocaleContextHolder.getLocale();
        String msgStr = messageSource.getMessage("organ.info", null, locale);
        List<ZtreeStrEntity> list = dutyDao.queryDutyTree();
        ZtreeStrEntity ztreeStrEntity = new ZtreeStrEntity();
        ztreeStrEntity.setId("0");
        ztreeStrEntity.setName(msgStr);
        ztreeStrEntity.setpId("-1");
        list.add(ztreeStrEntity);
        List<String> organList = dutyDao.getOrganByDutyId(dutyid);
        for(ZtreeStrEntity item:list){
            for(String organ_uuId: organList){
                if(organ_uuId.equals(item.getId())){
                    item.setChecked(true);
                }
            }
        }
        return list;
    }

    @Override
    public List<ZtreeStrEntity> getRoleByDutyId(String dutyid) {
        Locale locale = LocaleContextHolder.getLocale();
        String msgStr = messageSource.getMessage("role.manage.info", null, locale);
        List<ZtreeStrEntity> list = dutyDao.queryRoleTree();
        ZtreeStrEntity ztreeStrEntity = new ZtreeStrEntity();
        ztreeStrEntity.setId("0");
        ztreeStrEntity.setName(msgStr);
        ztreeStrEntity.setpId("-1");
        list.add(ztreeStrEntity);
        List<String> roleList = dutyDao.getOrganByDutyId(dutyid);
        for(ZtreeStrEntity item:list){
            for(String role_uuId: roleList){
                if(role_uuId.equals(item.getId())){
                    item.setChecked(true);
                }
            }
        }
        return list;
    }

    @Override
    public void setOrganByDutyId(String dutyid, String organUuidArray) {

        String[] organUuids = organUuidArray.split(",");
        for(String organUuid:organUuids){
            dutyDao.insertOrganAndDutyId(dutyid,organUuid);
        }
    }

    @Override
    public void setRoleByDutyId(String dutyid, String roleUuidArray) {

        String[] roleUuids = roleUuidArray.split(",");
        for(String roleUuid:roleUuids){
            dutyDao.insertRoleAndDutyId(dutyid,roleUuid);
        }
    }

    @Override
    public void addDutyInfoById(PtRRoleOrganEntity entity) {
        entity.setDutyid(DataUtil.getUUIDShort());
        dutyDao.addDutyInfoById(entity);
    }

    @Override
    public int deleteDutyInfoById(String dutyid) {
        List<PtUserEntity> userList = dutyDao.selectUserById(dutyid);
        if(userList.size()==0)
            {dutyDao.deleteDutyInfoById(dutyid);
             return 0;}
        else
            return 1;
    }

    @Override
    public PadingRstType<PtRRoleOrganEntity> queryDutyListPage2(SearchDutyEntity search, PagingBean paging) {
        paging.deal(PtRRoleOrganEntity.class);
        PadingRstType<PtRRoleOrganEntity> PtRRoleOrganEntityPadingRstType =  new PadingRstType<PtRRoleOrganEntity>();
        PtRRoleOrganEntityPadingRstType.setPage(paging.getPage());
        List<PtRRoleOrganEntity> list = dutyDao.queryDutyListPage2(search,paging);
        PtRRoleOrganEntityPadingRstType.setRawRecords(list);
        Integer total = dutyDao.queryDutyTotal(search);
        PtRRoleOrganEntityPadingRstType.setTotal(total);
        PtRRoleOrganEntityPadingRstType.putItems(PtRRoleOrganEntity.class,list);
        return PtRRoleOrganEntityPadingRstType;
    }


}
