package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class CmsVoteEntity implements Serializable
{
	@Columns("vote_uuid")
	private String voteUuid;

	@Columns("vote_theme")
	private String voteTheme;

	@Columns("A_count")
	private Integer aCount;

	@Columns("B_count")
	private Integer bCount;

	@Columns("C_count")
	private Integer cCount;

	@Columns("D_count")
	private Integer dCount;

	@Columns("A_dsc")
	private String aDsc;

	@Columns("B_dsc")
	private String bDsc;

	@Columns("C_dsc")
	private String cDsc;

	@Columns("D_dsc")
	private String dDsc;
	
	public String getVoteUuid() 
	{
		return voteUuid;
	}
	
	public void setVoteUuid(String voteUuid) 
	{
		this.voteUuid = voteUuid;
	}
	
	public String getVoteTheme() 
	{
		return voteTheme;
	}
	
	public void setVoteTheme(String voteTheme) 
	{
		this.voteTheme = voteTheme;
	}
	
	public Integer getACount() 
	{
		return aCount;
	}
	
	public void setACount(Integer aCount) 
	{
		this.aCount = aCount;
	}
	
	public Integer getBCount() 
	{
		return bCount;
	}
	
	public void setBCount(Integer bCount) 
	{
		this.bCount = bCount;
	}
	
	public Integer getCCount() 
	{
		return cCount;
	}
	
	public void setCCount(Integer cCount) 
	{
		this.cCount = cCount;
	}
	
	public Integer getDCount() 
	{
		return dCount;
	}
	
	public void setDCount(Integer dCount) 
	{
		this.dCount = dCount;
	}
	
	public String getADsc() 
	{
		return aDsc;
	}
	
	public void setADsc(String aDsc) 
	{
		this.aDsc = aDsc;
	}
	
	public String getBDsc() 
	{
		return bDsc;
	}
	
	public void setBDsc(String bDsc) 
	{
		this.bDsc = bDsc;
	}
	
	public String getCDsc() 
	{
		return cDsc;
	}
	
	public void setCDsc(String cDsc) 
	{
		this.cDsc = cDsc;
	}
	
	public String getDDsc() 
	{
		return dDsc;
	}
	
	public void setDDsc(String dDsc) 
	{
		this.dDsc = dDsc;
	}

}
