package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class PtRoleUserEntity implements Serializable
{
	@Columns("USER_UUID")
	private String userUuid;

	@Columns("ROLE_UUID")
	private String roleUuid;
	
	public String getUserUuid() 
	{
		return userUuid;
	}
	
	public void setUserUuid(String userUuid) 
	{
		this.userUuid = userUuid;
	}
	
	public String getRoleUuid() 
	{
		return roleUuid;
	}
	
	public void setRoleUuid(String roleUuid) 
	{
		this.roleUuid = roleUuid;
	}

}
