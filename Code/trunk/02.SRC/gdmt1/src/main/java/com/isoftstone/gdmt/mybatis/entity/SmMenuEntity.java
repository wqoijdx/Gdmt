package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class SmMenuEntity implements Serializable
{
	@Columns("menu_id")
	private Integer menuId;

	@Columns("parent_id")
	private Integer parentId;

	@Columns("name")
	private String name;

	@Columns("type")
	private Integer type;

	@Columns("url")
	private String url;

	@Columns("seq")
	private Integer seq;

	@Columns("create_time")
	private String createTime;

	@Columns("is_https")
	private Integer isHttps;

	@Columns("description")
	private String description;

	@Columns("privilege_code")
	private String privilegeCode;

	@Columns("level")
	private Integer level;

	@Columns("icon")
	private String icon;
	
	public Integer getMenuId() 
	{
		return menuId;
	}
	
	public void setMenuId(Integer menuId) 
	{
		this.menuId = menuId;
	}
	
	public Integer getParentId() 
	{
		return parentId;
	}
	
	public void setParentId(Integer parentId) 
	{
		this.parentId = parentId;
	}
	
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public Integer getType() 
	{
		return type;
	}
	
	public void setType(Integer type) 
	{
		this.type = type;
	}
	
	public String getUrl() 
	{
		return url;
	}
	
	public void setUrl(String url) 
	{
		this.url = url;
	}
	
	public Integer getSeq() 
	{
		return seq;
	}
	
	public void setSeq(Integer seq) 
	{
		this.seq = seq;
	}
	
	public String getCreateTime() 
	{
		return createTime;
	}
	
	public void setCreateTime(String createTime) 
	{
		this.createTime = createTime;
	}
	
	public Integer getIsHttps() 
	{
		return isHttps;
	}
	
	public void setIsHttps(Integer isHttps) 
	{
		this.isHttps = isHttps;
	}
	
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String description) 
	{
		this.description = description;
	}
	
	public String getPrivilegeCode() 
	{
		return privilegeCode;
	}
	
	public void setPrivilegeCode(String privilegeCode) 
	{
		this.privilegeCode = privilegeCode;
	}
	
	public Integer getLevel() 
	{
		return level;
	}
	
	public void setLevel(Integer level) 
	{
		this.level = level;
	}
	
	public String getIcon() 
	{
		return icon;
	}
	
	public void setIcon(String icon) 
	{
		this.icon = icon;
	}

}
