package com.isoftstone.gdmt.platform.controller;

import com.alibaba.fastjson.JSON;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Locale;

public class BaseConctroller {
    @Resource
    private MessageSource messageSource;
    protected String getSuccess(String code,String ... args){
        HashMap<String,Object> message = new HashMap<>();
        Locale locale = LocaleContextHolder.getLocale();
        String msgStr = messageSource.getMessage(code, args, locale);
        message.put("success", true);
        message.put("msg", msgStr);
        message.put("description", msgStr);
        return JSON.toJSONString(message);
    }

    protected String getFail(String code,String ... args){
        HashMap<String,Object> message = new HashMap<>();
        Locale locale = LocaleContextHolder.getLocale();
        String msgStr = messageSource.getMessage(code, args, locale);
        message.put("success", false);
        message.put("msg", msgStr);
        message.put("description", msgStr);
        return JSON.toJSONString(message);
    }
}
