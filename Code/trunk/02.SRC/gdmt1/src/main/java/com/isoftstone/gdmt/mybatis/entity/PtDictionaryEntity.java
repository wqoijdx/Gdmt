package com.isoftstone.gdmt.mybatis.entity;
import java.io.Serializable;
import com.isoftstone.gdmt.mybatis.annotation.Columns;


public class PtDictionaryEntity implements Serializable
{
	@Columns("dict_id")
	private Integer dictId;

	@Columns("dict_parent_id")
	private Integer dictParentId;

	@Columns("dict_group")
	private String dictGroup;

	@Columns("dic_code")
	private String dicCode;

	@Columns("description")
	private String description;

	@Columns("dic_sort")
	private Integer dicSort;
	
	public Integer getDictId() 
	{
		return dictId;
	}
	
	public void setDictId(Integer dictId) 
	{
		this.dictId = dictId;
	}
	
	public Integer getDictParentId() 
	{
		return dictParentId;
	}
	
	public void setDictParentId(Integer dictParentId) 
	{
		this.dictParentId = dictParentId;
	}
	
	public String getDictGroup() 
	{
		return dictGroup;
	}
	
	public void setDictGroup(String dictGroup) 
	{
		this.dictGroup = dictGroup;
	}
	
	public String getDicCode() 
	{
		return dicCode;
	}
	
	public void setDicCode(String dicCode) 
	{
		this.dicCode = dicCode;
	}
	
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String description) 
	{
		this.description = description;
	}
	
	public Integer getDicSort() 
	{
		return dicSort;
	}
	
	public void setDicSort(Integer dicSort) 
	{
		this.dicSort = dicSort;
	}

}
