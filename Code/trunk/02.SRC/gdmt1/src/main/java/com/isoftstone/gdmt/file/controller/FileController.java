package com.isoftstone.gdmt.file.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/disk")
public class FileController {
    @Secured("ROLE_cldk_data_show_file")
    @RequestMapping("/initPage")
    public String initPage(){
        return "test/fileShow";
    }
}
