<%@include file="../common/common.jsp" %>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<html>
<head>
    <title>Title</title>
    <script type="text/javascript">
        var _gridWidth;
        var _gridHeight;
        var _columnWidth;

        //页面自适应
        function resizePageSize() {
            _gridWidth = $(document).width() - 12;/*  -189 是去掉左侧 菜单的宽度，   -12 是防止浏览器缩小页面 出现滚动条 恢复页面时  折行的问题 */
            _gridHeight = $(document).height() - 32 - 80; /* -32 顶部主菜单高度，   -90 查询条件高度*/
        }

        $(function () {
            resizePageSize();
            $("#sysnch-btn-id").click(function (event) {
                $.ajax({
                    type: 'POST',
                    url: '${pageContext.request.contextPath}/disk/sysnchObjData',
                    dataType: 'json',
                    cache: false,
                    data: [],
                    success: function (data) {
                        message(data.msg);
                    },
                    error: function () {
                        message('<spring:message code="common.error"></spring:message>');
                    }
                });

            });

            _columnWidth = _gridWidth / 6;
            $("#flexigrid-id").flexigrid({
                width: _gridWidth,
                height: _gridHeight,
                url: "${pageContext.request.contextPath}/disk/queryDeleteFileListPaging",
                dataType: 'json',
                colModel: [
                    {
                        display: 'fileUuid',
                        name: 'fileUuid',
                        width: _columnWidth,
                        sortable: true,
                        align: 'center',
                        hide: 'true'
                    },
                    {
                        display: '<spring:message code="disk.file.name"></spring:message>',
                        name: 'fileName',
                        width: _columnWidth,
                        sortable: true,
                        align: 'center',process: function (v, _trid, _row){
                            var postDetail = '<span style="cursor:pointer;" onclick="preFile(\'' + v + '\'';
                            postDetail = postDetail + ');">'+ v +'</span>';
                            return postDetail;
                        }
                    },
                    {
                        display: '<spring:message code="disk.parent.uuid"></spring:message>',
                        name: 'parentUuid',
                        width: _columnWidth,
                        sortable: true,
                        align: 'center'
                    },
                    {
                        display: '<spring:message code="disk.file.type"></spring:message>',
                        name: 'fileType',
                        width: _columnWidth,
                        sortable: true,
                        align: 'center',
                        process: function (v, _trid, _row) {
                            return $("#seach-file-type-id").find("option[value=" + v + "]").text();
                        }
                    },
                    {
                        display: '<spring:message code="disk.file.capacity"></spring:message>',
                        name: 'fileCapacity',
                        width: _columnWidth,
                        sortable: true,
                        align: 'center'
                    },
                    {
                        display: '<spring:message code="disk.file.modtime"></spring:message>',
                        name: 'modtime',
                        width: _columnWidth,
                        sortable: true,
                        align: 'center'
                    },
                    {
                        display: '<spring:message code="file.operation"></spring:message>',
                        name: 'fileUuid',
                        width: _columnWidth,
                        sortable: false,
                        align: 'center',
                        process: function (v, _trid, _row) {
                            if (_row.fileType === 1) {
                                return '';
                            }
                            var postDetail = '<span style="cursor:pointer;" onclick="undeleteFile(\'' + v + '\'';
                            postDetail = postDetail + ');"<i class="glyphicon glyphicon-repeat"></i></span>';
                            return postDetail;
                        }
                    },

                ],
                resizable: false, //resizable table是否可伸缩
                useRp: true,
                usepager: true, //是否分页
                autoload: false, //自动加载，即第一次发起ajax请求
                hideOnSubmit: true, //是否在回调时显示遮盖
                showcheckbox: true, //是否显示多选框
                //rowhandler : rowDbclick, //是否启用行的扩展事情功能,在生成行时绑定事件，如双击，右键等
                rowbinddata: true,
                numCheckBoxTitle: "<spring:message code='common.selectall'/>",
                extParam: [],
                newp: 1
            });
            $("#flexigrid-id").flexReload();

            $("#query-btn-id").click(function (event) {
                var searchFileTypeVl = $("#seach-file-type-id").val();
                var searchFileNameVl = $("#search-file-name-id").val();

                $("#flexigrid-id").flexOptions({
                    extParam: [
                        {name: "seachFileType", value: searchFileTypeVl},
                        {name: "searchFileName", value: searchFileNameVl}
                    ],
                }).flexReload();

            });
        });

        function undeleteFile(fileUuid) {
            $.ajax({
                type : 'POST',
                url : '${pageContext.request.contextPath}/disk/undeleteFileByid',
                dataType : 'json',
                cache : false,
                data : [{name:"fileUuid",value:fileUuid}],
                success : function(data) {
                    message(data.msg);
                    $("#flexigrid-id").flexReload();
                },
                error : function() {
                    message("<spring:message code='common.error'/>");
                }
            });
        }
    </script>
</head>
<body>
<spring:message code="disk.title.tip"></spring:message>


<nav class="nav-divider">
        <span>
            <spring:message code="disk.file.name"></spring:message>
        </span>
    <input type="text" id="search-file-name-id"/>
    <span>
            <spring:message code="disk.file.type"></spring:message>
        </span>
    <select id="seach-file-type-id">
        <option value="">select empty</option>
        <option value="0">file</option>
        <option value="1">dir</option>
    </select>
    <a id="query-btn-id" href="#">
        <i class="glyphicon glyphicon-zoom-in"></i>
        <span>
                <spring:message code="common.query"></spring:message>
            </span>
    </a>

</nav>
<table id="flexigrid-id" style="display: block;margin: 0"></table>
<div id="file-upload-dialog-id" style="display: none">
    <form id="file-upload-form-id" method="post" action="${pageContext.request.contextPath}/disk/uploadFile"
          enctype="multipart/form-data">
        <input id="file-name-id" type="file" name="fileName" rep="^.{1,30}$"/>
    </form>
</div>

</body>
</html>
