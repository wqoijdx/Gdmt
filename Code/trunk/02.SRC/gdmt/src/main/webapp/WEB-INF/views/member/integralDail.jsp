<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/page.css">


<title><spring:message code='menber.list.integral.detail'/></title>

<script type="text/javascript">
var _gridWidth;
var _gridHeight;
//页面自适应
function resizePageSize(){
	_gridWidth = $(document).width()-12;/*  -189 是去掉左侧 菜单的宽度，   -12 是防止浏览器缩小页面 出现滚动条 恢复页面时  折行的问题 */
	_gridHeight = $(document).height()-32-80; /* -32 顶部主菜单高度，   -90 查询条件高度*/
}

$(function()
{
	resizePageSize();
	var _columnWidth= (_gridWidth-150)/4;
	
	$("#flexiGridID").flexigrid({
		width : _gridWidth,
		height : _gridHeight,
		url : "${pageContext.request.contextPath}/integral/getntegrallist",
		dataType : 'json',
		colModel : [ 
		 		   {display : 'ID',name : 'id',width : 150,sortable : false,align : 'center',hide : 'true'}, 
		 	       {display : "<spring:message code='menber.list.number'/>",name : 'memberId',width : _columnWidth, sortable : true,align : 'center'}, 
		 	       {display : "<spring:message code='menber.list.name'/>",name : 'name',width : _columnWidth, sortable : true,align : 'center'},
		 	       {display : "<spring:message code='menber.list.integral'/>",name : 'integral',width : _columnWidth, sortable : true,align : 'center'},
		 	       {display : "<spring:message code='menber.list.update.time'/>",name : 'updateTime',width : _columnWidth, sortable : true,align : 'center'}
		 		],
		resizable : false, //resizable table是否可伸缩
		usepager : true,
		useRp : true,
		usepager : true, //是否分页
		autoload : false, //自动加载，即第一次发起ajax请求
		hideOnSubmit : true, //是否在回调时显示遮盖
		showcheckbox : true, //是否显示多选框
		//rowhandler : rowDbclick, //是否启用行的扩展事情功能,在生成行时绑定事件，如双击，右键等
		rowbinddata : true,
		numCheckBoxTitle : "<spring:message code='common.selectall'/>"
	});	
	
	query([{"name": "memberId","value":${memberId}}]);

	$("#queryBtn").click(function( event ) {
		var searchMonth = $("#searchMonth").val();
		query([{"name": "month","value":searchMonth},
		       {"name": "memberId","value":${memberId}}]);
		
		
	});
});




function query(param1){
	$('#flexiGridID').flexOptions({
		newp: 1,
		extParam: param1||[],
    	url: '${pageContext.request.contextPath}/integral/getIntegrallist'
    }).flexReload();
}

</script>

</head>
<body>
<div style="font-size: 18px;font-weight: bold;">
  <spring:message code='menber.list.integral.detail'/>-> <spring:message code='menber.list.name'/>：${memberInfo.name}
 <hr>
</div>
<div style="padding-top: 5px;">
<input type="text" name="searchMonth" class="Wdate" empty="false" onclick="WdatePicker({dateFmt:'yyyy/MM'});" style="font-size: 18px;height: 25px;" id="searchMonth">

<a href="#" id="queryBtn" class="ui-state-default ui-corner-all cbe-button"><span class="ui-icon ui-icon-search"></span>查询</a>
</div>

<div class="tbl">
   <table id="flexiGridID" style="display: block;margin: 0px;"></table>
</div>

</body>
</html>