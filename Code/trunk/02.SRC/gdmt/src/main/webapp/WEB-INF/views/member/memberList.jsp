<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/page.css">

<title> <spring:message code="menber.page.title"/></title>

<script type="text/javascript">
var _gridWidth;
var _gridHeight;
//页面自适应
function resizePageSize(){
	_gridWidth = $(document).width()-12;/*  -189 是去掉左侧 菜单的宽度，   -12 是防止浏览器缩小页面 出现滚动条 恢复页面时  折行的问题 */
	_gridHeight = $(document).height()-32-80; /* -32 顶部主菜单高度，   -90 查询条件高度*/
}
function rowDbclick(r){
	$(r).dblclick(
	 function() {
		 var columnsArray = $(r).attr('ch').split("_FG$SP_");
		 $("#id_mod").val(columnsArray[0]);
		 $("#memberId_mod").val(columnsArray[1]);
		 $("#name_mod").val(columnsArray[2]);
		 $("#tel_mod").val(columnsArray[3]);
		 $("#remarks_mod").val(columnsArray[6]);
		 $("#updateMemberDailDialog").dialog("open");
	 });
}


$(function()
{
	resizePageSize();
	var _columnWidth= (_gridWidth-150)/7;
	
	$("#flexiGridID").flexigrid({
		width : _gridWidth,
		height : _gridHeight,
		url : "${pageContext.request.contextPath}/member/getDepartmentInfolist",
		dataType : 'json',
		colModel : [ 
		   {display : 'ID',name : 'id',width : 150,sortable : false,align : 'center',hide : 'true'}, 
		   {display : "<spring:message code='menber.list.number'/>",name : 'memberId',width : _columnWidth, sortable : true,align : 'center'},
           {display : "<spring:message code='menber.list.name'/>",name : 'name',width : _columnWidth, sortable : true,align : 'center'}, 
	       {display : "<spring:message code='menber.list.tel'/>",name : 'tel',width : _columnWidth, sortable : true,align : 'center'},
	       {display : " <spring:message code='menber.list.integral'/>",name : 'integral',width : _columnWidth, sortable : true,align : 'center'},
	       {display : "<spring:message code='menber.list.update.time'/>",name : 'updateTime',width : _columnWidth, sortable : true,align : 'center'},
	       {display : "<spring:message code='menber.list.consume.detail'/>",name : 'producer',width : _columnWidth, sortable : true,align : 'center',process: function(v,_trid,_row) {				 
				var htmlContents ='<span style="color:blue;cursor:pointer;" onclick="toConsumeDail(\''+_row.id+'\');"><spring:message code='common.look.up'/></span>';
		    	return htmlContents;
           }},
           {display : "<spring:message code='menber.list.integral.detail'/>",name : 'producer',width : _columnWidth, sortable : true,align : 'center',process: function(v,_trid,_row) {				 
				var htmlContents ='<span style="color:blue;cursor:pointer;" onclick="toIntegralDail(\''+_row.id+'\');"><spring:message code='common.look.up'/></span>';
		    	return htmlContents;
          }},
		],
		resizable : false, //resizable table是否可伸缩
		usepager : true,
		useRp : true,
		usepager : true, //是否分页
		autoload : false, //自动加载，即第一次发起ajax请求
		hideOnSubmit : true, //是否在回调时显示遮盖
		showcheckbox : true, //是否显示多选框
		rowhandler : rowDbclick, //是否启用行的扩展事情功能,在生成行时绑定事件，如双击，右键等
		rowbinddata : true,
		numCheckBoxTitle : "<spring:message code='common.selectall'/>"
	});	$("#flexiGridID").flexigrid({
		width : _gridWidth,
		height : _gridHeight,
		url : "${pageContext.request.contextPath}/member/getDepartmentInfolist",
		dataType : 'json',
		colModel : [
		   {display : 'ID',name : 'id',width : 150,sortable : false,align : 'center',hide : 'true'},
		   {display : "<spring:message code='menber.list.number'/>",name : 'memberId',width : _columnWidth, sortable : true,align : 'center'},
           {display : "<spring:message code='menber.list.name'/>",name : 'name',width : _columnWidth, sortable : true,align : 'center'},
	       {display : "<spring:message code='menber.list.tel'/>",name : 'tel',width : _columnWidth, sortable : true,align : 'center'},
	       {display : " <spring:message code='menber.list.integral'/>",name : 'integral',width : _columnWidth, sortable : true,align : 'center'},
	       {display : "<spring:message code='menber.list.update.time'/>",name : 'updateTime',width : _columnWidth, sortable : true,align : 'center'},
	       {display : "<spring:message code='menber.list.consume.detail'/>",name : 'producer',width : _columnWidth, sortable : true,align : 'center',process: function(v,_trid,_row) {
				var htmlContents ='<span style="color:blue;cursor:pointer;" onclick="toConsumeDail(\''+_row.id+'\');"><spring:message code='common.look.up'/></span>';
		    	return htmlContents;
           }},
           {display : "<spring:message code='menber.list.integral.detail'/>",name : 'producer',width : _columnWidth, sortable : true,align : 'center',process: function(v,_trid,_row) {
				var htmlContents ='<span style="color:blue;cursor:pointer;" onclick="toIntegralDail(\''+_row.id+'\');"><spring:message code='common.look.up'/></span>';
		    	return htmlContents;
          }},
		],
		resizable : false, //resizable table是否可伸缩
		usepager : true,
		useRp : true,
		usepager : true, //是否分页
		autoload : false, //自动加载，即第一次发起ajax请求
		hideOnSubmit : true, //是否在回调时显示遮盖
		showcheckbox : true, //是否显示多选框
		rowhandler : rowDbclick, //是否启用行的扩展事情功能,在生成行时绑定事件，如双击，右键等
		rowbinddata : true,
		numCheckBoxTitle : "<spring:message code='common.selectall'/>"
	});
	
	query();
	
	
	
	$("#addBtn").click(function( event ) {
        //message("增加页面");
        $("#addMemberDailFormId").resetForm();
        $("#addMemberDailDialog").dialog( "open" );
        
	});
	
	$("#queryBtn").click(function( event ) {
		var searchId = $("#searchId").val();
		var searchName = $("#searchName").val();
		var searchTel = $("#searchTel").val();
		query([{"name": "memberId","value":searchId},
		       {"name": "name","value":searchName},
		       {"name": "tel","value":searchTel}]);
	});
	
	$("#delBtn").click(function( event ) {
		
		var ids = searchTableColumn($("#flexiGridID"),0);
		if(ids.length > 0){
			$.ajax({
				type : 'POST',
				url : '${pageContext.request.contextPath}/member/deleteMemberInfo',
				dataType : 'json',
				cache : false,
				data : [ {
					name : 'ids',
					value : ids
				} ],
				success : function(data) {
					$('#flexiGridID').flexReload();
					message(data.msg);
				    
				},
				error : function() {
					message("<spring:message code='common.error'/>");
				}
			});
			
		}else{
			
			message("<spring:message code='common.empty'/>");
		}


		//message(ids);
	});
	

	$( "#updateMemberDailDialog" ).dialog({
		autoOpen : false,
		width : 400,
		modal : true,
		resizable : false,
		title: '修改页面',
		buttons: [
			{
				text: "确定",
				click: function() {


					$("#updateMemberDailFormId").submit();
					//$( this ).dialog( "close" );
				}
			},
			{
				text: "取消",
				click: function() {
					$( this ).dialog( "close" );
				}
			}
		]
	});
	
	$('#updateMemberDailFormId').ajaxForm({
		dataType: "json",
		success : function(data) {
			$('#flexiGridID').flexReload();
			$("#updateMemberDailDialog").dialog( "close" )
	     },
	     error : function() {
	    	 message("<spring:message code='common.error'/>");
	     },
		complete : function(response, status) {
	     
		}
	});
	
	

	
	
	
	$( "#addMemberDailDialog" ).dialog({
		autoOpen : false,
		width : 400,
		modal : true,
		resizable : false,
		title: "<spring:message code='common.add.pag'/>",
		buttons: [
			{
				text: "<spring:message code='common.confirm'/>",
				click: function() {					
					var memberName = $("#name");
					var memberId = $("#memberId");
					var memberTel = $("#tel");
					var bValid = true;
					var allFields = $([]).add(memberName).add(memberTel).add(memberId);
					allFields.removeClass("ui-state-error");
					allFields.removeClass("ui-state-error");
					bValid = bValid && checkLength(memberId, 1, 32);
					bValid = bValid && checkLength(memberName, 1, 32);
					bValid = bValid && checkLength(memberTel, 1, 16);
										
					if(!bValid){
						return;
					}
					
					$("#addMemberDailFormId").submit();
	
				}
			},
			{
				text: "<spring:message code='common.cancel'/>",
				click: function() {
					$( this ).dialog( "close" );
				}
			}
		]
	});
	
	
	$('#addMemberDailFormId').ajaxForm({
		dataType: "json",
		success : function(data) {
			$('#flexiGridID').flexReload();
			$("#addMemberDailDialog").dialog( "close" )
	     },
	     error : function() {
	        message("<spring:message code='common.error'/>");
	     },
		complete : function(response, status) {
	     
		}
	});
	
	$("#consumeBtn").click(function( event ) {
		var ids = searchTableColumn($("#flexiGridID"),0);
		if(ids.length == 1){
			var columnsArray = rowTableColumn($("#flexiGridID"),0);
			//if(columnsArray != null){
				$("#consumeDailFormId").resetForm();
				$("#ed_id").val(columnsArray[0]);
				$("#sp_member_id").text(columnsArray[1]);
				$("#sp_member_name").text(columnsArray[2]);				
				$("#consumeDailDialog").dialog( "open" );
			//}
		}else{
			message("<spring:message code='common.empty'/>");
		}
	});
	
	$( "#consumeDailDialog" ).dialog({
		autoOpen : false,
		width : 400,
		modal : true,
		resizable : false,
		title: "<spring:message code='member.consume'/>",
		buttons: [
			{
				text: "<spring:message code='common.confirm'/>",
				click: function() {
					var amount = $("#amount");
					var bValid = true;
					var allFields = $([]).add(amount);
					allFields.removeClass("ui-state-error");
					bValid = bValid && checkLength(amount, 1, 16);
										
					if(!bValid){
						return;
					}
					
					$("#consumeDailFormId").submit();
				}
			},
			{
				text: "<spring:message code='common.cancel'/>",
				click: function() {
					$( this ).dialog( "close" );
				}
			}
		]
	});
	
	$('#consumeDailFormId').ajaxForm({
		dataType: "json",
		success : function(data) {
			$('#flexiGridID').flexReload();
			$("#consumeDailDialog").dialog( "close" );
	     },
	     error : function() {
	        message("<spring:message code='common.error'/>");
	     },
		complete : function(response, status) {
	     
		}
	});
	
	$("#integralBtn").click(function( event ) {
		var ids = searchTableColumn($("#flexiGridID"),0);
		if(ids.length == 1){
			var columnsArray = rowTableColumn($("#flexiGridID"),0);
			$("#integralDailFormId").resetForm();
			$("#ed_id1").val(columnsArray[0]);	
			$("#sp_member_id1").text(columnsArray[1]);
			$("#sp_member_name1").text(columnsArray[2]);
			$("#sp_integral").text(columnsArray[4]);
			
			$("#integralDailDialog").dialog( "open" );
		}else{
			message("<spring:message code='common.empty'/>");
		}
	});
	
	$( "#integralDailDialog" ).dialog({
		autoOpen : false,
		width : 400,
		modal : true,
		resizable : false,
		title: "<spring:message code='member.exchange.integral'/>",
		buttons: [
			{
				text: "<spring:message code='common.confirm'/>",
				click: function() {
					var integral = $("#integral");
					var bValid = true;
					var allFields = $([]).add(integral);
					allFields.removeClass("ui-state-error");
					bValid = bValid && checkLength(integral, 1, 16);
					
					 total = parseInt($("#sp_integral").text());
					var increment = parseInt(integral.val());
					if(increment > total){
						integral.addClass( "ui-state-error" ).focus();
						return;
					}
										
					if(!bValid){
						return;
					}
					
					$("#integralDailFormId").submit();
				}
			},
			{
				text: "<spring:message code='common.cancel'/>",
				click: function() {
					$( this ).dialog( "close" );
				}
			}
		]
	});
	
	$('#integralDailFormId').ajaxForm({
		dataType: "json",
		success : function(data) {
			$('#flexiGridID').flexReload();
			$("#integralDailDialog").dialog( "close" );
	     },
	     error : function() {
	        message("<spring:message code='common.error'/>");
	     },
		complete : function(response, status) {
	     
		}
	});

});




function query(param1){
	$('#flexiGridID').flexOptions({
		newp: 1,
		extParam: param1||[],
    	url: '${pageContext.request.contextPath}/member/getMemberlist'
    }).flexReload();
}

function toConsumeDail(memberId){
	document.location.href = "${pageContext.request.contextPath}/consume/consumeDail?memberId=" + memberId;
}

function toIntegralDail(memberId){
	document.location.href = "${pageContext.request.contextPath}/integral/integralDail?memberId=" + memberId;
}
</script>

</head>
<body>
<div style="font-size: 18px;font-weight: bold;">
  会员列表
 <hr>
</div>
<div style="padding-top: 5px;">
<label> <spring:message code='menber.list.number'/>:</label> <input type="text" id="searchId"/>
<label> <spring:message code='menber.list.name'/>:</label> <input type="text" id="searchName"/>
<label> <spring:message code='menber.list.tel'/>:</label> <input type="text" id="searchTel"/>

<a href="#" id="queryBtn" class="ui-state-default ui-corner-all cbe-button"><span class="ui-icon ui-icon-search"></span><spring:message code='common.query'/></a>
<a href="#" id="delBtn" class="ui-state-default ui-corner-all cbe-button"><span class="ui-icon ui-icon-minusthick"></span><spring:message code='common.delete'/></a>
<a href="#" id="addBtn" class="ui-state-default ui-corner-all cbe-button"><span class="ui-icon ui-icon-plus"></span><spring:message code='common.add'/></a>
</div>
<div style="padding-top: 10px;padding-bottom: 5px;">
  <a href="#" id="consumeBtn" class="ui-state-default ui-corner-all cbe-button"><span class="ui-icon ui-icon-calculator"></span><spring:message code='member.consume'/></a>
  <a href="#" id="integralBtn" class="ui-state-default ui-corner-all cbe-button"><span class="ui-icon ui-icon-heart"></span><spring:message code='member.exchange.integral'/></a>
</div>
<div class="tbl">
   <table id="flexiGridID" style="display: block;margin: 0px;"></table>
</div>
 <div id="updateMemberDailDialog" style="display: none;">
	<form id="updateMemberDailFormId" action="${pageContext.request.contextPath}/member/updateMemberDail" method="post" >
	    <input type="hidden" id="id_mod" name="id" />
	    <!-- hidden -->
         <table class="table table-hover">
			<tr>
	   	 	  	<td class="fileName">
	   	 	  		<label><spring:message code='menber.list.number'/></label>
	   	 	  	</td>
	   	 	  	<td class="fileValue" >
	   	 	  		<input type="text" id="memberId_mod" name="memberId" />
	   	 	  		<span class="mand">*</span>
	   	 	  	</td>
	   	 	 </tr>
	   	 	 
	   	 	 <tr>
	   	 	  	<td class="fileName">
	   	 	  		<label><spring:message code='menber.list.name'/></label>
	   	 	  	</td>
	   	 	  	<td class="fileValue" >
	   	 	  		<input type="text" id="name_mod" name="name" />
	   	 	  		<span class="mand">*</span>
	   	 	  	</td>
	   	 	  </tr>
	   	 	  
              <tr>
	   	 	  	<td class="fileName">
	   	 	  		<label><spring:message code='menber.list.tel'/></label>
	   	 	  	</td>
	   	 	  	<td class="fileValue" >
	   	 	  		<input type="text" id="tel_mod" name="tel" />
	   	 	  		<span class="mand">*</span>
	   	 	  	</td>
	   	 	  </tr>
	   	 	  
	   	 	  <tr>
	   	 	  	<td class="fileName">
	   	 	  		<label><spring:message code='menber.list.remarks'/></label>
	   	 	  	</td>
	   	 	  	<td class="fileValue" >
	   	 	  		<textarea cols="18" rows="5" id="remarks_mod" name="remarks"></textarea>
	   	 	  	</td>
	   	 	  </tr>
	 
	   	 </table>
	   </form>  
   </div>



    <div id="addMemberDailDialog1" style="display: none;">
  <form>
 <form class="form-inline">
  <div class="form-group">
    <label for="exampleInputName2">Name</label>
    <input type="text" class="form-control" id="exampleInputName2" placeholder="Jane Doe">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail2">Email</label>
    <input type="email" class="form-control" id="exampleInputEmail2" placeholder="jane.doe@example.com">
  </div>
  <button type="submit" class="btn btn-default">Send invitation</button>
</form>
< /div>
 <div id="addMemberDailDialog" style="display: none;">
	<form id="addMemberDailFormId" action="${pageContext.request.contextPath}/member/addMemberDail" method="post" >
	    <table class="table table-hover">
			<tr class="form-group">
	   	 	  	<td class="fileName">
	   	 	  		<label><spring:message code='menber.list.number'/></label>
	   	 	  	</td>
	   	 	  	<td class="fileValue" >
	   	 	  		<input type="text"  id="memberId" name="memberId" />
	   	 	  		<span class="mand">*</span>
	   	 	  	</td>
	   	 	 </tr>
	   	 	  <tr>
	   	 	  	<td class="fileName">
	   	 	  		<label><spring:message code='menber.list.name'/></label>
	   	 	  	</td>
	   	 	  	<td class="fileValue" >
	   	 	  		<input type="text" id="name" name="name" />
	   	 	  		<span class="mand">*</span>
	   	 	  	</td>
	   	 	  </tr>
	   	 	  
              <tr>
	   	 	  	<td class="fileName">
	   	 	  		<label><spring:message code='menber.list.tel'/></label>
	   	 	  	</td>
	   	 	  	<td class="fileValue" >
	   	 	  		<input type="text" id="tel" name="tel" />
	   	 	  		<span class="mand">*</span>
	   	 	  	</td>
	   	 	  </tr>
	   	 	  
	   	 	  <tr>
	   	 	  	<td class="fileName">
	   	 	  		<label><spring:message code='menber.list.remarks'/></label>
	   	 	  	</td>
	   	 	  	<td class="fileValue" >
	   	 	  		<textarea cols="18" rows="5" id="remarks" name="remarks"></textarea>
	   	 	  	</td>
	   	 	  </tr>
	   	 </table>
	   </form>  
   </div>
   
  <div id="consumeDailDialog" style="display: none;">
	<form id="consumeDailFormId" action="${pageContext.request.contextPath}/consume/consumeMoney" method="post" >
	    <table class="table table-hover">
	   	 	  <tr>
	   	 	  	<td class="fileName">
	   	 	  		<label><spring:message code='menber.list.number'/></label>
	   	 	  	</td>
	   	 	  	<td class="fileValue" >
	   	 	  		<input type="hidden" id="ed_id" name="memberId" />
	   	 	  		<span id="sp_member_id"></span>
	   	 	  	</td>
	   	 	  </tr>
	   	 	  <tr>
	   	 	  	<td class="fileName">
	   	 	  		<label><spring:message code='menber.list.name'/></label>
	   	 	  	</td>
	   	 	  	<td class="fileValue" >
	   	 	  		<span id="sp_member_name"></span>
	   	 	  	</td>
	   	 	  </tr>
	   	 	  
              <tr>
	   	 	  	<td class="fileName">
	   	 	  		<label><spring:message code='member.consume.account'/></label>
	   	 	  	</td>
	   	 	  	<td class="fileValue" >
	   	 	  		<input type="number" id="amount" name="amount" />
	   	 	  		<span class="mand">*</span>
	   	 	  	</td>
	   	 	  </tr>
	   	 </table>
	   </form>  
   </div>
   
<div id="integralDailDialog" style="display: none;">
	<form id="integralDailFormId" action="${pageContext.request.contextPath}/integral/exchangeIntegral" method="post" >
	    <table class="table table-hover">
	   	 	  <tr >
	   	 	  	<td class="fileName">
	   	 	  		<label><spring:message code='menber.list.number'/></label>
	   	 	  	</td>
	   	 	  	<td class="fileValue" >
	   	 	  		<input type="hidden" id="ed_id1" name="memberId" />
	   	 	  		<span id="sp_member_id1"></span>
	   	 	  	</td>
	   	 	  </tr>
	   	 	  <tr>
	   	 	  	<td class="fileName">
	   	 	  		<label><spring:message code='menber.list.name'/></label>
	   	 	  	</td>
	   	 	  	<td class="fileValue" >
	   	 	  		<span id="sp_member_name1"></span>
	   	 	  	</td>
	   	 	  </tr>
	   	 	  
		   	  <tr>
	   	 	  	<td class="fileName">
	   	 	  		<label><spring:message code='menber.list.integral'/></label>
	   	 	  	</td>
	   	 	  	<td class="fileValue" >
	   	 	  		<span id="sp_integral"></span>
	   	 	  	</td>
	   	 	  </tr>
	   	 	  
              <tr>
	   	 	  	<td class="fileName">
	   	 	  		<label><spring:message code='member.exchange.integral'/></label>
	   	 	  	</td>
	   	 	  	<td class="fileValue" >
	   	 	  		<input type="number" id="integral" name="integral" />
	   	 	  		<span class="mand">*</span>
	   	 	  	</td>
	   	 	  </tr>
	   	 </table>
	   </form>  
   </div>
< /body>
< /html >