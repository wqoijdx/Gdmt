<%@include file="../common/common.jsp" %>
<html>
<head>
    <title>
        <spring:message code="disk.title.tip"></spring:message>
    </title>
    <script type="text/javascript">
        var _gridWidth;
        var _gridHeight;
        var _columnWidth;

        //页面自适应
        function resizePageSize() {
            _gridWidth = $(document).width() - 12;/*  -189 是去掉左侧 菜单的宽度，   -12 是防止浏览器缩小页面 出现滚动条 恢复页面时  折行的问题 */
            _gridHeight = $(document).height() - 32 - 80; /* -32 顶部主菜单高度，   -90 查询条件高度*/
        }

        $(function () {
            resizePageSize();
            $("#sysnch-btn-id").click(function (event) {
                $.ajax({
                    type: 'POST',
                    url: '${pageContext.request.contextPath}/disk/sysnchObjData',
                    dataType: 'json',
                    cache: false,
                    data: [],
                    success: function (data) {
                        message(data.msg);
                    },
                    error: function () {
                        message('<spring:message code="common.error"></spring:message>');
                    }
                });

            });

            _columnWidth = _gridWidth / 6;
            $("#flexigrid-id").flexigrid({
                width: _gridWidth,
                height: _gridHeight,
                url: "${pageContext.request.contextPath}/disk/queryFileListPaging",
                dataType: 'json',
                colModel: [
                    {
                        display: 'fileUuid',
                        name: 'fileUuid',
                        width: _columnWidth,
                        sortable: true,
                        align: 'center',
                        hide: 'true'
                    },
                    {
                        display: '<spring:message code="disk.file.name"></spring:message>',
                        name: 'fileName',
                        width: _columnWidth,
                        sortable: true,
                        align: 'center', process: function (v, _trid, _row) {
                            var postDetail = '<span style="cursor:pointer;" onclick="preFile(\'' + _row.filePath + '\'';
                            postDetail = postDetail + ');">' + v + '</span>';
                            return postDetail;
                        }
                    },
                    {
                        display: '<spring:message code="disk.parent.uuid"></spring:message>',
                        name: 'parentUuid',
                        width: _columnWidth,
                        sortable: true,
                        align: 'center'
                    },
                    {
                        display: '<spring:message code="disk.file.type"></spring:message>',
                        name: 'fileType',
                        width: _columnWidth,
                        sortable: true,
                        align: 'center',
                        process: function (v, _trid, _row) {
                            return $("#seach-file-type-id").find("option[value=" + v + "]").text();
                        }
                    },
                    {
                        display: '<spring:message code="disk.file.capacity"></spring:message>',
                        name: 'fileCapacity',
                        width: _columnWidth,
                        sortable: true,
                        align: 'center'
                    },
                    {
                        display: '<spring:message code="disk.file.modtime"></spring:message>',
                        name: 'modtime',
                        width: _columnWidth,
                        sortable: true,
                        align: 'center'
                    },
                    {
                        display: '<spring:message code="file.operation"></spring:message>',
                        name: 'fileUuid',
                        width: _columnWidth,
                        sortable: false,
                        align: 'center',
                        process: function (v, _trid, _row) {
                            if (_row.fileType === 1) {
                                return '';
                            }
                            var postDetail = '<span style="cursor:pointer;" onclick="downloadFile(\'' + v + '\'';
                            postDetail = postDetail + ');"<i class="glyphicon glyphicon-arrow-down"></i></span>';

                            postDetail = postDetail + '<span style="cursor:pointer;" onclick="shareFile(\'' + v + '\'';
                            postDetail = postDetail + ');"<i class="glyphicon glyphicon-new-window"></i></span>';

                            postDetail = postDetail + '<span style="cursor:pointer;" onclick="deleteFile(\'' + v + '\'';
                            postDetail = postDetail + ');"<i class="glyphicon glyphicon-trash"></i></span>';

                            postDetail +=  '<a style="color:#606674;cursor:pointer;margin-right:15px;" onclick="copyObject(\'' + _row.fileName + '\',\''+_row.fileUuid+'\');"><i class="glyphicon glyphicon-duplicate"></i></a>';
                            return postDetail;
                        }

                    },

                ],
                resizable: false, //resizable table是否可伸缩
                useRp: true,
                usepager: true, //是否分页
                autoload: false, //自动加载，即第一次发起ajax请求
                hideOnSubmit: true, //是否在回调时显示遮盖
                showcheckbox: true, //是否显示多选框
                rowhandler: rowDbclick, //是否启用行的扩展事情功能,在生成行时绑定事件，如双击，右键等
                rowbinddata: true,
                numCheckBoxTitle: "<spring:message code='common.selectall'/>",
                extParam: [],
                newp: 1
            });
            $("#flexigrid-id").flexReload();

            $("#query-btn-id").click(function (event) {
                var searchFileTypeVl = $("#seach-file-type-id").val();
                var searchFileNameVl = $("#search-file-name-id").val();

                $("#flexigrid-id").flexOptions({
                    extParam: [
                        {name: "seachFileType", value: searchFileTypeVl},
                        {name: "searchFileName", value: searchFileNameVl}
                    ],
                }).flexReload();

            });

            $("#file-upload-dialog-id").dialog({
                autoOpen: false,
                width: 400,
                modal: true,
                resizable: true,
                title: '<spring:message code="disk.file.upload"></spring:message>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function () {
                            var allFields = $([]).add($("#file-name-id"));
                            if (verification(allFields)) {
                                $("#file-upload-form-id").submit();
                            }
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                ]
            });

            $("#upload-btn-id").click(function (event) {
                event.stopPropagation();
                $("#file-upload-form-id").resetForm();

                $("#file-upload-dialog-id").dialog('open');
            });

            $('#file-upload-form-id').ajaxForm({
                dataType: "json",
                success: function (data) {
                    $("#file-upload-dialog-id").dialog('close');
                    $("#flexigrid-id").flexReload();
                    message(data.msg)
                },
                error: function () {
                    message('<spring:message code="common.error"></spring:message>');
                },
                complete: function (response, status) {

                }
            });

            $("#file-share-dialog-id").dialog({
                autoOpen: false,
                width: 400,
                modal: true,
                resizable: true,
                title: '<spring:message code="disk.file.share"></spring:message>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function () {
                            var allFields = $([]).add($("#share-start-date-id")).add($("#share-end-date-id"));
                            if (verification(allFields)) {
                                $("#file-share-form-id").submit();
                            }
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                ]

            });
            $('#file-share-form-id').ajaxForm({
                dataType: "json",
                success: function (data) {
                    $("#file-share-dialog-id").dialog('close');
                    $("#show-share-url-contents-id").text(data.msg);
                    $("#show-share-url-qr-code-id").attr("src", "${pageContext.request.contextPath}/disk/shareQRCode.svg?shareCode=" + data.msg)
                    $("#show-share-url-dialog-id").dialog($("#show-share-url-dialog-id")).dialog('open');
                },
                error: function () {
                    message('<spring:message code="common.error"></spring:message>');
                },
                complete: function (response, status) {

                }
            });

            $("#show-share-url-dialog-id").dialog({
                autoOpen: false,
                width: 400,
                modal: true,
                resizable: true,
                title: '<spring:message code="disk.file.share"></spring:message>',
                buttons: [
                    {
                        text: '<spring:message code="disk.file.copy"></spring:message>',
                        click: function () {
                            copyContents($("#show-share-url-contents-id").text());
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                ]
            });


            $("#pre-file-type-doc-dialog-id").dialog({
                autoOpen: false,
                width: 800,
                modal: true,
                resizable: true,
                title: '<spring:message code="disk.file.name"></spring:message>',
                buttons: [
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                ]
            });

            $("#pre-file-type-docx-dialog-id").dialog({
                autoOpen: false,
                width: 800,
                modal: true,
                resizable: true,
                title: '<spring:message code="disk.file.name"></spring:message>',
                buttons: [
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                ]
            });

            $("#pre-file-type-audio-dialog-id").dialog({
                autoOpen: false,
                width: 300,
                modal: true,
                resizable: true,
                title: '<spring:message code="disk.file.name"></spring:message>',
                buttons: [
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function () {
                            $("#pre-file-type-audio-control-id")[0].pause();
                            $(this).dialog('close');
                        }
                    }
                ]
            });

            $("#pre-file-type-txt-dialog-id").dialog({
                autoOpen: false,
                width: 800,
                modal: true,
                resizable: true,
                title: '<spring:message code="disk.file.name"></spring:message>',
                buttons: [
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                ]
            });

            $("#pre-file-type-jpg-dialog-id").dialog({
                autoOpen: false,
                width: 1000,
                modal: true,
                resizable: true,
                title: '<spring:message code="disk.file.name"></spring:message>',
                buttons: [
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                ]
            });

            $("#pre-file-type-png-dialog-id").dialog({
                autoOpen: false,
                width: 1000,
                modal: true,
                resizable: true,
                title: '<spring:message code="disk.file.name"></spring:message>',
                buttons: [
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                ]
            });

            $("#pre-file-type-mp4-dialog-id").dialog({
                autoOpen: false,
                width: 500,
                modal: true,
                resizable: true,
                title: '<spring:message code="disk.file.name"></spring:message>',
                buttons: [
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function () {
                            $("#pre-file-type-mp4-dialog-id").dialog('open').parent()
                                .css("width",_gridWidth + "px")
                                .css("left",0);
                            $("#pre-file-type-mp4-dialog-id").parent().find("a.ui-dialog-titlebar-close").click(function () {
                                $("#pre-file-type-mp4-control-id")[0].pause();
                            });

                            $(this).dialog('close');
                        }
                    }
                ]
            });

            $("#mod-file-name-dialog-id").dialog({
                autoOpen: false,
                width: 400,
                modal: true,
                resizable: true,
                title: '<spring:message code="disk.file.name"></spring:message>',
                buttons: [
                    {
                        text: '<spring:message code="common.confirm"></spring:message>',
                        click: function () {
                            var allFields = $([]).add($("#mod-file-name-id"));
                            if (verification(allFields)) {
                                $("#mod-file-name-form-id").submit();
                            }
                        }
                    },
                    {
                        text: '<spring:message code="common.cancel"></spring:message>',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                ]
            });
            $("#mod-file-name-form-id").ajaxForm({
                dataType: "json",
                success: function (data) {
                    $("#mod-file-name-dialog-id").dialog('close');
                    message(data.msg);
                    $("#flexigrid-id").flexReload();

                }
            });
        });


        function downloadFile(fileUuid) {
            location.href = "${pageContext.request.contextPath}/disk/downloadFile/" + fileUuid;
        }

        function shareFile(fileUuid) {
            $("#share-file-uuid-id").val(fileUuid);
            $("#file-share-dialog-id").dialog('open');
        }

        function deleteFile(fileUuid) {
            $.ajax({
                type: 'POST',
                url: '${pageContext.request.contextPath}/disk/deleteFileByid',
                dataType: 'json',
                cache: false,
                data: [{name: "fileUuid", value: fileUuid}],
                success: function (data) {
                    message(data.msg);
                    $("#flexigrid-id").flexReload();
                },
                error: function () {
                    message("<spring:message code='common.error'/>");
                }
            });
        }

        function rowDbclick(r) {
            $(r).dblclick(
                function () {
                    var columnsArray = $(r).attr('ch').split("_FG$SP_");
                    id = columnsArray[0];
                    $.ajax({
                        type: 'POST',
                        url: '${pageContext.request.contextPath}/disk/queryClassInfoById',
                        dataType: 'json',
                        cache: false,
                        data: [{name: "fileUuId", value: id}],
                        success: function (data) {
                            console.log(data);
                            $("#mod-file-uuid-id").val(data.fileUuid);
                            $("#mod-file-name-id").val(data.fileName);
                            $("#mod-file-name-dialog-id").dialog('open');
                        },
                        error: function () {
                            message("<spring:message code='common.error'/>");
                        }
                    });
                });
        }


        function preFile(filePath) {
            // console.info(filePath);
            var docRep = /doc$/i;
            var docxRep = /docx$/i;
            var mp3Rep = /mp3$/i;
            var txtRep = /txt$/i;
            var jpgRep = /jpg$/i;
            var pngRep = /png$/i;
            var mp4Rep = /mp4$/i;
            if (docRep.test(filePath)) {
                // console.info("docRep");
                $.ajax({
                    type: 'POST',
                    url: '${pageContext.request.contextPath}/disk/getLinkCopy',
                    dataType: 'json',
                    cache: false,
                    data: [{name: "obsKey", value: filePath}],
                    success: function (data) {
                        // 同步浏览文件名
                        $("#pre-file-type-doc-dialog-id").dialog('option','title', filePath);
                        $("#preDocSrc-file-data-id").attr("src", data.msg);
                        $("#pre-file-type-doc-dialog-id").dialog('open');
                    },
                    error: function () {
                        message('<spring:message code="common.error"></spring:message>');
                    }
                });
            }

            if (docxRep.test(filePath)) {
                $.ajax({
                    type: 'POST',
                    url: '${pageContext.request.contextPath}/disk/getLinkCopy',
                    dataType: 'json',
                    cache: false,
                    data: [{name: "obsKey", value: filePath}],
                    success: function (data) {
                        // 同步浏览文件名
                        $("#pre-file-type-docx-dialog-id").dialog('option','title', filePath);
                        $("#preDocxSrc-file-data-id").attr("src", data.msg);
                        $("#pre-file-type-docx-dialog-id").dialog('open');
                    },
                    error: function () {
                        message('<spring:message code="common.error"></spring:message>');
                    }
                });
            }

            if (mp3Rep.test(filePath)) {
                $.ajax({
                    type: 'POST',
                    url: '${pageContext.request.contextPath}/disk/getLinkCopy',
                    dataType: 'json',
                    cache: false,
                    data: [{name: "obsKey", value: filePath}],
                    success: function (data) {
                        // 同步浏览文件名
                        // console.info("mp3Rep");
                        $("#pre-file-type-audio-dialog-id").dialog('option', 'title',filePath);
                        $("#pre-file-type-audio-control-id").attr("src", data.msg);
                        $("#pre-file-type-audio-dialog-id").dialog('open');
                        $("#pre-file-type-audio-control-id")[0].play();
                    },
                    error: function () {
                        message('<spring:message code="common.error"></spring:message>');
                    }
                });
            }
            if (txtRep.test(filePath)) {
                // console.info("docRep");
                $.ajax({
                    type: 'POST',
                    url: '${pageContext.request.contextPath}/disk/getLinkCopy',
                    dataType: 'json',
                    cache: false,
                    data: [{name: "obsKey", value: filePath}],
                    success: function (data) {
                        // 同步浏览文件名
                        $("#pre-file-type-txt-dialog-id").dialog('option','title', filePath);
                        $("#pri-file-txt-data-id").attr("src", data.msg);
                        $("#pre-file-type-txt-dialog-id").dialog('open');
                    },
                    error: function () {
                        message('<spring:message code="common.error"></spring:message>');
                    }
                });
            }

            if (jpgRep.test(filePath)) {
                $.ajax({
                    type: 'POST',
                    url: '${pageContext.request.contextPath}/disk/getLinkCopy',
                    dataType: 'json',
                    cache: false,
                    data: [{name: "obsKey", value: filePath}],
                    success: function (data) {
                        // 同步浏览文件名
                        $("#pre-file-type-jpg-dialog-id").dialog('option','title', filePath);
                        $("#pri-file-jpg-data-id").attr("src", data.msg);
                        $("#pre-file-type-jpg-dialog-id").dialog('open');
                    },
                    error: function () {
                        message('<spring:message code="common.error"></spring:message>');
                    }
                });
            }

            if (pngRep.test(filePath)) {
                $.ajax({
                    type: 'POST',
                    url: '${pageContext.request.contextPath}/disk/getLinkCopy',
                    dataType: 'json',
                    cache: false,
                    data: [{name: "obsKey", value: filePath}],
                    success: function (data) {
                        // 同步浏览文件名
                        $("#pre-file-type-png-dialog-id").dialog('option','title', filePath);
                        $("#pri-file-png-data-id").attr("src", data.msg);
                        $("#pre-file-type-png-dialog-id").dialog('open');
                    },
                    error: function () {
                        message('<spring:message code="common.error"></spring:message>');
                    }
                });
            }

            if (mp4Rep.test(filePath)) {
                $.ajax({
                    type: 'POST',
                    url: '${pageContext.request.contextPath}/disk/getLinkCopy',
                    dataType: 'json',
                    cache: false,
                    data: [{name: "obsKey", value: filePath}],
                    success: function (data) {
                        // 同步浏览文件名
                        // console.info("mp3Rep");
                        $("#pre-file-type-mp4-dialog-id").dialog('option', 'title',filePath);
                        $("#pre-file-type-mp4-control-id").attr("src", data.msg);
                        $("#pre-file-type-mp4-dialog-id").dialog('open');
                    },
                    error: function () {
                        message('<spring:message code="common.error"></spring:message>');
                    }
                });
            }
        }

        function copyObject(objectKey,fileId) {
            objectKey=encodeURIComponent(objectKey);
            $.ajax({
                type : 'POST',
                url : '${pageContext.request.contextPath}/disk/copy/',
                dataType : 'json',
                cache : false,
                data :{fileId:fileId,fileName:objectKey},
                success : function(data) {
                    message(data.msg);
                    $("#flexigrid-id").flexReload();
                },
                error : function() {
                    message("<spring:message code='common.error'/>");
                }
            });
        }

    </script>
</head>
<body>
<spring:message code="disk.title.tip"></spring:message>


<nav class="nav-divider">
        <span>
            <spring:message code="disk.file.name"></spring:message>
        </span>
    <input type="text" id="search-file-name-id"/>
    <span>
            <spring:message code="disk.file.type"></spring:message>
        </span>
    <select id="seach-file-type-id">
        <option value="">select empty</option>
        <option value="0">file</option>
        <option value="1">dir</option>
    </select>
    <a id="query-btn-id" href="#">
        <i class="glyphicon glyphicon-zoom-in"></i>
        <span>
                <spring:message code="common.query"></spring:message>
            </span>
    </a>

    <a id="sysnch-btn-id" href="#aaa">
        <i class="glyphicon glyphicon-refresh"></i>
        <span>
                <spring:message code="disk.btn.synch"></spring:message>
            </span>
    </a>

    <a id="upload-btn-id" href="#aaa">
        <i class="glyphicon glyphicon-open"></i>
        <span>
                <spring:message code="disk.file.upload"></spring:message>
            </span>
    </a>


</nav>
<table id="flexigrid-id" style="display: block;margin: 0"></table>
<div id="file-upload-dialog-id" style="display: none">
    <form id="file-upload-form-id" method="post" action="${pageContext.request.contextPath}/disk/uploadFile"
          enctype="multipart/form-data">
        <input id="file-name-id" type="file" name="fileName" rep="^.{1,30}$"/>
    </form>
</div>

<div id="file-share-dialog-id" style="display: none">
    <form id="file-share-form-id" method="post" action="${pageContext.request.contextPath}/disk/shareFile">
        <input id="share-file-uuid-id" type="hidden" name="fileUuid"/>
        <div class="form-group">
            <span for="share-start-date-id">
                <spring:message code="disk.file.start.date"></spring:message>
            </span>
            <input class="form-control" type="date" name="startDate" id="share-start-date-id"
                   rep="^[0-9]{4}-[0-9]{2}-[0-9]{2}$">
        </div>
        <div class="form-group">
            <span for="share-end-date-id">
                <spring:message code="disk.file.end.date"></spring:message>
            </span>
            <input class="form-control" type="date" name="endDate" id="share-end-date-id"
                   rep="^[0-9]{4}-[0-9]{2}-[0-9]{2}$">
        </div>
    </form>

</div>

<div id="mod-file-name-dialog-id" style="display: none">
    <form id="mod-file-name-form-id" method="post"
          action="${pageContext.request.contextPath}/disk/modifyFileNameByFileUuid">
        <input id="mod-file-uuid-id" type="hidden" name="fileUuid"/>
        <div class="form-group">
            <span for="mod-file-name-id">
                <spring:message code="disk.file.name"></spring:message>
            </span>
            <input class="form-control" type="text" name="fileName" id="mod-file-name-id"
                   rep="^.{1,130}$">
        </div>

    </form>

</div>

<div id="show-share-url-dialog-id" style="display: none">
    <div id="show-share-url-contents-id">

    </div>
    <span>
        <spring:message code="disk.share.qrcode">

        </spring:message>
    </span>
    <img id="show-share-url-qr-code-id" src="">
</div>

<div id="pre-file-type-doc-dialog-id" style="display: none">
    <iframe id="preDocSrc-file-data-id" src="" width="100%" height="100%" frameborder="1">
    </iframe>
</div>

<div id="pre-file-type-docx-dialog-id" style="display: none">
    <iframe id="preDocxSrc-file-data-id" src="" width="100%" height="100%" frameborder="1">
    </iframe>
</div>

<div id="pre-file-type-audio-dialog-id" style="display: none">
    <audio id="pre-file-type-audio-control-id" controls>
        <source id="pri-file-audio-data-id" src="" type="audio/mpeg">
    </audio>
</div>

<div id="pre-file-type-txt-dialog-id">
    <iframe id="pri-file-txt-data-id" src="" width="100%" height="100%" frameborder="1">
    </iframe>
</div>

<div id="pre-file-type-jpg-dialog-id">
    <img id="pri-file-jpg-data-id" src="" width="100%" height="100%">
</div>

<div id="pre-file-type-png-dialog-id">
    <img id="pri-file-png-data-id" src="" width="100%" height="100%">
</div>

<div id="pre-file-type-mp4-dialog-id">
    <video  id="pre-file-type-mp4-control-id" width="320" height="240" controls="controls" src="">
        <source id="pre-file-mp4-data-id">
    </video>

</div>

</body>
</html>
