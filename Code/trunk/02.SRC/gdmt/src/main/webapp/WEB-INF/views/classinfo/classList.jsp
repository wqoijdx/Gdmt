<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/page.css">

<title> <spring:message code="class.page.title"/></title>

<script type="text/javascript">
var _gridWidth;
var _gridHeight;
//页面自适应
function resizePageSize(){
	_gridWidth = $(document).width()-12;/*  -189 是去掉左侧 菜单的宽度，   -12 是防止浏览器缩小页面 出现滚动条 恢复页面时  折行的问题 */
	_gridHeight = $(document).height()-32-80; /* -32 顶部主菜单高度，   -90 查询条件高度*/
}
//ztree回调事件
function onClickNode(event, treeId, treeNode){
	console.info(treeNode.id);
	$("#classInfoId").val(treeNode.id);
	query([{name:"parentId",value:treeNode.id}]);
	queryClassInfoById(treeNode.id);
}
//根据班级ID查询班级信息
function queryClassInfoById(classId){
	$.ajax({
		type : 'POST',
		url : '${pageContext.request.contextPath}/classinfo/queryClassInfoById',
		dataType : 'json',
		cache : false,
		data : [{name:"classId",value:classId}],
		success : function(data) {
			if(data.length > 0){
				$("#modify-class-page-id").val(data[0].classId);
				$("#modify-class-page-name-id").val(data[0].className);
				$("#modify-class-parent-page-id").val(data[0].parentId);
			}
		},
		error : function() {
			message("<spring:message code='common.error'/>");
		}
	});

}

//read方法
$(function()
{
	resizePageSize();
	//重新加载ztree
	reloadZtree();
	//计算列的宽度
	var _columnWidth= (_gridWidth-150)/3;
	//初动表格
	$("#flexiGridID").flexigrid({
		width : _gridWidth,
		height : _gridHeight,
		url : "${pageContext.request.contextPath}/classinfo/getClassInfolist",
		dataType : 'json',
		colModel : [
			{display : 'classID',name : 'classId',width : 150,sortable : false,align : 'center',hide : 'true'},
			{display : "<spring:message code='class.name'/>",name : 'className',width : _columnWidth, sortable : true,align : 'center'},
			{display : "<spring:message code='class.parent.id'/>",name : 'parentId',width : _columnWidth, sortable : true,align : 'center'},
			{display : "<spring:message code='class.create.time'/>",name : 'createTime',width : _columnWidth, sortable : true,align : 'center'}
		],
		resizable : false, //resizable table是否可伸缩
		usepager : true,
		useRp : true,
		usepager : true, //是否分页
		autoload : false, //自动加载，即第一次发起ajax请求
		hideOnSubmit : true, //是否在回调时显示遮盖
		showcheckbox : true, //是否显示多选框
		rowhandler : rowDbclick, //是否启用行的扩展事情功能,在生成行时绑定事件，如双击，右键等
		rowbinddata : true,
		numCheckBoxTitle : "<spring:message code='common.selectall'/>"
	});
	//调用查询表格
    query();
    //绑定查询按钮的双击事件
    $("#queryBtn").click(function (event) {
		var searchClassName = $("#searchClassName").val();
		var searchStartTime = $("#searchStartTime").val();
		var searchEndTime = $("#searchEndTime").val();
		var classInfoId = $("#classInfoId").val();
		query([
			{name:"parentId",value:classInfoId},
			{name:"searchClassName",value:searchClassName},
			{name:"searchStartTime",value:searchStartTime},
			{name:"searchEndTime",value:searchEndTime}
		]);
	});
	//绑定增加按钮的双击事件
    $("#addBtn").click(function (evnent) {
		evnent.stopPropagation();
		console.info("addBtn");
		$("#addClassDialogId").dialog('open');
		$("#addClassFormId").resetForm();
	});
	// $("#addBtn").parent().click(function (evnent) {
	// 	console.info("parent");
	// });
	//初始化增加班级对话框
	$("#addClassDialogId").dialog({
		autoOpen : false,
		width : 400,
		modal : true,
		resizable : false,
		title: "<spring:message code='member.consume'/>",
		buttons: [
			{
				text: "<spring:message code='common.confirm'/>",
				click: function() {
					$("#addClassFormId").submit();
				}
			},
			{
				text: "<spring:message code='common.cancel'/>",
				click: function() {
					$( this ).dialog( "close" );
				}
			}
		]
	});
	//初始化增加班级异步表单
	$("#addClassFormId").ajaxForm({
		dataType: "json",
		success : function(data) {
			$('#flexiGridID').flexReload();
			$("#addClassDialogId").dialog( "close" );
			message(data.msg);
			reloadZtree();
		},
		error : function() {
			message("<spring:message code='common.error'/>");
		},
		complete : function(response, status) {

		}
	});
	//初化化修改对话框
	$("#modifyClassDialogId").dialog({
		autoOpen : false,
		width : 400,
		modal : true,
		resizable : false,
		title: "<spring:message code='class.modify.title'/>",
		buttons: [
			{
				text: "<spring:message code='common.confirm'/>",
				click: function() {
					$("#mofifyClassFormId").submit();
				}
			},
			{
				text: "<spring:message code='common.cancel'/>",
				click: function() {
					$( this ).dialog( "close" );
				}
			}
		]
	});
	//初化化修改班级异步表单
	$("#mofifyClassFormId").ajaxForm({
		dataType: "json",
		success : function(data) {
			$('#flexiGridID').flexReload();
			$("#modifyClassDialogId").dialog( "close" );
			message(data.msg);
			reloadZtree();
		},
		error : function() {
			message("<spring:message code='common.error'/>");
		},
		complete : function(response, status) {

		}
	});
	//绑定删除按扭双击事件
	$("#delBtn").click(function (event) {
		var ids = searchTableColumn($("#flexiGridID"),0);
		if(ids.length > 0){
			$.ajax({
				type : 'POST',
				url : '${pageContext.request.contextPath}/classinfo/deleteClassInfo',
				dataType : 'json',
				cache : false,
				data : [ {
					name : 'ids',
					value : ids
				} ],
				success : function(data) {
					$('#flexiGridID').flexReload();
					reloadZtree();
					message(data.msg);
				},
				error : function() {
					message("<spring:message code='common.error'/>");
				}
			});

		}else{

			message("<spring:message code='common.empty'/>");
		}

	});
	//绑定tab页签双击事件
	$("#class-info-tab-id > li").click(function (event) {
		$(this).parent().find(".active").removeClass("active");
		$(this).find("a").addClass("active");
		var tabId = $(this).attr("tab");
		$("#" + tabId).parent().find(".in").removeClass("in").removeClass("active");
		$("#" + tabId).addClass("in").addClass("active");
		
	});
	//设置国际化页签的宽度自适应
	$("#tab-i18n-id").css("width",_gridWidth+"px");
	//初始化绑定的对话框
	$("#boundZtreeDialogId").dialog({
		autoOpen : false,
		width : 400,
		height : 400,
		modal : true,
		resizable : false,
		title: "<spring:message code='common.bound'/>",
		buttons: [
			{
				text: "<spring:message code='common.confirm'/>",
				click: function() {
					var parentId = null;
					var treeObj = $.fn.zTree.getZTreeObj("ulZtreeBondId");
					//获取所有选中节点，不包括禁用
					var nodes = treeObj.getCheckedNodes(true);
					if(nodes && nodes.length >0){
						for(var i=0; i < nodes.length; i++){
							parentId = nodes[i].id;
						}
					}
					var classId = $("#modify-class-page-id").val();
					console.log(parentId);
					if(parentId){
						$.ajax({
							type : 'POST',
							url : '${pageContext.request.contextPath}/classinfo/bondClassParentId',
							dataType : 'json',
							cache : false,
							data : [
								{name:"classId",value:classId},
								{name:"parentId",value:parentId}
							],
							success : function(data) {
								$('#flexiGridID').flexReload();
								$("#boundZtreeDialogId").dialog( "close" );
								reloadZtree();
								message(data.msg);
							},
							error : function() {
								message("<spring:message code='common.error'/>");
							}
						});
					}else{
						message("<spring:message code='common.empty'/>");
					}
				}
			},
			{
				text: "<spring:message code='common.cancel'/>",
				click: function() {
					$( this ).dialog( "close" );
				}
			}
		]
	});
	//设置绑定ztree的样式
	var settingBond = {
		check: {
			enable: true,
			chkStyle: "radio",
			radioType: "all"
		},
		data: {
			simpleData: {
				enable: true
			}
		},
		callback : {
			onClick : onClickNode
		}
	};
	//绑定 绑定按钮的双击事件
	$("#classBoundId").click(function (event) {
		var classId = $("#modify-class-page-id").val();
		if("" === classId){
			message("<spring:message code='common.empty'/>");
			return;
		}
		console.log(classId);
		$.ajax({
			type : 'POST',
			url : '${pageContext.request.contextPath}/classinfo/queryClassInfoZtreeSelectList',
			dataType : 'json',
			cache : false,
			data : [
					{name:"classId",value:classId}
					],
			success : function(data) {
				$.fn.zTree.init($("#ulZtreeBondId"), settingBond, data);
			},
			error : function() {
				message("<spring:message code='common.error'/>");
			}
		});

		$("#boundZtreeDialogId").dialog('open');
	});
	//绑定修改按钮的双击事件
	$("#pageUpdateByn").click(function (event) {
		$("#mofifyClassFormPageId").submit();
	});
	//初始化修改班级异步表单
	$("#mofifyClassFormPageId").ajaxForm({
		dataType: "json",
		success : function(data) {
			$('#flexiGridID').flexReload();
			$("#modifyClassDialogId").dialog( "close" );
			message(data.msg);
			reloadZtree();
		},
		error : function() {
			message("<spring:message code='common.error'/>");
		},
		complete : function(response, status) {

		}
	});

});
//定义表格查询的功能
function query(param1){
    $('#flexiGridID').flexOptions({
        newp: 1,
        extParam: param1||[]
    }).flexReload();
}
//定义左侧ztree的样式
var setting = {
	check: {
		enable: false
	},
	data: {
		simpleData: {
			enable: true
		}
	},
	callback : {
		onClick : onClickNode
	}
};
//定义左侧ztree加载的功能
function reloadZtree() {
	$.ajax({
		type : 'POST',
		url : '${pageContext.request.contextPath}/classinfo/queryClassInfoZtreeList',
		dataType : 'json',
		cache : false,
		data : [],
		success : function(data) {
			$.fn.zTree.init($("#classTreeID"), setting, data);
		},
		error : function() {
			message("<spring:message code='common.error'/>");
		}
	});

}
//定义表格 按行的双击事件
function rowDbclick(r){
	$(r).dblclick(
			function() {
				var columnsArray = $(r).attr('ch').split("_FG$SP_");
				id = columnsArray[0];
				$.ajax({
					type : 'POST',
					url : '${pageContext.request.contextPath}disk/classinfo/queryClassInfoById',
					dataType : 'json',
					cache : false,
					data : [{name:"classId",value:id}],
					success : function(data) {
						console.log(data);
						if(data.length > 0){
							$("#modify-class-id").val(data[0].classId);
							$("#modify-class-name-id").val(data[0].className);
							$("#modify-class-parent-id").val(data[0].parentId);
						}

						$("#modifyClassDialogId").dialog('open');
					},
					error : function() {
						message("<spring:message code='common.error'/>");
					}
				});
			});
}

</script>
<style type="text/css">
	.navbar-nav>li>a {
		line-height: 10px;
		font-size: 14px;
		font-weight: 700;
	}
	fieldset {
		display: block;
		margin-inline-start: 2px;
		margin-inline-end: 2px;
		padding-block-start: 0.35em;
		padding-inline-start: 0.75em;
		padding-inline-end: 0.75em;
		padding-block-end: 0.625em;
		min-inline-size: min-content;
		border-width: 2px;
		border-style: groove;
		border-image: initial;
	}
	fieldset>legend{
		font-size: 10px;
	}
	.nav-tabs {
		border-bottom: 1px solid #999;
	}
	.nav-tabs > li > .active,
	.nav-tabs > li > a:FOCUS {
		border-color: #999 #999 #fff;
	}

</style>

</head>
<body style="display: flex;flex-direction: row">
    <%--    班级主键 --%>
    <input  type="hidden" id="classInfoId"/>
	<%--    班级Ztree--%>
	<div id="clasInfoTreeID" class="zTreeDemoBackground left"
		 title="<spring:message code='class.page.title'/>" align="center" style="flex-basis: 200px">
		<spring:message code="class.page.title"/>
		<ul id="classTreeID" class="ztree"></ul>
	</div>
	<%--	工具条--%>
	<div style="display: flex;margin: 0px;flex-flow: 1;flex-direction: column">
		<nav  class="navbar-collapse collapse">
			<ul class="nav navbar-nav fa-ul">
				<li>
					<a id="classBoundId" href="#" class="btn btn-default btn-sm">
						<i class="glyphicon glyphicon-arrow-down"></i>
						<span><spring:message code="common.bound"></spring:message> </span>
					</a>
				</li>
				<li>
					<a href="#" class="btn btn-default btn-sm">
						<i class="glyphicon glyphicon-education"></i>

						<span><spring:message code="common.unbound"></spring:message> </span>
					</a>
				</li>
				<li>
					<a id="pageUpdateByn" href="#" class="btn btn-default btn-sm">
						<i class="glyphicon glyphicon-education"></i>
						<span><spring:message code="common.update"></spring:message> </span>
					</a>
				</li>
				<li>
					<a href="#" class="btn btn-default btn-sm">
						<i class="glyphicon glyphicon-education"></i>
						<span><spring:message code="common.add"></spring:message> </span>
					</a>
				</li>
				<li>
					<a href="#" class="btn btn-default btn-sm">
						<i class="glyphicon glyphicon-education"></i>
						<span><spring:message code="common.delete"></spring:message> </span>
					</a>
				</li>
			</ul>
		</nav>
		<%--		修改班级--%>
		<article style="flex-flow: 1">
			<form id="mofifyClassFormPageId" action="${pageContext.request.contextPath}/classinfo/modifyClassInfoById"
				  method="post" enctype="application/x-www-form-urlencoded">
				<fieldset>
					<legend>
						<span><spring:message code="common.base" /></span>
					</legend>
					<input type="hidden" id="modify-class-page-id" name="classId"/>
					<div class="form-group">
						<label for="modify-class-page-name-id"><spring:message code="class.name"></spring:message> </label>
						<input class="form-control" type="text" name="className" id="modify-class-page-name-id"/>
					</div>
					<div class="form-group">
						<label for="modify-class-parent-page-id"><spring:message code="class.parent.id"></spring:message> </label>
						<%--				<input class="form-control" type="text" name="className" id="add-class-parent-id"/>--%>
						<g:dataselect id="modify-class-parent-page-id" classez="form-control" ref="classInfoService" name="parentId"></g:dataselect>
					</div>
				</fieldset>
			</form>

		</article>
<%--		tab页签--%>
		<ul id="class-info-tab-id" class="nav nav-tabs ">
			<li tab="tab-flex-list-id">
				<a class="active" href="#"><spring:message code="class.info.mangage"/> </a>
			</li>
			<li tab="tab-i18n-id">
				<a href="#"><spring:message code="class.info.i18n"></spring:message> </a>
			</li>
		</ul>
<%--		tab页面内容--%>
		<section class="tab-content">
			<article id="tab-flex-list-id" class="tab-pane fade in active">
				<div style="padding-top: 5px;">
					<label> <spring:message code='class.name'/>:</label> <input type="text" id="searchClassName"/>
					<label> <spring:message code='common.start.time'/>:</label> <input type="date" id="searchStartTime"/>
					<label> <spring:message code='common.end.time'/>:</label> <input type="date" id="searchEndTime"/>

					<a href="#" id="queryBtn" class="ui-state-default ui-corner-all cbe-button"><span class="ui-icon ui-icon-search"></span><spring:message code='common.query'/></a>
					<a href="#" id="delBtn" class="ui-state-default ui-corner-all cbe-button"><span class="ui-icon ui-icon-minusthick"></span><spring:message code='common.delete'/></a>
					<a href="#" id="addBtn" class="ui-state-default ui-corner-all cbe-button"><span class="ui-icon ui-icon-plus"></span><spring:message code='common.add'/></a>
				</div>
<%--				table表格--%>
				<table id="flexiGridID" style="display: block;margin: 0px;flex-flow: 1"></table>
			</article>
			<article id="tab-i18n-id" class="tab-pane fade">
				国际化列表
			</article>

		</section>

	</div>

    <div id="addClassDialogId" style="display: none">
		<form id="addClassFormId" action="${pageContext.request.contextPath}/classinfo/addClassInfo"
		method="post" enctype="application/x-www-form-urlencoded">
			<div class="form-group">
				<label for="add-class-name-id"><spring:message code="class.name"></spring:message> </label>
				<input class="form-control" type="text" name="className" id="add-class-name-id"/>
			</div>
			<div class="form-group">
				<label for="add-class-parent-id"><spring:message code="class.parent.id"></spring:message> </label>
<%--				<input class="form-control" type="text" name="className" id="add-class-parent-id"/>--%>
				<g:dataselect id="add-class-parent-id" classez="form-control" ref="classInfoService" name="parentId"></g:dataselect>
			</div>
		</form>
	</div>


	<div id="modifyClassDialogId" style="display: none">
		<form id="mofifyClassFormId" action="${pageContext.request.contextPath}/classinfo/modifyClassInfoById"
			  method="post" enctype="application/x-www-form-urlencoded">
			<input type="hidden" id="modify-class-id" name="classId"/>
			<div class="form-group">
				<label for="modify-class-name-id"><spring:message code="class.name"></spring:message> </label>
				<input class="form-control" type="text" name="className" id="modify-class-name-id"/>
			</div>
			<div class="form-group">
				<label for="modify-class-parent-id"><spring:message code="class.parent.id"></spring:message> </label>
				<%--				<input class="form-control" type="text" name="className" id="add-class-parent-id"/>--%>
				<g:dataselect id="modify-class-parent-id" classez="form-control" ref="classInfoService" name="parentId"></g:dataselect>
			</div>
		</form>
	</div>

    <div id="boundZtreeDialogId" style="display: none">
		<ul id="ulZtreeBondId"  class="ztree"> </ul>
	</div>

</body>
</html>