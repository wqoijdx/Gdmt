<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="../common/common.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>


<title> <spring:message code="user.loggin.title"/></title>
<link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/static/images/iii.jpg?v=2" />
<style type="text/css">
body {
  padding-top: 20px;
  padding-bottom: 40px;
  background-color: #eee;
}
.fa-user:before {
    padding-left: 10px;
}
.fa-eye-slash:before {
     padding-left: 10px;
}

</style>
<script type="text/javascript">
function validity(){
    var list =$(".form-group");
    var flag =true;
    list.removeClass("has-error")
    list.each(function (index, value) {
        var input =$(this).find('.form-control[regex]');
        if(input.length > 0){
            var regexStr = input.attr("regex");
            var inputVl = input.val();
            var regexp =  new RegExp(regexStr,'i');
            if(regexp.test(inputVl)){
                console.log(" is true");
            }else{
                $(this).addClass("has-error");
                input.focus();
                flag &= false;
            }
        }
    });
    var passwordId = $("#password").val();
    var confirmPasswordId = $("#password1").val();
    if(passwordId !== confirmPasswordId){
        alert("确认密码不匹配.");
        //$("#tip-error-id").text("确认密码不匹配.");
        return;
    }

    if(!flag){
        alert("校验失败");
        //$("#tip-error-id").text("校验失败");
        return;
    }
    $("#register-form").submit();

}

$(function(){
	$("#registerUser").click(function (even) {
		console.log("增加");
		$("#register-form").resetForm();
		$("#register-dialog").dialog('open');
	});

	$("#register-dialog").dialog({
		autoOpen : false,
		width : 400,
		modal : true,
		resizable : false,
		title: "<spring:message code='system.config.add'/>",
		buttons: [
			{
				text: "<spring:message code='common.confirm'/>",
				click: function() {
					var addRoleUuid = $("#register-username");
					var addRemarks = $("#register-password");
					var addRoleName = $("#register-password2");
					var bValid = true;
					//对值的校验
					// var allFields = $([]).add(addRoleUuid).add(addRemarks).add(addRoleName);
					// allFields.removeClass("ui-addRoleUuid-error");
					//allFields.removeClass("ui-state-error");
					//bValid = bValid && checkLength(addRoleUuid, 1, 255);
					bValid = bValid && checkLength(addRemarks, 1, 32);
					bValid = bValid && checkLength(addRoleName, 1, 16);
					bValid = bValid && checkLength(addRoleUuid, 1, 32);

					if(!bValid){
						return;
					}
					$("#register-form").submit();
				}
			},
			{
				text: "<spring:message code='common.cancel'/>",
				click: function() {
					$("#register-dialog").dialog('close');
				}
			}
		]
	});

	$("#register-form").ajaxForm({

		dateType: "json",
		success: function (data) {
			//$("#flexigrid-id").flexReload();
			$("#register-dialog").dialog("close");
			message("<spring:message code='common.confirm' />");
		},
		error: function () {
			message("<spring:message code='common.error' />");
		}
	});
});
//表单提交前进行的校验
function check() {		
	return true;
}
function onClickCode() {
	$("#onCode_id").attr('src',"${pageContext.request.contextPath}/captchaCode?_dc="+ (new Date()).getTime());
}
</script>
</head>
<body >
	 <div class="navbar-header">
	    
	     	<a  href="${pageContext.request.contextPath}/sm/login?siteLanguage=zh" id="change_language_zh"  style="POSITION: absolute;left: 1180px; top: 28px; Z-INDEX:4; font-size: 10px; COLOR: #307096; width:87px; height: 26px; padding-right: 0px"><spring:message code="chinese" /></a>

	     	<a   href="${pageContext.request.contextPath}/sm/login?siteLanguage=en" id="change_language_en"  style="POSITION: absolute;left: 1232px; top: 28px; Z-INDEX:4; font-size: 10px; COLOR: #307096; width:87px; height: 26px; padding-left: 0px"><spring:message code="english" />

	   </div>
<div class="heaerbox,container" >
	<div class="header_inner">
		<div class="pull-left logo">
			<div class=" pull-left"><a target="_blank">
			 
			<c:if test="${siteLanguage != 'en'}">
			  	<img src="${pageContext.request.contextPath}/static/images/logo2.png" alt=""> 
			</c:if>
			<c:if test="${siteLanguage == 'en'}">
			  	<img src="${pageContext.request.contextPath}/static/images/logo3n.png" alt=""> 
			</c:if>
			
			</a></div>
			<div class=" pull-left ml10 welcom"><spring:message  code="user.login.welcome"/></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="login-wrap"> 

    <div class="banner-bg"> 
          <img src="${pageContext.request.contextPath}/static/images/pic.png" >
    </div>
	<div class="login-form ">
		<div class="form" >
			<h2>${tip}</h2>
			
			<form action="${pageContext.request.contextPath}/sm/vldtlogon" method="post"
					onsubmint="return check()">
					
					<p class="item_1 input_border">
						<label class="control-label"><spring:message code="user.name"/>：</label>
						<input type="text"  placeholder="用户名/邮箱" class="pass-label" name="userName" />
						<span class="icon fa fa-user"></span>
					</p>
					<p class="item_1">
						<label  class="control-label"><spring:message code = 'user.pwd'/></label>
						<input type="password" class="pass-label" name="password" />
						<span class="myeye fa fa-eye-slash"> </span>
					</p>
					<c:if test="${isAuthCode}">
					<p class="item_1" style="height: 100px;">
						<label  class="control-label"><spring:message code = 'user.verification'/></label>
						<input type="text" class="pass-label" name="captcha" />
						<img id="onCode_id" style="margin: 10px;" src="${pageContext.request.contextPath}/captchaCode" onClick="onClickCode();" id="onCode_id" />
					</p>
					</c:if>
					<p class="item_1" >
						<a id="forgetUser" style="float: right;padding-right: 10px" href="javascript:;"> <!-- onclick="redirect_reset(); --><spring:message code = 'user.forgetPwd'/></a>
					    <a id="registerUser" style="float: right;padding-right: 10px" href="#"> <!-- onclick="redirect_reset(); --><spring:message code = 'user.reg'/></a>
					</p>
					
				<p class="item_1">
					<button type="submit" class="btn btn-danger"><spring:message code = 'user.login'/></button></td>
				</p>
					
			</form>
		</div>
	 </div>
 </div>

<div id="register-dialog" style="display: none">
	<form id="register-form" action="${pageContext.request.contextPath}/ptUser/registerUser" method="post">
        <span id="tip-error-id" style="color: #FF0000;border: #FF0000"> </span>
		<div class="form-group">
			<label for="register-username"><spring:message code='pt.user.userName'/></label>
			<input class="form-control" regex="^[a-z,A-Z]\w{4,9}$" type="text" name="username" id="register-username" placeholder="<spring:message code='pt.user.userName'/>"/>
		</div>
		<div class="form-group">
			<label for="register-password"><spring:message code='pt.user.password'/></label>
			<input class="form-control" regex="^\w{4,9}$" type="password" name="password" id="register-password" placeholder="<spring:message code='pt.user.password'/>"/>
		</div>
		<div class="form-group">
			<label for="register-password2"><spring:message code='pt.user.password2'/></label>
			<input class="form-control" regex="^\w{4,9}$" type="password"  id="register-password2" placeholder="<spring:message code='pt.user.password2'/>"/>
		</div>
	</form>
</div>

</body>
</html>
