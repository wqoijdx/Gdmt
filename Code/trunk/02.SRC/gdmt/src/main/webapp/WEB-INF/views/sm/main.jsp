<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/static/images/favicon.ico?v=2" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/menu.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/menu.js"></script>
	<title><spring:message code="common.system.title"></spring:message></title>
	<style type="text/css">
		body {
			padding-bottom: 40px;
			background-color: #eee;
		}
		.btns{
			width: 70px;height: 40px;border: 1px solid #85B5D9; border-radius: 5px; line-height: 40px; text-align: center; font-size: 1.2rem; color:#2E6E9E;cursor: pointer;float: right;margin-right:18px;background-color: #EAF5FE;
		}
		.btns:hover{
			color:#40739e;
			border:1px solid #2980b9;
		}
		.ui-widget-content{
			border: 0px
		}
	</style>
	<script type="text/javascript" >
        var _gridWidth;
        var _gridWidth;
        var _left_menu_Width;
        var _gridWidth;
        //页面自适应
        function resizePageSize(){
            _gridWidth = $(document).width();/*  -189 是去掉左侧 菜单的宽度，   -12 是防止浏览器缩小页面 出现滚动条 恢复页面时  折行的问题 */
            _gridHeight = $(document).height() - 100; /* -32 顶部主菜单高度，   -90 查询条件高度*/

            _mainFrame_whidth= _gridWidth/7*6.9;
            $(".heaerbox").css("width",_gridWidth/7*6.7 + 20 + "px");
            $("#mainFrame").css("width",_mainFrame_whidth + "px");
            //$(".ui-menu").css("width",_gridWidth/7*6.7 - 20  + "px");
            //$(".ui-menu li ul").css("width","150px");

            //$(".menu").css("width",_left_menu_Width+ "px");
            $(".dl-second-slib").css("left",_left_menu_Width+ "px");
            $("#mainFrame").css("height",_gridHeight/1.05 -5+ "px");
            $(".menu").css("height",_gridHeight/1.05+ "px");
        }

        $(function(){
            //$( "#menu" ).menu({position: {at: "left bottom"}});
            resizePageSize();
            $(".dl-second-slib").bind("click",function(){
                if(parseFloat($(".menu").css("width"))>19){
                    $(".menu").css("width",0);
                    $(".menu").hide();
                    $(".dl-second-slib").css("left","0").css("background","url('${pageContext.request.contextPath}/static/images/left-slib.gif') no-repeat -6px center transparent");
                }else{
                    $(".menu").show();
                    //$(".menu").css("width",_left_menu_Width);
                    $(".dl-second-slib").css("left",_left_menu_Width).css("background","url('${pageContext.request.contextPath}/static/images/left-slib.gif') no-repeat 0px center transparent");
                }
            });

            $("#logouId").click(function( event ) {
                location.href="${pageContext.request.contextPath}/logout";

            });

            $(".topLink").mouseenter(function(){
                $(".topLinkGo").css("display","block");
                $(this).css("position","absolute");
                $(this).css("margin-left","-90px");
            }).mouseleave(function(){
                $(".topLinkGo").css("display","none");
                $(this).css("position","absolute");
                $(this).css("margin-left","-90px");
            });

            $("#updatepwdId").click(function (event) {
                {
                    $("#updatepwForm").resetForm();
                    $("#updatepwDialog").dialog("open");
                }
            });
            $("#updatepwDialog").dialog({
                autoOpen: false,
                width: 400,
                modal: true,
                resizable: false,
                title: '<spring:message code='user.passwd.modify'/>',
                buttons: [
                    {
                        text: "<spring:message code='common.confirm'/>",
                        click: function () {
                            var oldPassword = $("#oldPassword");
                            var resetPassword = $("#resetPassword");
                            var password = $("#password1");
                            var bValid = true;
                            var allFields = $([]).add(oldPassword).add(resetPassword).add(password);
                            allFields.removeClass("ui-state-error");
                            bValid = bValid && checkLength(oldPassword, 1, 16);
                            bValid = bValid && checkLength(resetPassword, 1, 16);
                            bValid = bValid && checkLength(password, 1, 16);
                            if(resetPassword.val() !== password.val() ){
                                resetPassword.addClass( "ui-state-error" ).focus();
                                return;
                            }

                            if(!bValid){
                                return;
                            }
                            $("#updatepwForm").submit();
                            $(this).dialog("close");
                        }
                    },
                    {
                        text: "<spring:message code='common.cancel'/>",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ]
            });
            $('#updatepwForm').ajaxForm({
                dataType: "json",
                success: function (data) {
                    message(data.msg);
                    $("#updatepwDialog").dialog("close");
                },
                error: function () {
                    message("<spring:message code='common.error'/>");
                },
                complete: function (response, status) {
                }
            });
            $("#updateFace").click(function(){
                $("#updateUser").css("display","block");
                var queryInfo = $(this).attr("data-id");
                $.ajax({
					type: 'post',
					async: false,
					dataType : 'json',
					url: '${pageContext.request.contextPath}/user/getUser',
					data: {"queryId": queryInfo},
					success: function (data) {
						$("#user_name").val(data.user_name);
						$("#email").val(data.email);
						$("#mobile").val(data.mobile);
						$("#nice_name").val(data.nice_name);
						$("#sign").val(data.sign);
					},
					error: function(msg){
						console.log(msg);
					}
				});
            });
            $("#cancel_btn").click(function(){
                $("#updateUser").css("display","none");
            });
            $("#update_btn").click(function(){
                $("#updateForm").submit();
                $("#updateUser").css("display","none");
            });
            //$( "#menu" ).menu({position: {at: "left bottom"}});
			//$(".nav-tabs > li:first").click();

        });

        function menuShowPage(url){

            if(url != null){
                $("#mainFrame").attr("src",url);
            }

        }
		function clickSysMenu(menuId) {
			$.ajax({
				type: 'post',
				async: false,
				dataType : 'json',
				url: '${pageContext.request.contextPath}/sm/getServiceMenu',
				data: {"menuId": menuId},
				success: function (data) {
					console.log(data);
					//sm-service-menu
					var menuList = "";
					$(data).each(function (index,vl) {
						menuList = menuList +  "<li><a href='#'>";
						if(vl.menuIcon !== ""){
							menuList = menuList +  "<i class='"+  vl.menuIcon +"'></i>";
						}
						menuList = menuList+ "<span>" + vl.menuName + "</span>";
						menuList = menuList + "</a>";
						if(vl.children){
							menuList = menuList +  "<ul class='sm-page-menu'>";
							$(vl.children).each(function (index_s,vl_s) {
								menuList = menuList +  "<li><a href='#' onclick=\"menuShowPage('${pageContext.request.contextPath}/" + vl_s.menuUrl+ "')\">";
								if(vl_s.menuIcon !== ""){
									menuList = menuList +  "<i class='"+  vl_s.menuIcon +"'></i>";
								}
								menuList = menuList+ "<span>" + vl_s.menuName + "</span>";
								menuList = menuList + "</a>";
								menuList = menuList + "</li>";
							});
							menuList = menuList +  "</ul>";
						}
						menuList = menuList + "</li>";
					});
					$(".sm-service-menu").html(menuList);
					console.log(menuList);
				},
				error: function(msg){
					console.log(msg);
				}
			})

		}

	</script>
</head>
<body style="display: flex;flex-direction: column">
<div class="heaerbox container" style="flex-basis: 100px;">
	<div class="header_inner">
		<div class="pull-left logo">
			<div class=" pull-left">
				<c:if test="${siteLanguage != 'en'}">
					<img src="${pageContext.request.contextPath}/static/images/logo2.png" alt="">
				</c:if>
				<c:if test="${siteLanguage == 'en'}">
					<img src="${pageContext.request.contextPath}/static/images/logo3n.png" alt="">
				</c:if>

			</div>

		</div>
	</div>

	<div style="float: right;color: black;padding-right: 100px;cursor: pointer;">
		<ul  class="topLink" style="position: absolute; margin-left: -90px;list-style: none;">
			<li>
				<a href="#" >
					${userInfo.niceName}
				</a>
				<span class="ui-icon ui-icon-triangle-1-s" style="float: right;"></span>
			</li>
			<li class="topLinkGo" style="display: none; background-color: rgb(255, 255, 255);">
				<a id="updateFace" data-id="${userInfo.niceName}"><spring:message code='user.information.modify'/></a>
			</li>
			<li class="topLinkGo" style="display: none; background-color: rgb(255, 255, 255);">
				<a id="updatepwdId"><spring:message code='user.passwd.modify'/></a>
			</li>
			<li class="topLinkGo"  style="display: none; background-color: rgb(255, 255, 255);">
				<a id="logouId"><spring:message code='user.logout'/></a>
			</li>
		</ul>
	</div>
</div>
<div class="sm-sys-menu">
 <ul class="nav nav-tabs">
	<c:forEach var="row1" items="${level}">
		<li>
			<a href="#" onclick="clickSysMenu(${row1.menuId})">
			<c:if test="${not empty row1.menuIcon}">
				<i class="${row1.menuIcon}"></i>
			</c:if>
			<c:out value="${row1.menuName}"/></a>
		</li>
	</c:forEach>
</div>

<div style="background-color: #f5f5f5;display: flex;flex-direction: row;flex-grow: 1">
		<ul class="sm-service-menu">

		</ul>
	<iframe id="mainFrame" name="iframe" style="flex-grow:1;padding-left:20px;padding-top: 5px;" frameborder="no" border="0" marginwidth="0" marginheight="0" src="${pageContext.request.contextPath}/disk/initPage">
	</iframe>
</div>

<div id="updatepwDialog" style="display: none;">
	<form id="updatepwForm" action="${pageContext.request.contextPath}/updatePwd" method="post" >
		<input type="hidden" name="userName" value="${userInfo.niceName}"/>
		<table>
			<tr>
				<td class="fileName"><label> <spring:message code='user.passwd.old'/></label></td>
				<td class="fileValue">
					<input type="password" id="oldPassword" name="oldPassword"/>
					<span class="mand">*</span></td>
			</tr>
			<tr>
				<td class="fileName"><label> <spring:message code='user.passwd.new'/></label></td>
				<td class="fileValue">
					<input type="password"id="resetPassword" name="password"/>
					<span class="mand">*</span></td>
			</tr>
			<tr>
				<td class="fileName"><label><spring:message code='user.passwd.confirm'/></label></td>
				<td class="fileValue"><input type="password" id="password1"/>
					<span class="mand">*</span></td>
			</tr>
		</table>
	</form>
</div>

</body>
</html>
