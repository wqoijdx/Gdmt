function verification(fields) {
    var bValid = true;
    fields.removeClass("ui-state-error");
    fields.each(function (index,item) {
        var obj = $(item);
        var repAttr =  obj.attr("rep");
        if(repAttr){
            var rep = new RegExp(repAttr,"i");
            if(!rep.test(obj.val())){
                obj.addClass("ui-state-error");
                obj.focus();
                bValid = false;
            }
        }
    });
    return  bValid;
}