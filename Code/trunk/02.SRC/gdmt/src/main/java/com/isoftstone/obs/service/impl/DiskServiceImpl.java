package com.isoftstone.obs.service.impl;


import com.isoftstone.obs.entity.FileTypeNameEntity;
import com.isoftstone.obs.entity.SearchFileEntity;
import com.isoftstone.obs.entity.ShareFileEntity;
import com.isoftstone.obs.repository.DiskDao;
import com.isoftstone.obs.service.DiskService;
import com.isoftstone.platform.common.uitl.DataUtil;
import com.isoftstone.platform.common.uitl.DateUtil;
import com.isoftstone.platform.common.uitl.QRCodeUtil;
import com.isoftstone.platform.entity.PadingRstType;
import com.isoftstone.platform.entity.PagingBean;
import com.isoftstone.platform.model.entity.PtFileEntity;
import com.obs.services.ObsClient;
import com.obs.services.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.xml.crypto.Data;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Service("diskService")
public class DiskServiceImpl implements DiskService {
    private Logger logger = LogManager.getLogger(this.getClass());
    public static final int FILE_TYPE_DIR = 1;
    public static final int FILE_TYPE_FILE = 0;


    @Value("${access.key}")
    private String accessKey;
    @Value("${secret.key}")
    private String secretKey;
    @Value("${end.point}")
    private String endPoint;
    @Value("${backet.name}")
    private String backetName;
    private ObsClient obsClient;

    private synchronized ObsClient getSigleInstance(){
        if(obsClient == null){
            obsClient = new ObsClient(accessKey,secretKey,endPoint);
        }
        return obsClient;
    }


    @Resource
    private DiskDao diskDao;

    @Override
    public void sysnchObjData() {
        obsClient = new ObsClient(accessKey, secretKey, endPoint);
        ObjectListing objectListing = obsClient.listObjects(backetName);
        List<ObsObject> obsObjects = objectListing.getObjects();
        String objKey;
        Long contentLenth;
        Set<PtFileEntity> ptFileEntitySet = new HashSet<>();
        FileTypeNameEntity fileTypeNameEntity;
        PtFileEntity ptFileEntity = null;
        String[] array;
        String filePathItem;
        String objKeySp;
        for (ObsObject item : obsObjects) {
            objKey = item.getObjectKey();
            contentLenth = item.getMetadata().getContentLength();
            array = objKey.split("/");
            if (array.length > 1) {
                objKeySp = "";
                for (int i = 0; i < array.length; i++) {
                    filePathItem = array[i];
                    if (i == array.length - 1) {
                        objKeySp += filePathItem;
                        generateFileReport(objKeySp, filePathItem, contentLenth, ptFileEntitySet);
                    } else {
                        objKeySp += filePathItem + "/";
                        generateFileReport(objKeySp, filePathItem + "/", 0l, ptFileEntitySet);
                    }
                }

            } else {
                generateFileReport(objKey, objKey, contentLenth, ptFileEntitySet);
            }
        }

        for (PtFileEntity item : ptFileEntitySet) {
            item.setParentUuid("0");
            for (PtFileEntity itemParent : ptFileEntitySet) {
                if (item.getFilePath().equals(itemParent.getFilePath() + item.getFileName())) {
                    item.setParentUuid(itemParent.getFileUuid());
                }
            }
        }
//        logger.info(JSON.toJSONString(ptFileEntitySet));
        for (PtFileEntity item : ptFileEntitySet) {
            diskDao.insertFileInfo(item);
        }

    }

    @Override
    public PadingRstType<PtFileEntity> queryFileListPaging(SearchFileEntity search, PagingBean paging) {
        paging.deal(PtFileEntity.class);
        PadingRstType<PtFileEntity> fileEntityFlexiGridEntity = new PadingRstType<>();
        fileEntityFlexiGridEntity.setPage(paging.getPage());
        List<PtFileEntity> ptFileEntityList = diskDao.queryFileListPaging(search, paging);
        fileEntityFlexiGridEntity.setRawRecords(ptFileEntityList);
        Integer total = diskDao.queryFileTotal(search);
        fileEntityFlexiGridEntity.setTotal(total);
        fileEntityFlexiGridEntity.putItems();
        return fileEntityFlexiGridEntity;
    }

    @Override
    public ResponseEntity<byte[]> downloadFile(String fileUuid) {
        PtFileEntity ptFileEntity = diskDao.queryFileInfoById(fileUuid);
        if (ptFileEntity == null || ptFileEntity.getFilePath() == null || ptFileEntity.getFileName() == null) {
            return null;
        }
        obsClient = new ObsClient(accessKey, secretKey, endPoint);
        ObsObject obsObject = obsClient.getObject(backetName, ptFileEntity.getFilePath());
        InputStream inputStream = obsObject.getObjectContent();

        byte[] block = new byte[1024];
        byte[] fileByte;
        Integer length = 0;
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();) {
            while ((length = inputStream.read(block)) > 0) {
                byteArrayOutputStream.write(block, 0, length);
                byteArrayOutputStream.flush();
            }
            fileByte = byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        HttpHeaders headers =
                new HttpHeaders();
        headers.setContentDispositionFormData("attachment",
                ptFileEntity.getFileName());//告知浏览器以下载方式打开
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);//设置MIME类型

        return new ResponseEntity<byte[]>(
                fileByte,
                headers,
                HttpStatus.CREATED);

    }

    @Override
    public void uploadFile(String name, InputStream inputStream) {
        obsClient = new ObsClient(accessKey, secretKey, endPoint);
        obsClient.putObject(backetName, name, inputStream);
        ObsObject obsObject = obsClient.getObject(backetName, name);

        PtFileEntity ptFileEntity = new PtFileEntity();
        ptFileEntity.setFileUuid(DataUtil.getUUID());
        ptFileEntity.setFileName(name);
        ptFileEntity.setFilePath(name);
        ptFileEntity.setFileType(FILE_TYPE_FILE);
        ptFileEntity.setParentUuid("0");
        ptFileEntity.setFileCapacity(DataUtil.doubleFormat(obsObject.getMetadata().getContentLength() / 1024f));
        diskDao.insertFileInfo(ptFileEntity);
    }

    @Override
    public String shareFile(ShareFileEntity shareFileEntity) {
        obsClient = new ObsClient(accessKey, secretKey, endPoint);
        PtFileEntity ptFileEntity = diskDao.queryFileInfoById(shareFileEntity.getFileUuid());
        if (ptFileEntity == null) {
            return "";
        }
        TemporarySignatureRequest request = new TemporarySignatureRequest();
        request.setBucketName(backetName);
        request.setObjectKey(ptFileEntity.getFilePath());
        Date startDate = DateUtil.stringToDate(shareFileEntity.getStartDate());
        request.setRequestDate(startDate);
        diskDao.shareFile(shareFileEntity.getFileUuid());

        Long minInterval = DateUtil.getMinuteInterval(shareFileEntity.getStartDate(), shareFileEntity.getEndDate());

        request.setExpires(minInterval);
        TemporarySignatureResponse signature = obsClient.createTemporarySignature(request);
        return signature.getSignedUrl();


    }

    @Override
    public ResponseEntity<byte[]> shareQRCode(String shareCode) {
        byte[] fileByte = null;
        byte[] block = new byte[1024];
        int length = 0;

        try (InputStream inputStream = QRCodeUtil.generateQRCode(shareCode);
             ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ) {
            while ((length = inputStream.read(block)) > 0) {
                byteArrayOutputStream.write(block, 0, length);
                byteArrayOutputStream.flush();
            }
            fileByte = byteArrayOutputStream.toByteArray();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentDispositionFormData("attachment",
                "shareQRCode.jpg");//告知浏览器以下载方式打开
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);//设置MIME类型

        return new ResponseEntity<byte[]>(
                fileByte,
                headers,
                HttpStatus.CREATED);
    }

    @Override
    public void deleteFileByid(String fileUuid) {
        diskDao.deleteFileByid(fileUuid);
    }

    @Override
    public void undeleteFileByid(String fileUuid) {
        diskDao.undeleteFileByid(fileUuid);
    }

    @Override
    public void toPageFileByid(String fileUuid) { diskDao.toPageFileByid(fileUuid); }


    @Override
    public String getLinkCopy(String obsKey) {
        obsClient = new ObsClient(accessKey, secretKey, endPoint);
        TemporarySignatureRequest request = new TemporarySignatureRequest();
        request.setBucketName(backetName);
        request.setObjectKey(obsKey);
        request.setRequestDate(new Date());
//        五分钟失效
        request.setExpires(300);
        TemporarySignatureResponse signature = obsClient.createTemporarySignature(request);
        String url = signature.getSignedUrl();
        return url;
    }


    public static void generateFileReport(String objKey, String fileName, Long contentLenth, Set<PtFileEntity> ptFileEntitySet) {
        PtFileEntity ptFileEntity;
        FileTypeNameEntity fileTypeNameEntity;
        ptFileEntity = new PtFileEntity();
        ptFileEntity.setFileUuid(DataUtil.getUUID());
        fileTypeNameEntity = authFileTypeDir(fileName, objKey, contentLenth);
        if (fileTypeNameEntity.isFileDirFlag()) {
            ptFileEntity.setFileType(FILE_TYPE_DIR);
        } else {
            ptFileEntity.setFileType(FILE_TYPE_FILE);
        }
        ptFileEntity.setFileName(fileTypeNameEntity.getFileName());
        ptFileEntity.setFilePath(fileTypeNameEntity.getObsKey());
        ptFileEntity.setFileCapacity(DataUtil.doubleFormat(contentLenth / 1024f));
        ptFileEntitySet.add(ptFileEntity);
    }

    @Override
    public PtFileEntity queryClassInfoById(String fileUuId) {
        return diskDao.queryFileInfoById(fileUuId);
    }

    @Override
    public PtFileEntity queryDeleteClassInfoById(String fileUuId) {
        return diskDao.queryDeleteFileInfoById(fileUuId);
    }

    @Override
    public PadingRstType<PtFileEntity> queryDeleteFileListPaging(SearchFileEntity search, PagingBean paging) {
        paging.deal(PtFileEntity.class);
        PadingRstType<PtFileEntity> fileEntityFlexiGridEntity = new PadingRstType<>();
        fileEntityFlexiGridEntity.setPage(paging.getPage());
        List<PtFileEntity> ptFileEntityList = diskDao.queryDeleteFileListPaging(search, paging);
        fileEntityFlexiGridEntity.setRawRecords(ptFileEntityList);
        Integer total = diskDao.queryFileTotal(search);
        fileEntityFlexiGridEntity.setTotal(total);
        fileEntityFlexiGridEntity.putItems();
        return fileEntityFlexiGridEntity;
    }

    @Override
    public void copy(String fileId,String fileName)
    {
        ObsClient obsClient = getSigleInstance();
        String destobjectname=getdestobjectname(fileName);
        CopyObjectResult result = obsClient.copyObject(backetName, fileName,
                backetName, destobjectname);
        System.out.println("\t" + result.getEtag());

        PtFileEntity ptFileEntityOld = diskDao.queryFileInfoById(fileId);
        PtFileEntity ptFileEntity = new PtFileEntity();
        ptFileEntity.setFileUuid(DataUtil.getUUID());
        ptFileEntity.setFileName(destobjectname);
        ptFileEntity.setFilePath(destobjectname);
        ptFileEntity.setFileType(FILE_TYPE_FILE);
        ptFileEntity.setParentUuid("0");
        ptFileEntity.setFileCapacity(ptFileEntityOld.getFileCapacity());
        diskDao.insertFileInfo(ptFileEntity);
    }

    public String getdestobjectname(String fileId)
    {
        String destobjectname="";
        int point_index=fileId.lastIndexOf('.');
        if(point_index!=-1)
            destobjectname=fileId.substring(0,point_index);
        else
            destobjectname=fileId;
        int num=2;//复制重复文件时加的后缀
        int search_type=1;//搜索方式
        int not_ectype=1;//判断有没有副本
        ObsClient obsClient = getSigleInstance();
        ObjectListing listobject=obsClient.listObjects(backetName);
        for(ObsObject obsObject : listobject.getObjects())
        {
            if(obsObject.getObjectKey().contains(destobjectname + " - 副本 (" + num + ")"))
            {
                search_type=2;
                not_ectype=0;
                num++;
                continue;
            }
            if(obsObject.getObjectKey().contains(destobjectname + " - 副本"))
            {
                search_type=2;
                not_ectype=0;
            }
            if(search_type==2)
            {
                if(obsObject.getObjectKey().contains(destobjectname + " - 副本 ("))
                {
                    continue;
                }
                else if(obsObject.getObjectKey().contains(destobjectname + " - 副本 - 副本"))
                {
                    continue;
                }
                else if(point_index!=-1)
                {
                    if(obsObject.getObjectKey().equals(destobjectname+" - 副本"+fileId.substring(point_index,fileId.length())))
                        break;
                    else
                    {
                        num=1;
                        break;
                    }
                }
                else
                {
                    if(obsObject.getObjectKey().equals(destobjectname+" - 副本"))
                        break;
                    else
                    {
                        num=1;
                        break;
                    }
                }
            }
        }
        if(point_index!=-1){
            if(not_ectype==1||num==1){
                destobjectname=destobjectname+" - 副本"+fileId.substring(point_index,fileId.length());
            }else{
                destobjectname=destobjectname+" - 副本 ("+num+")"+fileId.substring(point_index,fileId.length());
            }
        }else {
            if(not_ectype==1||num==1){
                destobjectname=destobjectname+" - 副本";
            }else{
                destobjectname=destobjectname+" - 副本 ("+num+")";
            }
        }
        return destobjectname;
    }


    @Override
    public PadingRstType<PtFileEntity> querytoPageListPaging(SearchFileEntity search, PagingBean paging) {
        paging.deal(PtFileEntity.class);
        PadingRstType<PtFileEntity> fileEntityFlexiGridEntity = new PadingRstType<>();
        fileEntityFlexiGridEntity.setPage(paging.getPage());
        List<PtFileEntity> ptFileEntityList = diskDao.querytoPageListPaging(search, paging);
        fileEntityFlexiGridEntity.setRawRecords(ptFileEntityList);
        Integer total = diskDao.queryFileTotal(search);
        fileEntityFlexiGridEntity.setTotal(total);
        fileEntityFlexiGridEntity.putItems();
        return fileEntityFlexiGridEntity;
    }

    @Override
    public void unshareInfoById(String fileUuId) {
        diskDao.unshareInfoById(fileUuId);
    }

    @Override
    public PadingRstType<PtFileEntity> stoPageistPaging(SearchFileEntity search, PagingBean paging) {
        paging.deal(PtFileEntity.class);
        PadingRstType<PtFileEntity> fileEntityFlexiGridEntity = new PadingRstType<>();
        fileEntityFlexiGridEntity.setPage(paging.getPage());
        List<PtFileEntity> ptFileEntityList = diskDao.stoPageistPaging(search, paging);
        fileEntityFlexiGridEntity.setRawRecords(ptFileEntityList);
        Integer total = diskDao.queryFileTotal(search);
        fileEntityFlexiGridEntity.setTotal(total);
        fileEntityFlexiGridEntity.putItems();
        return fileEntityFlexiGridEntity;
    }

    @Override
    public void stoPageListPaging(String fileUuid) {
        diskDao.stoPageListPaging(fileUuid);
    }

    @Override
    public void modifyFileNameByFileUuid(PtFileEntity ptFileEntityNew) {
        PtFileEntity ptFileEntityOld=diskDao.queryFileInfoById(ptFileEntityNew.getFileUuid());
        if(ptFileEntityNew==null || ptFileEntityNew.getFileName()==null||ptFileEntityNew==null){
            return;
        }
        if(ptFileEntityNew.getFileName().equals((ptFileEntityOld.getFileName())))
        {
            return;
        }
        obsClient = new ObsClient(accessKey,secretKey,endPoint);
        ObsObject obsObject = obsClient.getObject(backetName, ptFileEntityOld.getFilePath());
        obsClient.putObject(backetName,ptFileEntityNew.getFileName(),obsObject.getObjectContent());
        diskDao.modifyFileNameByFileUuid(ptFileEntityNew);
        obsClient.deleteObject(backetName,ptFileEntityOld.getFilePath());
    }


    public static FileTypeNameEntity authFileTypeDir(String filePath, String objKey, Long contentLenth) {
        FileTypeNameEntity fileTypeNameEntity = new FileTypeNameEntity();
        fileTypeNameEntity.setFileDirFlag(false);
        fileTypeNameEntity.setFileName(filePath);
        fileTypeNameEntity.setObsKey(objKey);
        if (filePath == null) {
            return fileTypeNameEntity;
        }
        int index = filePath.indexOf("/", filePath.length() - 1);
        if (index == -1 && contentLenth > 0) {
            return fileTypeNameEntity;
        }


        fileTypeNameEntity.setFileDirFlag(true);
        if (filePath.indexOf("/") == -1) {
            fileTypeNameEntity.setFileName(filePath + "/");
        }

        if (objKey.indexOf("/", objKey.length() - 1) == -1) {
            fileTypeNameEntity.setObsKey(objKey + "/");
        }
        return fileTypeNameEntity;
    }

}
