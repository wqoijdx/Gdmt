package com.isoftstone.member.entity;

import java.io.Serializable;

import com.isoftstone.platform.entity.Columns;

public class IntegralEntity implements Serializable
{
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -2122855985511941828L;
    
    /**
     * 消费Id
     */
    @Columns("nt.id")
    private int id;
    
    /**
     * 会员Id
     */
    @Columns("nt.member_id")
    private String memberId;
    
    /**
     * 姓名
     */
    @Columns("mm.name")
    private String name;
    
    /**
     * 积分
     */
    @Columns("nt.integral")
    private String integral;
    
    /**
     * 更新时间
     */
    @Columns("nt.update_time")
    private String updateTime;
    
    public int getId()
    {
        return id;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public String getMemberId()
    {
        return memberId;
    }
    
    public void setMemberId(String memberId)
    {
        this.memberId = memberId;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getIntegral()
    {
        return integral;
    }
    
    public void setIntegral(String integral)
    {
        this.integral = integral;
    }
    
    public String getUpdateTime()
    {
        return updateTime;
    }
    
    public void setUpdateTime(String updateTime)
    {
        this.updateTime = updateTime;
    }
    
}
