package com.isoftstone.platform.model.entity;
import com.isoftstone.platform.entity.Columns;

import java.io.Serializable;


public class PtClassInfoEntity implements Serializable
{
	@Columns("CLASS_ID")
	private Integer classId;

	@Columns("CLASS_NAME")
	private String className;

	@Columns("PARENT_ID")
	private String parentId;

	@Columns("create_time")
	private String createTime;
	
	public Integer getClassId() 
	{
		return classId;
	}
	
	public void setClassId(Integer classId) 
	{
		this.classId = classId;
	}
	
	public String getClassName() 
	{
		return className;
	}
	
	public void setClassName(String className) 
	{
		this.className = className;
	}
	
	public String getParentId() 
	{
		return parentId;
	}
	
	public void setParentId(String parentId) 
	{
		this.parentId = parentId;
	}
	
	public String getCreateTime() 
	{
		return createTime;
	}
	
	public void setCreateTime(String createTime) 
	{
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "PtClassInfoEntity{" +
				"classId=" + classId +
				", className='" + className + '\'' +
				", parentId='" + parentId + '\'' +
				", createTime='" + createTime + '\'' +
				'}';
	}
}
