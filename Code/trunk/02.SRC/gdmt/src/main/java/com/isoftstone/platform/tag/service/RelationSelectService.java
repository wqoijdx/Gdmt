package com.isoftstone.platform.tag.service;

import java.util.List;

import com.isoftstone.platform.tag.entity.SelectTagEntity;

public interface RelationSelectService extends DataSelectService
{
    
    List<SelectTagEntity> querySubTagData(String code, String language);
    
}
