package com.isoftstone.member.controller;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.isoftstone.member.entity.MemberEntity;
import com.isoftstone.member.entity.MemberSearch;
import com.isoftstone.member.service.MemberService;
import com.isoftstone.platform.controller.BaseController;
import com.isoftstone.platform.entity.PadingRstType;
import com.isoftstone.platform.entity.PagingBean;

@Controller
@RequestMapping("/member")
public class MemberController extends BaseController
{
    
    private final static Logger logger = Logger.getLogger(MemberController.class);
    
    @Resource
    private MemberService memberService;

    /**
     * 前转会员主页
     * @param model
     * @return
     */
    @RequestMapping("/memberList")
    @RequiresPermissions("gdmt_data_MENU_103")
    public String initWarehouseList(Model model)
    {
        return "member/memberList";
    }

    /**
     * 获取会员table列表
     * @param search
     * @param pagingBean
     * @return
     */
    @RequestMapping(value = "/getMemberlist", produces = "application/json")
    @ResponseBody
    public String getMemberlist(MemberSearch search, PagingBean pagingBean)
    {
        logger.info(pagingBean);
        PadingRstType<MemberEntity> rst = memberService.queryMemberByPageList(search, pagingBean);
        
        return JSON.toJSONString(rst);
    }

    /**
     * 删除会员信息
     * @param ids
     * @return
     */
    @RequestMapping(value = "/deleteMemberInfo", produces = "application/json")
    @ResponseBody
    public String deleteMemberInfo(@RequestParam("ids")
    String ids)
    {
        logger.info(ids);
        memberService.deleteMemberInfo(ids);
        return buildSuccessJsonMsg("warehouse.delete.sucess", null);
    }

    /**
     * 修改会员信息
     * @param memberEntity
     * @return
     */
    @RequestMapping(value = "/updateMemberDail", produces = "application/json")
    @ResponseBody
    public String updateMemberDail(MemberEntity memberEntity)
    {
        logger.info(memberEntity);
        memberService.updateMemberDail(memberEntity);
        return buildSuccessJsonMsg("warehouse.update.sucess", null);
    }

    /**
     * 增加会员信息
     * @param memberEntity
     * @return
     */
    @RequestMapping(value = "/addMemberDail", produces = "application/json")
    @ResponseBody
    public String addMemberDail(MemberEntity memberEntity)
    {
        logger.info(memberEntity);
        memberService.addMemberDail(memberEntity);
        return buildSuccessJsonMsg("warehouse.add.sucess", null);
    }
    
}
