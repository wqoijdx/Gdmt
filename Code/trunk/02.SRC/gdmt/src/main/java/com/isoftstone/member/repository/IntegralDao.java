package com.isoftstone.member.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.isoftstone.member.entity.IntegralEntity;
import com.isoftstone.member.entity.SearchMember;
import com.isoftstone.platform.entity.PagingBean;

@Repository("integralDao")
public interface IntegralDao
{
    
    void increaseIntegral(@Param("integral")
    IntegralEntity integralEntity);
    
    List<IntegralEntity> queryIntegralByPageList(@Param("search")
    SearchMember searchMember, @Param("paging")
    PagingBean pagingBean);
    
    Integer queryIntegralTotal(@Param("search")
    SearchMember searchMember);
    
}
