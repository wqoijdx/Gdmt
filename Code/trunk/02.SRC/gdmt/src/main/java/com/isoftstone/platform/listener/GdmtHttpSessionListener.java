package com.isoftstone.platform.listener;

import java.util.List;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.isoftstone.platform.listener.service.ConfigService;
import com.isoftstone.platform.model.entity.PtSysConfigEntity;

public class GdmtHttpSessionListener implements HttpSessionListener
{
    
    Logger logger = Logger.getLogger(GdmtHttpSessionListener.class.getName());
    
    @Override
    public void sessionCreated(HttpSessionEvent se)
    {
        
        HttpSession session = se.getSession();
        ServletContext sc = session.getServletContext();
        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(sc);
        ConfigService configService = webApplicationContext.getBean("configService", ConfigService.class);
        List<PtSysConfigEntity> list = configService.queryConfigList();
        for (PtSysConfigEntity smConfigEntity : list)
        {
            session.setAttribute(smConfigEntity.getCfgName(), smConfigEntity.getCfgValue());
        }
        SessionLocaleResolver localeResolver =
            webApplicationContext.getBean("localeResolver", SessionLocaleResolver.class);
        String siteLanguage = (String)session.getAttribute("siteLanguage");
        if (siteLanguage != null)
        {
            Locale defaultLocale = new Locale(siteLanguage);
            localeResolver.setDefaultLocale(defaultLocale);
        }
        //localeResolver
        
        // logger.info(se);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {

    }
}
