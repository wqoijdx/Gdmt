package com.isoftstone.platform.tag.controller;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.alibaba.fastjson.JSON;
import com.isoftstone.platform.tag.entity.SelectTagEntity;
import com.isoftstone.platform.tag.service.RelationSelectService;

@Controller
@RequestMapping("/tag")
public class TagController
{
    
    private RelationSelectService relationSelectService;
    
    Logger logger = Logger.getLogger(TagController.class.getName());
    
    @RequestMapping(value = "/tagSubList", produces = "application/json")
    @ResponseBody
    public String getMenuList(HttpServletRequest request)
    {
        String code = request.getParameter("code");
        String obj = request.getParameter("obj");
        logger.info("code: " + code + "  obj: " + obj);
        String language = (String)request.getSession().getAttribute("siteLanguage");
        List<SelectTagEntity> selectList = null;
        
        if (obj != null)
        {
            ServletContext sc = request.getServletContext();
            WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(sc);
            relationSelectService = webApplicationContext.getBean(obj, RelationSelectService.class);
            selectList = relationSelectService.querySubTagData(code, language);
            logger.info(selectList);
            return JSON.toJSONString(selectList);
        }
        //= relationSelectService.querySubTagData(code, language);
        //= menuService.getMenuList(search, pagingBean);
        logger.info(selectList);
        return JSON.toJSONString(selectList);
    }
}
