package com.isoftstone.obs.entity;

import java.io.Serializable;

public class FileTypeNameEntity implements Serializable {
    private String fileName;
    private String obsKey;
    private Boolean fileDirFlag;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public boolean isFileDirFlag() {
        return fileDirFlag;
    }

    public void setFileDirFlag(boolean fileDirFlag) {
        this.fileDirFlag = fileDirFlag;
    }

    public String getObsKey() {
        return obsKey;
    }

    public void setObsKey(String obsKey) {
        this.obsKey = obsKey;
    }

    public Boolean getFileDirFlag() {
        return fileDirFlag;
    }

    public void setFileDirFlag(Boolean fileDirFlag) {
        this.fileDirFlag = fileDirFlag;
    }

    @Override
    public String toString() {
        return "FileTypeNameEntity{" +
                "fileName='" + fileName + '\'' +
                ", obsKey='" + obsKey + '\'' +
                ", fileDirFlag=" + fileDirFlag +
                '}';

    }
}
