package com.isoftstone.platform.sm.service.impl;

import com.isoftstone.platform.common.uitl.Md5Util;
import com.isoftstone.platform.model.entity.PtMenuEntity;
import com.isoftstone.platform.model.entity.PtUserEntity;
import com.isoftstone.platform.sm.entity.PasswdEntity;
import com.isoftstone.platform.sm.entity.SysLeveMenu;
import com.isoftstone.platform.sm.repository.UserInfoDao;
import com.isoftstone.platform.sm.service.UserInfoService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service("userInfoService")
public class UserInfoServiceImpl implements UserInfoService
{
    @Resource
    private UserInfoDao userInfoDao;
    
    @Value("#{propertiesConfig['passwd.key']}")
    private String passwdKey;
    
    @Override
    public PtUserEntity autoUserInfo(String userName, String password)
    {
        
        password = Md5Util.md5(password, passwdKey);
        List<PtUserEntity> userInfoList = userInfoDao.autoUserInfo(userName, password, passwdKey);
        if (userInfoList.size() > 0)
        {
            return userInfoList.get(0);
        }
        return null;
    }
    
    @Override
    public boolean updatePwd(PasswdEntity passwdEntity)
    {
        String password = Md5Util.md5(passwdEntity.getOldPassword(), passwdKey);
        passwdEntity.setOldPassword(password);
        List<PtUserEntity> userInfoList = userInfoDao.autoUserInfo(passwdEntity.getUserName(), password, passwdKey);
        if (userInfoList.size() > 0)
        {
            password = Md5Util.md5(passwdEntity.getPassword(), passwdKey);
            passwdEntity.setPassword(password);
            userInfoDao.updatePwd(passwdEntity);
            return true;
        }
        return false;
    }
    
    @Override
    public List<SysLeveMenu> getMenu(String language, String uuid)
    {
        //Session session = SecurityUtils.getSubject().getSession();
        
        List<PtMenuEntity> menuMap = userInfoDao.selectMenu(language, uuid);// 取出全部菜单
        //session.setAttribute("sysMenuList", menuMap);
        List<SysLeveMenu> sysLeveMenuList = new ArrayList<SysLeveMenu>();// 建立一个SysLeveMenu实体对象列表
        // 建立一个SysLeveMenu实体对象
        SysLeveMenu sysLeveMenu1 = null;
        SysLeveMenu sysLeveMenu2 = null;
        SysLeveMenu sysLeveMenu3 = null;
        
        for (PtMenuEntity sysMenuItem1 : menuMap)
        {
            if (sysMenuItem1 == null)
            {
                continue;
            }
            if (sysMenuItem1.getParentId() == null)
            {
                continue;
            }
            if ("0".equals(sysMenuItem1.getParentId()))
            {
                sysLeveMenu1 = new SysLeveMenu();
                sysLeveMenu1.setMenuName(sysMenuItem1.getMenuName());// TODO
                sysLeveMenu1.setMenuId(sysMenuItem1.getMenuId());
                sysLeveMenu1.setParentId(sysMenuItem1.getParentId());
                sysLeveMenu1.setMenuUrl(sysMenuItem1.getMenuUrl());
                sysLeveMenu1.setMenuIcon(sysMenuItem1.getMenuIcon());
                sysLeveMenuList.add(sysLeveMenu1);
                for (PtMenuEntity sysMenuItem2 : menuMap)
                {
                    if (sysMenuItem2.getParentId().equals(sysMenuItem1.getMenuId()))
                    {
                        sysLeveMenu2 = new SysLeveMenu();
                        sysLeveMenu2.setMenuName(sysMenuItem2.getMenuName());// TODO
                        sysLeveMenu2.setMenuId(sysMenuItem2.getMenuId());
                        sysLeveMenu2.setParentId(sysMenuItem2.getParentId());
                        sysLeveMenu2.setMenuUrl(sysMenuItem2.getMenuUrl());
                        sysLeveMenu2.setMenuIcon(sysMenuItem2.getMenuIcon());
                        sysLeveMenu1.addChildren(sysLeveMenu2);
                        for (PtMenuEntity sysMenuItem3 : menuMap)
                        {
                            if (sysMenuItem3.getParentId().equals(sysMenuItem2.getMenuId()))
                            {
                                sysLeveMenu3 = new SysLeveMenu();
                                sysLeveMenu3.setMenuName(sysMenuItem3.getMenuName());// TODO
                                sysLeveMenu3.setMenuId(sysMenuItem3.getMenuId());
                                sysLeveMenu3.setParentId(sysMenuItem3.getParentId());
                                sysLeveMenu3.setMenuUrl(sysMenuItem3.getMenuUrl());
                                sysLeveMenu3.setMenuIcon(sysMenuItem3.getMenuIcon());
                                sysLeveMenu2.addChildren(sysLeveMenu3);
                            }
                        }
                    }
                }
            }
        }
        return sysLeveMenuList;
    }
    
    @Override
    public List<PtMenuEntity> selectMenu(String siteLanguage, String userUuid)
    {
        // TODO Auto-generated method stub
        return userInfoDao.selectMenu(siteLanguage, userUuid);
    }
    
}
