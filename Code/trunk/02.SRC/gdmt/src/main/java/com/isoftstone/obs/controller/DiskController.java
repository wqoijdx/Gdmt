package com.isoftstone.obs.controller;


import com.isoftstone.obs.entity.SearchFileEntity;
import com.isoftstone.obs.entity.ShareFileEntity;
import com.isoftstone.obs.service.DiskService;
import com.isoftstone.platform.common.uitl.PlatformUtil;
import com.isoftstone.platform.controller.BaseController;
import com.isoftstone.platform.entity.PadingRstType;
import com.isoftstone.platform.entity.PagingBean;
import com.isoftstone.platform.model.entity.PtFileEntity;
import org.apache.commons.fileupload.FileItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedHashMap;
import java.util.Map;

@Controller
@RequestMapping("/disk")
public class DiskController extends BaseController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private DiskService diskService;
    @RequestMapping("/initPage")
    public String initPage(){
        return "disk/diskPage";
    }

    @RequestMapping("/sysnchObjData")
    @ResponseBody
    public String sysnchObjData(){
        diskService.sysnchObjData();
        return buildSuccessJsonMsg("disk.sysnch.data.success");
    }
    @RequestMapping("/queryFileListPaging")
    @ResponseBody
    public PadingRstType<PtFileEntity> queryFileListPaging(SearchFileEntity search, PagingBean paging){
        logger.info(search);
        logger.info(paging);
        PadingRstType<PtFileEntity> fileEntityFlexiGridEntity = diskService.queryFileListPaging(search,paging);
        return fileEntityFlexiGridEntity;
    }

    @RequestMapping("/queryDeleteFileListPaging")
    @ResponseBody
    public PadingRstType<PtFileEntity> queryDeleteFileListPaging(SearchFileEntity search, PagingBean paging){
        logger.info(search);
        logger.info(paging);
        PadingRstType<PtFileEntity> fileEntityFlexiGridEntity = diskService.queryDeleteFileListPaging(search,paging);
        return fileEntityFlexiGridEntity;
    }

    @RequestMapping("/stoPageistPaging")
    @ResponseBody
    public PadingRstType<PtFileEntity> stoPageistPaging(SearchFileEntity search, PagingBean paging){
        logger.info(search);
        logger.info(paging);
        PadingRstType<PtFileEntity> fileEntityFlexiGridEntity = diskService.stoPageistPaging(search,paging);
        return fileEntityFlexiGridEntity;
    }

    @RequestMapping("/querytoPageListPaging")
    @ResponseBody
    public PadingRstType<PtFileEntity> querytoPageListPaging(SearchFileEntity search, PagingBean paging){
        logger.info(search);
        logger.info(paging);
        PadingRstType<PtFileEntity> fileEntityFlexiGridEntity = diskService.querytoPageListPaging(search,paging);
        return fileEntityFlexiGridEntity;
    }



    @RequestMapping("/downloadFile/{fileUuid}")
    @ResponseBody
    public ResponseEntity<byte[]> downloadFile(@PathVariable("fileUuid") String fileUuid){
        logger.info("fileUuid:" + fileUuid);
        ResponseEntity<byte[]> responseEntity = diskService.downloadFile(fileUuid);
        return responseEntity;
    }

    @RequestMapping("/uploadFile")
    @ResponseBody
    public String uploadFile(MultipartHttpServletRequest request) throws IOException {
        CommonsMultipartFile multipartFile = (CommonsMultipartFile)request.getFile("fileName");
        FileItem fileItem = multipartFile.getFileItem();

        diskService.uploadFile(fileItem.getName(),multipartFile.getInputStream());

        return buildSuccessJsonMsg("disk.upload.file.success");
    }

    @RequestMapping("/shareFile")
    @ResponseBody
    public String shareFile(ShareFileEntity shareFileEntity) {
        logger.info(shareFileEntity);
        String msgStr = diskService.shareFile(shareFileEntity);
        Map<String, Object> message = new LinkedHashMap<>();

        message.put("success", true);
        message.put("msg", msgStr);
        message.put("description", msgStr);
        String rst = PlatformUtil.toJSON(message);

        return rst;
    }

    @RequestMapping("/shareQRCode.svg")
    @ResponseBody
    public ResponseEntity<byte[]> shareQRCode(@RequestParam("shareCode") String shareCode){
        logger.info("shareCode:" + shareCode);
        ResponseEntity<byte[]> responseEntity = diskService.shareQRCode(shareCode);
        return responseEntity;
    }
    @RequestMapping("/queryClassInfoById")
    @ResponseBody
    public PtFileEntity queryClassInfoById(HttpServletRequest request){
        String fileUuId=request.getParameter("fileUuId");
        logger.info("fileUuId"+fileUuId);
        PtFileEntity ptFileEntity=diskService.queryClassInfoById(fileUuId);
        return ptFileEntity;
    }

    @RequestMapping("/queryDeleteClassInfoById")
    @ResponseBody
    public PtFileEntity queryDeleteClassInfoById(HttpServletRequest request){
        String fileUuId=request.getParameter("fileUuId");
        logger.info("fileUuId"+fileUuId);
        PtFileEntity ptFileEntity=diskService.queryDeleteClassInfoById(fileUuId);
        return ptFileEntity;
    }

    @RequestMapping("/unshareInfoById")
    @ResponseBody
    public String unshareInfoById(HttpServletRequest request){
        String fileUuId=request.getParameter("fileUuId");
        logger.info("fileUuId"+fileUuId);
        diskService.unshareInfoById(fileUuId);
        return  buildSuccessJsonMsg("disk.unshare.file.success");
    }

    @RequestMapping("/deleteFileByid")
    @ResponseBody
    public String deleteFileByid(HttpServletRequest request){
        String fileUuid = request.getParameter("fileUuid");
        logger.info("fileUuid:"+fileUuid);

        diskService.deleteFileByid(fileUuid);

        return buildSuccessJsonMsg("disk.delete.file.success");
    }

    @RequestMapping("/undeleteFileByid")
    @ResponseBody
    public String undeleteFileByid(HttpServletRequest request){
        String fileUuid = request.getParameter("fileUuid");
        logger.info("fileUuid:"+fileUuid);

        diskService.undeleteFileByid(fileUuid);

        return buildSuccessJsonMsg("disk.undelete.file.success");
    }

    @RequestMapping("/stoPageListPaging")
    @ResponseBody
    public String stoPageListPaging(HttpServletRequest request){
        String fileUuid = request.getParameter("fileUuid");
        logger.info("fileUuid:"+fileUuid);

        diskService.stoPageListPaging(fileUuid);

        return buildSuccessJsonMsg("disk.unshare.file.success");
    }

    @RequestMapping("/modifyFileNameByFileUuid")
    @ResponseBody
    public String odifyFileNameByFileUuid(PtFileEntity ptFileEntity) throws IOException {
        logger.info(ptFileEntity);
        diskService.modifyFileNameByFileUuid(ptFileEntity);
        return buildSuccessJsonMsg("disk.modify.file.success");
    }
    @RequestMapping("/getLinkCopy")
    @ResponseBody
    public String getLinkCopy(@RequestParam("obsKey") String obsKey) throws IOException {
        logger.info("obsKey"+obsKey);
        String msgStr = diskService.getLinkCopy(obsKey);
        Map<String, Object> message = new LinkedHashMap<>();

        message.put("success", true);
        message.put("msg", msgStr);
        message.put("description", msgStr);
        String rst = PlatformUtil.toJSON(message);

        return rst;
    }

    @RequestMapping("/toPageFileByid")
    @ResponseBody
    public String toPageFileByid(HttpServletRequest request){
        String fileUuid = request.getParameter("fileUuid");
        logger.info("fileUuid:"+fileUuid);

        diskService.toPageFileByid(fileUuid);

        return buildSuccessJsonMsg("disk.toPage.file.success");
    }

    @RequestMapping("/recycleFile")
    public String recycleFile(){
        return "disk/recycleFile";
    }

    @RequestMapping(value = "/copy/")
    @ResponseBody
    public String copy(@RequestParam String fileId,@RequestParam String fileName) throws IOException
    {
        if(fileName.isEmpty())
//            file.copy.fail
            return buildSuccessJsonMsg("file.copy.fail",null);
        try {
            fileName = new java.net.URI(fileName).getPath();//解码
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        diskService.copy(fileId,fileName);
//        file.copy.success
        return buildSuccessJsonMsg("file.copy.success",null);

    }

    @RequestMapping("/stoPage")
    public String stoPage(){
        return "disk/stoPage";
    }
}
