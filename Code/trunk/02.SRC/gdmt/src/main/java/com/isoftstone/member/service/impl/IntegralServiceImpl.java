package com.isoftstone.member.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.isoftstone.member.entity.IntegralEntity;
import com.isoftstone.member.entity.SearchMember;
import com.isoftstone.member.repository.IntegralDao;
import com.isoftstone.member.repository.MemberDao;
import com.isoftstone.member.service.IntegralService;
import com.isoftstone.platform.entity.PadingRstType;
import com.isoftstone.platform.entity.PagingBean;

@Service("integralService")
public class IntegralServiceImpl implements IntegralService
{
    @Resource
    private IntegralDao integralDao;
    
    @Resource
    private MemberDao memberDao;
    
    @Override
    public void increaseIntegral(IntegralEntity integralEntity)
    {
        integralDao.increaseIntegral(integralEntity);
        memberDao.increaseIntegral(integralEntity);
        
    }
    
    @Override
    public PadingRstType<IntegralEntity> queryIntegralByPageList(SearchMember searchMember, PagingBean pagingBean)
    {
        pagingBean.deal(IntegralEntity.class);
        List<IntegralEntity> integralBeanList = integralDao.queryIntegralByPageList(searchMember, pagingBean);
        Integer total = integralDao.queryIntegralTotal(searchMember);
        PadingRstType<IntegralEntity> integralBean = new PadingRstType<IntegralEntity>();
        integralBean.setPage(pagingBean.getPage());
        integralBean.setTotal(total);
        integralBean.putItems(IntegralEntity.class, integralBeanList);
        
        return integralBean;
    }
    
}
