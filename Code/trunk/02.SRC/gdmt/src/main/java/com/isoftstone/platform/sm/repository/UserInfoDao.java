package com.isoftstone.platform.sm.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.isoftstone.platform.model.entity.PtMenuEntity;
import com.isoftstone.platform.model.entity.PtUserEntity;
import com.isoftstone.platform.sm.entity.PasswdEntity;

@Repository("userInfoDao")
public interface UserInfoDao
{
    
    List<PtUserEntity> autoUserInfo(@Param("userName")
    String userName, @Param("password")
    String password, @Param("passwdKey")
    String passwdKey);
    
    void updatePwd(@Param("passwd")
    PasswdEntity passwdEntity);
    
    List<PtMenuEntity> selectMenu(@Param("language")
    String language, @Param("userUuid")
    String userUuid);
    
}
