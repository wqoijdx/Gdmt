package com.isoftstone.platform.tag.service;

import java.util.List;

import com.isoftstone.platform.tag.entity.SelectTagEntity;

public interface DataSelectService
{
    
    List<SelectTagEntity> queryTagElementList(String language);
    
}
