package com.isoftstone.platform.listener.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.isoftstone.platform.listener.repository.ConfigDao;
import com.isoftstone.platform.listener.service.ConfigService;
import com.isoftstone.platform.model.entity.PtSysConfigEntity;

@Service("configService")
public class ConfigServiceImpl implements ConfigService
{
    @Resource
    private ConfigDao configDao;
    
    @Override
    public List<PtSysConfigEntity> queryConfigList()
    {
        // TODO Auto-generated method stub
        return configDao.queryConfigList();
    }
    
}
