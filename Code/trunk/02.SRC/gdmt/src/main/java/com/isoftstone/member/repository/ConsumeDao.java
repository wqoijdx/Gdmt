package com.isoftstone.member.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.isoftstone.member.entity.ConsumeEntity;
import com.isoftstone.member.entity.SearchMember;
import com.isoftstone.platform.entity.PagingBean;

@Repository("consumeDao")
public interface ConsumeDao
{
    
    void consumeMoney(@Param("consume")
    ConsumeEntity consumeEntity);
    
    List<ConsumeEntity> queryConsumeByPageList(@Param("search")
    SearchMember searchMember, @Param("paging")
    PagingBean pagingBean);
    
    Integer queryConsumeTotal(@Param("search")
    SearchMember searchMember);
    
}
