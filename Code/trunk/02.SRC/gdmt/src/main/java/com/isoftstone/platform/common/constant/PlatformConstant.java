package com.isoftstone.platform.common.constant;

public interface PlatformConstant
{
    /** 验证码 放到session中的key **/
    String CAPTCHA_CODE_KEY = "CAPTCHA_CODE_KEY";
    
    /** 验证码  放到session中的时间 key **/
    String CAPTCHA_CODE_GEN_TIME_KEY = "CAPTCHAE_CODE_GEN_TIME";
    
}
