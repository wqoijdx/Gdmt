package com.isoftstone.platform.tag.entity;

import java.io.Serializable;

public class SelectTagEntity implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 3836317209902463119L;
    
    private String code;
    
    private String dictName;
    
    public String getCode()
    {
        return code;
    }
    
    public void setCode(String code)
    {
        this.code = code;
    }
    
    public String getDictName()
    {
        return dictName;
    }
    
    public void setDictName(String dictName)
    {
        this.dictName = dictName;
    }
    
    @Override
    public String toString()
    {
        return "SelectTagEntity [code=" + code + ", dictName=" + dictName + "]";
    }
    
}
