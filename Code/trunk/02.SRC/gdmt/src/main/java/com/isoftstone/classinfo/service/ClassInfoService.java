package com.isoftstone.classinfo.service;

import com.isoftstone.classinfo.entity.ClassInfoTreeEntity;
import com.isoftstone.classinfo.entity.SearchClassInfoEntity;
import com.isoftstone.platform.entity.PadingRstType;
import com.isoftstone.platform.entity.PagingBean;
import com.isoftstone.platform.model.entity.PtClassInfoEntity;

import java.util.List;

public interface ClassInfoService {

    List<ClassInfoTreeEntity> queryClassInfoZtreeList();

    PadingRstType<PtClassInfoEntity> getClassInfolist(SearchClassInfoEntity search, PagingBean pagingBean);

    void addClassInfo(PtClassInfoEntity ptClassInfoEntity);

    void deleteClassInfo(String ids);

    List<PtClassInfoEntity> queryClassInfoById(String id);

    void modifyClassInfoById(PtClassInfoEntity ptClassInfoEntity);

    void bondClassParentId(PtClassInfoEntity ptClassInfoEntity);

    List<ClassInfoTreeEntity> queryClassInfoZtreeSelectList(String classId);
}
