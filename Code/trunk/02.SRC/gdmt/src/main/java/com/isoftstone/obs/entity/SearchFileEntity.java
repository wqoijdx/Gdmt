package com.isoftstone.obs.entity;

import java.io.Serializable;

public class SearchFileEntity implements Serializable {
    private String seachFileType;
    private String searchFileName;

    public String getSeachFileType() {
        return seachFileType;
    }

    public String getSearchFileName() {
        return searchFileName;
    }

    public void setSearchFileName(String searchFileName) {
        this.searchFileName = searchFileName;
    }

    public void setSeachFileType(String seachFileType) {
        this.seachFileType = seachFileType;
    }

    @Override
    public String toString() {
        return "SearchFileEntity{" +
                "seachFileType='" + seachFileType + '\'' +
                ", searchFileName='" + searchFileName + '\'' +
                '}';

    }
}
