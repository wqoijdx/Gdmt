package com.isoftstone.platform.tag.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.isoftstone.platform.model.entity.PtDictionaryEntity;
import com.isoftstone.platform.tag.repository.DctnrDao;
import com.isoftstone.platform.tag.service.DctnrService;

@Service("dctnrService")
public class DctnrServiceImpl implements DctnrService
{
    @Value("${default.lang}")
    private String language;
    
    @Autowired
    private DctnrDao dctnrDao;
    
    @Override
    public List<PtDictionaryEntity> queryDicCodeListByeGroup(String group, String language)
    {
        // TODO Auto-generated method stub
        if (language == null || "".equals(language))
        {
            language = this.language;
        }
        return dctnrDao.queryDicCodeListByeGroup(group, language);
    }
    
}