package com.isoftstone.platform.model.entity ;
import com.isoftstone.platform.entity.Columns;

import java.io.Serializable;


public class PtRoleMenuEntity implements Serializable
{
	@Columns("ROLE_UUID")
	private String roleUuid;

	@Columns("menu_id")
	private Integer menuId;
	
	public String getRoleUuid() 
	{
		return roleUuid;
	}
	
	public void setRoleUuid(String roleUuid) 
	{
		this.roleUuid = roleUuid;
	}
	
	public Integer getMenuId() 
	{
		return menuId;
	}
	
	public void setMenuId(Integer menuId) 
	{
		this.menuId = menuId;
	}

	@Override
	public String toString() {
		return "PtRoleMenuEntity{" +
				"roleUuid='" + roleUuid + '\'' +
				", menuId=" + menuId +
				'}';
	}
}
