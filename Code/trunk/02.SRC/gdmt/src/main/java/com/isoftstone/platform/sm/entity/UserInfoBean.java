package com.isoftstone.platform.sm.entity;

import java.io.Serializable;

import com.isoftstone.platform.entity.Columns;

public class UserInfoBean implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -8386328996767747121L;
    
    @Columns("user_id")
    private Long userId;
    
    @Columns("user_name")
    private String userName;
    
    @Columns("password")
    private String password;
    
    @Columns("nick_name")
    private String nickName;
    
    @Columns("gender")
    private String gender;
    
    @Columns("telephone")
    private String telephone;
    
    @Columns("email")
    private String email;
    
    @Columns("description")
    private String description;
    
    @Columns("update_time")
    private String updateTime;
    
    @Columns("create_time")
    private String createTime;
    
    public Long getUserId()
    {
        return userId;
    }
    
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }
    
    public String getUserName()
    {
        return userName;
    }
    
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    public String getPassword()
    {
        return password;
    }
    
    public void setPassword(String password)
    {
        this.password = password;
    }
    
    public String getNickName()
    {
        return nickName;
    }
    
    public void setNickName(String nickName)
    {
        this.nickName = nickName;
    }
    
    public String getGender()
    {
        return gender;
    }
    
    public void setGender(String gender)
    {
        this.gender = gender;
    }
    
    public String getTelephone()
    {
        return telephone;
    }
    
    public void setTelephone(String telephone)
    {
        this.telephone = telephone;
    }
    
    public String getEmail()
    {
        return email;
    }
    
    public void setEmail(String email)
    {
        this.email = email;
    }
    
    public String getDescription()
    {
        return description;
    }
    
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    public String getUpdateTime()
    {
        return updateTime;
    }
    
    public void setUpdateTime(String updateTime)
    {
        this.updateTime = updateTime;
    }
    
    public String getCreateTime()
    {
        return createTime;
    }
    
    public void setCreateTime(String createTime)
    {
        this.createTime = createTime;
    }
    
}
