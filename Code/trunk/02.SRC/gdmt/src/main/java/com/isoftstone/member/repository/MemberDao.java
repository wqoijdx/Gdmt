package com.isoftstone.member.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.isoftstone.member.entity.IntegralEntity;
import com.isoftstone.member.entity.MemberEntity;
import com.isoftstone.member.entity.MemberSearch;
import com.isoftstone.platform.entity.PagingBean;

@Repository("memberDao")
public interface MemberDao
{
    
    void addMemberDail(@Param("member")
    MemberEntity memberEntity);
    
    List<MemberEntity> queryMemberByPageList(@Param("search")
    MemberSearch search, @Param("paging")
    PagingBean pagingBean);
    
    Integer queryMemberTotal(@Param("search")
    MemberSearch search);
    
    void updateMemberDail(@Param("member")
    MemberEntity memberEntity);
    
    void deleteMemberInfo(@Param("ids")
    String[] idsArray);
    
    void increaseIntegral(@Param("integral")
    IntegralEntity integralEntity);
    
    List<MemberEntity> queryMemberListById(@Param("id")
    String id);
}
