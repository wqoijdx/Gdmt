package com.isoftstone.member.entity;

import java.io.Serializable;

import com.isoftstone.platform.entity.Columns;

public class MemberEntity implements Serializable
{
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1422999134997612610L;
    
    @Columns("id")
    private int id;
    
    /**
     * 会员卡号
     */
    @Columns("memberId")
    private String memberId;
    
    /**
     * 姓名
     */
    @Columns("name")
    private String name;
    
    /**
     * 电话
     */
    @Columns("tel")
    private String tel;
    
    /**
     * 积分
     */
    @Columns("integral")
    private double integral;
    
    /**
     * 更新时间
     */
    @Columns("update_time")
    private String updateTime;
    
    /**
     * 备注
     */
    @Columns("remarks")
    private String remarks;
    
    public int getId()
    {
        return id;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getTel()
    {
        return tel;
    }
    
    public void setTel(String tel)
    {
        this.tel = tel;
    }
    
    public double getIntegral()
    {
        return integral;
    }
    
    public void setIntegral(double integral)
    {
        this.integral = integral;
    }
    
    public String getUpdateTime()
    {
        return updateTime;
    }
    
    public void setUpdateTime(String updateTime)
    {
        this.updateTime = updateTime;
    }
    
    public String getRemarks()
    {
        return remarks;
    }
    
    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }
    
    public String getMemberId()
    {
        return memberId;
    }
    
    public void setMemberId(String memberId)
    {
        this.memberId = memberId;
    }
    
}
