package com.isoftstone.platform.model.entity ;
import com.isoftstone.platform.entity.Columns;

import java.io.Serializable;


public class PtMenuEntity implements Serializable
{
	@Columns("menu_id")
	private String menuId;

	@Columns("menu_name")
	private String menuName;

	@Columns("parent_id")
	private String parentId;

	@Columns("menu_type")
	private Integer menuType;

	@Columns("menu_level")
	private Integer menuLevel;

	@Columns("menu_url")
	private String menuUrl;

	@Columns("privilege_code")
	private String privilegeCode;

	@Columns("is_https")
	private Integer isHttps;

	@Columns("menu_icon")
	private String menuIcon;

	@Columns("menu_seq")
	private String menuSeq;

	@Columns("create_time")
	private String createTime;
	
	public String getMenuId() 
	{
		return menuId;
	}
	
	public void setMenuId(String menuId) 
	{
		this.menuId = menuId;
	}
	
	public String getMenuName() 
	{
		return menuName;
	}
	
	public void setMenuName(String menuName) 
	{
		this.menuName = menuName;
	}
	
	public String getParentId() 
	{
		return parentId;
	}
	
	public void setParentId(String parentId) 
	{
		this.parentId = parentId;
	}
	
	public Integer getMenuType() 
	{
		return menuType;
	}
	
	public void setMenuType(Integer menuType) 
	{
		this.menuType = menuType;
	}
	
	public Integer getMenuLevel() 
	{
		return menuLevel;
	}
	
	public void setMenuLevel(Integer menuLevel) 
	{
		this.menuLevel = menuLevel;
	}
	
	public String getMenuUrl() 
	{
		return menuUrl;
	}
	
	public void setMenuUrl(String menuUrl) 
	{
		this.menuUrl = menuUrl;
	}
	
	public String getPrivilegeCode() 
	{
		return privilegeCode;
	}
	
	public void setPrivilegeCode(String privilegeCode) 
	{
		this.privilegeCode = privilegeCode;
	}
	
	public Integer getIsHttps() 
	{
		return isHttps;
	}
	
	public void setIsHttps(Integer isHttps) 
	{
		this.isHttps = isHttps;
	}
	
	public String getMenuIcon() 
	{
		return menuIcon;
	}
	
	public void setMenuIcon(String menuIcon) 
	{
		this.menuIcon = menuIcon;
	}
	
	public String getMenuSeq() 
	{
		return menuSeq;
	}
	
	public void setMenuSeq(String menuSeq) 
	{
		this.menuSeq = menuSeq;
	}
	
	public String getCreateTime() 
	{
		return createTime;
	}
	
	public void setCreateTime(String createTime) 
	{
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "PtMenuEntity{" +
				"menuId='" + menuId + '\'' +
				", menuName='" + menuName + '\'' +
				", parentId='" + parentId + '\'' +
				", menuType=" + menuType +
				", menuLevel=" + menuLevel +
				", menuUrl='" + menuUrl + '\'' +
				", privilegeCode='" + privilegeCode + '\'' +
				", isHttps=" + isHttps +
				", menuIcon='" + menuIcon + '\'' +
				", menuSeq='" + menuSeq + '\'' +
				", createTime='" + createTime + '\'' +
				'}';
	}
}
