package com.isoftstone.classinfo.service.impl;

import com.isoftstone.classinfo.entity.ClassInfoTreeEntity;
import com.isoftstone.classinfo.entity.SearchClassInfoEntity;
import com.isoftstone.classinfo.repository.ClassInfoDao;
import com.isoftstone.classinfo.service.ClassInfoService;
import com.isoftstone.platform.entity.PadingRstType;
import com.isoftstone.platform.entity.PagingBean;
import com.isoftstone.platform.model.entity.PtClassInfoEntity;
import com.isoftstone.platform.tag.entity.SelectTagEntity;
import com.isoftstone.platform.tag.service.DataSelectService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("classInfoService")
public class ClassInfoServiceImpl implements ClassInfoService, DataSelectService {
    @Resource
    private ClassInfoDao classInfoDao;
    @Override
    public List<ClassInfoTreeEntity> queryClassInfoZtreeList() {
        return classInfoDao.queryClassInfoZtreeList();
    }
    @Resource
    private ClassInfoTreeEntity classInfoTreeEntity;
    @Override
    public PadingRstType<PtClassInfoEntity> getClassInfolist(SearchClassInfoEntity search, PagingBean pagingBean) {
        pagingBean.deal(PtClassInfoEntity.class);
        PadingRstType<PtClassInfoEntity> padingRstType = new PadingRstType<PtClassInfoEntity>();
        padingRstType.setPage(pagingBean.getPage());
        List<PtClassInfoEntity> list = classInfoDao.getClassInfolistByPage(search,pagingBean);
        padingRstType.setRawRecords(list);
        padingRstType.setPage(pagingBean.getPage());
        Integer total = classInfoDao.getClassInfoListTotal(search);
        padingRstType.setTotal(total);
        padingRstType.putItems(PtClassInfoEntity.class,list);
        return padingRstType;
    }

    @Override
    public void addClassInfo(PtClassInfoEntity ptClassInfoEntity) {
        if("".equals(ptClassInfoEntity.getParentId())){
            ptClassInfoEntity.setParentId(classInfoTreeEntity.getId()+"");
        }
        classInfoDao.addClassInfo(ptClassInfoEntity);
    }

    @Override
    public void deleteClassInfo(String ids) {
        String[] idArray = ids.split(",");
        classInfoDao.deleteClassInfo(idArray);
    }

    @Override
    public List<PtClassInfoEntity> queryClassInfoById(String id) {
        return classInfoDao.queryClassInfoById(id);
    }

    @Override
    public void modifyClassInfoById(PtClassInfoEntity ptClassInfoEntity) {
        classInfoDao.modifyClassInfoById(ptClassInfoEntity);
    }

    @Override
    public void bondClassParentId(PtClassInfoEntity ptClassInfoEntity) {
        classInfoDao.bondClassParentId(ptClassInfoEntity);
    }

    @Override
    public List<ClassInfoTreeEntity> queryClassInfoZtreeSelectList(String classId) {
        List<ClassInfoTreeEntity> list = classInfoDao.queryClassInfoZtreeList();
        List<PtClassInfoEntity> ptClassInfoEntityList = classInfoDao.queryClassInfoById(classId);
        if(ptClassInfoEntityList.size() > 0){
            for(ClassInfoTreeEntity item:list){
                if(ptClassInfoEntityList.get(0).getParentId().equals(item.getpId()+"")){
                    item.setChecked(true);
                    break;
                }
            }
        }
        return list;
    }

    @Override
    public List<SelectTagEntity> queryTagElementList(String language) {
        return classInfoDao.queryTagElementList();
    }
}
