package com.isoftstone.classinfo.controller;

import com.isoftstone.classinfo.entity.ClassInfoTreeEntity;
import com.isoftstone.classinfo.entity.SearchClassInfoEntity;
import com.isoftstone.classinfo.service.ClassInfoService;
import com.isoftstone.platform.controller.BaseController;
import com.isoftstone.platform.entity.PadingRstType;
import com.isoftstone.platform.entity.PagingBean;
import com.isoftstone.platform.model.entity.PtClassInfoEntity;
import org.apache.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 班级管理
 */
@Controller
@RequestMapping("/classinfo")
public class ClassInfoController extends BaseController {
    Logger logger =  Logger.getLogger(ClassInfoController.class.getName());
    @Resource
    private ClassInfoService classInfoService;
    @Resource
    private ClassInfoTreeEntity classInfoTreeEntity;

    /**
     * 班级管理前转页面
     * @return
     */
    @RequiresPermissions("gdmt_class_info")
    @RequestMapping("/page")
    public String initPage(){
        return "classinfo/classList";
    }

    //queryClassInfoZtreeList

    /**
     * 获取班级Ztree列表
     * @return
     */
    @RequiresPermissions("gdmt_class_info")
    @RequestMapping("/queryClassInfoZtreeList")
    @ResponseBody
    public List<ClassInfoTreeEntity> queryClassInfoZtreeList(){
        List<ClassInfoTreeEntity> list = classInfoService.queryClassInfoZtreeList();
        list.add(classInfoTreeEntity);
        return list;
    }

    /**
     * 分页查询班级table列表
     * @param search
     * @param pagingBean
     * @return
     */
    @RequestMapping("/getClassInfolist")
    @ResponseBody
    @RequiresPermissions("gdmt_class_info")
    public PadingRstType<PtClassInfoEntity> getClassInfolist(SearchClassInfoEntity search, PagingBean pagingBean){
        // request.getParameter("parentId");
        logger.info(search);
        PadingRstType<PtClassInfoEntity> padingRstType = classInfoService.getClassInfolist(search,pagingBean);
        return padingRstType;
    }

    /**
     * 增加班级信息
     * @param ptClassInfoEntity
     * @return
     */
    @RequestMapping("/addClassInfo")
    @ResponseBody
    @RequiresPermissions("gdmt_class_info")
    public String addClassInfo(PtClassInfoEntity ptClassInfoEntity){
        // request.getParameter("parentId");
        logger.info(ptClassInfoEntity);
       // PadingRstType<PtClassInfoEntity> padingRstType = classInfoService.getClassInfolist(search,pagingBean);
        classInfoService.addClassInfo(ptClassInfoEntity);
        return buildSuccessJsonMsg("class.add.sucess", null);
    }

    /**
     * 删除级级信息
     * @param request
     * @return
     */
    @RequestMapping("/deleteClassInfo")
    @ResponseBody
    @RequiresPermissions("gdmt_class_info")
    public String deleteClassInfo(HttpServletRequest request){
        String ids = request.getParameter("ids");
        logger.info(ids);
        classInfoService.deleteClassInfo(ids);
        return buildSuccessJsonMsg("class.del.sucess", null);
    }

    /**
     * 根据班级ID查询班级信息
     * @param id
     * @return
     */
    @RequestMapping("/queryClassInfoById")
    @ResponseBody
    @RequiresPermissions("gdmt_class_info")
    public List<PtClassInfoEntity> queryClassInfoById(@RequestParam("classId") String id){
        // request.getParameter("parentId");
        logger.info(id);
        List<PtClassInfoEntity> list = classInfoService.queryClassInfoById(id);
        return list;
    }

    /**
     * 根据班级ID修改班级信息
     * @param ptClassInfoEntity
     * @return
     */
    @RequestMapping("/modifyClassInfoById")
    @ResponseBody
    @RequiresPermissions("gdmt_class_info")
    public String modifyClassInfoById(PtClassInfoEntity ptClassInfoEntity){
        // request.getParameter("parentId");
        logger.info(ptClassInfoEntity);
        classInfoService.modifyClassInfoById(ptClassInfoEntity);
        return buildSuccessJsonMsg("class.modify.sucess", null);
    }
//bondClassParentId

    /**
     * 绑定班级信息
     * @param ptClassInfoEntity
     * @return
     */
    @RequestMapping("/bondClassParentId")
    @ResponseBody
    @RequiresPermissions("gdmt_class_info")
    public String bondClassParentId(PtClassInfoEntity ptClassInfoEntity){
        // request.getParameter("parentId");
        logger.info(ptClassInfoEntity);
        classInfoService.bondClassParentId(ptClassInfoEntity);
        return buildSuccessJsonMsg("common.bound.success", null);
    }

    /**
     * 查询已选定的班级Ztree列表
     * @param classId
     * @return
     */
    @RequiresPermissions("gdmt_class_info")
    @RequestMapping("/queryClassInfoZtreeSelectList")
    @ResponseBody
    public List<ClassInfoTreeEntity> queryClassInfoZtreeSelectList(@RequestParam("classId") String classId){
        List<ClassInfoTreeEntity> list = classInfoService.queryClassInfoZtreeSelectList(classId);
        return list;
    }
    //@RequiresPermissions("gdmt_class_info")
    @RequestMapping("/lineSmoothed")
    public String report(){
        return "report/lineSmoothed";
    }

    @RequestMapping("/queryReport")
    @ResponseBody
    public String queryReport(){
        String str = " [ \t\t{ \t\t\t\"year\": \"1950\", \t\t\t\"value\": -0.307 \t\t}, \t\t{ \t\t\t\"year\": \"1951\", \t\t\t\"value\": -0.168 \t\t}, \t\t{ \t\t\t\"year\": \"1952\", \t\t\t\"value\": -0.073 \t\t}, \t\t{ \t\t\t\"year\": \"1953\", \t\t\t\"value\": -0.027 \t\t}, \t\t{ \t\t\t\"year\": \"1954\", \t\t\t\"value\": -0.251 \t\t}, \t\t{ \t\t\t\"year\": \"1955\", \t\t\t\"value\": -0.281 \t\t}, \t\t{ \t\t\t\"year\": \"1956\", \t\t\t\"value\": -0.348 \t\t}, \t\t{ \t\t\t\"year\": \"1957\", \t\t\t\"value\": -0.074 \t\t}, \t\t{ \t\t\t\"year\": \"1958\", \t\t\t\"value\": -0.011 \t\t}, \t\t{ \t\t\t\"year\": \"1959\", \t\t\t\"value\": -0.074 \t\t}, \t\t{ \t\t\t\"year\": \"1960\", \t\t\t\"value\": -0.124 \t\t}, \t\t{ \t\t\t\"year\": \"1961\", \t\t\t\"value\": -0.024 \t\t}, \t\t{ \t\t\t\"year\": \"1962\", \t\t\t\"value\": -0.022 \t\t}, \t\t{ \t\t\t\"year\": \"1963\", \t\t\t\"value\": 0 \t\t}, \t\t{ \t\t\t\"year\": \"1964\", \t\t\t\"value\": -0.296 \t\t}, \t\t{ \t\t\t\"year\": \"1965\", \t\t\t\"value\": -0.217 \t\t}, \t\t{ \t\t\t\"year\": \"1966\", \t\t\t\"value\": -0.147 \t\t}, \t\t{ \t\t\t\"year\": \"1967\", \t\t\t\"value\": -0.15 \t\t}, \t\t{ \t\t\t\"year\": \"1968\", \t\t\t\"value\": -0.16 \t\t}, \t\t{ \t\t\t\"year\": \"1969\", \t\t\t\"value\": -0.011 \t\t}, \t\t{ \t\t\t\"year\": \"1970\", \t\t\t\"value\": -0.068 \t\t}, \t\t{ \t\t\t\"year\": \"1971\", \t\t\t\"value\": -0.19 \t\t}, \t\t{ \t\t\t\"year\": \"1972\", \t\t\t\"value\": -0.056 \t\t}, \t\t{ \t\t\t\"year\": \"1973\", \t\t\t\"value\": 0.077 \t\t}, \t\t{ \t\t\t\"year\": \"1974\", \t\t\t\"value\": -0.213 \t\t}, \t\t{ \t\t\t\"year\": \"1975\", \t\t\t\"value\": -0.17 \t\t}, \t\t{ \t\t\t\"year\": \"1976\", \t\t\t\"value\": -0.254 \t\t}, \t\t{ \t\t\t\"year\": \"1977\", \t\t\t\"value\": 0.019 \t\t}, \t\t{ \t\t\t\"year\": \"1978\", \t\t\t\"value\": -0.063 \t\t}, \t\t{ \t\t\t\"year\": \"1979\", \t\t\t\"value\": 0.05 \t\t}, \t\t{ \t\t\t\"year\": \"1980\", \t\t\t\"value\": 0.077 \t\t}, \t\t{ \t\t\t\"year\": \"1981\", \t\t\t\"value\": 0.12 \t\t}, \t\t{ \t\t\t\"year\": \"1982\", \t\t\t\"value\": 0.011 \t\t}, \t\t{ \t\t\t\"year\": \"1983\", \t\t\t\"value\": 0.177 \t\t}, \t\t{ \t\t\t\"year\": \"1984\", \t\t\t\"value\": -0.021 \t\t}, \t\t{ \t\t\t\"year\": \"1985\", \t\t\t\"value\": -0.037 \t\t}, \t\t{ \t\t\t\"year\": \"1986\", \t\t\t\"value\": 0.03 \t\t}, \t\t{ \t\t\t\"year\": \"1987\", \t\t\t\"value\": 0.179 \t\t}, \t\t{ \t\t\t\"year\": \"1988\", \t\t\t\"value\": 0.18 \t\t}, \t\t{ \t\t\t\"year\": \"1989\", \t\t\t\"value\": 0.104 \t\t}, \t\t{ \t\t\t\"year\": \"1990\", \t\t\t\"value\": 0.255 \t\t}, \t\t{ \t\t\t\"year\": \"1991\", \t\t\t\"value\": 0.21 \t\t}, \t\t{ \t\t\t\"year\": \"1992\", \t\t\t\"value\": 0.065 \t\t}, \t\t{ \t\t\t\"year\": \"1993\", \t\t\t\"value\": 0.11 \t\t}, \t\t{ \t\t\t\"year\": \"1994\", \t\t\t\"value\": 0.172 \t\t}, \t\t{ \t\t\t\"year\": \"1995\", \t\t\t\"value\": 0.269 \t\t}, \t\t{ \t\t\t\"year\": \"1996\", \t\t\t\"value\": 0.141 \t\t}, \t\t{ \t\t\t\"year\": \"1997\", \t\t\t\"value\": 0.353 \t\t}, \t\t{ \t\t\t\"year\": \"1998\", \t\t\t\"value\": 0.548 \t\t}, \t\t{ \t\t\t\"year\": \"1999\", \t\t\t\"value\": 0.298 \t\t}, \t\t{ \t\t\t\"year\": \"2000\", \t\t\t\"value\": 0.267 \t\t}, \t\t{ \t\t\t\"year\": \"2001\", \t\t\t\"value\": 0.411 \t\t}, \t\t{ \t\t\t\"year\": \"2002\", \t\t\t\"value\": 0.462 \t\t}, \t\t{ \t\t\t\"year\": \"2003\", \t\t\t\"value\": 0.47 \t\t}, \t\t{ \t\t\t\"year\": \"2004\", \t\t\t\"value\": 0.445 \t\t}, \t\t{ \t\t\t\"year\": \"2005\", \t\t\t\"value\": 0.47 \t\t} \t]";
        return str;
    }


}
