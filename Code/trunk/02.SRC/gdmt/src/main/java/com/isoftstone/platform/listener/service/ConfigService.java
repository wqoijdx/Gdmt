package com.isoftstone.platform.listener.service;

import java.util.List;

import com.isoftstone.platform.model.entity.PtSysConfigEntity;

public interface ConfigService
{
    
    List<PtSysConfigEntity> queryConfigList();
    
}
