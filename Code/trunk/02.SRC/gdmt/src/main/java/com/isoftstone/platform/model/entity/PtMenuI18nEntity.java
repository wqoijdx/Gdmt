package com.isoftstone.platform.model.entity ;
import com.isoftstone.platform.entity.Columns;

import java.io.Serializable;


public class PtMenuI18nEntity implements Serializable
{
	@Columns("menu_id")
	private String menuId;

	@Columns("lang_id")
	private Integer langId;

	@Columns("menu_name")
	private String menuName;
	
	public String getMenuId() 
	{
		return menuId;
	}
	
	public void setMenuId(String menuId) 
	{
		this.menuId = menuId;
	}
	
	public Integer getLangId() 
	{
		return langId;
	}
	
	public void setLangId(Integer langId) 
	{
		this.langId = langId;
	}
	
	public String getMenuName() 
	{
		return menuName;
	}
	
	public void setMenuName(String menuName) 
	{
		this.menuName = menuName;
	}

	@Override
	public String toString() {
		return "PtMenuI18nEntity{" +
				"menuId='" + menuId + '\'' +
				", langId=" + langId +
				", menuName='" + menuName + '\'' +
				'}';
	}
}
