package com.isoftstone.obs.repository;


import com.isoftstone.obs.entity.SearchFileEntity;
import com.isoftstone.platform.entity.PagingBean;
import com.isoftstone.platform.model.entity.PtFileEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("diskDao")
public interface DiskDao {
    void insertFileInfo(@Param("entity") PtFileEntity item);

    List<PtFileEntity> queryFileListPaging(@Param("search") SearchFileEntity search, @Param("paging") PagingBean paging);

    Integer queryFileTotal(@Param("search") SearchFileEntity search);

    PtFileEntity queryFileInfoById(@Param("fileUuid") String fileUuid);

    void deleteFileByid(@Param("fileUuid") String fileUuid);

    void shareFile(@Param("fileUuid") String fileUuid);

    void modifyFileNameByFileUuid(@Param("entity") PtFileEntity ptFileEntityNew);

    void undeleteFileByid(@Param("fileUuid") String fileUuid);

    PtFileEntity queryDeleteFileInfoById(@Param("fileUuId") String fileUuId);

    void toPageFileByid(String fileUuid);

    List<PtFileEntity> queryDeleteFileListPaging(@Param("search") SearchFileEntity search,@Param("paging") PagingBean paging);


    List<PtFileEntity> querytoPageListPaging(@Param("search") SearchFileEntity search,@Param("paging") PagingBean paging);

    void unshareInfoById(@Param("fileUuid") String fileUuId);

    List<PtFileEntity> stoPageistPaging(@Param("search") SearchFileEntity search,@Param("paging") PagingBean paging);

    void stoPageListPaging(@Param("fileUuid") String fileUuid);
}
