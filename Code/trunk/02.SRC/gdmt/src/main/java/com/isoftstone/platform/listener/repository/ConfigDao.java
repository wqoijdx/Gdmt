package com.isoftstone.platform.listener.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.isoftstone.platform.model.entity.PtSysConfigEntity;

@Repository("configDao")
public interface ConfigDao
{
    
    List<PtSysConfigEntity> queryConfigList();
    
}
