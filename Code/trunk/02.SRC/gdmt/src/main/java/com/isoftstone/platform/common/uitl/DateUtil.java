package com.isoftstone.platform.common.uitl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil
{
    /**
     * 单位分钟
     * <一句话功能简述>
     * <功能详细描述>
     * @param dateTime
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static Integer getPassMinuteTime(Date dateTime)
    {
        if (dateTime == null)
        {
            return null;
        }
        
        Long milliseconds = Calendar.getInstance().getTime().getTime() - dateTime.getTime();
        
        return (int)(milliseconds / 1000 / 60);
    }

    public static String formatDate(Date lastModified) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(lastModified);
    }

    public static Date stringToDate(String startDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return simpleDateFormat.parse(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Long getMinuteInterval(String startDate, String endDate) {
        Date start = stringToDate(startDate);
        Date end = stringToDate(endDate);
        if(start == null || end == null){
            return 0l;
        }
        return (end.getTime()-start.getTime())/(1000*60);
    }
}
