package com.isoftstone.classinfo.repository;

import com.isoftstone.classinfo.entity.ClassInfoTreeEntity;
import com.isoftstone.classinfo.entity.SearchClassInfoEntity;
import com.isoftstone.platform.entity.PagingBean;
import com.isoftstone.platform.model.entity.PtClassInfoEntity;
import com.isoftstone.platform.tag.entity.SelectTagEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("classInfoDao")
public interface ClassInfoDao {
    List<ClassInfoTreeEntity> queryClassInfoZtreeList();

    List<PtClassInfoEntity> getClassInfolistByPage(@Param("search") SearchClassInfoEntity search, @Param("paging") PagingBean pagingBean);

    Integer getClassInfoListTotal(@Param("search") SearchClassInfoEntity search);

    void addClassInfo(@Param("entity") PtClassInfoEntity ptClassInfoEntity);

    List<SelectTagEntity> queryTagElementList();

    void deleteClassInfo(@Param("ids") String[] idArray);

    List<PtClassInfoEntity> queryClassInfoById(@Param("id") String id);

    void modifyClassInfoById(@Param("entity") PtClassInfoEntity ptClassInfoEntity);

    void bondClassParentId(@Param("entity") PtClassInfoEntity ptClassInfoEntity);
}
