package com.isoftstone.member.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.isoftstone.member.entity.ConsumeEntity;
import com.isoftstone.member.entity.IntegralEntity;
import com.isoftstone.member.entity.SearchMember;
import com.isoftstone.member.repository.ConsumeDao;
import com.isoftstone.member.service.ConsumeService;
import com.isoftstone.member.service.IntegralService;
import com.isoftstone.platform.common.uitl.DataUtil;
import com.isoftstone.platform.entity.PadingRstType;
import com.isoftstone.platform.entity.PagingBean;

@Service("consumeService")
public class ConsumeServiceImpl implements ConsumeService
{
    @Resource
    private ConsumeDao consumeDao;
    
    @Resource
    IntegralService integralService;
    
    @Value("#{propertiesConfig['itgrl.cffcnt']}")
    private String itgrlCffcnt = "2";
    
    @Override
    public void consumeMoney(ConsumeEntity consumeEntity)
    {
        IntegralEntity integralEntity = new IntegralEntity();
        integralEntity.setMemberId(consumeEntity.getMemberId());
        String integral = DataUtil.doubleDivide(consumeEntity.getAmount(), itgrlCffcnt);
        integralEntity.setIntegral(integral);
        consumeDao.consumeMoney(consumeEntity);
        
        integralService.increaseIntegral(integralEntity);
        
    }
    
    @Override
    public PadingRstType<ConsumeEntity> queryConsumeByPageList(SearchMember searchMember, PagingBean pagingBean)
    {
        
        pagingBean.deal(ConsumeEntity.class);
        List<ConsumeEntity> consumeBeanList = consumeDao.queryConsumeByPageList(searchMember, pagingBean);
        Integer total = consumeDao.queryConsumeTotal(searchMember);
        PadingRstType<ConsumeEntity> consumeBean = new PadingRstType<ConsumeEntity>();
        consumeBean.setPage(pagingBean.getPage());
        consumeBean.setTotal(total);
        consumeBean.putItems(ConsumeEntity.class, consumeBeanList);
        
        return consumeBean;
    }
}
