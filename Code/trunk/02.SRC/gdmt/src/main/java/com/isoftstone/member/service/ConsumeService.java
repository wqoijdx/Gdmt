package com.isoftstone.member.service;

import com.isoftstone.member.entity.ConsumeEntity;
import com.isoftstone.member.entity.SearchMember;
import com.isoftstone.platform.entity.PadingRstType;
import com.isoftstone.platform.entity.PagingBean;

public interface ConsumeService
{
    /**
     * 消费金额
     * <一句话功能简述>
     * <功能详细描述>
     * @param consumeEntity
     * @see [类、类#方法、类#成员]
     */
    void consumeMoney(ConsumeEntity consumeEntity);
    
    PadingRstType<ConsumeEntity> queryConsumeByPageList(SearchMember searchMember, PagingBean pagingBean);
    
}
