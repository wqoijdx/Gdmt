package com.isoftstone.member.service;

import java.util.List;

import com.isoftstone.member.entity.MemberEntity;
import com.isoftstone.member.entity.MemberSearch;
import com.isoftstone.platform.entity.PadingRstType;
import com.isoftstone.platform.entity.PagingBean;

public interface MemberService
{
    /**
     * 增加会员
     * <一句话功能简述>
     * <功能详细描述>
     * @param memberEntity
     * @see [类、类#方法、类#成员]
     */
    void addMemberDail(MemberEntity memberEntity);
    
    /**
     * 分页查询会员信息
     * <一句话功能简述>
     * <功能详细描述>
     * @param memberEntity
     * @param pagingBean
     * @return
     * @see [类、类#方法、类#成员]
     */
    PadingRstType<MemberEntity> queryMemberByPageList(MemberSearch search, PagingBean pagingBean);
    
    /**
     * 更新会员信息
     * <一句话功能简述>
     * <功能详细描述>
     * @param memberEntity
     * @see [类、类#方法、类#成员]
     */
    void updateMemberDail(MemberEntity memberEntity);
    
    /**
     * 删除会员信息
     * <一句话功能简述>
     * <功能详细描述>
     * @param ids
     * @see [类、类#方法、类#成员]
     */
    void deleteMemberInfo(String ids);
    
    /**
     * 根据id,查询会员信息
     * <一句话功能简述>
     * <功能详细描述>
     * @param memberId
     * @return
     * @see [类、类#方法、类#成员]
     */
    List<MemberEntity> queryMemberListById(String id);
    
}
