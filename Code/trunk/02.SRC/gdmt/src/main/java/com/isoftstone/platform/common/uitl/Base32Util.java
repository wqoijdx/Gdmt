package com.isoftstone.platform.common.uitl;

import java.math.BigInteger;
import java.security.MessageDigest;

public class Base32Util {

    public static  String getBase32Deal(String srcStr){
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            byte[] bytes = srcStr.getBytes();
            messageDigest.update(bytes);
            String rstStr = new BigInteger(1, messageDigest.digest()).toString(36);
            return rstStr;
        } catch (Exception e) {
            throw new RuntimeException("Could not hash input string.", e);
        }
    }


    public static void main(String[] args) {
        if(args.length < 1){
            System.out.println("please the version for grade :<version> \r\n for exampale:5.1.1");
            return;
        }
        System.out.println("[input] gradle version is:  " + args[0]);
        String desc="https://services.gradle.org/distributions/gradle-"+args[0]+"-all.zip";
        String gradleBase64 = getBase32Deal(desc);
        System.out.println("[out] gradle " + args[0] + " Base64  is:  ");
        System.out.println(gradleBase64);

    }
}
