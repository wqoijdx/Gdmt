package com.isoftstone.member.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.isoftstone.member.entity.ConsumeEntity;
import com.isoftstone.member.entity.MemberEntity;
import com.isoftstone.member.entity.SearchMember;
import com.isoftstone.member.service.ConsumeService;
import com.isoftstone.member.service.MemberService;
import com.isoftstone.platform.controller.BaseController;
import com.isoftstone.platform.entity.PadingRstType;
import com.isoftstone.platform.entity.PagingBean;

@Controller
@RequestMapping("/consume")
public class ConsumeController extends BaseController
{
    
    private final static Logger logger = Logger.getLogger(ConsumeController.class);
    
    @Resource
    private MemberService memberService;
    
    @Resource
    private ConsumeService consumeService;
    
    @RequestMapping("/consumeDail")
    @RequiresPermissions("gdmt_data_MENU_101")
    public String initConsumeDail(Model model, HttpServletRequest request)
    {
        Object memberId = request.getParameter("memberId");
        model.addAttribute("memberId", memberId);
        List<MemberEntity> memberList = memberService.queryMemberListById("" + memberId);
        if (memberList.size() > 0)
        {
            model.addAttribute("memberInfo", memberList.get(0));
        }
        return "member/consumeDail";
    }
    
    @RequestMapping(value = "/getConsumelist", produces = "application/json")
    @ResponseBody
    public String getConsumelist(SearchMember searchMember, PagingBean pagingBean)
    {
        logger.info(searchMember);
        PadingRstType<ConsumeEntity> rst = consumeService.queryConsumeByPageList(searchMember, pagingBean);
        
        return JSON.toJSONString(rst);
    }
    
    @RequestMapping(value = "/consumeMoney", produces = "application/json")
    @ResponseBody
    public String consumeMoney(ConsumeEntity consumeEntity)
    {
        logger.info(consumeEntity);
        consumeService.consumeMoney(consumeEntity);
        return buildSuccessJsonMsg("warehouse.add.sucess", null);
    }
    
}
