package com.isoftstone.member.entity;

public class SearchMember
{
    /**
     * 月份
     */
    private String month;
    
    /**
     * 会员Id
     */
    private String memberId;
    
    public String getMonth()
    {
        return month;
    }
    
    public void setMonth(String month)
    {
        this.month = month;
    }
    
    public String getMemberId()
    {
        return memberId;
    }
    
    public void setMemberId(String memberId)
    {
        this.memberId = memberId;
    }
    
}
