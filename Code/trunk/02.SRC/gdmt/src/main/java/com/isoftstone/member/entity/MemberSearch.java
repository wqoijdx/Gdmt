package com.isoftstone.member.entity;

import java.io.Serializable;

public class MemberSearch implements Serializable
{
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 8121701831121615606L;
    
    private String id;
    
    /**
     * 会员卡号
     */
    private String memberId;
    
    /**
     * 姓名
     */
    private String name;
    
    /**
     * 电话
     */
    private String tel;
    
    public String getId()
    {
        return id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getTel()
    {
        return tel;
    }
    
    public void setTel(String tel)
    {
        this.tel = tel;
    }
    
    public String getMemberId()
    {
        return memberId;
    }
    
    public void setMemberId(String memberId)
    {
        this.memberId = memberId;
    }
    
}
