package com.isoftstone.platform.tag;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.isoftstone.platform.common.MessageHolder;
import com.isoftstone.platform.model.entity.PtDictionaryEntity;
import com.isoftstone.platform.tag.service.DctnrService;

/*
 * 自定义标签实现类
 */
public class DctnrTag extends SimpleTagSupport
{
    
    Logger logger = Logger.getLogger(DctnrTag.class.getName());
    
    private String id;
    
    private String classez;
    
    private String style;
    
    private String name;
    
    private String group;
    
    public String getId()
    {
        return id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getClassez()
    {
        return classez;
    }
    
    public void setClassez(String classez)
    {
        this.classez = classez;
    }
    
    public String getStyle()
    {
        return style;
    }
    
    public void setStyle(String style)
    {
        this.style = style;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getGroup()
    {
        return group;
    }
    
    public void setGroup(String group)
    {
        this.group = group;
    }
    
    private DctnrService dctnrService;
    
    private MessageHolder messageHolder;
    
    @Override
    public void doTag()
        throws JspException, IOException
    {
        
        String language = (String)getJspContext().getAttribute("siteLanguage");
        logger.info("language" + language);
        
        PageContext pag = (PageContext)getJspContext();
        ServletContext sc = pag.getServletContext();
        
        language = (String)pag.getSession().getAttribute("siteLanguage");
        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(sc);
        dctnrService = webApplicationContext.getBean("dctnrService", DctnrService.class);
        
        messageHolder = webApplicationContext.getBean("messageHolder", MessageHolder.class);
        
        List<PtDictionaryEntity> list = dctnrService.queryDicCodeListByeGroup(group, language);
        logger.info("list" + list);
        JspWriter out = getJspContext().getOut();
        out.write("<select ");
        if (id != null)
        {
            out.write(" id='");
            out.write(id);
            out.write("'");
        }
        
        if (classez != null)
        {
            out.write(" class='");
            out.write(classez);
            out.write("'");
        }
        
        if (style != null)
        {
            out.write(" style='");
            out.write(style);
            out.write("'");
        }
        if (name != null)
        {
            out.write(" name='");
            out.write(name);
            out.write("'");
        }
        out.write("<option value=''>");
        out.write(messageHolder.getMessage("common.select"));
        out.write("</option>");
        for (PtDictionaryEntity PtDictionary1entity : list)
        {
            out.write("<option value='");
            out.write(PtDictionary1entity.getDicCode());
            out.write("'>");
            out.write(PtDictionary1entity.getDictName());
            out.write("</option>");
        }
        out.write("</select>");
    }
}
