package com.isoftstone.platform.tag.service;

import java.util.List;

import com.isoftstone.platform.model.entity.PtDictionaryEntity;

public interface DctnrService
{
    
    /**
     * 根据group查询字典的信息
     * 
     * @param group
     * @param language
     * @return
     * @see [类、类#方法、类#成员]
     */
    List<PtDictionaryEntity> queryDicCodeListByeGroup(String group, String language);
    
}
