package com.isoftstone.platform.tag.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.isoftstone.platform.model.entity.PtDictionaryEntity;

@Repository("dctnrDao")
public interface DctnrDao
{
    
    List<PtDictionaryEntity> queryDicCodeListByeGroup(@Param("group")
    String group, @Param("lang")
    String language);
    
}
