package com.isoftstone.platform.shiro;

import org.apache.shiro.authc.UsernamePasswordToken;

public class CaptchaUsernamePasswordToken extends UsernamePasswordToken
{
    
    /**
     * ��֤��
     **/
    private String captcha;
    
    /**
     * 
     */
    private static final long serialVersionUID = 2873488723127579539L;
    
    public CaptchaUsernamePasswordToken()
    {
        super();
    }
    
    public CaptchaUsernamePasswordToken(String username, String password, String captcha)
    {
        super(username, password);
        this.captcha = captcha;
        
    }
    
    public String getCaptcha()
    {
        return captcha;
    }
    
    public void setCaptcha(String captcha)
    {
        this.captcha = captcha;
    }
    
}
