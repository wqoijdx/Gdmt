package com.isoftstone.platform.sm.service;

import java.util.List;

import com.isoftstone.platform.model.entity.PtMenuEntity;
import com.isoftstone.platform.model.entity.PtUserEntity;
import com.isoftstone.platform.sm.entity.PasswdEntity;
import com.isoftstone.platform.sm.entity.SysLeveMenu;

public interface UserInfoService
{
    
    PtUserEntity autoUserInfo(String userName, String password);
    
    boolean updatePwd(PasswdEntity passwdEntity);
    
    List<SysLeveMenu> getMenu(String language, String uuid);
    
    List<PtMenuEntity> selectMenu(String siteLanguage, String userUuid);
    
}
