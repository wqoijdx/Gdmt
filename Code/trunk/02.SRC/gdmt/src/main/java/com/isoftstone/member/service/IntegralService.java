package com.isoftstone.member.service;

import com.isoftstone.member.entity.IntegralEntity;
import com.isoftstone.member.entity.SearchMember;
import com.isoftstone.platform.entity.PadingRstType;
import com.isoftstone.platform.entity.PagingBean;

public interface IntegralService
{
    /**
     * 增加积分
     * <一句话功能简述>
     * <功能详细描述>
     * @param integralEntity 
     * @see [类、类#方法、类#成员]
     */
    void increaseIntegral(IntegralEntity integralEntity);
    
    /**
     * 分员查询积分
     * <一句话功能简述>
     * <功能详细描述>
     * @param searchMember
     * @param pagingBean
     * @return
     * @see [类、类#方法、类#成员]
     */
    PadingRstType<IntegralEntity> queryIntegralByPageList(SearchMember searchMember, PagingBean pagingBean);
    
}
