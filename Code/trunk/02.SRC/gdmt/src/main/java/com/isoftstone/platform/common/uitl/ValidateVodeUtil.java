package com.isoftstone.platform.common.uitl;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

import javax.imageio.ImageIO;

public class ValidateVodeUtil
{
    
    /**
     * 默认图片宽度
     */
    private static final int DEFAULT_WIDTH = 90;
    
    /**
     * 默认图片高度
     */
    private static final int DEFAULT_HEIGHT = 23;
    
    /**
     * 默认干扰线数量
     */
    private static final int DEFAULT_LINESIZE = 40;
    
    /**
     * 默认随机产生字符数量
     */
    private static final int DEFAULT_STRINGNUM = 5;
    
    /**
     * 随机产生的字符串
     */
    private static String randString = "23456789ABCDEFGHJKLMNPQRSTUVWXYZ";
    
    /**
     * 产生随机数
     */
    private static Random random = new Random();
    
    public static String genValidateVode(OutputStream ops)
        throws IOException
    {
        
        // BufferedImage类是具有缓冲区的Image类,Image类是用于描述图像信息的类
        BufferedImage image = new BufferedImage(DEFAULT_WIDTH, DEFAULT_HEIGHT, BufferedImage.TYPE_INT_BGR);
        
        // 产生Image对象的Graphics对象,改对象可以在图像上进行各种绘制操作
        Graphics g = image.getGraphics();
        
        g.fillRect(0, 0, DEFAULT_WIDTH, DEFAULT_HEIGHT);
        g.setFont(new Font("Times New Roman", Font.ROMAN_BASELINE, 18));
        g.setColor(getRandColor(110, 133));
        
        // 绘制干扰线
        for (int i = 0; i <= DEFAULT_LINESIZE; i++)
        {
            drowLine(g);
        }
        
        // 绘制随机字符
        String randomString = "";
        for (int i = 1; i <= DEFAULT_STRINGNUM; i++)
        {
            randomString = drowString(g, randomString, i);
        }
        
        g.dispose();
        ImageIO.write(image, "JPEG", ops);
        
        return randomString;
        
    }
    
    /**
     * 绘制干扰线
     */
    private static void drowLine(Graphics g)
    {
        int x = random.nextInt(DEFAULT_WIDTH);
        int y = random.nextInt(DEFAULT_HEIGHT);
        int xl = random.nextInt(13);
        int yl = random.nextInt(15);
        g.drawLine(x, y, x + xl, y + yl);
    }
    
    /**
     * 获取随机的字符
     */
    public static String getRandomString(int num)
    {
        return String.valueOf(randString.charAt(num));
    }
    
    /**
     * 绘制字符串
     */
    private static String drowString(Graphics g, String randomString, int i)
    {
        g.setFont(getFont());
        g.setColor(new Color(random.nextInt(101), random.nextInt(111), random.nextInt(121)));
        String rand = String.valueOf(getRandomString(random.nextInt(randString.length())));
        randomString += rand;
        g.translate(random.nextInt(3), random.nextInt(3));
        g.drawString(rand, 13 * i, 16);
        return randomString;
    }
    
    /**
     * 获得字体
     */
    private static Font getFont()
    {
        return new Font("Fixedsys", Font.CENTER_BASELINE, 16);
    }
    
    /**
     * 获得颜色
     */
    private static Color getRandColor(int fc, int bc)
    {
        if (fc > 255)
            fc = 255;
        if (bc > 255)
            bc = 255;
        int r = fc + random.nextInt(bc - fc - 16);
        int g = fc + random.nextInt(bc - fc - 14);
        int b = fc + random.nextInt(bc - fc - 18);
        return new Color(r, g, b);
    }


    public static String numString()
    {
        String randomString="";
        for (int i = 1; i <= DEFAULT_STRINGNUM; i++) {
            String rand = String.valueOf(getRandomString(random.nextInt(randString.length())));
            randomString += rand;
        }
        return randomString;
    }


}
