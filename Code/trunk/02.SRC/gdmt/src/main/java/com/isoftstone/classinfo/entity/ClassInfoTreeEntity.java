package com.isoftstone.classinfo.entity;

import java.io.Serializable;
//ObjectStream
public class ClassInfoTreeEntity  implements Serializable {
    private Integer id = 0;
    private Boolean checked = false;
    private Integer pId = 0;
    private Boolean open = true;
    private String name = "";

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Integer getpId() {
        return pId;
    }

    public void setpId(Integer pId) {
        this.pId = pId;
    }

    public Boolean getOpen() {
        return open;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ClassInfoTreeEntity{" +
                "id=" + id +
                ", checked=" + checked +
                ", pId=" + pId +
                ", open=" + open +
                ", name='" + name + '\'' +
                '}';
    }
}
