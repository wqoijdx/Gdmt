package com.isoftstone.platform.model.entity ;
import com.isoftstone.platform.entity.Columns;

import java.io.Serializable;


public class PtRoleUserEntity implements Serializable
{
	@Columns("USER_UUID")
	private String userUuid;

	@Columns("ROLE_UUID")
	private String roleUuid;
	
	public String getUserUuid() 
	{
		return userUuid;
	}
	
	public void setUserUuid(String userUuid) 
	{
		this.userUuid = userUuid;
	}
	
	public String getRoleUuid() 
	{
		return roleUuid;
	}
	
	public void setRoleUuid(String roleUuid) 
	{
		this.roleUuid = roleUuid;
	}

	@Override
	public String toString() {
		return "PtRoleUserEntity{" +
				"userUuid='" + userUuid + '\'' +
				", roleUuid='" + roleUuid + '\'' +
				'}';
	}
}
