package com.isoftstone.platform.sm.controller;

import com.isoftstone.platform.common.constant.PlatformConstant;
import com.isoftstone.platform.common.uitl.ValidateVodeUtil;
import com.isoftstone.platform.controller.BaseController;
import com.isoftstone.platform.model.entity.PtUserEntity;
import com.isoftstone.platform.sm.entity.PasswdEntity;
import com.isoftstone.platform.sm.entity.SysLeveMenu;
import com.isoftstone.platform.sm.service.UserInfoService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Controller
public class LogginController extends BaseController
{
    
    /**
     * 验证码的有效时间
     */
    private final static Integer EFFECTIVE_TIME = 10;
    
    @Value("#{propertiesConfig['default.lang']}")
    private String defaultLang = "zh";
    
    @Value("#{propertiesConfig['https.port']}")
    private String httpsPort = "18443";
    
    //is.https
    @Value("#{propertiesConfig['is.https']}")
    private boolean isHttps = false;
    
    @Resource
    private UserInfoService userInfoService;
    
    private String url = null;
    
    @RequestMapping("/sm/login")
    public String initPage(HttpServletRequest request)
    {
        userInfoService.autoUserInfo("admin", "123456");
        String siteLanguage = request.getParameter("siteLanguage");
        
        if (siteLanguage == null)
        {
            siteLanguage = defaultLang;
        }
        HttpSession session = request.getSession();
        session.setAttribute("siteLanguage", siteLanguage);
        
        if (isHttps)
        {
            String scheme = request.getScheme();
            if ("http".equals(scheme))
            {
                
                String contextPath = request.getContextPath();
                url = "https://" + request.getServerName() + ":" + httpsPort + contextPath + "/sm/login";
                
                return "redirect:" + url;
            }
        }
        
        return "sm/login";
    }
    
    @RequestMapping("/session")
    public String initSessionPage()
    {
        return "sm/session";
    }
    
    @RequestMapping("/sm/logout")
    public String initLogoutPage(ServletRequest request)
    {
        HttpServletRequest hrequest = (HttpServletRequest)request;
        HttpSession session = hrequest.getSession();
        
        session.setAttribute("userInfo", null);
        return "redirect:sm/login";
    }
    
    //账号登录
    @RequestMapping(value = "/sm/vldtlogon", method = RequestMethod.POST)
    public String vldtlogin(HttpServletRequest request)
    {
        if (SecurityUtils.getSubject().isAuthenticated())
        {
            
                return "redirect:/main";
        }
        request.setAttribute("tip", this.messageHolder.getMessage("verification.failure", null, "verification.failure"));
        
        return "/sm/login";
        
    }
    
    @RequestMapping(value = "/sm/vldtlogon", method = RequestMethod.GET)
    public String vldtlogonGet()
    {
        return "redirect:/sm/login";
        
    }
    
    @RequestMapping(value = "/captchaCode")
    public void genValidateVode(HttpServletRequest request, HttpServletResponse response)
        throws IOException
    {
        
        HttpSession session = request.getSession();
        
        String randomString = ValidateVodeUtil.genValidateVode(response.getOutputStream());
        session.removeAttribute(PlatformConstant.CAPTCHA_CODE_KEY);
        session.removeAttribute(PlatformConstant.CAPTCHA_CODE_GEN_TIME_KEY);
        
        session.setAttribute(PlatformConstant.CAPTCHA_CODE_KEY, randomString);
        session.setAttribute(PlatformConstant.CAPTCHA_CODE_GEN_TIME_KEY, new Date());
        
    }
    
    @RequestMapping("/main")
    public String main(HttpServletRequest request)
    {
        
        HttpSession session = request.getSession();
        String siteLanguage = (String)session.getAttribute("siteLanguage");
        if (siteLanguage == null)
        {
            siteLanguage = defaultLang;
        }
        PtUserEntity userEntity = (PtUserEntity)session.getAttribute("userInfo");
        List<SysLeveMenu> level = userInfoService.getMenu(siteLanguage, userEntity.getUserUuid());
        session.setAttribute("level", level);
        return "/sm/main";
    }
    
    @RequestMapping(value = "/updatePwd", produces = "application/json")
    @ResponseBody
    public String updatePwd(PasswdEntity passwdEntity)
    {
        boolean rst = userInfoService.updatePwd(passwdEntity);
        if (rst)
        {
            return buildSuccessJsonMsg("commmon.passwd.success", null);
        }
        return buildFailJsonMsg("commmon.passwd.fail", null);
    }
    
    @RequestMapping(value = "/shiro/privilege")
    public String privilege()
    {
        return "/shiro/privilege";
    }

    @RequestMapping("/sm/getServiceMenu")
    @ResponseBody
    public List<SysLeveMenu> getServiceMenu(HttpServletRequest request)
    {
        String menuId =request.getParameter("menuId");
        HttpSession session = request.getSession();
        List<SysLeveMenu> SysLeveMenuList = (List<SysLeveMenu>) session.getAttribute("level");
        for(SysLeveMenu menu:SysLeveMenuList){
            if(menuId.equals(menu.getMenuId())){
                return menu.getChildren();
            }
        }
        return null;
    }

    @RequestMapping(value = "/registerpage")
    public String getRegisterPage(){
        return "redirect:/sm/register";
    }

}
