package com.isoftstone.obs.service;


import com.isoftstone.obs.entity.SearchFileEntity;
import com.isoftstone.obs.entity.ShareFileEntity;
import com.isoftstone.platform.entity.PadingRstType;
import com.isoftstone.platform.entity.PagingBean;
import com.isoftstone.platform.model.entity.PtFileEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.http.ResponseEntity;


import java.io.InputStream;

public interface DiskService {
    void sysnchObjData();

    PadingRstType<PtFileEntity> queryFileListPaging(SearchFileEntity search, PagingBean paging);

    ResponseEntity<byte[]> downloadFile(String fileUuid) ;

    void uploadFile(String name, InputStream inputStream);

    String shareFile(ShareFileEntity shareFileEntity);

    ResponseEntity<byte[]> shareQRCode(String shareCode);


    String getLinkCopy(String obsKey);

    void deleteFileByid(String fileUuid);

    PtFileEntity queryClassInfoById(String fileUuId);

    void modifyFileNameByFileUuid(PtFileEntity ptFileEntity);

    void undeleteFileByid(String fileUuid);

    void toPageFileByid(String fileUuid);

    PtFileEntity queryDeleteClassInfoById(String fileUuId);

    PadingRstType<PtFileEntity> queryDeleteFileListPaging(SearchFileEntity search, PagingBean paging);

    void copy(String fileId,String fileName);



    PadingRstType<PtFileEntity> querytoPageListPaging(SearchFileEntity search, PagingBean paging);

    void unshareInfoById(@Param("fileUuId") String fileUuId);

    PadingRstType<PtFileEntity> stoPageistPaging( SearchFileEntity search,  PagingBean paging);

    void stoPageListPaging(@Param("fileUuid") String fileUuid);
}
