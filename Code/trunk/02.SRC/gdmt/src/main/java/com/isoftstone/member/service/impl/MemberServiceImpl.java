package com.isoftstone.member.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.isoftstone.member.entity.MemberEntity;
import com.isoftstone.member.entity.MemberSearch;
import com.isoftstone.member.repository.MemberDao;
import com.isoftstone.member.service.MemberService;
import com.isoftstone.platform.entity.PadingRstType;
import com.isoftstone.platform.entity.PagingBean;

@Service("memberService")
public class MemberServiceImpl implements MemberService
{
    @Resource
    MemberDao memberDao;
    
    @Override
    public void addMemberDail(MemberEntity memberEntity)
    {
        memberDao.addMemberDail(memberEntity);
    }
    
    @Override
    public PadingRstType<MemberEntity> queryMemberByPageList(MemberSearch search, PagingBean pagingBean)
    {
        pagingBean.deal(MemberEntity.class);
        List<MemberEntity> memberBeanList = memberDao.queryMemberByPageList(search, pagingBean);
        //        log.info(JSON.toJSONString(warehouseBeanList));
        Integer total = memberDao.queryMemberTotal(search);
        PadingRstType<MemberEntity> memberBean = new PadingRstType<MemberEntity>();
        memberBean.setPage(pagingBean.getPage());
        memberBean.setTotal(total);
        memberBean.putItems(MemberEntity.class, memberBeanList);
        
        return memberBean;
    }
    
    @Override
    public void updateMemberDail(MemberEntity memberEntity)
    {
        memberDao.updateMemberDail(memberEntity);
        
    }
    
    @Override
    public void deleteMemberInfo(String ids)
    {
        String[] idsArray = ids.split(",");
        memberDao.deleteMemberInfo(idsArray);
        
    }
    
    @Override
    public List<MemberEntity> queryMemberListById(String id)
    {
        List<MemberEntity> memberList = memberDao.queryMemberListById(id);
        return memberList;
    }
}
