package com.isoftstone.platform.common.uitl;

import java.math.BigDecimal;

public class FileUtil {
    /**
     * 文件后缀符
     */
    private static final String FILE_SFF_SMBL = ".";

    /**
     * 文件分隔符1
     */
    public static final String FILE_SPRT_SMBL2 = "/";

    /**
     * 文件分隔符2
     */
    public static final String FILE_SPRT_SMBL1 = "\\";

    /**
     * 文件结尾符
     */
    public static final String FILE_SPRT_END1 = "\r\n";

    /**
     * 文件结尾符
     */
    public static final String FILE_SPRT_END2 = "\n";

    public static String getFileName(String filePath)
    {
        if (null == filePath || "".equals(filePath))
        {
            return null;
        }
        String filePath1 = filePath.replace(FILE_SPRT_SMBL1, FILE_SPRT_SMBL2);
        int startIndex = filePath1.lastIndexOf(FILE_SPRT_SMBL2);
        int stopIndex = filePath1.indexOf(FILE_SFF_SMBL);
        String fileName = filePath1.substring(startIndex + 1);
        return fileName;
    }

    public static String clcltn(float v) {
        BigDecimal bigDecimal = new BigDecimal(v);
        float rst = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
        return  rst + "";
    }

    public static void main(String[] args) {
        System.out.println(clcltn(826.11426f));
    }
}
