package com.isoftstone.platform.model.entity;
import java.io.Serializable;
import java.util.Objects;

import com.isoftstone.platform.entity.Columns;


public class PtFileEntity implements Serializable
{
	@Columns("file_uuid")
	private String fileUuid;

	@Columns("parent_uuid")
	private String parentUuid;

	@Columns("file_type")
	private Integer fileType;

	@Columns("file_capacity")
	private String fileCapacity;

	@Columns("file_name")
	private String fileName;

	@Columns("user_uuid")
	private String userUuid;

	@Columns("file_path")
	private String filePath;

	@Columns("remarks")
	private String remarks;

	@Columns("MODTIME")
	private String modtime;

	@Columns("DEL_FLAG")
	private Integer delFlag;
	
	public String getFileUuid() 
	{
		return fileUuid;
	}
	
	public void setFileUuid(String fileUuid) 
	{
		this.fileUuid = fileUuid;
	}
	
	public String getParentUuid() 
	{
		return parentUuid;
	}
	
	public void setParentUuid(String parentUuid) 
	{
		this.parentUuid = parentUuid;
	}
	
	public Integer getFileType() 
	{
		return fileType;
	}
	
	public void setFileType(Integer fileType) 
	{
		this.fileType = fileType;
	}
	
	public String getFileCapacity() 
	{
		return fileCapacity;
	}
	
	public void setFileCapacity(String fileCapacity) 
	{
		this.fileCapacity = fileCapacity;
	}
	
	public String getFileName() 
	{
		return fileName;
	}
	
	public void setFileName(String fileName) 
	{
		this.fileName = fileName;
	}
	
	public String getUserUuid() 
	{
		return userUuid;
	}
	
	public void setUserUuid(String userUuid) 
	{
		this.userUuid = userUuid;
	}
	
	public String getFilePath() 
	{
		return filePath;
	}
	
	public void setFilePath(String filePath) 
	{
		this.filePath = filePath;
	}
	
	public String getRemarks() 
	{
		return remarks;
	}
	
	public void setRemarks(String remarks) 
	{
		this.remarks = remarks;
	}
	
	public String getModtime() 
	{
		return modtime;
	}
	
	public void setModtime(String modtime) 
	{
		this.modtime = modtime;
	}
	
	public Integer getDelFlag() 
	{
		return delFlag;
	}
	
	public void setDelFlag(Integer delFlag) 
	{
		this.delFlag = delFlag;
	}

	@Override
	public String toString() {
		return "PtFileEntity{" +
				"fileUuid='" + fileUuid + '\'' +
				", parentUuid='" + parentUuid + '\'' +
				", fileType=" + fileType +
				", fileCapacity='" + fileCapacity + '\'' +
				", fileName='" + fileName + '\'' +
				", userUuid='" + userUuid + '\'' +
				", filePath='" + filePath + '\'' +
				", remarks='" + remarks + '\'' +
				", modtime='" + modtime + '\'' +
				", delFlag=" + delFlag +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		PtFileEntity that = (PtFileEntity) o;
		return Objects.equals(filePath, that.filePath);
	}

	@Override
	public int hashCode() {
		return Objects.hash(filePath);
	}
}
