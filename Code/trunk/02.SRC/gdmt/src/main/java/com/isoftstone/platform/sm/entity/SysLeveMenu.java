package com.isoftstone.platform.sm.entity;

import com.isoftstone.platform.entity.Columns;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SysLeveMenu implements Serializable
{
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 2291208319604635760L;
    
    private String menuName;
    
    private String menuId;
    
    private String parentId;
    
    private String menuUrl;

    private String menuIcon;
    
    @Columns("menu.menu_privilege")
    private String menuPrivilege;
    
    private List<SysLeveMenu> children = new ArrayList<SysLeveMenu>();
    
    public String getMenuName()
    {
        return menuName;
    }
    
    public void setMenuName(String menuName)
    {
        this.menuName = menuName;
    }

    @Override
    public String toString() {
        return "SysLeveMenu{" +
                "menuName='" + menuName + '\'' +
                ", menuId='" + menuId + '\'' +
                ", parentId='" + parentId + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", menuIcon='" + menuIcon + '\'' +
                ", menuPrivilege='" + menuPrivilege + '\'' +
                ", children=" + children +
                '}';
    }

    public String getMenuUrl()
    {
        return menuUrl;
    }
    
    public void setMenuUrl(String menuUrl)
    {
        this.menuUrl = menuUrl;
    }
    
    public List<SysLeveMenu> getChildren()
    {
        return children;
    }
    
    public void setChildren(List<SysLeveMenu> children)
    {
        this.children = children;
    }
    
    public void addChildren(SysLeveMenu sysLeveMenu)
    {
        children.add(sysLeveMenu);
    }
    
    public String getMenuPrivilege()
    {
        return menuPrivilege;
    }
    
    public void setMenuPrivilege(String menuPrivilege)
    {
        this.menuPrivilege = menuPrivilege;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
}
