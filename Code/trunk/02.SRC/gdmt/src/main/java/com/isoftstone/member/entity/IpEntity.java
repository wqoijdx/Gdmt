package com.isoftstone.member.entity;

public class IpEntity
{
    private int id;
    private String memberId;
    private String name;
    private String tel;
    private float integral;
    private String updateTime;
    private String remarks;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getMemberId()
    {
        return memberId;
    }

    public void setMemberId(String memberId)
    {
        this.memberId = memberId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getTel()
    {
        return tel;
    }

    public void setTel(String tel)
    {
        this.tel = tel;
    }

    public float getIntegral()
    {
        return integral;
    }

    public void setIntegral(float integral)
    {
        this.integral = integral;
    }

    public String getUpdateTime()
    {
        return updateTime;
    }

    public void setUpdateTime(String updateTime)
    {
        this.updateTime = updateTime;
    }

    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    @Override
    public String toString()
    {
        return "IpEntity{" +
            "id=" + id +
            ", memberId='" + memberId + '\'' +
            ", name='" + name + '\'' +
            ", tel='" + tel + '\'' +
            ", integral=" + integral +
            ", updateTime='" + updateTime + '\'' +
            ", remarks='" + remarks + '\'' +
            '}';
    }
}
