package com.isoftstone.platform.controller;

import com.isoftstone.platform.common.MessageHolder;
import com.isoftstone.platform.common.uitl.PlatformUtil;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.Map;

public class BaseController
{
    @Resource
    public MessageHolder messageHolder;
    
    protected String buildSuccessJsonMsg(String msg, Object ... args)
    {
        Map<String, Object> message = new LinkedHashMap<>();
        String msgStr = this.messageHolder.getMessage(msg, args, msg);
        message.put("success", true);
        message.put("msg", msgStr);
        message.put("description", msgStr);
        String rst = PlatformUtil.toJSON(message);
        return rst;
    }
    
    protected String buildFailJsonMsg(String msg, Object[] args)
    {
        Map<String, Object> message = new LinkedHashMap<>();
        String msgStr = this.messageHolder.getMessage(msg, args, msg);
        message.put("success", false);
        message.put("msg", msgStr);
        message.put("description", msgStr);
        String rst = PlatformUtil.toJSON(message);
        return rst;
    }


    //lcg

    
}
