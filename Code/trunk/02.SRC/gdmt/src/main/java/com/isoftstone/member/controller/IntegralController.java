package com.isoftstone.member.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.isoftstone.member.entity.IntegralEntity;
import com.isoftstone.member.entity.MemberEntity;
import com.isoftstone.member.entity.SearchMember;
import com.isoftstone.member.service.ConsumeService;
import com.isoftstone.member.service.IntegralService;
import com.isoftstone.member.service.MemberService;
import com.isoftstone.platform.common.uitl.DataUtil;
import com.isoftstone.platform.controller.BaseController;
import com.isoftstone.platform.entity.PadingRstType;
import com.isoftstone.platform.entity.PagingBean;

@Controller
@RequestMapping("/integral")
public class IntegralController extends BaseController
{
    
    private final static Logger logger = Logger.getLogger(IntegralController.class);
    
    @Resource
    private MemberService memberService;
    
    @Resource
    private ConsumeService consumeService;
    
    @Resource
    private IntegralService integralService;
    
    @RequestMapping("/integralDail")
    @RequiresPermissions("gdmt_data_MENU_102")
    public String initIntegralDail(Model model, HttpServletRequest request)
    {
        Object memberId = request.getParameter("memberId");
        model.addAttribute("memberId", memberId);
        List<MemberEntity> memberList = memberService.queryMemberListById("" + memberId);
        if (memberList.size() > 0)
        {
            model.addAttribute("memberInfo", memberList.get(0));
        }
        return "member/integralDail";
    }
    
    @RequestMapping(value = "/getIntegrallist", produces = "application/json")
    @ResponseBody
    public String getIntegrallist(SearchMember searchMember, PagingBean pagingBean)
    {
        logger.info(searchMember);
        PadingRstType<IntegralEntity> rst = integralService.queryIntegralByPageList(searchMember, pagingBean);
        
        return JSON.toJSONString(rst);
    }
    
    @RequestMapping(value = "/exchangeIntegral", produces = "application/json")
    @ResponseBody
    public String exchangeIntegral(IntegralEntity integralEntity)
    {
        logger.info(integralEntity);
        
        String integral = DataUtil.doubleInverse(integralEntity.getIntegral());
        integralEntity.setIntegral(integral);
        integralService.increaseIntegral(integralEntity);
        return buildSuccessJsonMsg("warehouse.add.sucess", null);
    }
    
}
