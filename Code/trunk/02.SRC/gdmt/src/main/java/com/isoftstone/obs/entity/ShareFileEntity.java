package com.isoftstone.obs.entity;

import com.isoftstone.platform.entity.Columns;

import java.io.Serializable;

public class ShareFileEntity implements Serializable {
    @Columns("fileUuid")
    private String fileUuid;
    @Columns("startDate")
    private String startDate;
    @Columns("endDate")
    private  String endDate;

    public String getFileUuid() {
        return fileUuid;
    }

    public void setFileUuid(String fileUuid) {
        this.fileUuid = fileUuid;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "ShareFileEntity{" +
                "fileUuid='" + fileUuid + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';

    }
}
