package com.isoftstone.platform.tag;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.isoftstone.platform.common.MessageHolder;
import com.isoftstone.platform.tag.entity.SelectTagEntity;
import com.isoftstone.platform.tag.service.DataSelectService;
import com.isoftstone.platform.tag.service.RelationSelectService;

public class RelationSelectTag extends SimpleTagSupport
{
    Logger logger = Logger.getLogger(DctnrTag.class.getName());
    
    private String id;
    
    private String classez;
    
    private String style;
    
    private String name;
    
    private String ref;
    
    private String subTagId;
    
    public String getId()
    {
        return id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getClassez()
    {
        return classez;
    }
    
    public void setClassez(String classez)
    {
        this.classez = classez;
    }
    
    public String getStyle()
    {
        return style;
    }
    
    public void setStyle(String style)
    {
        this.style = style;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getRef()
    {
        return ref;
    }
    
    public void setRef(String ref)
    {
        this.ref = ref;
    }
    
    public String getSubTagId()
    {
        return subTagId;
    }
    
    public void setSubTagId(String subTagId)
    {
        this.subTagId = subTagId;
    }
    
    private DataSelectService dataSelectService;
    
    private MessageHolder messageHolder;
    
    @Override
    public void doTag()
        throws JspException, IOException
    {
        
        // session.setAttribute("siteLanguage",siteLanguage);
        //String language = (String)getJspContext().getAttribute("siteLanguage");
        //logger.info("language" + language);
        // ServletContext sc;
        
        PageContext pag = (PageContext)getJspContext();
        ServletContext sc = pag.getServletContext();
        HttpServletRequest request = (HttpServletRequest)pag.getRequest();
        String language = (String)pag.getSession().getAttribute("siteLanguage");
        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(sc);
        // webApplicationContext.getBean("roleService");
        
        // dctnrService = webApplicationContext.getBean("dctnrService123",
        // DctnrService.class);
        dataSelectService = webApplicationContext.getBean(ref, RelationSelectService.class);
        
        messageHolder = webApplicationContext.getBean("messageHolder", MessageHolder.class);
        
        List<SelectTagEntity> list = dataSelectService.queryTagElementList(language);
        logger.info("list" + list);
        JspWriter out = getJspContext().getOut();
        out.write("<select ");
        if (id != null)
        {
            out.write(" id='");
            out.write(id);
            out.write("'");
        }
        
        if (classez != null)
        {
            out.write(" class='");
            out.write(classez);
            out.write("'");
        }
        
        if (subTagId != null)
        {
            out.write(" subTagId='");
            out.write(subTagId);
            out.write("'");
        }
        if (style != null)
        {
            out.write(" style='");
            out.write(style);
            out.write("'");
        }
        if (name != null)
        {
            out.write(" name='");
            out.write(name);
            out.write("'");
        }
        
        out.write(" onchange='relationAction(this)''");
        
        if (ref != null)
        {
            out.write(" serviceObj='");
            out.write(ref);
            out.write("'");
        }
        
        out.write(">'");
        out.write("<option value=''>");
        out.write(messageHolder.getMessage("common.select"));
        out.write("</option>");
        for (SelectTagEntity SelectTagEntity : list)
        {
            out.write("<option value='");
            out.write(SelectTagEntity.getCode());
            out.write("'>");
            out.write(SelectTagEntity.getDictName());
            out.write("</option>");
        }
        out.write("</select>");
        out.write("<script type=\"text/javascript\">\r\n");
        out.write("\tfunction relationAction(obj){\r\n");
        out.write("\t\tvar obj1 = $(obj);\r\n");
        out.write("\t\tvar val = $(obj).val();\r\n");
        out.write("\t\tvar subTagId  = obj1.attr(\"subTagId\");\r\n");
        out.write("\t\tvar serviceObj =  obj1.attr(\"serviceObj\");\r\n");
        out.write("\t\t$.ajax({\r\n");
        out.write("\t\t\ttype : 'POST',\r\n");
        out.write("\t\t\turl : '");
        out.write(request.getContextPath());
        out.write("/tag/tagSubList',\r\n");
        out.write("\t\t\tdataType : 'json',\r\n");
        out.write("\t\t\tcache : false,\r\n");
        out.write("\t\t\tdata : [ \r\n");
        out.write("\t\t\t{\r\n");
        out.write("\t\t\t\tname : 'code',\r\n");
        out.write("\t\t\t\tvalue : val\r\n");
        out.write("\t\t\t},\r\n");
        out.write("\t\t\t{\r\n");
        out.write("\t\t\t\tname : 'obj',\r\n");
        out.write("\t\t\t\tvalue : serviceObj\r\n");
        out.write("\t\t\t}\r\n");
        out.write("\t\t\t],\r\n");
        out.write("\t\t\tsuccess : function(data) {\r\n");
        out.write("\t\t\t\tvar domSum = $( \"#\" + subTagId);\r\n");
        out.write("\t\t\t\tvar a = domSum.val();\r\n");
        out.write("\t\t\t\tvar opStr =\"\";\r\n");
        out.write("\t\t\t\tif(data != null){\r\n");
        
        out.write("\t\t\t\t\t\topStr +=\"<option value=''>\"\r\n");
        out.write("\t\t\t\t\t\topStr += '" + messageHolder.getMessage("common.select") + "'\r\n");
        out.write("\t\t\t\t\t\topStr +=\"</option>\"\r\n");
        
        out.write("\t\t\t\t\tfor(var i = 0; i < data.length; i ++){\r\n");
        out.write("\t\t\t\t\t\tvar code = data[i].code;\r\n");
        out.write("\t\t\t\t\t\tvar dictName = data[i].dictName;\r\n");
        out.write("\t\t\t\t\t\topStr +=\"<option value='\"+ code  +  \"'>\"\r\n");
        out.write("\t\t\t\t\t\topStr += dictName\r\n");
        out.write("\t\t\t\t\t\topStr +=\"</option>\"\r\n");
        out.write("\t\t\t\t\t}\r\n");
        out.write("\t\t\t\t}\r\n");
        out.write("\t\t\t\tdomSum.html(opStr);\r\n");
        out.write("\t\t\t},\r\n");
        out.write("\t\t\terror : function() {\r\n");
        out.write("\t\t\t\tmessage(\"");
        out.write(messageHolder.getMessage("common.error"));
        out.write("\");\r\n");
        out.write("\t\t\t}\r\n");
        out.write("\t\t});\r\n");
        out.write("\r\n");
        out.write("\t\r\n");
        out.write("\t}\r\n");
        out.write("</script>\r\n");
    }
}
