package com.isoftstone.platform.common.uitl;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.io.*;
import java.nio.file.Path;
import java.util.Hashtable;

public final class QRCodeUtil {
    public  static ByteArrayOutputStream generateQRCodeOut(String src){
        int width=300;
        int height=300;
        String format="png";
        Hashtable hints=new Hashtable();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        hints.put(EncodeHintType.MARGIN, 2);
        try {
            BitMatrix bitMatrix=new MultiFormatWriter().encode(src, BarcodeFormat.QR_CODE, width, height,hints);
            MatrixToImageWriter.writeToStream(bitMatrix,format,byteArrayOutputStream);
        } catch (WriterException | IOException e) {
            e.printStackTrace();
        }
        return byteArrayOutputStream;
    }

    public  static InputStream generateQRCode(String src){
        ByteArrayOutputStream byteArrayOutputStream = generateQRCodeOut(src);
                ByteArrayInputStream byteArrayInputStream = new  ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        return byteArrayInputStream;
    }

    public static void main(String[] args) throws IOException {
        InputStream generateQRCode = generateQRCode("asdfasdfasdfasdfasdf");
        FileOutputStream fileOutputStream = new FileOutputStream("D:\\a2\\a.jpg");
        byte[] block = new byte[1024];
        int length = 0;
        while((length = generateQRCode.read(block)) > 0){
            fileOutputStream.write(block,0,length);
            fileOutputStream.flush();
        }
        fileOutputStream.close();
    }
}
