package com.isoftstone.classinfo.entity;

import java.io.Serializable;

public class SearchClassInfoEntity implements Serializable {
    private String parentId;
    private String searchClassName;
    private String searchStartTime;
    private String searchEndTime;

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getSearchClassName() {
        return searchClassName;
    }

    public void setSearchClassName(String searchClassName) {
        this.searchClassName = searchClassName;
    }

    public String getSearchStartTime() {
        return searchStartTime;
    }

    public void setSearchStartTime(String searchStartTime) {
        this.searchStartTime = searchStartTime;
    }

    public String getSearchEndTime() {
        return searchEndTime;
    }

    public void setSearchEndTime(String searchEndTime) {
        this.searchEndTime = searchEndTime;
    }

    @Override
    public String toString() {
        return "SearchClassInfoEntity{" +
                "parentId='" + parentId + '\'' +
                ", searchClassName='" + searchClassName + '\'' +
                ", searchStartTime='" + searchStartTime + '\'' +
                ", searchEndTime='" + searchEndTime + '\'' +
                '}';
    }
}
