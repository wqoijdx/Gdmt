package com.isoftstone.member.entity;

import java.io.Serializable;

import com.isoftstone.platform.entity.Columns;

/**
 * 消费实体
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  fhxin
 * @version  [CBEMS V100R002C01, 2019-6-26]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ConsumeEntity implements Serializable
{
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -8072574668811442420L;
    
    /**
     * 消费Id
     */
    @Columns("cn.id")
    private int id;
    
    /**
     * 会员Id
     */
    @Columns("cn.member_id")
    private String memberId;
    
    /**
     * 姓名
     */
    @Columns("mm.name")
    private String name;
    
    /**
     * 消费金额
     */
    @Columns("cn.amount")
    private String amount;
    
    /**
     * 更新时间
     */
    @Columns("cn.update_time")
    private String updateTime;
    
    public int getId()
    {
        return id;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public String getMemberId()
    {
        return memberId;
    }
    
    public void setMemberId(String memberId)
    {
        this.memberId = memberId;
    }
    
    public String getAmount()
    {
        return amount;
    }
    
    public void setAmount(String amount)
    {
        this.amount = amount;
    }
    
    public String getUpdateTime()
    {
        return updateTime;
    }
    
    public void setUpdateTime(String updateTime)
    {
        this.updateTime = updateTime;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
}
