package com.isoftstone.platform.sm.entity;

import java.io.Serializable;

public class PasswdEntity implements Serializable
{
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 3105393734717753318L;
    
    /**
     * 用户名称
     */
    private String userName;
    
    /**
     * 老密码
     */
    private String oldPassword;
    
    /**
     * 新密码
     */
    private String password;
    
    public String getUserName()
    {
        return userName;
    }
    
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    public String getOldPassword()
    {
        return oldPassword;
    }
    
    public void setOldPassword(String oldPassword)
    {
        this.oldPassword = oldPassword;
    }
    
    public String getPassword()
    {
        return password;
    }
    
    public void setPassword(String password)
    {
        this.password = password;
    }
    
}
