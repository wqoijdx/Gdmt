#database & user

use mysql;

#delete database & user
drop database if exists  cldk_data;
#delete from user where user='cldk_data';
#drop user if exists  cldk_data;
flush privileges;

#create database & user
create database  cldk_data default character set utf8 collate utf8_general_ci;
grant all on  cldk_data.* to 'cldk_data'@'localhost' identified by 'Huawei_123' with grant option;
grant all on  cldk_data.* to 'cldk_data'@'%' identified by 'Huawei_123' with grant option;

flush privileges;
use  cldk_data;

-- ----------------------------
-- Table structure for `pt_dictionary`
-- ----------------------------
DROP TABLE IF EXISTS `pt_dictionary`;
CREATE TABLE `pt_dictionary` (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_parent_id` bigint(20) DEFAULT '0',
  `dict_group` varchar(64) NOT NULL COMMENT '字典分组',
  `dic_code` varchar(32) NOT NULL COMMENT '代码',
  `description` varchar(64) DEFAULT NULL COMMENT '备注',
  `dic_sort` int(11) DEFAULT NULL COMMENT '显示排序',
  PRIMARY KEY (`dict_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='字典数据表';

-- ----------------------------
-- Records of pt_dictionary
-- ----------------------------
INSERT INTO `pt_dictionary` VALUES ('5', '0', 'sex', '0', '女', '1');
INSERT INTO `pt_dictionary` VALUES ('6', '0', 'sex', '1', '男', '2');
INSERT INTO `pt_dictionary` VALUES ('7', '0', 'group', '0', '全部', '1');
INSERT INTO `pt_dictionary` VALUES ('8', '0', 'group', '1', '区组', '2');
INSERT INTO `pt_dictionary` VALUES ('9', '0', 'menu.https', '0', '0 非https', '1');
INSERT INTO `pt_dictionary` VALUES ('10', '0', 'menu.https', '1', '1 https', '2');
INSERT INTO `pt_dictionary` VALUES ('11', '0', 'organ.type', '0', '组织类型  集团侧', '1');
INSERT INTO `pt_dictionary` VALUES ('12', '0', 'organ.type', '1', '组织类型  阳光专区供应商', '2');
INSERT INTO `pt_dictionary` VALUES ('13', '0', 'organ.type', '2', '组织类型  电厂侧', '3');
INSERT INTO `pt_dictionary` VALUES ('14', '0', 'organ.type', '3', '组织类型  分公司侧', '4');
INSERT INTO `pt_dictionary` VALUES ('15', '0', 'menu.level', '0', '一级菜单', '1');
INSERT INTO `pt_dictionary` VALUES ('16', '0', 'menu.level', '1', '二级菜单', '2');
INSERT INTO `pt_dictionary` VALUES ('17', '0', 'menu.level', '2', '三级菜单', '3');

-- ----------------------------
-- Table structure for `pt_dictionary_i18n`
-- ----------------------------
DROP TABLE IF EXISTS `pt_dictionary_i18n`;
CREATE TABLE `pt_dictionary_i18n` (
  `dict_id` bigint(20) NOT NULL COMMENT '字典编码',
  `dict_name` varchar(64) DEFAULT NULL COMMENT '字典名称',
  `language_id` bigint(20) NOT NULL COMMENT '语言ID',
  PRIMARY KEY (`dict_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典数据国际化表';

-- ----------------------------
-- Records of pt_dictionary_i18n
-- ----------------------------
INSERT INTO `pt_dictionary_i18n` VALUES ('5', 'female', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('5', '女', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('6', 'male', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('6', '男', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('7', 'all', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('7', '全部', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('8', 'group', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('8', '区组', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('9', 'No', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('9', '否', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('10', 'Yes', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('10', '是', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('11', 'Group side', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('11', '集团侧', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('12', 'Suppliers of sunshine zone', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('12', '阳光专区供应商', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('13', 'Power plant side', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('13', '电厂侧', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('14', 'Branch side', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('14', '分公司侧', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('15', 'level 1', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('15', '1 级', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('16', 'level 2', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('16', '2 级', '2');
INSERT INTO `pt_dictionary_i18n` VALUES ('17', 'level 3', '1');
INSERT INTO `pt_dictionary_i18n` VALUES ('17', '3 级', '2');

-- ----------------------------
-- Table structure for `pt_file`
-- ----------------------------
DROP TABLE IF EXISTS `pt_file`;
CREATE TABLE `pt_file` (
  `file_uuid` varchar(36) NOT NULL COMMENT '文件id',
  `parent_uuid` varchar(36) NOT NULL DEFAULT '0' COMMENT '父文件id',
  `file_type` int(11) NOT NULL DEFAULT '0' COMMENT '文件类型 0:文件 1:文件夹',
  `file_capacity` varchar(36) DEFAULT NULL COMMENT '文件容量',
  `file_name` varchar(100) NOT NULL COMMENT '文件名',
  `user_uuid` varchar(36) DEFAULT NULL COMMENT '用户id',
  `file_path` varchar(516) DEFAULT NULL COMMENT '用户id',
  `remarks` varchar(516) DEFAULT NULL COMMENT '备注',
  `MODTIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEL_FLAG` int(11) NOT NULL DEFAULT '0' COMMENT '是否删除标识 0:正常 1:删除',
  PRIMARY KEY (`file_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件信息表';

-- ----------------------------
-- Records of pt_file
-- ----------------------------

-- ----------------------------
-- Table structure for `pt_language`
-- ----------------------------
DROP TABLE IF EXISTS `pt_language`;
CREATE TABLE `pt_language` (
  `lang_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '语言ID',
  `lang_type` varchar(30) DEFAULT NULL COMMENT '语言类型',
  `lang_desc` varchar(20) DEFAULT NULL COMMENT '语言描述',
  PRIMARY KEY (`lang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='语言表';

-- ----------------------------
-- Records of pt_language
-- ----------------------------
INSERT INTO `pt_language` VALUES ('1', 'en', 'English');
INSERT INTO `pt_language` VALUES ('2', 'zh', '汉语');

-- ----------------------------
-- Table structure for `pt_menu`
-- ----------------------------
DROP TABLE IF EXISTS `pt_menu`;
CREATE TABLE `pt_menu` (
  `menu_id` varchar(32) NOT NULL,
  `menu_name` varchar(32) DEFAULT NULL COMMENT '菜单名称',
  `parent_id` varchar(32) DEFAULT NULL COMMENT '父菜单ID',
  `menu_type` int(10) DEFAULT NULL COMMENT '菜单类型，0:菜单目录 1:菜单 2:页面功能',
  `menu_level` int(11) DEFAULT NULL COMMENT '菜单目录级数 1 2 3',
  `menu_url` varchar(256) DEFAULT NULL COMMENT '菜单url',
  `privilege_code` varchar(32) DEFAULT NULL COMMENT '菜单权限',
  `is_https` int(11) DEFAULT NULL,
  `menu_icon` varchar(255) DEFAULT NULL COMMENT '菜单图标类名',
  `menu_seq` varchar(10) DEFAULT NULL COMMENT '排序',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pt_menu
-- ----------------------------
INSERT INTO `pt_menu` VALUES ('1000', '业务管理', '0', '0', '1', '', '', null, 'glyphicon glyphicon-align-justify', '2', null);
INSERT INTO `pt_menu` VALUES ('1100', '文件管理', '1000', '0', '2', '', '', null, 'fa fa-credit-card', '1', null);
INSERT INTO `pt_menu` VALUES ('1110', '全部文件', '1100', '1', '3', 'disk/initPage', 'cldk_data_price', null, 'fa fa-file-text-o', '1', null);
INSERT INTO `pt_menu` VALUES ('1111', '文件上传', '1110', '2', '4', null, 'cldk_file_add', null, null, null, null);
INSERT INTO `pt_menu` VALUES ('1112', '删除文件', '1110', '2', '4', null, 'cldk_file_del', null, null, null, null);
INSERT INTO `pt_menu` VALUES ('1120', '回收站', '1100', '1', '3', 'disk/recycleFile', 'cldk_data_offer', null, 'fa fa-trash-o', '1', null);
INSERT INTO `pt_menu` VALUES ('1130', '收到分享', '1100', '0', '3', 'quoteinfo/page', 'cldk_data_quote', null, 'fa fa-location-arrow', '1', null);
INSERT INTO `pt_menu` VALUES ('1140', '我的收藏', '1100', '1', '3', 'gdpinfo/page', 'cldk_data_gdp', null, 'fa fa-star-half-o', '1', null);
INSERT INTO `pt_menu` VALUES ('1150', '我的分享', '1100', '1', '3', 'disk/stoPage', 'cldk_data_sto', null, 'fa fa-paperclip', '1', null);
INSERT INTO `pt_menu` VALUES ('2000', '系统管理', '0', '0', '1', 'temp.jsp', '', null, 'glyphicon glyphicon-align-justify', '3', null);
INSERT INTO `pt_menu` VALUES ('2100', '后台管理', '2000', '0', '2', '', '', null, 'fa fa-credit-card', '2', null);
INSERT INTO `pt_menu` VALUES ('2110', '角色管理', '2100', '0', '3', 'role/rolePage', 'cldk_data_role', null, 'fa fa-file-text-o', '', null);
INSERT INTO `pt_menu` VALUES ('2111', '角色查询', '2110', '2', '4', null, 'cldk_data_role_query', null, null, null, null);
INSERT INTO `pt_menu` VALUES ('2112', '角色增加', '2110', '2', '4', null, 'cldk_data_role_add', null, null, null, null);
INSERT INTO `pt_menu` VALUES ('2113', '角色修改', '2110', '2', '4', null, 'cldk_data_role_mod', null, null, null, null);
INSERT INTO `pt_menu` VALUES ('2114', '分配权限', '2110', '2', '4', null, 'cldk_data_role_menu', null, null, null, null);
INSERT INTO `pt_menu` VALUES ('2115', '角色删除', '2110', '2', '4', null, 'cldk_data_role_del', null, null, null, null);
INSERT INTO `pt_menu` VALUES ('2120', '用户管理', '2100', '0', '3', '/user/userPage', 'cldk_data_user', null, 'fa fa-file-text-o', '', null);
INSERT INTO `pt_menu` VALUES ('2121', '用户查询', '2120', '2', '4', null, 'cldk_data_user_query', null, null, null, null);
INSERT INTO `pt_menu` VALUES ('2122', '用户增加', '2120', '2', '4', null, 'cldk_data_user_add', null, null, null, null);
INSERT INTO `pt_menu` VALUES ('2123', '用户修改', '2120', '2', '4', null, 'cldk_data_user_mod', null, null, null, null);
INSERT INTO `pt_menu` VALUES ('2124', '用户删除', '2120', '2', '4', null, 'cldk_data_user_del', null, null, null, null);
INSERT INTO `pt_menu` VALUES ('2125', '角色分配', '2120', '2', '4', null, 'cldk_data_user_role', null, null, null, null);
INSERT INTO `pt_menu` VALUES ('2200', '系统配置', '2000', '0', '2', '', '', null, 'fa fa-credit-card', '3', null);
INSERT INTO `pt_menu` VALUES ('2210', '菜单', '2200', '0', '3', 'temp.jsp', 'cldk_data_menu', null, 'fa fa-file-text-o', '', null);
INSERT INTO `pt_menu` VALUES ('2220', '配置', '2200', '0', '3', 'temp.jsp', 'cldk_data_config', null, 'fa fa-file-text-o', '', null);
INSERT INTO `pt_menu` VALUES ('2230', '日志', '2200', '0', '3', 'temp.jsp', 'cldk_data_log', null, 'fa fa-file-text-o', '', null);

-- ----------------------------
-- Table structure for `pt_menu_i18n`
-- ----------------------------
DROP TABLE IF EXISTS `pt_menu_i18n`;
CREATE TABLE `pt_menu_i18n` (
  `menu_id` varchar(32) NOT NULL DEFAULT '0' COMMENT '菜单ID',
  `lang_id` int(10) NOT NULL DEFAULT '0' COMMENT '语言ID',
  `menu_name` varchar(64) DEFAULT NULL COMMENT '国际化 菜单',
  PRIMARY KEY (`menu_id`,`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='国际化表';

-- ----------------------------
-- Records of pt_menu_i18n
-- ----------------------------
INSERT INTO `pt_menu_i18n` VALUES ('1000', '1', 'serviceManager');
INSERT INTO `pt_menu_i18n` VALUES ('1000', '2', '业务管理');
INSERT INTO `pt_menu_i18n` VALUES ('1100', '1', 'fileManager');
INSERT INTO `pt_menu_i18n` VALUES ('1100', '2', '文件管理');
INSERT INTO `pt_menu_i18n` VALUES ('1110', '1', 'AllFile');
INSERT INTO `pt_menu_i18n` VALUES ('1110', '2', '全部文件');
INSERT INTO `pt_menu_i18n` VALUES ('1120', '1', 'recycleBin');
INSERT INTO `pt_menu_i18n` VALUES ('1120', '2', '回收站');
INSERT INTO `pt_menu_i18n` VALUES ('1130', '1', 'receiveShare');
INSERT INTO `pt_menu_i18n` VALUES ('1130', '2', '收到分享');
INSERT INTO `pt_menu_i18n` VALUES ('1140', '1', 'myCollection');
INSERT INTO `pt_menu_i18n` VALUES ('1140', '2', '我的收藏');
INSERT INTO `pt_menu_i18n` VALUES ('1150', '1', 'myShare');
INSERT INTO `pt_menu_i18n` VALUES ('1150', '2', '我的分享');
INSERT INTO `pt_menu_i18n` VALUES ('2000', '1', 'management');
INSERT INTO `pt_menu_i18n` VALUES ('2000', '2', '系统管理');
INSERT INTO `pt_menu_i18n` VALUES ('2100', '1', 'userRole');
INSERT INTO `pt_menu_i18n` VALUES ('2100', '2', '后台管理');
INSERT INTO `pt_menu_i18n` VALUES ('2110', '1', 'role');
INSERT INTO `pt_menu_i18n` VALUES ('2110', '2', '角色管理');
INSERT INTO `pt_menu_i18n` VALUES ('2120', '1', 'user');
INSERT INTO `pt_menu_i18n` VALUES ('2120', '2', '用户管理');
INSERT INTO `pt_menu_i18n` VALUES ('2200', '1', 'system');
INSERT INTO `pt_menu_i18n` VALUES ('2200', '2', '系统配置');
INSERT INTO `pt_menu_i18n` VALUES ('2210', '1', 'menu');
INSERT INTO `pt_menu_i18n` VALUES ('2210', '2', '菜单');
INSERT INTO `pt_menu_i18n` VALUES ('2220', '1', 'config');
INSERT INTO `pt_menu_i18n` VALUES ('2220', '2', '配置');
INSERT INTO `pt_menu_i18n` VALUES ('2230', '1', 'log');
INSERT INTO `pt_menu_i18n` VALUES ('2230', '2', '日志');

-- ----------------------------
-- Table structure for `pt_role`
-- ----------------------------
DROP TABLE IF EXISTS `pt_role`;
CREATE TABLE `pt_role` (
  `role_uuid` varchar(255) NOT NULL COMMENT '角色资源ID',
  `role_name` varchar(32) DEFAULT NULL COMMENT '角色名称',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of pt_role
-- ----------------------------
INSERT INTO `pt_role` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '管理员', '管理员');
INSERT INTO `pt_role` VALUES ('1111', '普通人员', '普通人员');
INSERT INTO `pt_role` VALUES ('25f79a2bced711eabd63f430b915d5f2', '研发人员', '研发人员');
INSERT INTO `pt_role` VALUES ('33c25a3e003f48a397ef65c223009d26', '项目经理', '项目经理');
INSERT INTO `pt_role` VALUES ('3995161cced711eabd63f430b915d5f2', '测试人员', '测试人员');
INSERT INTO `pt_role` VALUES ('4a323fb1ceeb11eabd63f430b915d5f2', '资料', '资料');

-- ----------------------------
-- Table structure for `pt_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `pt_role_menu`;
CREATE TABLE `pt_role_menu` (
  `ROLE_UUID` varchar(255) NOT NULL COMMENT '角色资源ID',
  `menu_id` int(20) NOT NULL DEFAULT '0' COMMENT '菜单ID',
  PRIMARY KEY (`ROLE_UUID`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色资源配置';

-- ----------------------------
-- Records of pt_role_menu
-- ----------------------------
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2000');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2100');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2110');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2111');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2112');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2113');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2114');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2115');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2120');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2121');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2122');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2123');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2124');
INSERT INTO `pt_role_menu` VALUES ('10ac1faf2f0c4e8c84a8366178699c8c', '2125');
INSERT INTO `pt_role_menu` VALUES ('1111', '1000');
INSERT INTO `pt_role_menu` VALUES ('1111', '1100');
INSERT INTO `pt_role_menu` VALUES ('1111', '1110');
INSERT INTO `pt_role_menu` VALUES ('1111', '1111');
INSERT INTO `pt_role_menu` VALUES ('1111', '1112');
INSERT INTO `pt_role_menu` VALUES ('1111', '1120');
INSERT INTO `pt_role_menu` VALUES ('1111', '1130');
INSERT INTO `pt_role_menu` VALUES ('1111', '1140');
INSERT INTO `pt_role_menu` VALUES ('1111', '1150');
INSERT INTO `pt_role_menu` VALUES ('25f79a2bced711eabd63f430b915d5f2', '2000');
INSERT INTO `pt_role_menu` VALUES ('25f79a2bced711eabd63f430b915d5f2', '2100');
INSERT INTO `pt_role_menu` VALUES ('25f79a2bced711eabd63f430b915d5f2', '2120');
INSERT INTO `pt_role_menu` VALUES ('25f79a2bced711eabd63f430b915d5f2', '2121');
INSERT INTO `pt_role_menu` VALUES ('25f79a2bced711eabd63f430b915d5f2', '2122');
INSERT INTO `pt_role_menu` VALUES ('25f79a2bced711eabd63f430b915d5f2', '2123');
INSERT INTO `pt_role_menu` VALUES ('25f79a2bced711eabd63f430b915d5f2', '2124');
INSERT INTO `pt_role_menu` VALUES ('25f79a2bced711eabd63f430b915d5f2', '2125');
INSERT INTO `pt_role_menu` VALUES ('3995161cced711eabd63f430b915d5f2', '2000');
INSERT INTO `pt_role_menu` VALUES ('3995161cced711eabd63f430b915d5f2', '2200');
INSERT INTO `pt_role_menu` VALUES ('3995161cced711eabd63f430b915d5f2', '2210');
INSERT INTO `pt_role_menu` VALUES ('3995161cced711eabd63f430b915d5f2', '2220');
INSERT INTO `pt_role_menu` VALUES ('3995161cced711eabd63f430b915d5f2', '2230');
INSERT INTO `pt_role_menu` VALUES ('4a323fb1ceeb11eabd63f430b915d5f2', '2000');
INSERT INTO `pt_role_menu` VALUES ('4a323fb1ceeb11eabd63f430b915d5f2', '2100');
INSERT INTO `pt_role_menu` VALUES ('4a323fb1ceeb11eabd63f430b915d5f2', '2110');
INSERT INTO `pt_role_menu` VALUES ('4a323fb1ceeb11eabd63f430b915d5f2', '2111');
INSERT INTO `pt_role_menu` VALUES ('4a323fb1ceeb11eabd63f430b915d5f2', '2120');
INSERT INTO `pt_role_menu` VALUES ('4a323fb1ceeb11eabd63f430b915d5f2', '2121');
INSERT INTO `pt_role_menu` VALUES ('4a323fb1ceeb11eabd63f430b915d5f2', '2200');
INSERT INTO `pt_role_menu` VALUES ('4a323fb1ceeb11eabd63f430b915d5f2', '2210');
INSERT INTO `pt_role_menu` VALUES ('4a323fb1ceeb11eabd63f430b915d5f2', '2220');
INSERT INTO `pt_role_menu` VALUES ('4a323fb1ceeb11eabd63f430b915d5f2', '2230');

-- ----------------------------
-- Table structure for `pt_role_user`
-- ----------------------------
DROP TABLE IF EXISTS `pt_role_user`;
CREATE TABLE `pt_role_user` (
  `USER_UUID` varchar(255) NOT NULL,
  `ROLE_UUID` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色表';

-- ----------------------------
-- Records of pt_role_user
-- ----------------------------
INSERT INTO `pt_role_user` VALUES ('001fddf8608c4cd1bbac81dde4e0c701', '10ac1faf2f0c4e8c84a8366178699c8c');
INSERT INTO `pt_role_user` VALUES ('011d83b63034470ab6225a2ead643cf7', '25f79a2bced711eabd63f430b915d5f2');
INSERT INTO `pt_role_user` VALUES ('01635ed157e445bba6725675bdd8ff67', '33c25a3e003f48a397ef65c223009d26');
INSERT INTO `pt_role_user` VALUES ('02d7938e06934f48b92baec5348a8609', '3995161cced711eabd63f430b915d5f2');
INSERT INTO `pt_role_user` VALUES ('0311e42abab4467a80aefd2ebd18ab19', '4a323fb1ceeb11eabd63f430b915d5f2');
INSERT INTO `pt_role_user` VALUES ('04325342590745fdad116befc6e812a8', '4a323fb1ceeb11eabd63f430b915d5f2');

-- ----------------------------
-- Table structure for `pt_security_log`
-- ----------------------------
DROP TABLE IF EXISTS `pt_security_log`;
CREATE TABLE `pt_security_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `user_uuid` varchar(255) NOT NULL,
  `log_system` int(11) NOT NULL COMMENT '子系统',
  `log_module` int(11) NOT NULL COMMENT '模块',
  `log_action` int(11) NOT NULL COMMENT '操作动作',
  `log_contents` varchar(256) NOT NULL COMMENT '操作内容',
  `create_time` datetime NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='安全日志';

-- ----------------------------
-- Records of pt_security_log
-- ----------------------------

-- ----------------------------
-- Table structure for `pt_sys_config`
-- ----------------------------
DROP TABLE IF EXISTS `pt_sys_config`;
CREATE TABLE `pt_sys_config` (
  `cfg_id` int(11) NOT NULL AUTO_INCREMENT,
  `cfg_name` varchar(64) NOT NULL,
  `cfg_value` varchar(64) NOT NULL,
  `remarks` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`cfg_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of pt_sys_config
-- ----------------------------
INSERT INTO `pt_sys_config` VALUES ('20', 'isAuthCode', 'false', '开启登录的验证码');
INSERT INTO `pt_sys_config` VALUES ('21', 'siteLanguage', 'zh', '默认语方类型');

-- ----------------------------
-- Table structure for `pt_user`
-- ----------------------------
DROP TABLE IF EXISTS `pt_user`;
CREATE TABLE `pt_user` (
  `USER_UUID` varchar(255) NOT NULL,
  `USER_NAME` varchar(32) DEFAULT NULL COMMENT '登录名',
  `PASSWORD` varchar(32) DEFAULT NULL COMMENT '密码',
  `EMAIL` varchar(32) DEFAULT NULL COMMENT '邮件地址',
  `MOBILE` varchar(255) DEFAULT NULL COMMENT '电话',
  `NICE_NAME` varchar(64) DEFAULT NULL COMMENT '真实姓名',
  `REGISTERDATE` datetime DEFAULT NULL COMMENT '注册日期',
  `REMARK` varchar(512) DEFAULT NULL COMMENT '备注',
  `DEL_FLAG` int(11) NOT NULL COMMENT '是否删除标识 0:正常 1:删除',
  `MODTIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`USER_UUID`),
  UNIQUE KEY `mobileIndex` (`MOBILE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pt_user
-- ----------------------------
INSERT INTO `pt_user` VALUES ('001fddf8608c4cd1bbac81dde4e0c701', 'zhaowei', 'ae00cfce6cf6566c7dc53015ca184c1a', 'zhaowei@isoftstone.com', '13131363553', '赵伟', '2020-07-30 16:16:20', null, '0', '2020-07-30 16:20:25');
INSERT INTO `pt_user` VALUES ('011d83b63034470ab6225a2ead643cf7', 'hanguobing', 'ae00cfce6cf6566c7dc53015ca184c1a', 'hanguobing@isoftstone.com', '13605350862', '韩国兵', '2020-07-30 16:16:20', null, '0', '2020-07-30 16:20:25');
INSERT INTO `pt_user` VALUES ('01635ed157e445bba6725675bdd8ff67', 'renjunxy', 'ae00cfce6cf6566c7dc53015ca184c1a', 'renjunxy@isoftstone.com', '13846028986', '任俊', '2020-07-30 16:16:20', null, '0', '2020-07-30 16:20:25');
INSERT INTO `pt_user` VALUES ('02d7938e06934f48b92baec5348a8609', 'yanwenyi', 'ae00cfce6cf6566c7dc53015ca184c1a', 'yanwenyi@isoftstone.com', '13897145171', '严文毅', '2020-07-30 16:16:20', null, '0', '2020-07-30 16:20:25');
INSERT INTO `pt_user` VALUES ('0311e42abab4467a80aefd2ebd18ab19', 'zhangjifang', 'ae00cfce6cf6566c7dc53015ca184c1a', 'zhangjifang@isoftstone.com', '13124524956', '张吉方', '2020-07-30 16:16:20', null, '0', '2020-07-30 16:20:25');
INSERT INTO `pt_user` VALUES ('04325342590745fdad116befc6e812a8', 'lijuan', 'ae00cfce6cf6566c7dc53015ca184c1a', 'lijuan@isoftstone.com', '13213283541', '李娟', '2020-07-30 16:16:20', null, '0', '2020-07-30 16:20:25');
INSERT INTO `pt_user` VALUES ('1c5bd24fe2e54b8daee18ce10a92c4a2', 'yueheng', 'ae00cfce6cf6566c7dc53015ca184c1a', 'yueheng@isoftstone.com', '13212705634', '岳衡', '2020-07-30 16:16:20', null, '0', '2020-07-30 16:20:25');
INSERT INTO `pt_user` VALUES ('1cb3253577f948e3b4287a8014bda341', 'wangpu', 'ae00cfce6cf6566c7dc53015ca184c1a', 'wangpu@isoftstone.com', '13126495609', '王璞', '2020-07-30 16:16:20', null, '0', '2020-07-30 16:20:25');
INSERT INTO `pt_user` VALUES ('2c969910eea145718c344cc13763c3a7', 'gaoxingui', 'ae00cfce6cf6566c7dc53015ca184c1a', 'gaoxingui@isoftstone.com', '13651869712', '高新贵', '2020-07-30 16:16:20', null, '0', '2020-07-30 16:20:25');
INSERT INTO `pt_user` VALUES ('999', 'admin', 'ae00cfce6cf6566c7dc53015ca184c1a', 'admin@isoftstone.com', '13461106251', '管理员', '2020-07-30 16:16:20', null, '0', '2020-07-30 16:20:25');

DROP TRIGGER IF EXISTS `pt_user_delete_trigger`;
 DELIMITER $
 CREATE TRIGGER `pt_user_delete_trigger` BEFORE DELETE ON `pt_user` FOR EACH ROW BEGIN
 DECLARE cout int(11);
    set cout=(select count(1) from  pt_role_user where USER_UUID = OLD.USER_UUID);
    if( cout > 0 ) THEN
 			set @tip_msg =  CONCAT('角色授权正在使用:',OLD.USER_NAME);
			signal sqlstate 'ERROR' set message_text =  @tip_msg ;
 		end if;
END $
DELIMITER ;

drop TRIGGER if EXISTS pt_user_create_trigger;
DELIMITER $
CREATE TRIGGER pt_user_create_trigger BEFORE INSERT ON pt_user FOR EACH ROW
BEGIN
	if (NEW.USER_UUID = '' or NEW.USER_UUID is null)  then
		set NEW.USER_UUID = REPLACE(UUID(),'-','');
  end if;
END $
DELIMITER ;

