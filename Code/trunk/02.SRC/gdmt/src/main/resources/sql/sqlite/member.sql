CREATE TABLE consumeDail (
    id          INTEGER         PRIMARY KEY AUTOINCREMENT,
    member_id   VARCHAR (32) NOT NULL,
    amount      NUMERIC (11, 2) NOT NULL,
    update_time DATETIME
);

CREATE TABLE integralDail (
    id          INTEGER         PRIMARY KEY AUTOINCREMENT,
    member_id   VARCHAR (32) NOT NULL,
    integral    NUMERIC (11, 2) NOT NULL,
    update_time DATETIME
);

CREATE TABLE member (
    id          INTEGER         PRIMARY KEY AUTOINCREMENT,
    member_id   VARCHAR (32) NOT NULL,
    name        TIME (256) NOT NULL,
    integral    NUMERIC (11, 2),
    remarks     VARCHAR (516),
    update_time DATETIME,
    tel         VARCHAR (20) 
);



CREATE TABLE sm_user (
  user_id INTEGER PRIMARY KEY AUTOINCREMENT,
  account varchar(32) ,
  password varchar(256),
  name varchar(32) ,
  gender int(11),
  telephone varchar(32),
  email varchar(128),
  resetpwd int(11) ,
  description varchar(256) ,
  creator bigint(20) ,
  create_time datetime ,
  account_type int(11) ,
  del_flag int(11) ,
  is_active int(11) 
);

INSERT INTO sm_user (
is_active,del_flag,email,telephone,gender,name,password,account,user_id
) 
VALUES (
1,0,'fhxin@isoftstone.com',18885245451,1,'admin','61893f4706cb9295489e7692a31bcec9','admin',1
);