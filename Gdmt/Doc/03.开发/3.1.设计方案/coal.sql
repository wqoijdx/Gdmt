/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : coal

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-02-06 15:42:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for fultbpurchasingcancel
-- ----------------------------
DROP TABLE IF EXISTS `fultbpurchasingcancel`;
CREATE TABLE `fultbpurchasingcancel` (
  `cancleapplyid` varchar(36) NOT NULL,
  `purchapplyid` varchar(36) DEFAULT NULL,
  `operdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `autoid` decimal(8,0) DEFAULT NULL,
  `operuser` varchar(64) DEFAULT NULL,
  `createuser` varchar(64) DEFAULT NULL,
  `createdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `createuserid` varchar(64) DEFAULT NULL,
  `createuserdeptid` varchar(64) DEFAULT NULL,
  `createuserdeptcode` varchar(64) DEFAULT NULL,
  `cancelname` varchar(64) DEFAULT NULL,
  `canceldate` datetime DEFAULT NULL,
  `cancelstate` decimal(8,0) DEFAULT NULL,
  `executeid` varchar(64) DEFAULT NULL,
  `cancelreason` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`cancleapplyid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='燃料采购计划作废';

-- ----------------------------
-- Records of fultbpurchasingcancel
-- ----------------------------

-- ----------------------------
-- Table structure for pt_attachment
-- ----------------------------
DROP TABLE IF EXISTS `pt_attachment`;
CREATE TABLE `pt_attachment` (
  `id` varchar(64) NOT NULL,
  `attachment_name` varchar(200) DEFAULT NULL,
  `upload_date` datetime DEFAULT NULL,
  `upload_ip` varchar(64) DEFAULT NULL,
  `attachment_size` decimal(8,0) DEFAULT NULL,
  `attachment_url` varchar(200) DEFAULT NULL,
  `attachment_type` varchar(64) DEFAULT NULL,
  `belongid` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件地址表';

-- ----------------------------
-- Records of pt_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for pt_data_access
-- ----------------------------
DROP TABLE IF EXISTS `pt_data_access`;
CREATE TABLE `pt_data_access` (
  `da_uuid` varchar(255) NOT NULL,
  `da_name` varchar(32) DEFAULT NULL,
  `s_condition` varchar(512) DEFAULT NULL,
  `role_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`da_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据过滤条件';

-- ----------------------------
-- Records of pt_data_access
-- ----------------------------

-- ----------------------------
-- Table structure for pt_department
-- ----------------------------
DROP TABLE IF EXISTS `pt_department`;
CREATE TABLE `pt_department` (
  `id` varchar(32) NOT NULL,
  `organ_uuid` varchar(255) DEFAULT NULL,
  `agency_id` varchar(32) DEFAULT NULL,
  `branch_code` varchar(32) DEFAULT NULL,
  `branch_name` varchar(50) DEFAULT NULL,
  `branch_type` varchar(32) DEFAULT NULL,
  `belong_center` varchar(32) DEFAULT NULL,
  `bewrite` varchar(200) DEFAULT NULL,
  `estate` char(1) DEFAULT NULL,
  `modifierid` varchar(32) DEFAULT NULL,
  `modtime` datetime DEFAULT NULL,
  `commiterid` varchar(32) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `flag` char(1) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='国电部门';

-- ----------------------------
-- Records of pt_department
-- ----------------------------

-- ----------------------------
-- Table structure for pt_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `pt_dictionary`;
CREATE TABLE `pt_dictionary` (
  `id` varchar(64) NOT NULL COMMENT 'ID',
  `dicgroup` varchar(64) DEFAULT NULL COMMENT '组名称',
  `dicname` varchar(200) DEFAULT NULL COMMENT '显示名称',
  `diccode` varchar(64) DEFAULT NULL COMMENT '编码',
  `dicsort` decimal(8,0) DEFAULT NULL COMMENT '排序号',
  `dicisno` varchar(64) DEFAULT NULL COMMENT '是否可用'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典表';

-- ----------------------------
-- Records of pt_dictionary
-- ----------------------------

-- ----------------------------
-- Table structure for pt_dictionary_sql
-- ----------------------------
DROP TABLE IF EXISTS `pt_dictionary_sql`;
CREATE TABLE `pt_dictionary_sql` (
  `id` varchar(64) NOT NULL,
  `code` varchar(64) DEFAULT NULL,
  `s_sql` varchar(500) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='动态数据';

-- ----------------------------
-- Records of pt_dictionary_sql
-- ----------------------------

-- ----------------------------
-- Table structure for pt_mail
-- ----------------------------
DROP TABLE IF EXISTS `pt_mail`;
CREATE TABLE `pt_mail` (
  `maill_uuid` varchar(255) NOT NULL,
  `smtp_host` varchar(64) DEFAULT NULL,
  `smtp_port` varchar(16) DEFAULT NULL,
  `auth` decimal(1,0) DEFAULT NULL,
  `email_address` varchar(128) DEFAULT NULL,
  `email_username` varchar(128) DEFAULT NULL,
  `email_password` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`maill_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='邮箱地址';

-- ----------------------------
-- Records of pt_mail
-- ----------------------------

-- ----------------------------
-- Table structure for pt_menu
-- ----------------------------
DROP TABLE IF EXISTS `pt_menu`;
CREATE TABLE `pt_menu` (
  `menu_uuid` varchar(255) NOT NULL,
  `menu_name` varchar(32) DEFAULT NULL,
  `menu_url` varchar(256) DEFAULT NULL,
  `in_use` varchar(1) DEFAULT NULL,
  `menu_order` decimal(10,0) DEFAULT NULL,
  `parent_uuid` varchar(255) DEFAULT NULL,
  `menu_target` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`menu_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单目录';

-- ----------------------------
-- Records of pt_menu
-- ----------------------------

-- ----------------------------
-- Table structure for pt_menu_directory
-- ----------------------------
DROP TABLE IF EXISTS `pt_menu_directory`;
CREATE TABLE `pt_menu_directory` (
  `id` varchar(32) NOT NULL COMMENT '菜单目录id（树型结构层级编码）',
  `dir_code` varchar(32) DEFAULT NULL COMMENT '菜单目录编号',
  `dir_name` varchar(100) DEFAULT NULL COMMENT '菜单目录名称',
  `dir_level_number` decimal(2,0) DEFAULT NULL COMMENT '菜单目录级数',
  `dir_type` char(1) DEFAULT NULL COMMENT '菜单目录类型：0，业务；1，系统；2，实施；4，集成；5，Demo',
  `resource_id` varchar(32) DEFAULT NULL COMMENT '菜单资源id',
  `parent_id` varchar(32) DEFAULT NULL COMMENT '父目录id',
  `isitem` char(1) NOT NULL DEFAULT '1' COMMENT '明细否：0 非明细；1 明细',
  `dir_order` decimal(9,0) DEFAULT NULL COMMENT '菜单目录顺序号',
  `status` char(1) DEFAULT NULL COMMENT '是否删除：0 是；1 否',
  `flag` char(1) DEFAULT NULL COMMENT '启/停状态：0 停用；1 启用',
  `menu_group_id` varchar(32) DEFAULT NULL COMMENT '菜单类别id',
  `locale` varchar(32) NOT NULL DEFAULT 'zh_CN' COMMENT '当前国际化标志',
  `res_uuid` varchar(255) DEFAULT NULL,
  `realid` varchar(32) DEFAULT NULL COMMENT 'id',
  PRIMARY KEY (`id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单目录表';

-- ----------------------------
-- Records of pt_menu_directory
-- ----------------------------

-- ----------------------------
-- Table structure for pt_organ
-- ----------------------------
DROP TABLE IF EXISTS `pt_organ`;
CREATE TABLE `pt_organ` (
  `organ_uuid` varchar(255) NOT NULL,
  `organ_code` varchar(32) DEFAULT NULL,
  `organ_name` varchar(32) DEFAULT NULL,
  `organ_type` varchar(16) DEFAULT NULL,
  `in_use` varchar(1) DEFAULT NULL,
  `parent_uuid` varchar(255) DEFAULT NULL,
  `status` char(1) DEFAULT NULL COMMENT '是否删除',
  `modifierid` varchar(32) DEFAULT NULL COMMENT '创建人ID',
  `modtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `description` varchar(400) DEFAULT NULL COMMENT '描述',
  `account_code` varchar(128) DEFAULT NULL COMMENT '资金账户code',
  PRIMARY KEY (`organ_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='组织机构';

-- ----------------------------
-- Records of pt_organ
-- ----------------------------

-- ----------------------------
-- Table structure for pt_organ_post
-- ----------------------------
DROP TABLE IF EXISTS `pt_organ_post`;
CREATE TABLE `pt_organ_post` (
  `post_uuid` varchar(128) DEFAULT NULL COMMENT '编号',
  `post_code` varchar(128) DEFAULT NULL COMMENT '岗位编码',
  `organ_uuid` varchar(128) DEFAULT NULL COMMENT '人员编码'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='职位岗位表';

-- ----------------------------
-- Records of pt_organ_post
-- ----------------------------

-- ----------------------------
-- Table structure for pt_res
-- ----------------------------
DROP TABLE IF EXISTS `pt_res`;
CREATE TABLE `pt_res` (
  `res_uuid` varchar(255) NOT NULL,
  `id` varchar(32) DEFAULT NULL COMMENT '菜单类别id',
  `res_id` varchar(32) DEFAULT NULL,
  `res_name` varchar(32) DEFAULT NULL,
  `res_url` varchar(256) DEFAULT NULL,
  `res_type` varchar(255) DEFAULT NULL,
  `parent_uuid` varchar(255) DEFAULT NULL,
  `res_order` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`res_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资源表';

-- ----------------------------
-- Records of pt_res
-- ----------------------------

-- ----------------------------
-- Table structure for pt_role
-- ----------------------------
DROP TABLE IF EXISTS `pt_role`;
CREATE TABLE `pt_role` (
  `role_uuid` varchar(255) NOT NULL,
  `role_id` varchar(32) DEFAULT NULL,
  `role_name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`role_uuid`),
  KEY `ak_key_2` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Records of pt_role
-- ----------------------------

-- ----------------------------
-- Table structure for pt_role_res
-- ----------------------------
DROP TABLE IF EXISTS `pt_role_res`;
CREATE TABLE `pt_role_res` (
  `role_uuid` varchar(255) NOT NULL,
  `id` varchar(32) DEFAULT NULL COMMENT '菜单目录id（树型结构层级编码）',
  PRIMARY KEY (`role_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色资源配置';

-- ----------------------------
-- Records of pt_role_res
-- ----------------------------

-- ----------------------------
-- Table structure for pt_role_user
-- ----------------------------
DROP TABLE IF EXISTS `pt_role_user`;
CREATE TABLE `pt_role_user` (
  `user_uuid` varchar(255) NOT NULL,
  `role_uuid` varchar(255) NOT NULL,
  PRIMARY KEY (`role_uuid`,`user_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色配置';

-- ----------------------------
-- Records of pt_role_user
-- ----------------------------

-- ----------------------------
-- Table structure for pt_r_role_organ
-- ----------------------------
DROP TABLE IF EXISTS `pt_r_role_organ`;
CREATE TABLE `pt_r_role_organ` (
  `dutyid` varchar(32) NOT NULL COMMENT '岗位ID',
  `name` varchar(255) DEFAULT NULL COMMENT '岗位名称',
  `organ_uuid` varchar(255) DEFAULT NULL COMMENT '组织ID',
  `role_uuid` varchar(255) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`dutyid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='岗位表（角色和单位的挂接）';

-- ----------------------------
-- Records of pt_r_role_organ
-- ----------------------------

-- ----------------------------
-- Table structure for pt_r_user_duty_org
-- ----------------------------
DROP TABLE IF EXISTS `pt_r_user_duty_org`;
CREATE TABLE `pt_r_user_duty_org` (
  `user_dutyid` varchar(32) NOT NULL COMMENT '主键',
  `user_uuid` varchar(255) DEFAULT NULL COMMENT '用户ID',
  `dutyid` varchar(32) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_dutyid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户、岗位对应表';

-- ----------------------------
-- Records of pt_r_user_duty_org
-- ----------------------------

-- ----------------------------
-- Table structure for pt_sys_config
-- ----------------------------
DROP TABLE IF EXISTS `pt_sys_config`;
CREATE TABLE `pt_sys_config` (
  `id` varchar(64) NOT NULL,
  `code` varchar(64) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `url` varchar(64) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统配置';

-- ----------------------------
-- Records of pt_sys_config
-- ----------------------------

-- ----------------------------
-- Table structure for pt_user
-- ----------------------------
DROP TABLE IF EXISTS `pt_user`;
CREATE TABLE `pt_user` (
  `user_uuid` varchar(255) NOT NULL,
  `id` varchar(32) DEFAULT NULL,
  `username` varchar(32) DEFAULT NULL COMMENT '登录名',
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  `email` varchar(32) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `enabled` decimal(1,0) DEFAULT NULL,
  `account_non_expired` decimal(1,0) DEFAULT NULL,
  `credentials_non_expired` decimal(1,0) DEFAULT NULL,
  `account_non_locked` decimal(1,0) DEFAULT NULL,
  `organ_uuid` varchar(64) DEFAULT NULL COMMENT '机构ID',
  `nice_name` varchar(64) DEFAULT NULL COMMENT '姓名',
  `is_admin` decimal(1,0) DEFAULT NULL,
  `maximumsessions` decimal(1,0) DEFAULT '1' COMMENT '1',
  `registerdate` datetime DEFAULT NULL COMMENT '注册日期',
  `lastlogintime` datetime DEFAULT NULL COMMENT '最近登陆时间',
  `pwdque` varchar(128) DEFAULT NULL COMMENT '密保问题',
  `pwdans` varchar(128) DEFAULT NULL COMMENT '密保答案',
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `depid` varchar(32) DEFAULT NULL COMMENT '部门ID',
  `status` char(1) NOT NULL COMMENT '是否删除',
  `modtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '操作时间',
  `modifierid` varchar(32) DEFAULT NULL COMMENT '操作人ID',
  `is_sum` decimal(1,0) DEFAULT NULL COMMENT '是否阳光用户（0否1是）',
  PRIMARY KEY (`user_uuid`),
  KEY `ak_key_2` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of pt_user
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu_group
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_group`;
CREATE TABLE `sys_menu_group` (
  `id` varchar(32) NOT NULL COMMENT '菜单类别id',
  `code` varchar(32) DEFAULT NULL COMMENT '菜单类别编号',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单类别名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单类别表';

-- ----------------------------
-- Records of sys_menu_group
-- ----------------------------

-- ----------------------------
-- Table structure for t_account
-- ----------------------------
DROP TABLE IF EXISTS `t_account`;
CREATE TABLE `t_account` (
  `account_id` varchar(128) NOT NULL COMMENT '账户ID',
  `account_code` varchar(128) DEFAULT NULL COMMENT '账户code',
  `total_account` decimal(8,0) DEFAULT NULL COMMENT '总账户',
  `balance_account` decimal(8,0) DEFAULT NULL COMMENT '可用余额',
  `freeze_account` decimal(8,0) DEFAULT NULL COMMENT '冻结账户',
  `organ_code` varchar(128) DEFAULT NULL COMMENT '组织机构编码',
  `membership_fees` decimal(8,0) DEFAULT NULL COMMENT '会员费',
  `bid_account` decimal(8,0) DEFAULT NULL COMMENT '投标保证金',
  `performance_account` decimal(8,0) DEFAULT NULL COMMENT ' 履约保证金',
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='保证金账户表';

-- ----------------------------
-- Records of t_account
-- ----------------------------

-- ----------------------------
-- Table structure for t_fulblacklist
-- ----------------------------
DROP TABLE IF EXISTS `t_fulblacklist`;
CREATE TABLE `t_fulblacklist` (
  `blackid` varchar(36) NOT NULL,
  `id` varchar(36) DEFAULT NULL,
  `organ_code` varchar(64) DEFAULT NULL COMMENT '申请注册的组织机构代码',
  `supplierid` varchar(64) DEFAULT NULL COMMENT '供应商ID',
  `createuser` varchar(64) DEFAULT NULL COMMENT '申请人',
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '申请日期',
  `blackreason` varchar(512) DEFAULT NULL COMMENT '列入黑名单原因',
  `flowstate` decimal(8,0) DEFAULT NULL COMMENT '审核状态(0,电厂申请，1分公司审核通过，2集团审核通过，3分公司驳回，4集团驳回）',
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`blackid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='供应商黑名单审核信息';

-- ----------------------------
-- Records of t_fulblacklist
-- ----------------------------

-- ----------------------------
-- Table structure for t_fulblacklist_attachment
-- ----------------------------
DROP TABLE IF EXISTS `t_fulblacklist_attachment`;
CREATE TABLE `t_fulblacklist_attachment` (
  `attachment_id` decimal(11,0) NOT NULL,
  `blackid` varchar(36) DEFAULT NULL,
  `attachment_path` varchar(255) NOT NULL COMMENT '附件路径',
  `attachment_name` varchar(100) NOT NULL COMMENT '附件名称',
  `filename` varchar(100) DEFAULT NULL COMMENT '文件名',
  `download_count` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '下载次数',
  `uploaddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '上传日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='黑名单附件表;';

-- ----------------------------
-- Records of t_fulblacklist_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for t_fultbbelongelectric
-- ----------------------------
DROP TABLE IF EXISTS `t_fultbbelongelectric`;
CREATE TABLE `t_fultbbelongelectric` (
  `id` varchar(36) NOT NULL,
  `supplierid` varchar(64) DEFAULT NULL COMMENT '供应商ID',
  `organizationcode` varchar(64) DEFAULT NULL COMMENT '供应商组织机构代码',
  `organ_code` varchar(64) DEFAULT NULL COMMENT '申请注册的组织机构代码',
  `operdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `operuser` varchar(64) DEFAULT NULL,
  `createuser` varchar(64) DEFAULT NULL COMMENT '申请人',
  `createdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '申请日期',
  `createuserid` varchar(64) DEFAULT NULL,
  `createuserdeptid` varchar(64) DEFAULT NULL,
  `createuserdeptcode` varchar(64) DEFAULT NULL,
  `dcorgan_code` varchar(64) DEFAULT NULL COMMENT '审核人所在单位编码',
  `dcverifyman` varchar(64) DEFAULT NULL COMMENT '审核人',
  `dcverifytime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '审核时间',
  `dcverifystate` varchar(64) DEFAULT NULL COMMENT '审核结果',
  `gsorgan_code` varchar(64) DEFAULT NULL COMMENT '分公司单位编码',
  `verifyman` varchar(64) DEFAULT NULL COMMENT '分公司审核人',
  `verifytime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '分公司审核时间',
  `verifystate` varchar(64) DEFAULT NULL COMMENT '分公司审核结果',
  `blackliststate` decimal(8,0) DEFAULT NULL COMMENT '黑名单状态(1表示黑名单）',
  `blackreason` varchar(512) DEFAULT NULL COMMENT '列入黑名单原因',
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `flowstate` decimal(8,0) DEFAULT NULL COMMENT '审核状态(0,申请，1电厂审核通过，2分公司通过，3分公司驳回）',
  `suppliertype` varchar(2) DEFAULT NULL COMMENT '供应商类型（1临时供应商，2一般供应商，3内部供应商，4重点供应商）',
  `companyid` varchar(64) DEFAULT NULL,
  `gsorganid` varchar(64) DEFAULT NULL,
  `gsverifyman` varchar(64) DEFAULT NULL,
  `gsverifytime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='供应商注册审核信息';

-- ----------------------------
-- Records of t_fultbbelongelectric
-- ----------------------------

-- ----------------------------
-- Table structure for t_fultbpurchasingapply
-- ----------------------------
DROP TABLE IF EXISTS `t_fultbpurchasingapply`;
CREATE TABLE `t_fultbpurchasingapply` (
  `purchapplyid` varchar(36) NOT NULL,
  `user_uuid` varchar(255) NOT NULL,
  `templateid` varchar(36) DEFAULT NULL,
  `operdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `autoid` decimal(10,0) DEFAULT NULL,
  `operuser` varchar(64) DEFAULT NULL,
  `createuser` varchar(64) DEFAULT NULL,
  `createdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `createuserid` varchar(64) DEFAULT NULL,
  `createuserdeptid` varchar(64) DEFAULT NULL,
  `createuserdeptcode` varchar(64) DEFAULT NULL,
  `permitstatus` varchar(16) DEFAULT NULL,
  `billnumber` varchar(64) DEFAULT NULL,
  `datafrom` varchar(16) DEFAULT NULL,
  `datafromname` varchar(64) DEFAULT NULL,
  `writername` varchar(64) DEFAULT NULL,
  `writeraccount` varchar(64) DEFAULT NULL,
  `writerdepartid` varchar(64) DEFAULT NULL,
  `writerdepartname` varchar(64) DEFAULT NULL,
  `writedate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `jhtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `jhtime_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `coaltype` varchar(64) DEFAULT NULL,
  `qty` decimal(19,5) DEFAULT NULL,
  `qty_end` decimal(19,5) DEFAULT NULL,
  `yunshu_mode` varchar(64) DEFAULT NULL,
  `jiaohuo_mode` varchar(64) DEFAULT NULL,
  `yanshou_mode` varchar(64) DEFAULT NULL,
  `jiesuan_mode` varchar(64) DEFAULT NULL,
  `type_1` varchar(64) DEFAULT NULL,
  `type_2` varchar(64) DEFAULT NULL,
  `type_3` varchar(64) DEFAULT NULL,
  `type_4` varchar(64) DEFAULT NULL,
  `type_5` varchar(64) DEFAULT NULL,
  `type_6` varchar(64) DEFAULT NULL,
  `type_7` varchar(64) DEFAULT NULL,
  `type_8` varchar(64) DEFAULT NULL,
  `remark` text,
  `signname` varchar(64) DEFAULT NULL,
  `signaccount` varchar(64) DEFAULT NULL,
  `jhtime2` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type_32` varchar(64) DEFAULT NULL,
  `createbyah` varchar(16) DEFAULT NULL,
  `is_fb` varchar(16) DEFAULT NULL,
  `source` varchar(32) DEFAULT NULL,
  `applystate` varchar(64) DEFAULT NULL COMMENT '申请状态',
  `executestate` varchar(64) DEFAULT NULL COMMENT '报批状态',
  `executeid` varchar(36) DEFAULT NULL,
  `applystatenum` varchar(36) DEFAULT NULL COMMENT '申请开始为0,正在审批1,结束为2',
  `executestatenum` varchar(36) DEFAULT NULL COMMENT '报批开始为0,正在审批1,结束为2',
  `executecase` varchar(1024) DEFAULT NULL COMMENT '执行情况',
  `isouter` varchar(36) DEFAULT NULL COMMENT '是否是外部数据',
  `type` varchar(36) DEFAULT NULL COMMENT '公开0，邀请1',
  `maxlimitprice` varchar(64) DEFAULT NULL COMMENT '最高限价',
  `paymode` text,
  `type_323` varchar(64) DEFAULT NULL,
  `type_333` varchar(64) DEFAULT NULL,
  `maxremark` varchar(1024) DEFAULT NULL,
  `type_11` varchar(64) DEFAULT NULL COMMENT '干基基准高位发热量',
  `type_12` varchar(64) DEFAULT NULL COMMENT '干基基准全水分',
  `type_14` varchar(64) DEFAULT NULL COMMENT '干基基准全硫',
  `type_16` varchar(64) DEFAULT NULL COMMENT '干基基准灰分',
  `coalclass` varchar(64) DEFAULT NULL,
  `kgj_1` varchar(64) DEFAULT NULL COMMENT '空干基水分',
  `kgj_2` varchar(64) DEFAULT NULL COMMENT '空干基全硫',
  `kgj_3` varchar(64) DEFAULT NULL COMMENT '空干基挥发分(小)',
  `kgj_4` varchar(64) DEFAULT NULL COMMENT '空干基挥发分(大)',
  PRIMARY KEY (`purchapplyid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='燃料采购计划申请单';

-- ----------------------------
-- Records of t_fultbpurchasingapply
-- ----------------------------

-- ----------------------------
-- Table structure for t_fultbpurchasingapply2
-- ----------------------------
DROP TABLE IF EXISTS `t_fultbpurchasingapply2`;
CREATE TABLE `t_fultbpurchasingapply2` (
  `purchapplyid` varchar(36) NOT NULL,
  `user_uuid` varchar(255) NOT NULL,
  `templateid` varchar(36) DEFAULT NULL,
  `operdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `autoid` decimal(10,0) DEFAULT NULL,
  `operuser` varchar(64) DEFAULT NULL,
  `createuser` varchar(64) DEFAULT NULL,
  `createdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `createuserid` varchar(64) DEFAULT NULL,
  `createuserdeptid` varchar(64) DEFAULT NULL,
  `createuserdeptcode` varchar(64) DEFAULT NULL,
  `permitstatus` varchar(16) DEFAULT NULL,
  `billnumber` varchar(64) DEFAULT NULL,
  `datafrom` varchar(16) DEFAULT NULL,
  `datafromname` varchar(64) DEFAULT NULL,
  `writername` varchar(64) DEFAULT NULL,
  `writeraccount` varchar(64) DEFAULT NULL,
  `writerdepartid` varchar(64) DEFAULT NULL,
  `writerdepartname` varchar(64) DEFAULT NULL,
  `writedate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `jhtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `jhtime_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `coaltype` varchar(64) DEFAULT NULL,
  `qty` decimal(19,5) DEFAULT NULL,
  `qty_end` decimal(19,5) DEFAULT NULL,
  `yunshu_mode` varchar(64) DEFAULT NULL,
  `jiaohuo_mode` varchar(64) DEFAULT NULL,
  `yanshou_mode` varchar(64) DEFAULT NULL,
  `jiesuan_mode` varchar(64) DEFAULT NULL,
  `type_1` varchar(64) DEFAULT NULL,
  `type_2` varchar(64) DEFAULT NULL,
  `type_3` varchar(64) DEFAULT NULL,
  `type_4` varchar(64) DEFAULT NULL,
  `type_5` varchar(64) DEFAULT NULL,
  `type_6` varchar(64) DEFAULT NULL,
  `type_7` varchar(64) DEFAULT NULL,
  `type_8` varchar(64) DEFAULT NULL,
  `remark` text,
  `signname` varchar(64) DEFAULT NULL,
  `signaccount` varchar(64) DEFAULT NULL,
  `jhtime2` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type_32` varchar(64) DEFAULT NULL,
  `createbyah` varchar(16) DEFAULT NULL,
  `is_fb` varchar(16) DEFAULT NULL,
  `source` varchar(32) DEFAULT NULL,
  `applystate` varchar(64) DEFAULT NULL COMMENT '申请状态',
  `executestate` varchar(64) DEFAULT NULL COMMENT '报批状态',
  `executeid` varchar(36) DEFAULT NULL,
  `applystatenum` varchar(36) DEFAULT NULL COMMENT '申请开始为0,正在审批1,结束为2',
  `executestatenum` varchar(36) DEFAULT NULL COMMENT '报批开始为0,正在审批1,结束为2',
  `executecase` varchar(1024) DEFAULT NULL COMMENT '执行情况',
  `isouter` varchar(36) DEFAULT NULL COMMENT '是否是外部数据',
  `type` varchar(36) DEFAULT NULL COMMENT '公开0，邀请1',
  `maxlimitprice` varchar(64) DEFAULT NULL COMMENT '最高限价',
  `paymode` text,
  `type_323` varchar(64) DEFAULT NULL,
  `type_333` varchar(64) DEFAULT NULL,
  `maxremark` varchar(1024) DEFAULT NULL,
  `type_11` varchar(64) DEFAULT NULL COMMENT '干基基准高位发热量',
  `type_12` varchar(64) DEFAULT NULL COMMENT '干基基准全水分',
  `type_14` varchar(64) DEFAULT NULL COMMENT '干基基准全硫',
  `type_16` varchar(64) DEFAULT NULL COMMENT '干基基准灰分',
  `coalclass` varchar(64) DEFAULT NULL,
  `kgj_1` varchar(64) DEFAULT NULL COMMENT '空干基水分',
  `kgj_2` varchar(64) DEFAULT NULL COMMENT '空干基全硫',
  `kgj_3` varchar(64) DEFAULT NULL COMMENT '空干基挥发分(小)',
  `kgj_4` varchar(64) DEFAULT NULL COMMENT '空干基挥发分(大)',
  PRIMARY KEY (`purchapplyid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='燃料采购计划申请单2';

-- ----------------------------
-- Records of t_fultbpurchasingapply2
-- ----------------------------

-- ----------------------------
-- Table structure for t_fultbpurchasingapplydetail
-- ----------------------------
DROP TABLE IF EXISTS `t_fultbpurchasingapplydetail`;
CREATE TABLE `t_fultbpurchasingapplydetail` (
  `applydetailid` varchar(36) NOT NULL,
  `purchapplyid` varchar(36) DEFAULT NULL,
  `supplierid` varchar(64) DEFAULT NULL,
  `user_uuid` varchar(255) DEFAULT NULL,
  `operdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `autoid` decimal(8,0) DEFAULT NULL,
  `operuser` varchar(64) DEFAULT NULL,
  `createuser` varchar(64) DEFAULT NULL,
  `createdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `createuserid` varchar(64) DEFAULT NULL,
  `createuserdeptid` varchar(64) DEFAULT NULL,
  `createuserdeptcode` varchar(64) DEFAULT NULL,
  `purchasingapplyid` varchar(64) DEFAULT NULL,
  `qty` decimal(19,5) DEFAULT NULL,
  `qty_end` decimal(19,5) DEFAULT NULL,
  `qcalmin` decimal(19,5) DEFAULT NULL,
  `feedback` varchar(1024) DEFAULT NULL,
  `remark` varchar(1024) DEFAULT NULL,
  `linktel` varchar(64) DEFAULT NULL,
  `permitstatus` varchar(64) DEFAULT NULL,
  `bjtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_zb` varchar(64) DEFAULT NULL,
  `jb_name` varchar(64) DEFAULT NULL,
  `jd_name` varchar(64) DEFAULT NULL,
  `sp_name` varchar(64) DEFAULT NULL,
  `localplace` varchar(1024) DEFAULT NULL,
  `hasmodified` decimal(8,0) DEFAULT '0',
  `is_bj` varchar(10) DEFAULT NULL,
  `organizationcode` varchar(64) DEFAULT NULL,
  `ischeckedbyfgs` varchar(64) DEFAULT NULL COMMENT '是否被分公司选中',
  `ischeckedbydc` varchar(64) DEFAULT NULL COMMENT '是否被电厂选中',
  `zbqty` varchar(64) DEFAULT NULL,
  `zbprice` varchar(64) DEFAULT NULL,
  `lastbjdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '最后一次报价时间',
  `sendport` varchar(1024) DEFAULT NULL,
  `lf` decimal(19,5) DEFAULT NULL,
  `pricemin` varchar(64) DEFAULT NULL,
  `fare` varchar(64) DEFAULT NULL,
  `hff` float DEFAULT NULL,
  `hff_end` float DEFAULT NULL,
  `ash` float DEFAULT NULL,
  `qsf` float DEFAULT NULL,
  PRIMARY KEY (`applydetailid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='燃料采购计划供应商报价';

-- ----------------------------
-- Records of t_fultbpurchasingapplydetail
-- ----------------------------

-- ----------------------------
-- Table structure for t_fultbsupplier
-- ----------------------------
DROP TABLE IF EXISTS `t_fultbsupplier`;
CREATE TABLE `t_fultbsupplier` (
  `supplierid` varchar(64) NOT NULL,
  `membertypeid` varchar(36) DEFAULT NULL COMMENT '类型ID',
  `user_uuid` varchar(255) DEFAULT NULL,
  `createuser` varchar(64) DEFAULT NULL COMMENT '创建人\r\n            创建日期\r\n            修改人\r\n            修改日期\r\n            供应商名称\r\n            供应商编号\r\n            供应商简称\r\n            法人代表\r\n            注册资金\r\n            备用字段\r\n            经营执照编号\r\n            经营许可证编号\r\n            创建人',
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建日期',
  `operuser` varchar(64) DEFAULT NULL COMMENT '修改人',
  `operdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改日期',
  `name` varchar(64) DEFAULT NULL COMMENT '供应商名称',
  `code` varchar(64) DEFAULT NULL COMMENT '供应商编号',
  `shortname` varchar(64) DEFAULT NULL COMMENT '供应商简称',
  `corporation` varchar(64) DEFAULT NULL COMMENT '法人代表',
  `financing` decimal(19,5) DEFAULT NULL COMMENT '注册资金',
  `licence` varchar(64) DEFAULT NULL COMMENT '经营执照编号',
  `coallicence` varchar(64) DEFAULT NULL COMMENT '经营许可证编号',
  `taxcode` varchar(64) DEFAULT NULL COMMENT '税务登记证代码',
  `linkman` varchar(64) DEFAULT NULL COMMENT '联系人',
  `linktel` varchar(64) DEFAULT NULL COMMENT '联系电话',
  `faxcode` varchar(64) DEFAULT NULL COMMENT '传真号码',
  `address` varchar(256) DEFAULT NULL COMMENT '联系地址',
  `postalcode` varchar(64) DEFAULT NULL COMMENT '邮政编码',
  `isstop` decimal(10,0) DEFAULT NULL COMMENT '是否停用',
  `creditlevel` varchar(64) DEFAULT NULL COMMENT '信誉等级',
  `norder` decimal(10,0) DEFAULT NULL COMMENT '顺序号',
  `organizationcode` varchar(64) DEFAULT NULL COMMENT '组织机构代码',
  `conemail` varchar(64) DEFAULT NULL COMMENT '联系人Email',
  `mexplain` text COMMENT '特别说明',
  `registerplace` varchar(256) DEFAULT NULL COMMENT '公司注册地',
  `corporationidentity` varchar(64) DEFAULT NULL COMMENT '法人代表身份证',
  `openbank` varchar(256) DEFAULT NULL COMMENT '开户银行',
  `bankaccount` varchar(256) DEFAULT NULL COMMENT '银行账号',
  `coalsource` text COMMENT '煤源存放地点、数量、质量',
  `transportmode` text COMMENT '运输方式及保障能力',
  `introduce` text COMMENT '供应商介绍',
  `achievement` text COMMENT '供应商业绩',
  `times` decimal(8,0) DEFAULT NULL COMMENT '次数',
  `clicklogintime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nclicklogintime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `autoid` decimal(10,0) DEFAULT NULL,
  `supplierkind` varchar(64) DEFAULT NULL COMMENT '备用字段',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `lastlogintime` varchar(64) DEFAULT NULL COMMENT '最近登陆时间',
  `stepnumber` decimal(10,0) DEFAULT NULL COMMENT '注册到第几步',
  `createuserid` varchar(64) DEFAULT NULL,
  `createuserdeptid` varchar(64) DEFAULT NULL,
  `createuserdeptcode` varchar(64) DEFAULT NULL,
  `minespecialcost` decimal(19,5) DEFAULT NULL,
  `railcarriage` decimal(19,5) DEFAULT NULL,
  `datafrom` varchar(64) DEFAULT NULL,
  `is_fb` varchar(16) DEFAULT NULL,
  `pt_audit` varchar(2) DEFAULT NULL COMMENT '是否通过(平台审核：0否，1是，默认为0)',
  `is_caauth` varchar(2) DEFAULT NULL COMMENT '是否通过CA认证（0否，1是，默认为0）',
  PRIMARY KEY (`supplierid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='供应商基本信息';

-- ----------------------------
-- Records of t_fultbsupplier
-- ----------------------------

-- ----------------------------
-- Table structure for t_fultbsupplier2
-- ----------------------------
DROP TABLE IF EXISTS `t_fultbsupplier2`;
CREATE TABLE `t_fultbsupplier2` (
  `supplierid` varchar(36) NOT NULL,
  `membertypeid` varchar(36) DEFAULT NULL COMMENT '类型ID',
  `user_uuid` varchar(255) DEFAULT NULL,
  `createuser` varchar(64) DEFAULT NULL COMMENT '创建人\r\n            创建日期\r\n            修改人\r\n            修改日期\r\n            供应商名称\r\n            供应商编号\r\n            供应商简称\r\n            法人代表\r\n            注册资金\r\n            备用字段\r\n            经营执照编号\r\n            经营许可证编号\r\n            创建人',
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建日期',
  `operuser` varchar(64) DEFAULT NULL COMMENT '修改人',
  `operdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改日期',
  `name` varchar(64) DEFAULT NULL COMMENT '供应商名称',
  `code` varchar(64) DEFAULT NULL COMMENT '供应商编号',
  `shortname` varchar(64) DEFAULT NULL COMMENT '供应商简称',
  `corporation` varchar(64) DEFAULT NULL COMMENT '法人代表',
  `financing` decimal(19,5) DEFAULT NULL COMMENT '注册资金',
  `licence` varchar(64) DEFAULT NULL COMMENT '经营执照编号',
  `coallicence` varchar(64) DEFAULT NULL COMMENT '经营许可证编号',
  `taxcode` varchar(64) DEFAULT NULL COMMENT '税务登记证代码',
  `linkman` varchar(64) DEFAULT NULL COMMENT '联系人',
  `linktel` varchar(64) DEFAULT NULL COMMENT '联系电话',
  `faxcode` varchar(64) DEFAULT NULL COMMENT '传真号码',
  `address` varchar(256) DEFAULT NULL COMMENT '联系地址',
  `postalcode` varchar(64) DEFAULT NULL COMMENT '邮政编码',
  `isstop` decimal(10,0) DEFAULT NULL COMMENT '是否停用',
  `creditlevel` varchar(64) DEFAULT NULL COMMENT '信誉等级',
  `norder` decimal(10,0) DEFAULT NULL COMMENT '顺序号',
  `organizationcode` varchar(64) DEFAULT NULL COMMENT '组织机构代码',
  `conemail` varchar(64) DEFAULT NULL COMMENT '联系人Email',
  `mexplain` text COMMENT '特别说明',
  `registerplace` varchar(256) DEFAULT NULL COMMENT '公司注册地',
  `corporationidentity` varchar(64) DEFAULT NULL COMMENT '法人代表身份证',
  `openbank` varchar(256) DEFAULT NULL COMMENT '开户银行',
  `bankaccount` varchar(256) DEFAULT NULL COMMENT '银行账号',
  `coalsource` text COMMENT '煤源存放地点、数量、质量',
  `transportmode` text COMMENT '运输方式及保障能力',
  `introduce` text COMMENT '供应商介绍',
  `achievement` text COMMENT '供应商业绩',
  `times` decimal(8,0) DEFAULT NULL COMMENT '次数',
  `clicklogintime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nclicklogintime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `autoid` decimal(10,0) DEFAULT NULL,
  `supplierkind` varchar(64) DEFAULT NULL COMMENT '备用字段',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `lastlogintime` varchar(64) DEFAULT NULL COMMENT '最近登陆时间',
  `stepnumber` decimal(10,0) DEFAULT NULL COMMENT '注册到第几步',
  `pwdque` varchar(128) DEFAULT NULL COMMENT '密保问题',
  `pwdans` varchar(128) DEFAULT NULL COMMENT '密保答案',
  `createuserid` varchar(64) DEFAULT NULL,
  `createuserdeptid` varchar(64) DEFAULT NULL,
  `createuserdeptcode` varchar(64) DEFAULT NULL,
  `minespecialcost` decimal(19,5) DEFAULT NULL,
  `railcarriage` decimal(19,5) DEFAULT NULL,
  `datafrom` varchar(64) DEFAULT NULL,
  `is_fb` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`supplierid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='采购商基本信息';

-- ----------------------------
-- Records of t_fultbsupplier2
-- ----------------------------

-- ----------------------------
-- Table structure for t_fultbsupplier_attachment
-- ----------------------------
DROP TABLE IF EXISTS `t_fultbsupplier_attachment`;
CREATE TABLE `t_fultbsupplier_attachment` (
  `attachment_id` decimal(11,0) NOT NULL,
  `supplierid` varchar(36) DEFAULT NULL COMMENT '供应商ID',
  `attachment_path` varchar(255) NOT NULL COMMENT '附件路径',
  `attachment_name` varchar(100) NOT NULL COMMENT '附件名称',
  `filename` varchar(100) DEFAULT NULL COMMENT '文件名',
  `download_count` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '下载次数'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='供应商附件表;';

-- ----------------------------
-- Records of t_fultbsupplier_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for t_fultbtemplate
-- ----------------------------
DROP TABLE IF EXISTS `t_fultbtemplate`;
CREATE TABLE `t_fultbtemplate` (
  `templateid` varchar(36) NOT NULL,
  `operdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `autoid` decimal(10,0) DEFAULT NULL,
  `operuser` varchar(64) DEFAULT NULL,
  `createuser` varchar(64) DEFAULT NULL,
  `createdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `createuserid` varchar(64) DEFAULT NULL,
  `createuserdeptid` varchar(64) DEFAULT NULL,
  `createuserdeptcode` varchar(64) DEFAULT NULL,
  `permitstatus` varchar(64) DEFAULT NULL,
  `billnumber` varchar(64) DEFAULT NULL,
  `datafrom` varchar(16) DEFAULT NULL,
  `datafromname` varchar(64) DEFAULT NULL,
  `writername` varchar(64) DEFAULT NULL,
  `writeraccount` varchar(64) DEFAULT NULL,
  `writerdepartid` varchar(64) DEFAULT NULL,
  `writerdepartname` varchar(64) DEFAULT NULL,
  `writedate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `jhtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `jhtime_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `coaltype` varchar(64) DEFAULT NULL,
  `qty` decimal(19,5) DEFAULT NULL,
  `qty_end` decimal(19,5) DEFAULT NULL,
  `yunshu_mode` varchar(64) DEFAULT NULL,
  `jiaohuo_mode` varchar(64) DEFAULT NULL,
  `yanshou_mode` varchar(64) DEFAULT NULL,
  `jiesuan_mode` varchar(64) DEFAULT NULL,
  `type_1` varchar(64) DEFAULT NULL,
  `type_2` varchar(64) DEFAULT NULL,
  `type_3` varchar(64) DEFAULT NULL,
  `type_4` varchar(64) DEFAULT NULL,
  `type_5` varchar(64) DEFAULT NULL,
  `type_6` varchar(64) DEFAULT NULL,
  `type_7` varchar(64) DEFAULT NULL,
  `type_8` varchar(64) DEFAULT NULL,
  `remark` varchar(1024) DEFAULT NULL,
  `signname` varchar(64) DEFAULT NULL,
  `signaccount` varchar(64) DEFAULT NULL,
  `jhtime2` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type_32` varchar(64) DEFAULT NULL,
  `createbyah` varchar(16) DEFAULT NULL,
  `is_fb` varchar(16) DEFAULT NULL,
  `source` varchar(32) DEFAULT NULL,
  `type` varchar(36) DEFAULT NULL,
  `type_323` varchar(64) DEFAULT NULL,
  `type_333` varchar(64) DEFAULT NULL,
  `maxremark` varchar(1024) DEFAULT NULL,
  `type_11` varchar(64) DEFAULT NULL,
  `type_12` varchar(64) DEFAULT NULL,
  `type_14` varchar(64) DEFAULT NULL,
  `type_16` varchar(64) DEFAULT NULL,
  `coalclass` varchar(64) DEFAULT NULL,
  `kgj_1` varchar(64) DEFAULT NULL COMMENT '空干基水分',
  `kgj_2` varchar(64) DEFAULT NULL COMMENT '空干基全硫',
  `kgj_3` varchar(64) DEFAULT NULL COMMENT '空干基挥发分(小)',
  `kgj_4` varchar(64) DEFAULT NULL COMMENT '空干基挥发分(大)',
  PRIMARY KEY (`templateid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='燃料采购申请单模版';

-- ----------------------------
-- Records of t_fultbtemplate
-- ----------------------------

-- ----------------------------
-- Table structure for t_fultbwinbidding
-- ----------------------------
DROP TABLE IF EXISTS `t_fultbwinbidding`;
CREATE TABLE `t_fultbwinbidding` (
  `bidid` varchar(36) NOT NULL,
  `applydetailid` varchar(36) DEFAULT NULL COMMENT '供应商报价ID',
  `purchapplyid` varchar(36) NOT NULL COMMENT '采购单ID',
  `supplierid` varchar(64) DEFAULT NULL COMMENT '供应商ID',
  `user_uuid` varchar(255) DEFAULT NULL COMMENT '用户ID',
  `operdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `autoid` decimal(10,0) DEFAULT NULL,
  `operuser` varchar(64) DEFAULT NULL,
  `createuser` varchar(64) DEFAULT NULL,
  `createdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `createuserid` varchar(64) DEFAULT NULL,
  `createuserdeptid` varchar(64) DEFAULT NULL,
  `createuserdeptcode` varchar(64) DEFAULT NULL,
  `organcode` varchar(64) DEFAULT NULL,
  `zbqty` varchar(64) DEFAULT NULL COMMENT '中标数量（默认为最后一次报价数量）',
  `zbprice` varchar(64) DEFAULT NULL COMMENT '中标价格（默认为最后一次报价价格）',
  `is_bid` varchar(2) DEFAULT NULL COMMENT '是否中标（0否，1是，默认为0）',
  PRIMARY KEY (`bidid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='中标信息';

-- ----------------------------
-- Records of t_fultbwinbidding
-- ----------------------------

-- ----------------------------
-- Table structure for t_membertype
-- ----------------------------
DROP TABLE IF EXISTS `t_membertype`;
CREATE TABLE `t_membertype` (
  `membertypeid` varchar(36) NOT NULL COMMENT '类型ID',
  `membertypename` varchar(64) NOT NULL COMMENT '会员类型名称（采购商、供应商、贸易商等）',
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`membertypeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员类型表，用户存储会员类型信息';

-- ----------------------------
-- Records of t_membertype
-- ----------------------------

-- ----------------------------
-- Table structure for t_supgradeitem_data
-- ----------------------------
DROP TABLE IF EXISTS `t_supgradeitem_data`;
CREATE TABLE `t_supgradeitem_data` (
  `id` varchar(32) NOT NULL COMMENT '标识',
  `supplierid` varchar(36) DEFAULT NULL,
  `time_type_id` varchar(10) DEFAULT NULL COMMENT '时间类型:日,周,月,季,年',
  `item_id` varchar(32) DEFAULT NULL,
  `time_id` decimal(19,0) DEFAULT NULL,
  `item_code` varchar(50) DEFAULT NULL COMMENT '本编码可以从标准指标编码选择，也可自己添加，只代表指标编码，不冠前缀',
  `sys_res_code` varchar(200) DEFAULT NULL COMMENT '机构编码',
  `buyercode` varchar(32) DEFAULT NULL COMMENT '采购商编码',
  `item_value_fact` varchar(50) DEFAULT NULL COMMENT '实际值',
  `item_value_report` varchar(50) DEFAULT NULL COMMENT '上报值',
  `item_value_approve` varchar(50) DEFAULT NULL COMMENT '审批值',
  `state_id` decimal(2,0) DEFAULT NULL COMMENT '1：草稿状态 (保存)\r\n            2：初始状态 (提交)\r\n            3:  前期修改状态 (提交后到上报时间点前的修改状态)\r\n            4:  后期修改状态 (上报时间点后的修改状态)',
  `version_id` decimal(4,0) DEFAULT NULL,
  `fill_in_person` varchar(32) DEFAULT NULL,
  `date_id` datetime DEFAULT NULL COMMENT '日期标识',
  `end_update_date` datetime DEFAULT NULL,
  `remarks` varchar(100) DEFAULT NULL COMMENT '备注',
  `auditing_flag` varchar(1) DEFAULT NULL COMMENT '审核标志',
  `report_set` varchar(32) DEFAULT NULL COMMENT '上报类型:1-不上报，2-上报集团，3-上报分公司，4-上报集团和分公司',
  `is_gs_check` varchar(10) DEFAULT NULL COMMENT '是否公司审核：1：是，分公司审核；0：否，分公司不审核',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='供应商评级数据明细表（每月评一次）\r\n\r\n';

-- ----------------------------
-- Records of t_supgradeitem_data
-- ----------------------------

-- ----------------------------
-- Table structure for t_suppliergrade
-- ----------------------------
DROP TABLE IF EXISTS `t_suppliergrade`;
CREATE TABLE `t_suppliergrade` (
  `gradeid` varchar(32) NOT NULL COMMENT '主键',
  `grade_name` varchar(128) DEFAULT NULL COMMENT '名称',
  `sort` decimal(10,0) DEFAULT NULL COMMENT '排序',
  `remark` varchar(128) DEFAULT NULL COMMENT '备注'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='存储供应商等级分类数据（重要供应商、一般供应商、临时供应商等）';

-- ----------------------------
-- Records of t_suppliergrade
-- ----------------------------

-- ----------------------------
-- Table structure for t_suppliergrade5
-- ----------------------------
DROP TABLE IF EXISTS `t_suppliergrade5`;
CREATE TABLE `t_suppliergrade5` (
  `supplierid` varchar(36) DEFAULT NULL,
  `gradeid` varchar(32) NOT NULL COMMENT '主键',
  `grade_name` varchar(128) DEFAULT NULL COMMENT '名称',
  `sort` decimal(10,0) DEFAULT NULL COMMENT '排序',
  `remark` varchar(128) DEFAULT NULL COMMENT '备注'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='存储供应商等级分类数据（重要供应商、一般供应商、临时供应商等）';

-- ----------------------------
-- Records of t_suppliergrade5
-- ----------------------------

-- ----------------------------
-- Table structure for t_suppliergradeitem
-- ----------------------------
DROP TABLE IF EXISTS `t_suppliergradeitem`;
CREATE TABLE `t_suppliergradeitem` (
  `item_id` varchar(32) NOT NULL COMMENT '指标标识',
  `item_code` varchar(50) NOT NULL COMMENT '指标代码',
  `item_name` varchar(128) DEFAULT NULL COMMENT '指标名称',
  `mprecision` decimal(8,0) DEFAULT NULL COMMENT '精度',
  `sort` decimal(10,0) DEFAULT NULL COMMENT '排序',
  `weight` decimal(6,2) DEFAULT NULL COMMENT '权重',
  `remark` varchar(128) DEFAULT NULL COMMENT '备注'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='存储供应商评级相关权重指标';

-- ----------------------------
-- Records of t_suppliergradeitem
-- ----------------------------

-- ----------------------------
-- Table structure for t_watrr_account
-- ----------------------------
DROP TABLE IF EXISTS `t_watrr_account`;
CREATE TABLE `t_watrr_account` (
  `watera_account_id` varchar(128) NOT NULL COMMENT '流水账ID',
  `account_id` varchar(128) DEFAULT NULL COMMENT '账户ID',
  `watera_account_code` varchar(128) DEFAULT NULL COMMENT '流水账code',
  `organ_code` varchar(128) DEFAULT NULL COMMENT '单位_code',
  `opt_member` varchar(128) DEFAULT NULL COMMENT '操作人',
  `opt_time` datetime DEFAULT NULL COMMENT '操作时间',
  `account` decimal(8,0) DEFAULT NULL COMMENT '金额',
  `type` varchar(2) DEFAULT NULL COMMENT '缴纳类型(1入账2出账)',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `remittance_type` varchar(128) DEFAULT NULL COMMENT '汇款方式',
  `remittance_time` datetime DEFAULT NULL COMMENT '汇款时间',
  PRIMARY KEY (`watera_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='账户流水帐(现金缴纳和退还)';

-- ----------------------------
-- Records of t_watrr_account
-- ----------------------------

-- ----------------------------
-- Table structure for t_watrr_earnest_account
-- ----------------------------
DROP TABLE IF EXISTS `t_watrr_earnest_account`;
CREATE TABLE `t_watrr_earnest_account` (
  `t_watrr_earnest_account` varchar(128) NOT NULL COMMENT '流水账ID',
  `account_id` varchar(128) DEFAULT NULL COMMENT '账户ID',
  `purchapplyid` varchar(36) DEFAULT NULL,
  `t_watrr_earnest_account_code` varchar(128) DEFAULT NULL COMMENT '流水账code',
  `organ_code` varchar(128) DEFAULT NULL COMMENT '操作单位',
  `opt_member` varchar(128) DEFAULT NULL COMMENT '操作人',
  `opt_time` datetime DEFAULT NULL COMMENT '时间',
  `account` datetime DEFAULT NULL COMMENT '金额',
  `type` varchar(16) DEFAULT NULL COMMENT '操作种类（1.冻结，2解冻）',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `option_remark` varchar(500) DEFAULT NULL COMMENT '操作说明',
  PRIMARY KEY (`t_watrr_earnest_account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='账户流水帐(平台保证金虚拟账户冻结和解冻)';

-- ----------------------------
-- Records of t_watrr_earnest_account
-- ----------------------------
