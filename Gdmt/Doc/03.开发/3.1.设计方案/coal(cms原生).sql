/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : coal

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-02-06 14:47:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for jc_acquisition
-- ----------------------------
DROP TABLE IF EXISTS `jc_acquisition`;
CREATE TABLE `jc_acquisition` (
  `acquisition_id` decimal(11,0) NOT NULL,
  `acq_name` varchar(50) NOT NULL COMMENT '采集名称',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '停止时间',
  `status` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '当前状态(0:静止;1:采集;2:暂停)',
  `curr_num` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '当前号码',
  `curr_item` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '当前条数',
  `total_item` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '每页总条数',
  `pause_time` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '暂停时间(毫秒)',
  `page_encoding` varchar(20) NOT NULL DEFAULT 'GBK' COMMENT '页面编码',
  `plan_list` text COMMENT '采集列表',
  `dynamic_addr` varchar(255) DEFAULT NULL COMMENT '动态地址',
  `dynamic_start` decimal(11,0) DEFAULT NULL COMMENT '页码开始',
  `dynamic_end` decimal(11,0) DEFAULT NULL COMMENT '页码结束',
  `linkset_start` varchar(255) DEFAULT NULL COMMENT '内容链接区开始',
  `linkset_end` varchar(255) DEFAULT NULL COMMENT '内容链接区结束',
  `link_start` varchar(255) DEFAULT NULL COMMENT '内容链接开始',
  `link_end` varchar(255) DEFAULT NULL COMMENT '内容链接结束',
  `title_start` varchar(255) DEFAULT NULL COMMENT '标题开始',
  `title_end` varchar(255) DEFAULT NULL COMMENT '标题结束',
  `keywords_start` varchar(255) DEFAULT NULL COMMENT '关键字开始',
  `keywords_end` varchar(255) DEFAULT NULL COMMENT '关键字结束',
  `description_start` varchar(255) DEFAULT NULL COMMENT '描述开始',
  `description_end` varchar(255) DEFAULT NULL COMMENT '描述结束',
  `content_start` varchar(255) DEFAULT NULL COMMENT '内容开始',
  `content_end` varchar(255) DEFAULT NULL COMMENT '内容结束',
  `pagination_start` varchar(255) DEFAULT NULL COMMENT '内容分页开始',
  `pagination_end` varchar(255) DEFAULT NULL COMMENT '内容分页结束',
  `queue` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '队列',
  `repeat_check_type` varchar(20) DEFAULT NULL COMMENT '重复类型',
  `img_acqu` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否采集图片',
  `content_prefix` varchar(255) DEFAULT NULL COMMENT '内容地址补全url',
  `img_prefix` varchar(255) DEFAULT NULL COMMENT '图片地址补全url',
  `view_start` varchar(255) DEFAULT NULL COMMENT '浏览量开始',
  `view_end` varchar(255) DEFAULT NULL COMMENT '浏览量结束',
  `view_id_start` varchar(255) DEFAULT NULL COMMENT 'id前缀',
  `view_id_end` varchar(255) DEFAULT NULL COMMENT 'id后缀',
  `view_link` varchar(255) DEFAULT NULL COMMENT '浏览量动态访问地址',
  `releasetime_start` varchar(255) DEFAULT NULL COMMENT '发布时间开始',
  `releasetime_end` varchar(255) DEFAULT NULL COMMENT '发布时间结束',
  `author_start` varchar(255) DEFAULT NULL COMMENT '作者开始',
  `author_end` varchar(255) DEFAULT NULL COMMENT '作者结束',
  `origin_start` varchar(255) DEFAULT NULL COMMENT '来源开始',
  `origin_end` varchar(255) DEFAULT NULL COMMENT '来源结束',
  `releasetime_format` varchar(255) DEFAULT NULL COMMENT '发布时间格式',
  PRIMARY KEY (`acquisition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS采集表;';

-- ----------------------------
-- Records of jc_acquisition
-- ----------------------------

-- ----------------------------
-- Table structure for jc_acquisition_history
-- ----------------------------
DROP TABLE IF EXISTS `jc_acquisition_history`;
CREATE TABLE `jc_acquisition_history` (
  `history_id` decimal(11,0) NOT NULL,
  `channel_url` varchar(255) NOT NULL COMMENT '栏目地址',
  `content_url` varchar(255) NOT NULL COMMENT '内容地址',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `description` varchar(20) NOT NULL COMMENT '描述',
  `content_id` decimal(11,0) DEFAULT NULL COMMENT '内容',
  PRIMARY KEY (`history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='采集历史记录表;';

-- ----------------------------
-- Records of jc_acquisition_history
-- ----------------------------

-- ----------------------------
-- Table structure for jc_acquisition_temp
-- ----------------------------
DROP TABLE IF EXISTS `jc_acquisition_temp`;
CREATE TABLE `jc_acquisition_temp` (
  `temp_id` decimal(11,0) NOT NULL,
  `channel_url` varchar(255) NOT NULL COMMENT '栏目地址',
  `content_url` varchar(255) NOT NULL COMMENT '内容地址',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `percent` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '百分比',
  `description` varchar(20) NOT NULL COMMENT '描述',
  `seq` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '顺序',
  PRIMARY KEY (`temp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='采集进度临时表;';

-- ----------------------------
-- Records of jc_acquisition_temp
-- ----------------------------

-- ----------------------------
-- Table structure for jc_advertising
-- ----------------------------
DROP TABLE IF EXISTS `jc_advertising`;
CREATE TABLE `jc_advertising` (
  `advertising_id` decimal(11,0) NOT NULL,
  `ad_name` varchar(100) NOT NULL COMMENT '广告名称',
  `category` varchar(50) NOT NULL COMMENT '广告类型',
  `ad_code` text COMMENT '广告代码',
  `ad_weight` decimal(11,0) NOT NULL DEFAULT '1' COMMENT '广告权重',
  `display_count` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '展现次数',
  `click_count` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '点击次数',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `is_enabled` char(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
  PRIMARY KEY (`advertising_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS广告表;';

-- ----------------------------
-- Records of jc_advertising
-- ----------------------------

-- ----------------------------
-- Table structure for jc_advertising_attr
-- ----------------------------
DROP TABLE IF EXISTS `jc_advertising_attr`;
CREATE TABLE `jc_advertising_attr` (
  `attr_name` varchar(50) NOT NULL COMMENT '名称',
  `attr_value` varchar(255) DEFAULT NULL COMMENT '值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS广告属性表;';

-- ----------------------------
-- Records of jc_advertising_attr
-- ----------------------------

-- ----------------------------
-- Table structure for jc_advertising_space
-- ----------------------------
DROP TABLE IF EXISTS `jc_advertising_space`;
CREATE TABLE `jc_advertising_space` (
  `adspace_id` decimal(11,0) NOT NULL,
  `ad_name` varchar(100) NOT NULL COMMENT '名称',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `is_enabled` char(1) NOT NULL COMMENT '是否启用',
  PRIMARY KEY (`adspace_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS广告版位表;';

-- ----------------------------
-- Records of jc_advertising_space
-- ----------------------------

-- ----------------------------
-- Table structure for jc_channel
-- ----------------------------
DROP TABLE IF EXISTS `jc_channel`;
CREATE TABLE `jc_channel` (
  `channel_id` decimal(11,0) NOT NULL,
  `channel_path` varchar(30) DEFAULT NULL COMMENT '访问路径',
  `lft` decimal(11,0) NOT NULL DEFAULT '1' COMMENT '树左边',
  `rgt` decimal(11,0) NOT NULL DEFAULT '2' COMMENT '树右边',
  `priority` decimal(11,0) NOT NULL DEFAULT '10' COMMENT '排列顺序',
  `has_content` decimal(1,0) NOT NULL DEFAULT '1' COMMENT '是否有内容',
  `is_display` decimal(1,0) NOT NULL DEFAULT '1' COMMENT '是否显示',
  `workflow_id` decimal(11,0) DEFAULT NULL COMMENT '工作流id',
  PRIMARY KEY (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS栏目表;';

-- ----------------------------
-- Records of jc_channel
-- ----------------------------

-- ----------------------------
-- Table structure for jc_channel_attr
-- ----------------------------
DROP TABLE IF EXISTS `jc_channel_attr`;
CREATE TABLE `jc_channel_attr` (
  `attr_name` varchar(30) NOT NULL COMMENT '名称',
  `attr_value` varchar(255) DEFAULT NULL COMMENT '值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS栏目扩展属性表;';

-- ----------------------------
-- Records of jc_channel_attr
-- ----------------------------

-- ----------------------------
-- Table structure for jc_channel_count
-- ----------------------------
DROP TABLE IF EXISTS `jc_channel_count`;
CREATE TABLE `jc_channel_count` (
  `channel_id` decimal(11,0) NOT NULL,
  `views` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '日访问数',
  `views_month` decimal(11,0) NOT NULL DEFAULT '0',
  `views_week` decimal(11,0) NOT NULL DEFAULT '0',
  `views_day` decimal(11,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS栏目访问量计数表';

-- ----------------------------
-- Records of jc_channel_count
-- ----------------------------

-- ----------------------------
-- Table structure for jc_channel_department
-- ----------------------------
DROP TABLE IF EXISTS `jc_channel_department`;
CREATE TABLE `jc_channel_department` (
  `channel_id` decimal(11,0) NOT NULL,
  `department_id` decimal(11,0) NOT NULL,
  PRIMARY KEY (`channel_id`,`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS栏目部门关联表;';

-- ----------------------------
-- Records of jc_channel_department
-- ----------------------------

-- ----------------------------
-- Table structure for jc_channel_ext
-- ----------------------------
DROP TABLE IF EXISTS `jc_channel_ext`;
CREATE TABLE `jc_channel_ext` (
  `channel_id` decimal(11,0) NOT NULL,
  `channel_name` varchar(100) NOT NULL COMMENT '名称',
  `final_step` decimal(1,0) DEFAULT '2' COMMENT '终审级别',
  `after_check` decimal(1,0) DEFAULT NULL COMMENT '审核后(1:不能修改删除;2:修改后退回;3:修改后不变)',
  `is_static_channel` char(1) NOT NULL DEFAULT '0' COMMENT '是否栏目静态化',
  `is_static_content` char(1) NOT NULL DEFAULT '0' COMMENT '是否内容静态化',
  `is_access_by_dir` char(1) NOT NULL DEFAULT '1' COMMENT '是否使用目录访问',
  `is_list_child` char(1) NOT NULL DEFAULT '0' COMMENT '是否使用子栏目列表',
  `page_size` decimal(11,0) NOT NULL DEFAULT '20' COMMENT '每页多少条记录',
  `channel_rule` varchar(150) DEFAULT NULL COMMENT '栏目页生成规则',
  `content_rule` varchar(150) DEFAULT NULL COMMENT '内容页生成规则',
  `link` varchar(255) DEFAULT NULL COMMENT '外部链接',
  `tpl_channel` varchar(100) DEFAULT NULL COMMENT '栏目页模板',
  `tpl_content` varchar(100) DEFAULT NULL COMMENT '内容页模板',
  `title_img` varchar(100) DEFAULT NULL COMMENT '缩略图',
  `content_img` varchar(100) DEFAULT NULL COMMENT '内容图',
  `has_title_img` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '内容是否有缩略图',
  `has_content_img` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '内容是否有内容图',
  `title_img_width` decimal(11,0) NOT NULL DEFAULT '139' COMMENT '内容标题图宽度',
  `title_img_height` decimal(11,0) NOT NULL DEFAULT '139' COMMENT '内容标题图高度',
  `content_img_width` decimal(11,0) NOT NULL DEFAULT '310' COMMENT '内容内容图宽度',
  `content_img_height` decimal(11,0) NOT NULL DEFAULT '310' COMMENT '内容内容图高度',
  `comment_control` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '评论(0:匿名;1:会员;2:关闭)',
  `allow_updown` decimal(1,0) NOT NULL DEFAULT '1' COMMENT '顶踩(true:开放;false:关闭)',
  `is_blank` decimal(1,0) NOT NULL DEFAULT '1' COMMENT '是否新窗口打开',
  `title` varchar(255) DEFAULT NULL COMMENT 'TITLE',
  `keywords` varchar(255) DEFAULT NULL COMMENT 'KEYWORDS',
  `description` varchar(255) DEFAULT NULL COMMENT 'DESCRIPTION',
  `allow_share` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '分享(true:开放;false:关闭)',
  `allow_score` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '评分(true:开放;false:关闭)',
  PRIMARY KEY (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS栏目内容表;';

-- ----------------------------
-- Records of jc_channel_ext
-- ----------------------------

-- ----------------------------
-- Table structure for jc_channel_model
-- ----------------------------
DROP TABLE IF EXISTS `jc_channel_model`;
CREATE TABLE `jc_channel_model` (
  `channel_id` decimal(11,0) NOT NULL,
  `tpl_content` varchar(100) DEFAULT NULL COMMENT '内容模板',
  `priority` decimal(11,0) NOT NULL DEFAULT '10' COMMENT '排序',
  PRIMARY KEY (`channel_id`,`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='栏目可选内容模型表;';

-- ----------------------------
-- Records of jc_channel_model
-- ----------------------------

-- ----------------------------
-- Table structure for jc_channel_txt
-- ----------------------------
DROP TABLE IF EXISTS `jc_channel_txt`;
CREATE TABLE `jc_channel_txt` (
  `channel_id` decimal(11,0) NOT NULL,
  `txt` text COMMENT '栏目内容',
  `txt1` text COMMENT '扩展内容1',
  `txt2` text COMMENT '扩展内容2',
  `txt3` text COMMENT '扩展内容3',
  PRIMARY KEY (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS栏目文本表;';

-- ----------------------------
-- Records of jc_channel_txt
-- ----------------------------

-- ----------------------------
-- Table structure for jc_channel_user
-- ----------------------------
DROP TABLE IF EXISTS `jc_channel_user`;
CREATE TABLE `jc_channel_user` (
  `channel_id` decimal(11,0) NOT NULL,
  `user_id` decimal(11,0) NOT NULL,
  PRIMARY KEY (`channel_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS栏目用户关联表;';

-- ----------------------------
-- Records of jc_channel_user
-- ----------------------------

-- ----------------------------
-- Table structure for jc_chnl_group_contri
-- ----------------------------
DROP TABLE IF EXISTS `jc_chnl_group_contri`;
CREATE TABLE `jc_chnl_group_contri` (
  `channel_id` decimal(11,0) NOT NULL,
  `group_id` decimal(11,0) NOT NULL,
  PRIMARY KEY (`channel_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS栏目投稿会员组关联表;';

-- ----------------------------
-- Records of jc_chnl_group_contri
-- ----------------------------

-- ----------------------------
-- Table structure for jc_chnl_group_view
-- ----------------------------
DROP TABLE IF EXISTS `jc_chnl_group_view`;
CREATE TABLE `jc_chnl_group_view` (
  `channel_id` decimal(11,0) NOT NULL,
  `group_id` decimal(11,0) NOT NULL,
  PRIMARY KEY (`channel_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS栏目浏览会员组关联表;';

-- ----------------------------
-- Records of jc_chnl_group_view
-- ----------------------------

-- ----------------------------
-- Table structure for jc_comment
-- ----------------------------
DROP TABLE IF EXISTS `jc_comment`;
CREATE TABLE `jc_comment` (
  `comment_id` decimal(11,0) NOT NULL,
  `create_time` datetime NOT NULL COMMENT '评论时间',
  `reply_time` datetime DEFAULT NULL COMMENT '回复时间',
  `ups` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '支持数',
  `downs` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '反对数',
  `is_recommend` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否推荐',
  `is_checked` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否审核',
  `score` decimal(11,0) DEFAULT NULL COMMENT '评分',
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS评论表;';

-- ----------------------------
-- Records of jc_comment
-- ----------------------------

-- ----------------------------
-- Table structure for jc_comment_ext
-- ----------------------------
DROP TABLE IF EXISTS `jc_comment_ext`;
CREATE TABLE `jc_comment_ext` (
  `ip` varchar(50) DEFAULT NULL COMMENT 'IP地址',
  `text` text COMMENT '评论内容',
  `reply` text COMMENT '回复内容'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS评论扩展表;';

-- ----------------------------
-- Records of jc_comment_ext
-- ----------------------------

-- ----------------------------
-- Table structure for jc_config
-- ----------------------------
DROP TABLE IF EXISTS `jc_config`;
CREATE TABLE `jc_config` (
  `config_id` decimal(11,0) NOT NULL,
  `context_path` varchar(20) DEFAULT '/jeecms' COMMENT '部署路径',
  `servlet_point` varchar(20) DEFAULT NULL COMMENT 'Servlet挂载点',
  `port` decimal(11,0) DEFAULT NULL COMMENT '端口',
  `db_file_uri` varchar(50) NOT NULL DEFAULT '/dbfile.svl?n=' COMMENT '数据库附件访问地址',
  `is_upload_to_db` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '上传附件至数据库',
  `def_img` varchar(255) NOT NULL DEFAULT '/jeecms/r/cms/www/default/no_picture.gif' COMMENT '图片不存在时默认图片',
  `login_url` varchar(255) NOT NULL DEFAULT 'login.jspx' COMMENT '登录地址',
  `process_url` varchar(255) DEFAULT NULL COMMENT '登录后处理地址',
  `mark_on` decimal(1,0) NOT NULL DEFAULT '1' COMMENT '开启图片水印',
  `mark_width` decimal(11,0) NOT NULL DEFAULT '120' COMMENT '图片最小宽度',
  `mark_height` decimal(11,0) NOT NULL DEFAULT '120' COMMENT '图片最小高度',
  `mark_image` varchar(100) DEFAULT '/r/cms/www/watermark.png' COMMENT '图片水印',
  `mark_content` varchar(100) NOT NULL DEFAULT 'www.jeecms.com' COMMENT '文字水印内容',
  `mark_size` decimal(11,0) NOT NULL DEFAULT '20' COMMENT '文字水印大小',
  `mark_color` varchar(10) NOT NULL DEFAULT '#FF0000' COMMENT '文字水印颜色',
  `mark_alpha` decimal(11,0) NOT NULL DEFAULT '50' COMMENT '水印透明度（0-100）',
  `mark_position` decimal(11,0) NOT NULL DEFAULT '1' COMMENT '水印位置(0-5)',
  `mark_offset_x` decimal(11,0) NOT NULL DEFAULT '0' COMMENT 'x坐标偏移量',
  `mark_offset_y` decimal(11,0) NOT NULL DEFAULT '0' COMMENT 'y坐标偏移量',
  `count_clear_time` datetime NOT NULL COMMENT '计数器清除时间',
  `count_copy_time` datetime NOT NULL COMMENT '计数器拷贝时间',
  `download_code` varchar(32) NOT NULL DEFAULT 'JEECMS' COMMENT '下载防盗链md5混淆码',
  `download_time` decimal(11,0) NOT NULL DEFAULT '12' COMMENT '下载有效时间（小时）',
  `email_host` varchar(50) DEFAULT NULL COMMENT '邮件发送服务器',
  `email_encoding` varchar(20) DEFAULT NULL COMMENT '邮件发送编码',
  `email_username` varchar(100) DEFAULT NULL COMMENT '邮箱用户名',
  `email_password` varchar(100) DEFAULT NULL COMMENT '邮箱密码',
  `email_personal` varchar(100) DEFAULT NULL COMMENT '邮箱发件人',
  `email_validate` decimal(1,0) DEFAULT '0' COMMENT '开启邮箱验证',
  `office_home` varchar(255) NOT NULL COMMENT 'openoffice安装目录',
  `office_port` varchar(10) NOT NULL DEFAULT '8810' COMMENT 'openoffice端口',
  `swftools_home` varchar(255) NOT NULL COMMENT 'swftoos安装目录',
  `view_only_checked` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '只有终审才能浏览内容页',
  `inside_site` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '内网（通过站点路径区分站点）',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS配置表;';

-- ----------------------------
-- Records of jc_config
-- ----------------------------

-- ----------------------------
-- Table structure for jc_config_attr
-- ----------------------------
DROP TABLE IF EXISTS `jc_config_attr`;
CREATE TABLE `jc_config_attr` (
  `attr_name` varchar(30) NOT NULL COMMENT '名称',
  `attr_value` varchar(255) DEFAULT NULL COMMENT '值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS配置属性表;';

-- ----------------------------
-- Records of jc_config_attr
-- ----------------------------

-- ----------------------------
-- Table structure for jc_config_item
-- ----------------------------
DROP TABLE IF EXISTS `jc_config_item`;
CREATE TABLE `jc_config_item` (
  `modelitem_id` decimal(11,0) NOT NULL,
  `field` varchar(50) NOT NULL COMMENT '字段',
  `item_label` varchar(100) NOT NULL COMMENT '名称',
  `priority` decimal(11,0) NOT NULL DEFAULT '70' COMMENT '排序',
  `def_value` varchar(255) DEFAULT NULL COMMENT '默认值',
  `opt_value` varchar(255) DEFAULT NULL COMMENT '可选项',
  `text_size` varchar(20) DEFAULT NULL COMMENT '长度',
  `area_rows` varchar(3) DEFAULT NULL COMMENT '文本行数',
  `area_cols` varchar(3) DEFAULT NULL COMMENT '文本列数',
  `help` varchar(255) DEFAULT NULL COMMENT '帮助信息',
  `help_position` varchar(1) DEFAULT NULL,
  `data_type` decimal(11,0) NOT NULL DEFAULT '1' COMMENT '数据类型 "1":"字符串文本","2":"整型文本","3":"浮点型文本","4":"文本区","5":"日期","6":"下拉列表","7":"复选框","8":"单选框"',
  `is_required` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否必填',
  `category` decimal(11,0) NOT NULL DEFAULT '1' COMMENT '模型项目所属分类（1会员注册模型）',
  PRIMARY KEY (`modelitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS配置模型项表';

-- ----------------------------
-- Records of jc_config_item
-- ----------------------------

-- ----------------------------
-- Table structure for jc_content
-- ----------------------------
DROP TABLE IF EXISTS `jc_content`;
CREATE TABLE `jc_content` (
  `content_id` decimal(11,0) NOT NULL,
  `sort_date` datetime NOT NULL COMMENT '排序日期',
  `top_level` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '固顶级别',
  `has_title_img` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否有标题图',
  `is_recommend` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否推荐',
  `status` decimal(1,0) NOT NULL DEFAULT '2' COMMENT '状态(0:草稿;1:审核中;2:审核通过;3:回收站；4:投稿)',
  `views_day` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '日访问数',
  `comments_day` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '日评论数',
  `downloads_day` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '日下载数',
  `ups_day` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '日顶数',
  `score` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '得分',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS内容表;';

-- ----------------------------
-- Records of jc_content
-- ----------------------------

-- ----------------------------
-- Table structure for jc_contenttag
-- ----------------------------
DROP TABLE IF EXISTS `jc_contenttag`;
CREATE TABLE `jc_contenttag` (
  `priority` decimal(11,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS内容标签关联表;';

-- ----------------------------
-- Records of jc_contenttag
-- ----------------------------

-- ----------------------------
-- Table structure for jc_content_attachment
-- ----------------------------
DROP TABLE IF EXISTS `jc_content_attachment`;
CREATE TABLE `jc_content_attachment` (
  `priority` decimal(11,0) NOT NULL COMMENT '排列顺序',
  `attachment_path` varchar(255) NOT NULL COMMENT '附件路径',
  `attachment_name` varchar(100) NOT NULL COMMENT '附件名称',
  `filename` varchar(100) DEFAULT NULL COMMENT '文件名',
  `download_count` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '下载次数'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS内容附件表;';

-- ----------------------------
-- Records of jc_content_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for jc_content_attr
-- ----------------------------
DROP TABLE IF EXISTS `jc_content_attr`;
CREATE TABLE `jc_content_attr` (
  `attr_name` varchar(30) NOT NULL COMMENT '名称',
  `attr_value` varchar(255) DEFAULT NULL COMMENT '值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS内容扩展属性表;';

-- ----------------------------
-- Records of jc_content_attr
-- ----------------------------

-- ----------------------------
-- Table structure for jc_content_channel
-- ----------------------------
DROP TABLE IF EXISTS `jc_content_channel`;
CREATE TABLE `jc_content_channel` (
  `channel_id` decimal(11,0) NOT NULL,
  `content_id` decimal(11,0) NOT NULL,
  PRIMARY KEY (`channel_id`,`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS内容栏目关联表;';

-- ----------------------------
-- Records of jc_content_channel
-- ----------------------------

-- ----------------------------
-- Table structure for jc_content_check
-- ----------------------------
DROP TABLE IF EXISTS `jc_content_check`;
CREATE TABLE `jc_content_check` (
  `content_id` decimal(11,0) NOT NULL,
  `check_step` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '审核步数',
  `check_opinion` varchar(255) DEFAULT NULL COMMENT '审核意见',
  `is_rejected` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否退回',
  `check_date` datetime DEFAULT NULL COMMENT '终审时间',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS内容审核信息表; ';

-- ----------------------------
-- Records of jc_content_check
-- ----------------------------

-- ----------------------------
-- Table structure for jc_content_count
-- ----------------------------
DROP TABLE IF EXISTS `jc_content_count`;
CREATE TABLE `jc_content_count` (
  `content_id` decimal(11,0) NOT NULL,
  `views` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '总访问数',
  `views_month` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '月访问数',
  `views_week` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '周访问数',
  `views_day` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '日访问数',
  `comments` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '总评论数',
  `comments_month` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '月评论数',
  `comments_week` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '周评论数',
  `comments_day` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '日评论数',
  `downloads` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '总下载数',
  `downloads_month` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '月下载数',
  `downloads_week` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '周下载数',
  `downloads_day` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '日下载数',
  `ups` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '总顶数',
  `ups_month` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '月顶数',
  `ups_week` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '周顶数',
  `ups_day` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '日顶数',
  `downs` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '总踩数',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS内容计数表;';

-- ----------------------------
-- Records of jc_content_count
-- ----------------------------

-- ----------------------------
-- Table structure for jc_content_doc
-- ----------------------------
DROP TABLE IF EXISTS `jc_content_doc`;
CREATE TABLE `jc_content_doc` (
  `content_id` decimal(11,0) NOT NULL,
  `doc_path` varchar(255) NOT NULL COMMENT '文档路径',
  `swf_path` varchar(255) DEFAULT NULL COMMENT '转换的swf路径',
  `grain` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '财富收益',
  `down_need` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '下载需要财富',
  `is_open` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否开放',
  `file_suffix` varchar(10) NOT NULL COMMENT '文档文件格式',
  `avg_score` float NOT NULL DEFAULT '0' COMMENT '平均得分',
  `swf_num` decimal(11,0) NOT NULL DEFAULT '1' COMMENT 'swf文件总量',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文库相关;';

-- ----------------------------
-- Records of jc_content_doc
-- ----------------------------

-- ----------------------------
-- Table structure for jc_content_ext
-- ----------------------------
DROP TABLE IF EXISTS `jc_content_ext`;
CREATE TABLE `jc_content_ext` (
  `content_id` decimal(11,0) NOT NULL,
  `title` varchar(150) NOT NULL COMMENT '标题',
  `short_title` varchar(150) DEFAULT NULL COMMENT '简短标题',
  `author` varchar(100) DEFAULT NULL COMMENT '作者',
  `origin` varchar(100) DEFAULT NULL COMMENT '来源',
  `origin_url` varchar(255) DEFAULT NULL COMMENT '来源链接',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `release_date` datetime NOT NULL COMMENT '发布日期',
  `media_path` varchar(255) DEFAULT NULL COMMENT '媒体路径',
  `media_type` varchar(20) DEFAULT NULL COMMENT '媒体类型',
  `title_color` varchar(10) DEFAULT NULL COMMENT '标题颜色',
  `is_bold` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否加粗',
  `title_img` varchar(100) DEFAULT NULL COMMENT '标题图片',
  `content_img` varchar(100) DEFAULT NULL COMMENT '内容图片',
  `type_img` varchar(100) DEFAULT NULL COMMENT '类型图片',
  `link` varchar(255) DEFAULT NULL COMMENT '外部链接',
  `tpl_content` varchar(100) DEFAULT NULL COMMENT '指定模板',
  `need_regenerate` decimal(1,0) NOT NULL DEFAULT '1' COMMENT '需要重新生成静态页',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS内容扩展表;';

-- ----------------------------
-- Records of jc_content_ext
-- ----------------------------

-- ----------------------------
-- Table structure for jc_content_group_view
-- ----------------------------
DROP TABLE IF EXISTS `jc_content_group_view`;
CREATE TABLE `jc_content_group_view` (
  `content_id` decimal(11,0) NOT NULL,
  `group_id` decimal(11,0) NOT NULL,
  PRIMARY KEY (`content_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS内容浏览会员组关联表;';

-- ----------------------------
-- Records of jc_content_group_view
-- ----------------------------

-- ----------------------------
-- Table structure for jc_content_picture
-- ----------------------------
DROP TABLE IF EXISTS `jc_content_picture`;
CREATE TABLE `jc_content_picture` (
  `content_id` decimal(11,0) NOT NULL,
  `priority` decimal(11,0) NOT NULL COMMENT '排列顺序',
  `img_path` varchar(100) NOT NULL COMMENT '图片地址',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`content_id`,`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS内容图片表;';

-- ----------------------------
-- Records of jc_content_picture
-- ----------------------------

-- ----------------------------
-- Table structure for jc_content_share_check
-- ----------------------------
DROP TABLE IF EXISTS `jc_content_share_check`;
CREATE TABLE `jc_content_share_check` (
  `share_check_id` decimal(11,0) NOT NULL,
  `check_status` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '审核状态 0待审核 1审核通过 2推送',
  `check_opinion` varchar(255) DEFAULT NULL COMMENT '审核意见',
  `share_valid` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '共享有效性',
  PRIMARY KEY (`share_check_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS共享内容审核信息表;';

-- ----------------------------
-- Records of jc_content_share_check
-- ----------------------------

-- ----------------------------
-- Table structure for jc_content_tag
-- ----------------------------
DROP TABLE IF EXISTS `jc_content_tag`;
CREATE TABLE `jc_content_tag` (
  `tag_id` decimal(11,0) NOT NULL,
  `tag_name` varchar(50) NOT NULL COMMENT 'tag名称',
  `ref_counter` decimal(11,0) NOT NULL DEFAULT '1' COMMENT '被引用的次数',
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS内容TAG表;';

-- ----------------------------
-- Records of jc_content_tag
-- ----------------------------

-- ----------------------------
-- Table structure for jc_content_topic
-- ----------------------------
DROP TABLE IF EXISTS `jc_content_topic`;
CREATE TABLE `jc_content_topic` (
  `content_id` decimal(11,0) NOT NULL,
  `topic_id` decimal(11,0) NOT NULL,
  PRIMARY KEY (`content_id`,`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS专题内容关联表;';

-- ----------------------------
-- Records of jc_content_topic
-- ----------------------------

-- ----------------------------
-- Table structure for jc_content_txt
-- ----------------------------
DROP TABLE IF EXISTS `jc_content_txt`;
CREATE TABLE `jc_content_txt` (
  `content_id` decimal(11,0) NOT NULL,
  `txt` text COMMENT '文章内容',
  `txt1` text COMMENT '扩展内容1',
  `txt2` text COMMENT '扩展内容2',
  `txt3` text COMMENT '扩展内容3',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS内容文本表;';

-- ----------------------------
-- Records of jc_content_txt
-- ----------------------------

-- ----------------------------
-- Table structure for jc_content_type
-- ----------------------------
DROP TABLE IF EXISTS `jc_content_type`;
CREATE TABLE `jc_content_type` (
  `type_id` decimal(11,0) NOT NULL,
  `type_name` varchar(20) NOT NULL COMMENT '名称',
  `img_width` decimal(11,0) DEFAULT NULL COMMENT '图片宽',
  `img_height` decimal(11,0) DEFAULT NULL COMMENT '图片高',
  `has_image` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否有图片',
  `is_disabled` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否禁用',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS内容类型表;';

-- ----------------------------
-- Records of jc_content_type
-- ----------------------------

-- ----------------------------
-- Table structure for jc_department
-- ----------------------------
DROP TABLE IF EXISTS `jc_department`;
CREATE TABLE `jc_department` (
  `depart_id` decimal(11,0) NOT NULL,
  `depart_name` varchar(255) NOT NULL COMMENT '部门名称',
  `site_id` decimal(11,0) DEFAULT '0' COMMENT '站点',
  `priority` decimal(11,0) NOT NULL DEFAULT '1' COMMENT '排序',
  `weights` decimal(11,0) NOT NULL DEFAULT '1' COMMENT '权重(值越大，级别越高)',
  PRIMARY KEY (`depart_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门;';

-- ----------------------------
-- Records of jc_department
-- ----------------------------

-- ----------------------------
-- Table structure for jc_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `jc_dictionary`;
CREATE TABLE `jc_dictionary` (
  `id` decimal(11,0) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'name',
  `value` varchar(255) NOT NULL COMMENT 'value',
  `type` varchar(255) NOT NULL COMMENT 'type',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典表;';

-- ----------------------------
-- Records of jc_dictionary
-- ----------------------------

-- ----------------------------
-- Table structure for jc_directive_tpl
-- ----------------------------
DROP TABLE IF EXISTS `jc_directive_tpl`;
CREATE TABLE `jc_directive_tpl` (
  `tpl_id` decimal(11,0) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT '标签创建者',
  `description` varchar(1000) DEFAULT NULL,
  `code` text,
  PRIMARY KEY (`tpl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='标签模板';

-- ----------------------------
-- Records of jc_directive_tpl
-- ----------------------------

-- ----------------------------
-- Table structure for jc_file
-- ----------------------------
DROP TABLE IF EXISTS `jc_file`;
CREATE TABLE `jc_file` (
  `file_path` varchar(255) NOT NULL COMMENT '文件路径',
  `file_name` varchar(255) DEFAULT NULL COMMENT '文件名字',
  `file_isvalid` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否有效',
  `content_id` decimal(11,0) DEFAULT NULL COMMENT '内容id',
  PRIMARY KEY (`file_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件';

-- ----------------------------
-- Records of jc_file
-- ----------------------------

-- ----------------------------
-- Table structure for jc_friendlink
-- ----------------------------
DROP TABLE IF EXISTS `jc_friendlink`;
CREATE TABLE `jc_friendlink` (
  `friendlink_id` decimal(11,0) NOT NULL,
  `site_name` varchar(150) NOT NULL COMMENT '网站名称',
  `domain` varchar(255) NOT NULL COMMENT '网站地址',
  `logo` varchar(150) DEFAULT NULL COMMENT '图标',
  `email` varchar(100) DEFAULT NULL COMMENT '站长邮箱',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `views` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '点击次数',
  `is_enabled` char(1) NOT NULL DEFAULT '1' COMMENT '是否显示',
  `priority` decimal(11,0) NOT NULL DEFAULT '10' COMMENT '排列顺序',
  PRIMARY KEY (`friendlink_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS友情链接; ';

-- ----------------------------
-- Records of jc_friendlink
-- ----------------------------

-- ----------------------------
-- Table structure for jc_friendlink_ctg
-- ----------------------------
DROP TABLE IF EXISTS `jc_friendlink_ctg`;
CREATE TABLE `jc_friendlink_ctg` (
  `friendlinkctg_id` decimal(11,0) NOT NULL,
  `friendlinkctg_name` varchar(50) NOT NULL COMMENT '名称',
  `priority` decimal(11,0) NOT NULL DEFAULT '10' COMMENT '排列顺序',
  PRIMARY KEY (`friendlinkctg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS友情链接类别;';

-- ----------------------------
-- Records of jc_friendlink_ctg
-- ----------------------------

-- ----------------------------
-- Table structure for jc_group
-- ----------------------------
DROP TABLE IF EXISTS `jc_group`;
CREATE TABLE `jc_group` (
  `group_id` decimal(11,0) NOT NULL,
  `group_name` varchar(100) NOT NULL COMMENT '名称',
  `priority` decimal(11,0) NOT NULL DEFAULT '10' COMMENT '排列顺序',
  `need_captcha` decimal(1,0) NOT NULL DEFAULT '1' COMMENT '是否需要验证码',
  `need_check` decimal(1,0) NOT NULL DEFAULT '1' COMMENT '是否需要审核',
  `allow_per_day` decimal(11,0) NOT NULL DEFAULT '4096' COMMENT '每日允许上传KB',
  `allow_max_file` decimal(11,0) NOT NULL DEFAULT '1024' COMMENT '每个文件最大KB',
  `allow_suffix` varchar(255) DEFAULT 'jpg,jpeg,gif,png,bmp' COMMENT '允许上传的后缀',
  `is_reg_def` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否默认会员组',
  `allow_file_size` decimal(11,0) NOT NULL DEFAULT '4096' COMMENT '每个上传文库的文件大小限制kB',
  `allow_file_total` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '上传总数限制(0没有限制)',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS会员组表;';

-- ----------------------------
-- Records of jc_group
-- ----------------------------

-- ----------------------------
-- Table structure for jc_guestbook
-- ----------------------------
DROP TABLE IF EXISTS `jc_guestbook`;
CREATE TABLE `jc_guestbook` (
  `guestbook_id` decimal(11,0) NOT NULL,
  `ip` varchar(50) NOT NULL COMMENT '留言IP',
  `create_time` datetime NOT NULL COMMENT '留言时间',
  `replay_time` datetime DEFAULT NULL COMMENT '回复时间',
  `is_checked` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否审核',
  `is_recommend` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否推荐',
  PRIMARY KEY (`guestbook_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS留言;';

-- ----------------------------
-- Records of jc_guestbook
-- ----------------------------

-- ----------------------------
-- Table structure for jc_guestbook_ctg
-- ----------------------------
DROP TABLE IF EXISTS `jc_guestbook_ctg`;
CREATE TABLE `jc_guestbook_ctg` (
  `guestbookctg_id` decimal(11,0) NOT NULL,
  `ctg_name` varchar(100) NOT NULL COMMENT '名称',
  `priority` decimal(11,0) NOT NULL DEFAULT '10' COMMENT '排列顺序',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`guestbookctg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS留言类别;';

-- ----------------------------
-- Records of jc_guestbook_ctg
-- ----------------------------

-- ----------------------------
-- Table structure for jc_guestbook_ctg_department
-- ----------------------------
DROP TABLE IF EXISTS `jc_guestbook_ctg_department`;
CREATE TABLE `jc_guestbook_ctg_department` (
  `guestbookctg_id` decimal(11,0) NOT NULL,
  `depart_id` decimal(11,0) NOT NULL,
  PRIMARY KEY (`guestbookctg_id`,`depart_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS留言类别部门关联表;';

-- ----------------------------
-- Records of jc_guestbook_ctg_department
-- ----------------------------

-- ----------------------------
-- Table structure for jc_guestbook_ext
-- ----------------------------
DROP TABLE IF EXISTS `jc_guestbook_ext`;
CREATE TABLE `jc_guestbook_ext` (
  `title` varchar(255) DEFAULT NULL COMMENT '留言标题',
  `content` text COMMENT '留言内容',
  `reply` text COMMENT '回复内容',
  `email` varchar(100) DEFAULT NULL COMMENT '电子邮件',
  `phone` varchar(100) DEFAULT NULL COMMENT '电话',
  `qq` varchar(50) DEFAULT NULL COMMENT 'QQ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS留言内容;';

-- ----------------------------
-- Records of jc_guestbook_ext
-- ----------------------------

-- ----------------------------
-- Table structure for jc_job_apply
-- ----------------------------
DROP TABLE IF EXISTS `jc_job_apply`;
CREATE TABLE `jc_job_apply` (
  `job_apply_id` decimal(11,0) NOT NULL,
  `apply_time` datetime NOT NULL COMMENT '申请时间',
  PRIMARY KEY (`job_apply_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='职位申请表;';

-- ----------------------------
-- Records of jc_job_apply
-- ----------------------------

-- ----------------------------
-- Table structure for jc_keyword
-- ----------------------------
DROP TABLE IF EXISTS `jc_keyword`;
CREATE TABLE `jc_keyword` (
  `keyword_id` decimal(11,0) NOT NULL,
  `keyword_name` varchar(100) NOT NULL COMMENT '名称',
  `url` varchar(255) NOT NULL COMMENT '链接',
  `is_disabled` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否禁用',
  PRIMARY KEY (`keyword_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS内容关键词表;';

-- ----------------------------
-- Records of jc_keyword
-- ----------------------------

-- ----------------------------
-- Table structure for jc_log
-- ----------------------------
DROP TABLE IF EXISTS `jc_log`;
CREATE TABLE `jc_log` (
  `log_id` decimal(11,0) NOT NULL,
  `category` decimal(11,0) NOT NULL COMMENT '日志类型',
  `log_time` datetime NOT NULL COMMENT '日志时间',
  `ip` varchar(50) DEFAULT NULL COMMENT 'IP地址',
  `url` varchar(255) DEFAULT NULL COMMENT 'URL地址',
  `title` varchar(255) DEFAULT NULL COMMENT '日志标题',
  `content` varchar(255) DEFAULT NULL COMMENT '日志内容',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS日志表;';

-- ----------------------------
-- Records of jc_log
-- ----------------------------

-- ----------------------------
-- Table structure for jc_message
-- ----------------------------
DROP TABLE IF EXISTS `jc_message`;
CREATE TABLE `jc_message` (
  `msg_id` decimal(11,0) NOT NULL COMMENT '消息id',
  `msg_title` varchar(255) NOT NULL COMMENT '标题',
  `msg_content` text COMMENT '站内信息内容',
  `send_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '发送时间',
  `msg_status` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '消息状态0未读，1已读',
  `msg_box` decimal(11,0) NOT NULL DEFAULT '1' COMMENT '消息信箱 0收件箱 1发件箱 2草稿箱 3垃圾箱',
  PRIMARY KEY (`msg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站内信;';

-- ----------------------------
-- Records of jc_message
-- ----------------------------

-- ----------------------------
-- Table structure for jc_model
-- ----------------------------
DROP TABLE IF EXISTS `jc_model`;
CREATE TABLE `jc_model` (
  `model_id` decimal(11,0) NOT NULL,
  `model_name` varchar(100) NOT NULL COMMENT '名称',
  `model_path` varchar(100) NOT NULL COMMENT '路径',
  `tpl_channel_prefix` varchar(20) DEFAULT NULL COMMENT '栏目模板前缀',
  `tpl_content_prefix` varchar(20) DEFAULT NULL COMMENT '内容模板前缀',
  `title_img_width` decimal(11,0) NOT NULL DEFAULT '139' COMMENT '栏目标题图宽度',
  `title_img_height` decimal(11,0) NOT NULL DEFAULT '139' COMMENT '栏目标题图高度',
  `content_img_width` decimal(11,0) NOT NULL DEFAULT '310' COMMENT '栏目内容图宽度',
  `content_img_height` decimal(11,0) NOT NULL DEFAULT '310' COMMENT '栏目内容图高度',
  `priority` decimal(11,0) NOT NULL DEFAULT '10' COMMENT '排列顺序',
  `has_content` decimal(1,0) NOT NULL DEFAULT '1' COMMENT '是否有内容',
  `is_disabled` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否禁用',
  `is_def` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否默认模型',
  PRIMARY KEY (`model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS模型表;';

-- ----------------------------
-- Records of jc_model
-- ----------------------------

-- ----------------------------
-- Table structure for jc_model_item
-- ----------------------------
DROP TABLE IF EXISTS `jc_model_item`;
CREATE TABLE `jc_model_item` (
  `modelitem_id` decimal(11,0) NOT NULL,
  `field` varchar(50) NOT NULL COMMENT '字段',
  `item_label` varchar(100) NOT NULL COMMENT '名称',
  `priority` decimal(11,0) NOT NULL DEFAULT '70' COMMENT '排列顺序',
  `def_value` varchar(255) DEFAULT NULL COMMENT '默认值',
  `opt_value` varchar(255) DEFAULT NULL COMMENT '可选项',
  `text_size` varchar(20) DEFAULT NULL COMMENT '长度',
  `area_rows` varchar(3) DEFAULT NULL COMMENT '文本行数',
  `area_cols` varchar(3) DEFAULT NULL COMMENT '文本列数',
  `help` varchar(255) DEFAULT NULL COMMENT '帮助信息',
  `help_position` varchar(1) DEFAULT NULL COMMENT '帮助位置',
  `data_type` decimal(11,0) NOT NULL DEFAULT '1' COMMENT '数据类型',
  `is_single` decimal(1,0) NOT NULL DEFAULT '1' COMMENT '是否独占一行',
  `is_channel` decimal(1,0) NOT NULL DEFAULT '1' COMMENT '是否栏目模型项',
  `is_custom` decimal(1,0) NOT NULL DEFAULT '1' COMMENT '是否自定义',
  `is_display` decimal(1,0) NOT NULL DEFAULT '1' COMMENT '是否显示',
  PRIMARY KEY (`modelitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS模型项表;';

-- ----------------------------
-- Records of jc_model_item
-- ----------------------------

-- ----------------------------
-- Table structure for jc_origin
-- ----------------------------
DROP TABLE IF EXISTS `jc_origin`;
CREATE TABLE `jc_origin` (
  `origin_id` decimal(11,0) NOT NULL,
  `origin_name` varchar(255) NOT NULL COMMENT '来源名称',
  `ref_count` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '来源文章总数'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='来源';

-- ----------------------------
-- Records of jc_origin
-- ----------------------------

-- ----------------------------
-- Table structure for jc_plug
-- ----------------------------
DROP TABLE IF EXISTS `jc_plug`;
CREATE TABLE `jc_plug` (
  `plug_id` decimal(11,0) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT '插件名称',
  `path` varchar(255) NOT NULL COMMENT '文件路径',
  `description` varchar(2000) DEFAULT NULL COMMENT '描述',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `upload_time` datetime NOT NULL COMMENT '上传时间',
  `install_time` datetime DEFAULT NULL COMMENT '安装时间',
  `uninstall_time` datetime DEFAULT NULL COMMENT '卸载时间',
  `file_conflict` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '包含文件是否冲突',
  `is_used` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '使用状态(0未使用,1使用中)',
  `plug_perms` varchar(2000) DEFAULT NULL COMMENT '插件权限（,分隔各个权限配置）',
  PRIMARY KEY (`plug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='插件';

-- ----------------------------
-- Records of jc_plug
-- ----------------------------

-- ----------------------------
-- Table structure for jc_receiver_message
-- ----------------------------
DROP TABLE IF EXISTS `jc_receiver_message`;
CREATE TABLE `jc_receiver_message` (
  `msg_re_id` decimal(11,0) NOT NULL COMMENT '消息id',
  `msg_title` varchar(255) NOT NULL COMMENT '标题',
  `msg_content` text COMMENT '站内信息内容',
  `send_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '发送时间',
  `msg_status` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '消息状态0未读，1已读',
  `msg_box` decimal(11,0) NOT NULL DEFAULT '1' COMMENT '消息信箱 0收件箱 1发件箱 2草稿箱 3垃圾箱',
  PRIMARY KEY (`msg_re_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站内信收信表;';

-- ----------------------------
-- Records of jc_receiver_message
-- ----------------------------

-- ----------------------------
-- Table structure for jc_role
-- ----------------------------
DROP TABLE IF EXISTS `jc_role`;
CREATE TABLE `jc_role` (
  `role_id` decimal(11,0) NOT NULL,
  `role_name` varchar(100) NOT NULL COMMENT '角色名称',
  `priority` decimal(11,0) NOT NULL DEFAULT '10' COMMENT '排列顺序',
  `is_super` char(1) NOT NULL DEFAULT '0' COMMENT '拥有所有权限',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS角色表;';

-- ----------------------------
-- Records of jc_role
-- ----------------------------

-- ----------------------------
-- Table structure for jc_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `jc_role_permission`;
CREATE TABLE `jc_role_permission` (
  `uri` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS角色授权表;';

-- ----------------------------
-- Records of jc_role_permission
-- ----------------------------

-- ----------------------------
-- Table structure for jc_score_group
-- ----------------------------
DROP TABLE IF EXISTS `jc_score_group`;
CREATE TABLE `jc_score_group` (
  `score_group_id` decimal(11,0) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT '分组名',
  `description` varchar(1000) DEFAULT NULL COMMENT '描述',
  `enable` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否启用',
  `def` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否默认',
  PRIMARY KEY (`score_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='评分组';

-- ----------------------------
-- Records of jc_score_group
-- ----------------------------

-- ----------------------------
-- Table structure for jc_score_item
-- ----------------------------
DROP TABLE IF EXISTS `jc_score_item`;
CREATE TABLE `jc_score_item` (
  `score_item_id` decimal(11,0) NOT NULL,
  `name` varchar(255) NOT NULL,
  `score` decimal(11,0) NOT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `priority` decimal(11,0) NOT NULL DEFAULT '10',
  PRIMARY KEY (`score_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='评分项';

-- ----------------------------
-- Records of jc_score_item
-- ----------------------------

-- ----------------------------
-- Table structure for jc_score_record
-- ----------------------------
DROP TABLE IF EXISTS `jc_score_record`;
CREATE TABLE `jc_score_record` (
  `score_record_id` decimal(11,0) NOT NULL,
  `content_id` decimal(11,0) NOT NULL,
  `score_count` decimal(11,0) NOT NULL,
  PRIMARY KEY (`score_record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='评分记录';

-- ----------------------------
-- Records of jc_score_record
-- ----------------------------

-- ----------------------------
-- Table structure for jc_search_words
-- ----------------------------
DROP TABLE IF EXISTS `jc_search_words`;
CREATE TABLE `jc_search_words` (
  `word_id` decimal(11,0) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT '搜索词汇',
  `hit_count` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '搜索次数',
  `priority` decimal(11,0) NOT NULL DEFAULT '10' COMMENT '优先级',
  `name_initial` varchar(500) NOT NULL COMMENT '拼音首字母'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='搜索热词';

-- ----------------------------
-- Records of jc_search_words
-- ----------------------------

-- ----------------------------
-- Table structure for jc_sensitivity
-- ----------------------------
DROP TABLE IF EXISTS `jc_sensitivity`;
CREATE TABLE `jc_sensitivity` (
  `sensitivity_id` decimal(11,0) NOT NULL,
  `search` varchar(255) NOT NULL COMMENT '敏感词',
  `replacement` varchar(255) NOT NULL COMMENT '替换词',
  PRIMARY KEY (`sensitivity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS敏感词表;';

-- ----------------------------
-- Records of jc_sensitivity
-- ----------------------------

-- ----------------------------
-- Table structure for jc_site
-- ----------------------------
DROP TABLE IF EXISTS `jc_site`;
CREATE TABLE `jc_site` (
  `site_id` decimal(11,0) NOT NULL,
  `domain` varchar(50) NOT NULL COMMENT '域名',
  `site_path` varchar(20) NOT NULL COMMENT '路径',
  `site_name` varchar(100) NOT NULL COMMENT '网站名称',
  `short_name` varchar(100) NOT NULL COMMENT '简短名称',
  `protocol` varchar(20) NOT NULL DEFAULT 'http://' COMMENT '协议',
  `dynamic_suffix` varchar(10) NOT NULL DEFAULT '.jhtml' COMMENT '动态页后缀',
  `static_suffix` varchar(10) NOT NULL DEFAULT '.html' COMMENT '静态页后缀',
  `static_dir` varchar(50) DEFAULT NULL COMMENT '静态页存放目录',
  `is_index_to_root` char(1) NOT NULL DEFAULT '0' COMMENT '是否使用将首页放在根目录下',
  `is_static_index` char(1) NOT NULL DEFAULT '0' COMMENT '是否静态化首页',
  `locale_admin` varchar(10) NOT NULL DEFAULT 'zh_ch' COMMENT '后台本地化',
  `locale_front` varchar(10) NOT NULL DEFAULT 'zh_ch' COMMENT '前台本地化',
  `tpl_solution` varchar(50) NOT NULL DEFAULT 'default' COMMENT '模板方案',
  `final_step` decimal(1,0) NOT NULL DEFAULT '2' COMMENT '终审级别',
  `after_check` decimal(1,0) NOT NULL DEFAULT '2' COMMENT '审核后(1:不能修改删除;2:修改后退回;3:修改后不变)',
  `is_relative_path` char(1) NOT NULL DEFAULT '1' COMMENT '是否使用相对路径',
  `is_recycle_on` char(1) NOT NULL DEFAULT '1' COMMENT '是否开启回收站',
  `domain_alias` varchar(255) DEFAULT NULL COMMENT '域名别名',
  `domain_redirect` varchar(255) DEFAULT NULL COMMENT '域名重定向',
  `is_master` decimal(1,0) DEFAULT '0' COMMENT '是否主站',
  `tpl_index` varchar(255) DEFAULT NULL COMMENT '首页模板',
  `access_path` varchar(50) DEFAULT NULL COMMENT '访问路径',
  `keywords` varchar(255) DEFAULT NULL COMMENT '站点关键字',
  `description` varchar(255) DEFAULT NULL COMMENT '站点描述',
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS站点表; ';

-- ----------------------------
-- Records of jc_site
-- ----------------------------

-- ----------------------------
-- Table structure for jc_site_access
-- ----------------------------
DROP TABLE IF EXISTS `jc_site_access`;
CREATE TABLE `jc_site_access` (
  `access_id` decimal(11,0) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `access_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '访问时间',
  `access_date` datetime NOT NULL COMMENT '访问日期',
  `ip` varchar(50) NOT NULL COMMENT '访问ip',
  `area` varchar(50) DEFAULT NULL COMMENT '访问地区',
  `access_source` varchar(255) DEFAULT NULL COMMENT '访问来源',
  `external_link` varchar(255) DEFAULT NULL COMMENT '外部链接网址',
  `engine` varchar(50) DEFAULT NULL COMMENT '搜索引擎',
  `entry_page` varchar(255) DEFAULT NULL COMMENT '入口页面',
  `last_stop_page` varchar(255) DEFAULT NULL COMMENT '最后停留页面',
  `visit_second` decimal(11,0) DEFAULT NULL COMMENT '访问时长(秒)',
  `visit_page_count` decimal(11,0) DEFAULT NULL COMMENT '访问页面数',
  `operating_system` varchar(50) DEFAULT NULL COMMENT '操作系统',
  `browser` varchar(50) DEFAULT NULL COMMENT '浏览器',
  `keyword` varchar(255) DEFAULT NULL COMMENT '来访关键字',
  PRIMARY KEY (`access_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站点访问表';

-- ----------------------------
-- Records of jc_site_access
-- ----------------------------

-- ----------------------------
-- Table structure for jc_site_access_count
-- ----------------------------
DROP TABLE IF EXISTS `jc_site_access_count`;
CREATE TABLE `jc_site_access_count` (
  `access_count` decimal(11,0) NOT NULL,
  `page_count` decimal(11,0) NOT NULL DEFAULT '1' COMMENT '访问页数',
  `visitors` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '用户数',
  `statistic_date` datetime NOT NULL COMMENT '统计日期',
  PRIMARY KEY (`access_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='每日统计页数访问情况';

-- ----------------------------
-- Records of jc_site_access_count
-- ----------------------------

-- ----------------------------
-- Table structure for jc_site_access_pages
-- ----------------------------
DROP TABLE IF EXISTS `jc_site_access_pages`;
CREATE TABLE `jc_site_access_pages` (
  `access_pages_id` decimal(11,0) NOT NULL,
  `access_page` varchar(255) NOT NULL COMMENT '用户访问页面的索引',
  `session_id` varchar(32) NOT NULL,
  `access_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `access_date` datetime NOT NULL,
  `visit_second` decimal(11,0) NOT NULL DEFAULT '0',
  `page_index` decimal(11,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`access_pages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='访问详细页面表';

-- ----------------------------
-- Records of jc_site_access_pages
-- ----------------------------

-- ----------------------------
-- Table structure for jc_site_access_statistic
-- ----------------------------
DROP TABLE IF EXISTS `jc_site_access_statistic`;
CREATE TABLE `jc_site_access_statistic` (
  `access_statistic_id` decimal(11,0) NOT NULL,
  `statistic_date` datetime NOT NULL,
  `pv` decimal(11,0) NOT NULL DEFAULT '0' COMMENT 'pv量',
  `ip` decimal(11,0) NOT NULL DEFAULT '0' COMMENT 'ip量',
  `visitors` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '访客数量',
  `pages_aver` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '人均浏览次数',
  `visit_second_aver` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '人均访问时长（秒）',
  `statisitc_type` varchar(255) NOT NULL DEFAULT 'all' COMMENT '统计分类（all代表当天所有访问量的统计）',
  `statistic_column_value` varchar(255) DEFAULT '' COMMENT '统计列值',
  PRIMARY KEY (`access_statistic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='访问统计表';

-- ----------------------------
-- Records of jc_site_access_statistic
-- ----------------------------

-- ----------------------------
-- Table structure for jc_site_attr
-- ----------------------------
DROP TABLE IF EXISTS `jc_site_attr`;
CREATE TABLE `jc_site_attr` (
  `attr_name` varchar(30) NOT NULL COMMENT '名称',
  `attr_value` varchar(255) DEFAULT NULL COMMENT '值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS站点属性表;';

-- ----------------------------
-- Records of jc_site_attr
-- ----------------------------

-- ----------------------------
-- Table structure for jc_site_cfg
-- ----------------------------
DROP TABLE IF EXISTS `jc_site_cfg`;
CREATE TABLE `jc_site_cfg` (
  `cfg_name` varchar(30) NOT NULL COMMENT '名称',
  `cfg_value` varchar(255) DEFAULT NULL COMMENT '值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS站点配置表;';

-- ----------------------------
-- Records of jc_site_cfg
-- ----------------------------

-- ----------------------------
-- Table structure for jc_site_company
-- ----------------------------
DROP TABLE IF EXISTS `jc_site_company`;
CREATE TABLE `jc_site_company` (
  `site_id` decimal(11,0) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT '公司名称',
  `scale` varchar(255) DEFAULT NULL COMMENT '公司规模',
  `nature` varchar(255) DEFAULT NULL COMMENT '公司性质',
  `industry` varchar(1000) DEFAULT NULL COMMENT '公司行业',
  `contact` varchar(500) DEFAULT NULL COMMENT '联系方式',
  `description` text COMMENT '公司简介',
  `address` varchar(500) DEFAULT NULL COMMENT '公司地址',
  `longitude` float DEFAULT NULL COMMENT '经度',
  `latitude` float DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='公司信息;';

-- ----------------------------
-- Records of jc_site_company
-- ----------------------------

-- ----------------------------
-- Table structure for jc_site_flow
-- ----------------------------
DROP TABLE IF EXISTS `jc_site_flow`;
CREATE TABLE `jc_site_flow` (
  `flow_id` decimal(11,0) NOT NULL,
  `access_ip` varchar(50) NOT NULL DEFAULT '127.0.0.1' COMMENT '访问者ip',
  `access_date` varchar(50) DEFAULT NULL COMMENT '访问日期',
  `access_time` datetime DEFAULT NULL COMMENT '访问时间',
  `access_page` varchar(255) NOT NULL COMMENT '访问页面',
  `referer_website` varchar(255) DEFAULT NULL COMMENT '来访网站',
  `referer_page` varchar(255) DEFAULT NULL COMMENT '来访页面',
  `referer_keyword` varchar(255) DEFAULT NULL COMMENT '来访关键字',
  `area` varchar(50) DEFAULT NULL COMMENT '地区',
  `session_id` varchar(255) NOT NULL COMMENT 'cookie信息',
  PRIMARY KEY (`flow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站点流量统计表;';

-- ----------------------------
-- Records of jc_site_flow
-- ----------------------------

-- ----------------------------
-- Table structure for jc_site_model
-- ----------------------------
DROP TABLE IF EXISTS `jc_site_model`;
CREATE TABLE `jc_site_model` (
  `model_id` decimal(11,0) NOT NULL,
  `field` varchar(50) NOT NULL COMMENT '字段',
  `model_label` varchar(100) NOT NULL COMMENT '名称',
  `priority` decimal(11,0) NOT NULL DEFAULT '10' COMMENT '排列顺序',
  `upload_path` varchar(100) DEFAULT NULL COMMENT '上传路径',
  `text_size` varchar(20) DEFAULT NULL COMMENT '长度',
  `area_rows` varchar(3) DEFAULT NULL COMMENT '文本行数',
  `area_cols` varchar(3) DEFAULT NULL COMMENT '文本列数',
  `help` varchar(255) DEFAULT NULL COMMENT '帮助信息',
  `help_position` varchar(1) DEFAULT NULL COMMENT '帮助位置',
  `data_type` decimal(11,0) DEFAULT '1' COMMENT '0:编辑器;1:文本框;2:文本区;3:图片;4:附件',
  `is_single` decimal(1,0) DEFAULT '1' COMMENT '是否独占一行',
  PRIMARY KEY (`model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS站点信息模型表;';

-- ----------------------------
-- Records of jc_site_model
-- ----------------------------

-- ----------------------------
-- Table structure for jc_site_refer
-- ----------------------------
DROP TABLE IF EXISTS `jc_site_refer`;
CREATE TABLE `jc_site_refer` (
  `site_id` decimal(11,0) NOT NULL,
  `from_site_id` decimal(11,0) NOT NULL,
  PRIMARY KEY (`site_id`,`from_site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站点转载权限';

-- ----------------------------
-- Records of jc_site_refer
-- ----------------------------

-- ----------------------------
-- Table structure for jc_site_txt
-- ----------------------------
DROP TABLE IF EXISTS `jc_site_txt`;
CREATE TABLE `jc_site_txt` (
  `txt_name` varchar(30) NOT NULL COMMENT '名称',
  `txt_value` text COMMENT '值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS站点文本表; ';

-- ----------------------------
-- Records of jc_site_txt
-- ----------------------------

-- ----------------------------
-- Table structure for jc_task
-- ----------------------------
DROP TABLE IF EXISTS `jc_task`;
CREATE TABLE `jc_task` (
  `task_id` decimal(11,0) NOT NULL,
  `task_code` varchar(255) DEFAULT NULL COMMENT '任务执行代码',
  `task_type` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '任务类型(1首页静态化、2栏目页静态化、3内容页静态化、4采集、5分发)',
  `task_name` varchar(255) NOT NULL COMMENT '任务名称',
  `job_class` varchar(255) NOT NULL COMMENT '任务类',
  `execycle` decimal(1,0) NOT NULL DEFAULT '1' COMMENT '执行周期分类(1非表达式 2 cron表达式)',
  `day_of_month` decimal(11,0) DEFAULT NULL COMMENT '每月的哪天',
  `day_of_week` decimal(1,0) DEFAULT NULL COMMENT '周几',
  `hour` decimal(11,0) DEFAULT NULL COMMENT '小时',
  `minute` decimal(11,0) DEFAULT NULL COMMENT '分钟',
  `interval_hour` decimal(11,0) DEFAULT NULL COMMENT '间隔小时',
  `interval_minute` decimal(11,0) DEFAULT NULL COMMENT '间隔分钟',
  `task_interval_unit` decimal(1,0) DEFAULT NULL COMMENT '1分钟、2小时、3日、4周、5月',
  `cron_expression` varchar(255) DEFAULT NULL COMMENT '规则表达式',
  `is_enable` decimal(1,0) NOT NULL DEFAULT '1' COMMENT '是否启用',
  `task_remark` varchar(255) DEFAULT NULL COMMENT '任务说明',
  `user_id` decimal(11,0) NOT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务表;';

-- ----------------------------
-- Records of jc_task
-- ----------------------------

-- ----------------------------
-- Table structure for jc_task_attr
-- ----------------------------
DROP TABLE IF EXISTS `jc_task_attr`;
CREATE TABLE `jc_task_attr` (
  `param_name` varchar(30) NOT NULL COMMENT '参数名称',
  `param_value` varchar(255) DEFAULT NULL COMMENT '参数值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务参数表; ';

-- ----------------------------
-- Records of jc_task_attr
-- ----------------------------

-- ----------------------------
-- Table structure for jc_third_account
-- ----------------------------
DROP TABLE IF EXISTS `jc_third_account`;
CREATE TABLE `jc_third_account` (
  `account_id` decimal(20,0) NOT NULL,
  `account_key` varchar(255) NOT NULL DEFAULT '' COMMENT '第三方账号key',
  `username` varchar(100) NOT NULL COMMENT '关联用户名',
  `source` varchar(10) NOT NULL COMMENT '第三方账号平台(QQ、新浪微博等)',
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='第三方登录平台账号';

-- ----------------------------
-- Records of jc_third_account
-- ----------------------------

-- ----------------------------
-- Table structure for jc_topic
-- ----------------------------
DROP TABLE IF EXISTS `jc_topic`;
CREATE TABLE `jc_topic` (
  `topic_id` decimal(11,0) NOT NULL,
  `topic_name` varchar(150) NOT NULL COMMENT '名称',
  `short_name` varchar(150) DEFAULT NULL COMMENT '简称',
  `keywords` varchar(255) DEFAULT NULL COMMENT '关键字',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `title_img` varchar(100) DEFAULT NULL COMMENT '标题图',
  `content_img` varchar(100) DEFAULT NULL COMMENT '内容图',
  `tpl_content` varchar(100) DEFAULT NULL COMMENT '专题模板',
  `priority` decimal(11,0) NOT NULL DEFAULT '10' COMMENT '排列顺序',
  `is_recommend` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否推??',
  PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS专题表;';

-- ----------------------------
-- Records of jc_topic
-- ----------------------------

-- ----------------------------
-- Table structure for jc_user
-- ----------------------------
DROP TABLE IF EXISTS `jc_user`;
CREATE TABLE `jc_user` (
  `user_id` decimal(11,0) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT '用户名',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `register_time` datetime NOT NULL COMMENT '注册时间',
  `register_ip` varchar(50) NOT NULL DEFAULT '127.0.0.1' COMMENT '注册IP',
  `last_login_time` datetime NOT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(50) NOT NULL DEFAULT '127.0.0.1' COMMENT '最后登录IP',
  `login_count` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '登录次数',
  `rank` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '管理员级别',
  `upload_total` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '上传总大小',
  `upload_size` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '上传大小',
  `upload_date` datetime DEFAULT NULL COMMENT '上传日期',
  `is_admin` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否管理员',
  `is_viewonly_admin` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否只读管理员',
  `is_self_admin` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否只管理自己的数据',
  `is_disabled` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否禁用',
  `file_total` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '上传文库文档个数',
  `grain` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '文库财富值',
  `session_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS用户表;';

-- ----------------------------
-- Records of jc_user
-- ----------------------------

-- ----------------------------
-- Table structure for jc_user_attr
-- ----------------------------
DROP TABLE IF EXISTS `jc_user_attr`;
CREATE TABLE `jc_user_attr` (
  `user_id` decimal(11,0) NOT NULL,
  `attr_name` varchar(255) NOT NULL,
  `attr_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户属性表';

-- ----------------------------
-- Records of jc_user_attr
-- ----------------------------

-- ----------------------------
-- Table structure for jc_user_collection
-- ----------------------------
DROP TABLE IF EXISTS `jc_user_collection`;
CREATE TABLE `jc_user_collection` (
  `user_id` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '用户id',
  `content_id` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '内容id',
  PRIMARY KEY (`user_id`,`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户收藏关联表;';

-- ----------------------------
-- Records of jc_user_collection
-- ----------------------------

-- ----------------------------
-- Table structure for jc_user_ext
-- ----------------------------
DROP TABLE IF EXISTS `jc_user_ext`;
CREATE TABLE `jc_user_ext` (
  `user_id` decimal(11,0) NOT NULL,
  `realname` varchar(100) DEFAULT NULL COMMENT '真实姓名',
  `gender` decimal(1,0) DEFAULT NULL COMMENT '性别',
  `birthday` datetime DEFAULT NULL COMMENT '出生日期',
  `intro` varchar(255) DEFAULT NULL COMMENT '个人介绍',
  `comefrom` varchar(150) DEFAULT NULL COMMENT '来自',
  `qq` varchar(100) DEFAULT NULL COMMENT 'QQ',
  `msn` varchar(100) DEFAULT NULL COMMENT 'MSN',
  `phone` varchar(50) DEFAULT NULL COMMENT '电话',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机',
  `user_img` varchar(255) DEFAULT NULL COMMENT '用户头像',
  `user_signature` varchar(255) DEFAULT NULL COMMENT '用户个性签名',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS用户扩展信息表;';

-- ----------------------------
-- Records of jc_user_ext
-- ----------------------------

-- ----------------------------
-- Table structure for jc_user_menu
-- ----------------------------
DROP TABLE IF EXISTS `jc_user_menu`;
CREATE TABLE `jc_user_menu` (
  `menu_id` decimal(11,0) NOT NULL,
  `menu_name` varchar(255) NOT NULL COMMENT '菜单名称',
  `menu_url` varchar(255) NOT NULL COMMENT '菜单地址',
  `priority` decimal(11,0) NOT NULL DEFAULT '10',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户常用菜单';

-- ----------------------------
-- Records of jc_user_menu
-- ----------------------------

-- ----------------------------
-- Table structure for jc_user_resume
-- ----------------------------
DROP TABLE IF EXISTS `jc_user_resume`;
CREATE TABLE `jc_user_resume` (
  `user_id` decimal(11,0) NOT NULL,
  `resume_name` varchar(255) NOT NULL COMMENT '简历名称',
  `target_worknature` varchar(255) DEFAULT NULL COMMENT '期望工作性质',
  `target_workplace` varchar(255) DEFAULT NULL COMMENT '期望工作地点',
  `target_category` varchar(255) DEFAULT NULL COMMENT '期望职位类别',
  `target_salary` varchar(255) DEFAULT NULL COMMENT '期望月薪',
  `edu_school` varchar(255) DEFAULT NULL COMMENT '毕业学校',
  `edu_graduation` datetime DEFAULT NULL COMMENT '毕业时间',
  `edu_back` varchar(255) DEFAULT NULL COMMENT '学历',
  `edu_discipline` varchar(255) DEFAULT NULL COMMENT '专业',
  `recent_company` varchar(500) DEFAULT NULL COMMENT '最近工作公司名称',
  `company_industry` varchar(255) DEFAULT NULL COMMENT '最近公司所属行业',
  `company_scale` varchar(255) DEFAULT NULL COMMENT '公司规模',
  `job_name` varchar(255) DEFAULT NULL COMMENT '职位名称',
  `job_category` varchar(255) DEFAULT NULL COMMENT '职位类别',
  `job_start` datetime DEFAULT NULL COMMENT '工作起始时间',
  `subordinates` varchar(255) DEFAULT NULL COMMENT '下属人数',
  `job_description` text COMMENT '工作描述',
  `self_evaluation` varchar(2000) DEFAULT NULL COMMENT '自我评价',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户简历;';

-- ----------------------------
-- Records of jc_user_resume
-- ----------------------------

-- ----------------------------
-- Table structure for jc_user_role
-- ----------------------------
DROP TABLE IF EXISTS `jc_user_role`;
CREATE TABLE `jc_user_role` (
  `role_id` decimal(11,0) NOT NULL,
  `user_id` decimal(11,0) NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS用户角色关联表;';

-- ----------------------------
-- Records of jc_user_role
-- ----------------------------

-- ----------------------------
-- Table structure for jc_user_site
-- ----------------------------
DROP TABLE IF EXISTS `jc_user_site`;
CREATE TABLE `jc_user_site` (
  `usersite_id` decimal(11,0) NOT NULL,
  `check_step` decimal(1,0) NOT NULL DEFAULT '1' COMMENT '审核级别',
  `is_all_channel` decimal(1,0) NOT NULL DEFAULT '1' COMMENT '是否拥有所有栏目的权限',
  PRIMARY KEY (`usersite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS管理员站点表;';

-- ----------------------------
-- Records of jc_user_site
-- ----------------------------

-- ----------------------------
-- Table structure for jc_vote_item
-- ----------------------------
DROP TABLE IF EXISTS `jc_vote_item`;
CREATE TABLE `jc_vote_item` (
  `voteitem_id` decimal(11,0) NOT NULL,
  `title` varchar(255) NOT NULL COMMENT '标题',
  `vote_count` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '投票数量',
  `priority` decimal(11,0) NOT NULL DEFAULT '10' COMMENT '排列顺序',
  PRIMARY KEY (`voteitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS投票项;';

-- ----------------------------
-- Records of jc_vote_item
-- ----------------------------

-- ----------------------------
-- Table structure for jc_vote_record
-- ----------------------------
DROP TABLE IF EXISTS `jc_vote_record`;
CREATE TABLE `jc_vote_record` (
  `voterecored_id` decimal(11,0) NOT NULL,
  `vote_time` datetime NOT NULL COMMENT '投票时间',
  `vote_ip` varchar(50) NOT NULL COMMENT '投票IP',
  `vote_cookie` varchar(32) NOT NULL COMMENT '投票COOKIE',
  PRIMARY KEY (`voterecored_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS投票记录;';

-- ----------------------------
-- Records of jc_vote_record
-- ----------------------------

-- ----------------------------
-- Table structure for jc_vote_reply
-- ----------------------------
DROP TABLE IF EXISTS `jc_vote_reply`;
CREATE TABLE `jc_vote_reply` (
  `votereply_id` decimal(11,0) NOT NULL,
  `reply` text COMMENT '回复内容',
  PRIMARY KEY (`votereply_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='投票文本题目回复表;';

-- ----------------------------
-- Records of jc_vote_reply
-- ----------------------------

-- ----------------------------
-- Table structure for jc_vote_subtopic
-- ----------------------------
DROP TABLE IF EXISTS `jc_vote_subtopic`;
CREATE TABLE `jc_vote_subtopic` (
  `subtopic_id` decimal(11,0) NOT NULL,
  `title` varchar(255) NOT NULL COMMENT '标题',
  `subtopic_type` decimal(11,0) NOT NULL DEFAULT '1' COMMENT '类型（1单选，2多选，3文本）',
  `priority` decimal(11,0) DEFAULT NULL,
  PRIMARY KEY (`subtopic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='投票子题目;';

-- ----------------------------
-- Records of jc_vote_subtopic
-- ----------------------------

-- ----------------------------
-- Table structure for jc_vote_topic
-- ----------------------------
DROP TABLE IF EXISTS `jc_vote_topic`;
CREATE TABLE `jc_vote_topic` (
  `votetopic_id` decimal(11,0) NOT NULL,
  `title` varchar(255) NOT NULL COMMENT '标题',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `repeate_hour` decimal(11,0) DEFAULT NULL COMMENT '重复投票限制时间，单位小时，为空不允许重复投票',
  `total_count` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '总投票数',
  `multi_select` decimal(11,0) NOT NULL DEFAULT '1' COMMENT '最多可以选择几项',
  `is_restrict_member` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否限制会员',
  `is_restrict_ip` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否限制IP',
  `is_restrict_cookie` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否限制COOKIE',
  `is_disabled` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否禁用',
  `is_def` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否默认主题',
  PRIMARY KEY (`votetopic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS投票主题;';

-- ----------------------------
-- Records of jc_vote_topic
-- ----------------------------

-- ----------------------------
-- Table structure for jc_webservice
-- ----------------------------
DROP TABLE IF EXISTS `jc_webservice`;
CREATE TABLE `jc_webservice` (
  `service_id` decimal(11,0) NOT NULL,
  `service_address` varchar(255) NOT NULL COMMENT '接口地址',
  `target_namespace` varchar(255) DEFAULT NULL,
  `success_result` varchar(255) DEFAULT NULL COMMENT '正确返回值',
  `service_type` varchar(50) DEFAULT NULL COMMENT '接口类型',
  `service_operate` varchar(50) DEFAULT NULL COMMENT '接口操作',
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='接口表';

-- ----------------------------
-- Records of jc_webservice
-- ----------------------------

-- ----------------------------
-- Table structure for jc_webservice_auth
-- ----------------------------
DROP TABLE IF EXISTS `jc_webservice_auth`;
CREATE TABLE `jc_webservice_auth` (
  `auth_id` decimal(11,0) NOT NULL,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(50) NOT NULL COMMENT '密码',
  `system` varchar(100) NOT NULL COMMENT '系统',
  `is_enable` decimal(4,0) NOT NULL COMMENT '是否启用',
  PRIMARY KEY (`auth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='webservices认证表';

-- ----------------------------
-- Records of jc_webservice_auth
-- ----------------------------

-- ----------------------------
-- Table structure for jc_webservice_call_record
-- ----------------------------
DROP TABLE IF EXISTS `jc_webservice_call_record`;
CREATE TABLE `jc_webservice_call_record` (
  `record_id` decimal(11,0) NOT NULL,
  `service_code` varchar(50) NOT NULL COMMENT '接口识别码',
  `record_time` datetime NOT NULL COMMENT '调用时间',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='接口调用记录;';

-- ----------------------------
-- Records of jc_webservice_call_record
-- ----------------------------

-- ----------------------------
-- Table structure for jc_webservice_param
-- ----------------------------
DROP TABLE IF EXISTS `jc_webservice_param`;
CREATE TABLE `jc_webservice_param` (
  `priority` decimal(11,0) NOT NULL COMMENT '排列顺序',
  `param_name` varchar(100) NOT NULL COMMENT '参数名',
  `default_value` varchar(255) DEFAULT NULL COMMENT '默认值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='接口参数表;';

-- ----------------------------
-- Records of jc_webservice_param
-- ----------------------------

-- ----------------------------
-- Table structure for jc_workflow
-- ----------------------------
DROP TABLE IF EXISTS `jc_workflow`;
CREATE TABLE `jc_workflow` (
  `workflow_id` decimal(11,0) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT '名称',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `priority` decimal(11,0) NOT NULL DEFAULT '10' COMMENT '排序',
  `is_disabled` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否禁用',
  PRIMARY KEY (`workflow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工作流;';

-- ----------------------------
-- Records of jc_workflow
-- ----------------------------

-- ----------------------------
-- Table structure for jc_workflow_event
-- ----------------------------
DROP TABLE IF EXISTS `jc_workflow_event`;
CREATE TABLE `jc_workflow_event` (
  `event_id` decimal(11,0) NOT NULL,
  `date_id` decimal(11,0) NOT NULL COMMENT '数据标识',
  `start_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `next_step` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '下个步骤',
  `date_type` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '数据类型(0默认内容)',
  `is_finish` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否结束',
  `pass_num` decimal(11,0) DEFAULT NULL COMMENT '会签本节点通过人数',
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工作流轨迹;';

-- ----------------------------
-- Records of jc_workflow_event
-- ----------------------------

-- ----------------------------
-- Table structure for jc_workflow_event_user
-- ----------------------------
DROP TABLE IF EXISTS `jc_workflow_event_user`;
CREATE TABLE `jc_workflow_event_user` (
  `event_user_id` decimal(11,0) NOT NULL,
  PRIMARY KEY (`event_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工作流轨迹用户表;';

-- ----------------------------
-- Records of jc_workflow_event_user
-- ----------------------------

-- ----------------------------
-- Table structure for jc_workflow_node
-- ----------------------------
DROP TABLE IF EXISTS `jc_workflow_node`;
CREATE TABLE `jc_workflow_node` (
  `workflow_id` decimal(11,0) NOT NULL COMMENT '工作流',
  `priority` decimal(11,0) NOT NULL DEFAULT '10' COMMENT '排序',
  `is_countersign` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '1 会签 0普通流转',
  PRIMARY KEY (`workflow_id`,`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工作流节点;';

-- ----------------------------
-- Records of jc_workflow_node
-- ----------------------------

-- ----------------------------
-- Table structure for jc_workflow_record
-- ----------------------------
DROP TABLE IF EXISTS `jc_workflow_record`;
CREATE TABLE `jc_workflow_record` (
  `record_id` decimal(11,0) NOT NULL,
  `record_time` datetime NOT NULL COMMENT '创建时间',
  `opinion` varchar(255) DEFAULT NULL COMMENT '意见',
  `type` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '类型(1:通过  2退回 3保持)',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工作流日志记录;';

-- ----------------------------
-- Records of jc_workflow_record
-- ----------------------------

-- ----------------------------
-- Table structure for jo_authentication
-- ----------------------------
DROP TABLE IF EXISTS `jo_authentication`;
CREATE TABLE `jo_authentication` (
  `authentication_id` char(32) NOT NULL COMMENT '认证ID',
  `userid` decimal(11,0) NOT NULL COMMENT '用户ID',
  `username` varchar(100) NOT NULL COMMENT '用户名',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `login_time` datetime NOT NULL COMMENT '登录时间',
  `login_ip` varchar(50) NOT NULL COMMENT '登录ip',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='认证信息表;';

-- ----------------------------
-- Records of jo_authentication
-- ----------------------------

-- ----------------------------
-- Table structure for jo_config
-- ----------------------------
DROP TABLE IF EXISTS `jo_config`;
CREATE TABLE `jo_config` (
  `cfg_key` varchar(50) NOT NULL COMMENT '配置KEY',
  `cfg_value` varchar(255) DEFAULT NULL COMMENT '配置VALUE',
  PRIMARY KEY (`cfg_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='配置表;';

-- ----------------------------
-- Records of jo_config
-- ----------------------------

-- ----------------------------
-- Table structure for jo_ftp
-- ----------------------------
DROP TABLE IF EXISTS `jo_ftp`;
CREATE TABLE `jo_ftp` (
  `ftp_id` decimal(11,0) NOT NULL,
  `ftp_name` varchar(100) NOT NULL COMMENT '名称',
  `ip` varchar(50) NOT NULL COMMENT 'IP',
  `port` decimal(11,0) NOT NULL DEFAULT '21' COMMENT '端口号',
  `username` varchar(100) DEFAULT NULL COMMENT '登录名',
  `password` varchar(100) DEFAULT NULL COMMENT '登陆密码',
  `encoding` varchar(20) NOT NULL DEFAULT 'UTF-8' COMMENT '编码',
  `timeout` decimal(11,0) DEFAULT NULL COMMENT '超时时间',
  `ftp_path` varchar(255) DEFAULT NULL COMMENT '路径',
  `url` varchar(255) NOT NULL COMMENT '访问URL',
  PRIMARY KEY (`ftp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='FTP表;';

-- ----------------------------
-- Records of jo_ftp
-- ----------------------------

-- ----------------------------
-- Table structure for jo_template
-- ----------------------------
DROP TABLE IF EXISTS `jo_template`;
CREATE TABLE `jo_template` (
  `tpl_name` varchar(150) NOT NULL COMMENT '模板名称',
  `tpl_source` text COMMENT '模板内容',
  `last_modified` decimal(11,0) NOT NULL COMMENT '最后修改时间',
  `is_directory` decimal(1,0) NOT NULL DEFAULT '0' COMMENT '是否目录',
  PRIMARY KEY (`tpl_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='模板表;';

-- ----------------------------
-- Records of jo_template
-- ----------------------------

-- ----------------------------
-- Table structure for jo_upload
-- ----------------------------
DROP TABLE IF EXISTS `jo_upload`;
CREATE TABLE `jo_upload` (
  `filename` varchar(150) NOT NULL COMMENT '文件名',
  `length` decimal(11,0) NOT NULL COMMENT '文件大小(字节)',
  `last_modified` decimal(11,0) NOT NULL COMMENT '最后修改时间',
  `content` longblob NOT NULL COMMENT '内容',
  PRIMARY KEY (`filename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='上传附件表;';

-- ----------------------------
-- Records of jo_upload
-- ----------------------------

-- ----------------------------
-- Table structure for jo_user
-- ----------------------------
DROP TABLE IF EXISTS `jo_user`;
CREATE TABLE `jo_user` (
  `user_id` decimal(11,0) NOT NULL COMMENT '用户ID',
  `username` varchar(100) NOT NULL COMMENT '用户名',
  `email` varchar(100) DEFAULT NULL COMMENT '电子邮箱',
  `password` char(32) NOT NULL COMMENT '密码',
  `register_time` datetime NOT NULL COMMENT '注册时间',
  `register_ip` varchar(50) NOT NULL DEFAULT '127.0.0.1' COMMENT '注册IP',
  `last_login_time` datetime NOT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(50) NOT NULL DEFAULT '127.0.0.1' COMMENT '最后登录IP',
  `login_count` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '登录次数',
  `reset_key` char(32) DEFAULT NULL COMMENT '重置密码KEY',
  `reset_pwd` varchar(10) DEFAULT NULL COMMENT '重置密码VALUE',
  `error_time` datetime DEFAULT NULL COMMENT '登录错误时间',
  `error_count` decimal(11,0) NOT NULL DEFAULT '0' COMMENT '登录错误次数',
  `error_ip` varchar(50) DEFAULT NULL COMMENT '登录错误IP',
  `activation` decimal(1,0) NOT NULL DEFAULT '1' COMMENT '激活状态',
  `activation_code` char(32) DEFAULT NULL COMMENT '激活码',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表;';

-- ----------------------------
-- Records of jo_user
-- ----------------------------
